package com.android.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.widget.EditText;
import android.content.DialogInterface;

public class HelloAndroid extends Activity
{  
// load the library - name matches jni/Android.mk
    static {
        System.loadLibrary("ndkfoo");
    }
    
    // declare the native code function - must match ndkfoo.c
    private native String invokeNativeFunction();
 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        
        alert.setTitle("Title");
        alert.setMessage("Message");
        
// Set an EditText view to get user input 
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                // Do something with value!
            }
        });
        
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

        //  new AlertDialog.Builder(this).setMessage(invokeNativeFunction()).show();
    }
}
