#include <string.h>
#include <jni.h>
 
jstring Java_com_android_helloandroid_HelloAndroid_invokeNativeFunction(JNIEnv* env, jobject javaThis) {
  return (*env)->NewStringUTF(env, "Hello from native code!");
}
