(clear)

(define (make-butterfly x y r g b)
  (let ((p (build-polygons 100 triangle-strip)))
    (grab p) 
    (translate (vector x y 0))
    (pdata-index-map! 
     (lambda (i c)
       (vector (* r (/ (modulo i 30) 30))
               (* g (/ (modulo i 30) 30))
               (* b (/ (modulo i 20) 20))))
     "c")
    (pdata-index-map! 
     (lambda (i p)
       (let ((ii (* 0.6 3.141 i)))
         (vector (sin ii) (cos ii) (sin (* ii 0.34)))))
     "p")
    p))

(define objs (build-list 
              (lambda (n)
                (make-butterfly 
                 (* 5 (- (modulo (- n 1) 3) 1))
                 (* 3 (- (quotient (- n 1) 3) 1))
                 (rndf) (rndf) (rndf)))
              9))

(for-each 
 (lambda (p)
   (grab p) 
   (rotate (vector 0 (* 360 (rndf)) (* 360 (rndf))))
   (ungrab))
 objs)

              
;(set! frame-thunk 
;      (lambda ()
;        (for-each 
;         (lambda (p)
;           (grab p) 
;           (rotate (vector 0 p (* p 0.4)))
;           (ungrab))
;         objs)))

 

