(clear)

(define (make-butterfly x y r g b)
  (let ((p (build-polygons 100 triangle-strip)))
    (grab p) 
    (translate (vector x y 0))
    (pdata-index-map! 
     (lambda (i c)
       (vector (* r (/ (modulo i 30) 30))
               (* g (/ (modulo i 30) 30))
               (* b (/ (modulo i 20) 20))))
     "c")
    (pdata-index-map! 
     (lambda (i p)
       (let ((ii (* r 3.141 i)))
         (vector (sin ii) (cos ii) (sin (* ii 0.5)))))
     "p")
    p))

(scale (vector 3 3 3))
(define p (make-butterfly 0 0 (rndf) 1 1))

(set! frame-thunk 
      (lambda ()
        (grab p) 
        (rotate (vector 0 p (* p 0.4)))
        (ungrab)))

 

