#include <iostream>
#include "OSCServer.h"

using namespace std;
using namespace fluxus;

int main(int argc, char **argv)
{
	if (argc!=5)
	{
		cerr<<"usage: oscrec -r|-p srcportnumber dstporturl filename"<<endl;
		return -1;
	}

	Client client;
	Server server(argv[2],&client);
	client.SetDestination(argv[3]);

	EventRecorder recorder;
	server.SetRecorder(&recorder);
	recorder.SetMode(EventRecorder::RECORD);

	if (!strcmp(argv[1],"-p"))
	{
		recorder.SetMode(EventRecorder::PLAYBACK);
		recorder.Load(argv[4]);
	}
	
	server.Run();
	
	
	if (!strcmp(argv[1],"-r"))
	{
		char b;
		cin>>b;
		cerr<<"Saving "<<argv[4]<<endl;
		recorder.Save(argv[4]);
	}
	else
	{
		while(1)
		{
			server.PollRecorder();
		}
	}
	
	return 0;
}
