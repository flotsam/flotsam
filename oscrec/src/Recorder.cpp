// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <sys/time.h>
#include <iostream>
#include <fstream>
#include "Recorder.h"

using namespace fluxus;

EventRecorder::EventRecorder() :
m_Mode(OFF),
m_LastTimeSeconds(0),
m_TimeSeconds(0),
m_ManualTime(false)
{
}

EventRecorder::~EventRecorder()
{
}

bool EventRecorder::Get(vector<RecorderMessage*> &events)
{
	bool found=false;
	for (vector<RecorderMessage*>::iterator i=m_EventVec.begin(); i!=m_EventVec.end(); i++)
	{
		if ((*i)->Time>m_LastTimeSeconds && (*i)->Time<=m_TimeSeconds)
		{
			events.push_back(*i);
			found=true;
		}		
	}
	return found;
}

void EventRecorder::Record(RecorderMessage *event)
{
	event->Time=m_TimeSeconds;
	m_EventVec.push_back(event);
}

void EventRecorder::Reset()
{
	for (vector<RecorderMessage*>::iterator i=m_EventVec.begin(); i!=m_EventVec.end(); i++)
	{
		delete *i;
	}
	m_EventVec.clear();
}

void EventRecorder::ResetClock()
{
	m_TimeSeconds=0;
	m_LastTimeSeconds=0;
}

void EventRecorder::SetClock(double time)
{
	cerr<<"Setting clock: "<<time<<endl;
	m_ManualTime=true;
	m_TimeSeconds=time;
	m_LastTimeSeconds=time;
}

void EventRecorder::UpdateClock()
{
	if (!m_ManualTime)
	{
		double Delta;
		timeval ThisTime;
		gettimeofday(&ThisTime,NULL);
		Delta=(ThisTime.tv_sec-m_LastTime.tv_sec)+
			  (ThisTime.tv_usec-m_LastTime.tv_usec)*0.000001f;
		m_LastTime=ThisTime;
		IncClock(Delta);	
	}
}

void EventRecorder::IncClock(double delta)
{
	m_LastTimeSeconds=m_TimeSeconds;
	if (delta>0 && delta<1000) m_TimeSeconds+=delta;
	//cerr<<m_TimeSeconds<<" "<<delta<<endl;
}

void EventRecorder::Save(const string &filename)
{
	ofstream os(filename.c_str());
	int version = 1;
	os<<version<<endl;
	os<<m_EventVec.size()<<endl;
	for (vector<RecorderMessage*>::iterator i=m_EventVec.begin(); i!=m_EventVec.end(); i++)
	{
		os<<(*i)->Name<<" "<<(*i)->Time<<" "<<(*i)->Data<<endl;
	}
	
	os.close();		
}

void EventRecorder::Load(const string &filename)
{
	Reset();
	ifstream is(filename.c_str());
	int version;
	is>>version;
	unsigned int size;
	is>>size;
	unsigned int count=0;
	
	cerr<<version<<" "<<size<<endl;
	
	while(count<size)
	{
		RecorderMessage *event = new RecorderMessage;
		is>>event->Name;
		is>>event->Time;
		is>>event->Data;
		cerr<<"["<<event->Name<<" "<<event->Time<<"] ";
		m_EventVec.push_back(event);
		count++;
	}
	
	is.close();
	
	cerr<<"Loaded "<<size<<" messages"<<endl;
}
