(define (make-osc-reader filename dest)
  (define file 0)
  (define count 0)
  (define current '())
  
  (define (read-word file)
    (define (inner-read-token str)
      (let ((c (read-char file)))
        (cond
          ((eof-object? c)
           str)
          ((char-whitespace? c)
           str)
          (else
           (inner-read-token (string-append str (string c)))))))   
    (inner-read-token ""))
  
  (define (init)
    (set! file (open-input-file filename))
    (read-word file)
    (set! count (string->number (read-word file))))
  
  (define (update)
    (set! current (list  (read-word file)(read-word file)(read-word file)(read-word file)(read-word file)))
    (cond 
      ((> (time) (string->number (list-ref current 1)))
       (osc-destination dest)
       (osc-send (list-ref current 0) "f" (string->number (list-ref current 4)))
       (update))))
  
  (define (dispatch m)
    (cond
      ((eq? m 'update) update)
      (else
       (error "unknown method " m))))
  (init)
  dispatch)  