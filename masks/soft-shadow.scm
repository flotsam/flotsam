(define (make-projection n f e a)
    (vector 0      0  0 0
        0 (/ e a) 0 0
        0 0 (- (/ (+ f n) (- f n))) (- (/ (* 2 f n) (- f n)))
        0 0 -1 0))

(clear)
(clear-colour 0.5)

(define o (with-state
        (translate (vector 1 0 0))
        (scale 0.5)
        (load-primitive "omaha.obj")))

(define (make-shadow obj col rot)
    (let ((s (with-state
                    (blend-mode 'zero 'src-color)             
                    (hint-ignore-depth)
                    (opacity 0.5)
                    (colour col)
                    (hint-unlit)
                    (build-copy obj))))
        (with-primitive s
            (identity)
            (translate (vector -1 0 -3))
            (scale 5)
            (concat (make-projection 1 10 (/ 1 (tan (/ 30 2))) 1))
            (rotate rot)
            (translate (vector 0.2 0 -2)))
        s))

(define r (build-locator))

(define nlights 20)
(define lpos (build-list nlights (lambda (_) (vmul (srndvec) 200))))
(define lcol (build-list nlights (lambda (_) (rndvec))))

(define (animate)
    (with-primitive o
              (rotate (vector 1 0 0))
              (apply-transform))

    (destroy r)
    (set! r (build-locator))
    (with-state
        (parent r)
        (for ((i (in-range 0 nlights)))
            (make-shadow o (list-ref lcol i) 
                (vadd (vector 0 45 0) (list-ref lpos i))))))

(every-frame (animate))
 