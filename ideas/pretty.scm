;; pretty scheme
;; some better building blocks

(define-syntax with-state
  (syntax-rules ()
    ((_ a ...)
     (begin
       (push)
       (let ((r (begin a ...)))
       (pop)
       r)))))

(define-syntax with-primitive
  (syntax-rules ()
    ((_ a b ...)
     (begin
       (grab a)
       (let ((r (begin b ...)))
       (ungrab)
       r)))))
       
(define-syntax pdata-map!
  (syntax-rules ()
    ((_ proc pdata-write-name pdata-read-name ...)
     (letrec 
         ((loop (lambda (n)
                  (cond ((not (< n 0))
                         (pdata-set! pdata-write-name n 
                                     (proc (pdata-ref pdata-write-name n) 
                                           (pdata-ref pdata-read-name n) ...))
                         (loop (- n 1)))))))
       (loop (pdata-size))))))

    
(define (pdata-fold p s t)
    (define (loop n)  
        (cond 
            ((< n 0) s)
            (else
                (p (pdata-get t n) (loop (- n 1))))))
    (loop (pdata-size)))

(define (vec4->vec3 v)
    (vector (vector-ref v 0) (vector-ref v 1) (vector-ref v 2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(clear)

(define o (with-state
    (hint-vertcols)
    (colour (vector 0 0 1))
    (build-torus 1 2 30 30)))

(with-primitive o
    (pdata-map
        (lambda (v)
            (vadd (vec4->vec3 v) (vector (flxrnd) 0 0)))
        "c"))
        
 
(display (with-primitive o
    (vdiv (pdata-fold
        vadd
        (vector 0 0 0)
        "p") (pdata-size))))(newline)


