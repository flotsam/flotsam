
(define (push) (display "push")(newline))
(define (pop) (display "pop")(newline))
(define (grab a) (display "grab ")(display a)(newline))
(define (ungrab) (display "ungrab")(newline))


(define-syntax collect-state
  (syntax-rules ()
    ((_ a ...)
     (begin
       (push)
       a ...
       (pop)))))

(define-syntax with-primitive
  (syntax-rules ()
    ((_ a b ...)
     (begin
       (grab a)
       b ...
       (ungrab)))))

(push/pop 
 (display "hello")(newline))

(grab-with 5
           (display "hello")(newline))

;(define-syntax pdata-map
;  (syntax-rules ()
;    ((_ f p)
;     (begin
;       (

; (pdata-map
;   (lambda (vec)
;       (+ vec (vector 1 0 0)))
;   "p")

(map
 (lambda (e)
   (+ e 1))
 '(1 2 3 4 5))

(map
 (lambda (a b)
   (+ a b))
 '(1 2 3 4 5)
 '(1 2 3 4 5))

(foldl + 0 '(1 2 3 4 5))


(foldl 
 (lambda (a b)
   (* a b))
 1
 '(1 2 3 3 3))
