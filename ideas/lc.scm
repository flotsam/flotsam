
(load-extension "/usr/local/lib/plt/collects/fluxus-0.12/extensions/fluxus-osc.so")
(require fluxus-osc)
(osc-destination "osc.udp://127.0.0.1:4001")

(define (time->timestamp time)
  (let ((adjusted (+ time 2208988800L0))) 
    (let ((seconds (inexact->exact (floor adjusted)))) 
      (let ((frac (inexact->exact (floor (* (- adjusted seconds) 4294967295))))) 
        (vector seconds frac)))))

(define (sq x) (* x x))

(define (tick)
  (if (eq? (modulo clock 16) 0)
      (osc-send "/play" "iiiffffi" (list (vector-ref ts 0) (vector-ref ts 1) 3 
                                         (+ 220 (* (sq (modulo clock 56)) 0.1)) 440 1 0 79)))
  (if (eq? (modulo clock 8) 0)
      (osc-send "/play" "iiiffffi" (list (vector-ref ts 0) (vector-ref ts 1) 6 
                                         (+ 110 (* (sq (modulo clock 100)) 0.1)) 440 1 0.5 79)))
  (if #f;(eq? (modulo clock 4) 0)
      (osc-send "/play" "iiiffffi" (list (vector-ref ts 0) (vector-ref ts 1) 2 
                                         (* (random 10) 220) 0 1 0.5 79))))



(define t (/ (current-inexact-milliseconds) 1000))
(define clock 0)
(define tick-time 0.02)
(define ts (vector 0 0))
(define offset 1)
  
(define (loop)
  (sleep 0.0001)
  (cond 
    [(> (/ (current-inexact-milliseconds) 1000) t)
      (set! t (+ t tick-time))
      (set! ts (time->timestamp (+ t offset)))
      (set! clock (+ clock 1))
      (tick)])
  (loop))


(define thr (thread loop))

