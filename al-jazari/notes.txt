
qbot-core-block
        height
        trigger

qbot-core-block-grid

qbot-core-entity
        position
        direction
        stuck
        (update)
                check block-grid

qbot-core-entity-map

qbot-core
        (qbot-core-update)
                send list of actions to game-render w timestamp
        (sync time)
                change time relative to this, set bpm

qbot-core-bot - is an entity
        code
        (update)
                read code and update entity state
                run entity update

qbot-core knows nothing about fluxus, calls qbot-render by means of a list
of events and a timestamp
qbot-render intpolates events - entities state ("turning" "moving") and
completes them on time given

variable framerate

core evaluates ahead of time (1-2 secs, minimal for interactivity reasons)
bot update returns list of events, core collects them and sends to
renderer ->

time segment event list:
(timeA timeB ((botid ('turnleft 'trigger) (botid ('forward))))

time segment A+B sent to allow the core to keep all details on bpm etc

renderer...
runs realtime, so store and evaluate the list at time A (reject if too late)
state changes (turning, moving) start at A and finish at B
notes to be sent A timestamped to play at B, means triggers audible at end of
time in block, might be ok, might have to tweak time sooner, but depends on
beats per minute...

   A            B
   |            |
  -----------------

qbot-render-block
        object

qbot-render-bot
        object

qbot-render
        (add-events)
        (update)

qbot-render-synth
        builds play messages from event list


qbot-gui






348 506










