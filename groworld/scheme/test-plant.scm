;#lang scheme
(set-searchpaths (list "../"))
(require "plant.ss")
(require "ls.ss")

(clear)
(clear-colour (vector 0.75 0.75 1))

(light-diffuse 0 (vector 0 0 0))
(light-ambient 0 (vector 1 1 1))
(define l (make-light 'point 'free))
(light-diffuse l (vector 1 1 1))
(light-ambient l (vector 0.5 0.5 0.5))
(light-position l (vector 100 100 40))
(shadow-light l)
(ambient (vector 0.2 0.2 0.2))
(fog (vector 0.75 0.75 1) 0.02 1 100)

(define terrain (with-state
        (texture (load-texture "textures/ground.png"))
        ;        (colour (vector 0 0.5 0))
        (scale (vector 30 30 30))
        (load-primitive "meshes/terrain.obj")))

(with-primitive terrain
    (ambient (vector 0.5 0.5 0.5))
    (pdata-map!
        (lambda (t)
            (vmul t 10))
        "t")
    (apply-transform))

(for ((i (in-range 0 100)))
    (build-plant (vmul (crndvec) 50) 
        terrain (lsystem-generate 3 "A" '(("A" "F/[++[&&&L]A][--[^^^L]A]")))))