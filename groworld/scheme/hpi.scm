; try all the rules on this character - 
; returns #f if none are matched
(define (lsys-run-rules char rules)
  (foldl
   (lambda (rule str)
     (if str ; if str is not #f
         str ; then we have already found a rule, return it
         (if (char=? char (string-ref (car rule) 0)) ; check this rule
             (cadr rule) ; return the string
             #f))) ; no match
   #f
   rules))

; runs the lsystem rules on every character in a string, 
; returns the new string
(define (lsys-search-replace str rules pos result)
  (cond 
    ((>= pos (string-length str)) result)
    (else
     (let ((ret (lsys-run-rules (string-ref str pos) rules)))
       (if (string? ret)
          (lsys-search-replace str rules (+ pos 1) 
            (string-append result ret))
          (lsys-search-replace str rules (+ pos 1) 
            (string-append result (string (string-ref str pos)))))))))

; runs the search-replace multiple (n) times on a string
(define (ls-generate n str rules)
  (cond 
    ((zero? n) str)
    (else
     (ls-generate (- n 1) 
        (lsys-search-replace str rules 0 "") rules))))

; builds objects from a string
(define (ls-build string angle branch-scale)
  (for-each 
   (lambda (char)
     (cond 
       ((char=? #\F char)
        (with-state
         (translate (vector 1 0 0))
         (scale (vector 1 2 2))
         (rotate (vector 0 90 0))         
         (colour (vector 1 0.5 0.2))

         (with-primitive (build-ribbon 2)
            (texture (load-texture "textures/scribble.png"))
            (hint-unlit)
            (pdata-set! "w" 0 0.1)
            (pdata-set! "w" 1 0.1)
            (pdata-set! "p" 0 (vector 0 0 1))
            (pdata-set! "p" 1 (vector 0 0 0))))

;         (load-primitive "branch.obj"))
        (translate (vector 1 0 0)))
       ((char=? #\L char)
        (with-state
         (translate (vector 1 0 0))
         (scale (vector 1 0.5 0.5))
;         (rotate (vector 0 90 0))         
         (colour (vector 0.5 1 0.5))
         (texture (load-texture "textures/leaf.png"))
         (build-plane))
        (translate (vector 1 0 0)))
       ((char=? #\f char)
        (translate (vector 1 0 0)))
       ((char=? #\/ char)
        (rotate (vector angle 0 0)))
       ((char=? #\\ char)
        (rotate (vector (- angle) 0 0)))
       ((char=? #\+ char)
        (rotate (vector 0 angle 0)))
       ((char=? #\- char)
        (rotate (vector 0 (- angle) 0)))
       ((char=? #\^ char)
        (rotate (vector 0 0 angle)))
       ((char=? #\& char)
        (rotate (vector 0 0 (- angle))))
       ((char=? #\| char)
        (rotate (vector 0 0 180)))
       ((char=? #\[ char)
        (push)
        (scale (vector branch-scale branch-scale branch-scale)))
       ((char=? #\] char)
        (pop))))
   (string->list string)))

(clear)
(clear-colour (vector 0.2 0.2 0.2))
; run the actual lsystem code
(ls-build (ls-generate 5 "F" '(("F" "F[^F][+&FL]"))) 16 0.7))