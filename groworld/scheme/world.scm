(clear)

(define planet (build-torus 1 2 10 10))

(define seeds (with-state
    (hint-none)(hint-points)
    (point-width 5)
    (build-particles 1000)))

(with-primitive seeds
    (pdata-map!
        (lambda (p)
            (vmul (srndvec) 4))
        "p")
    (pdata-map!
        (lambda (p)
            (vector 1 1 1))
        "c"))
