#lang scheme

(define-struct grid (width height (data #:mutable) (top #:mutable)))

(define (build-grid w h)
  (let ((g (make-grid w h (make-vector (* w h) #\.) h)))
  (for ((i (in-range 0 w)))
       (grid-poke g i (- h 1) #\#))
    g))

(define (grid-display grid)
  (for ((i (in-range 0 (grid-height grid))))
       (for ((j (in-range 0 (grid-width grid))))
            (printf "~a" (vector-ref (grid-data grid) (+ j (* i (grid-width grid))))))
       (printf "~n")))

(define (grid-update-top grid on-update)
  (when
      (not (foldl
       (lambda (pos gap-found)
         (if (not gap-found)
             (eq? #\. (grid-peek grid (vector-ref pos 0) (vector-ref pos 1)))
             #t))
       #f
       (build-list (grid-width grid) (lambda (x) (vector x (grid-top grid))))))
    (set-grid-top! grid (- (grid-top grid) 1))
    (on-update (grid-top grid))))

(define (grid-poke grid x y s)
  (let ((x (modulo x (grid-width grid))))
    (when (and (>= y 0) (< y (grid-height grid)))
      (vector-set! (grid-data grid) (+ x (* y (grid-width grid))) s))))

(define (grid-peek grid x y)
  (let ((x (modulo x (grid-width grid))))
    (if (and (>= y 0) (< y (grid-height grid)))
        (vector-ref (grid-data grid) (+ x (* y (grid-width grid))))
        0)))

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define shapes
  (list (list (vector 0 1) (vector 0 0) (vector 1 0) (vector -1 0))
   (list (vector -1 1) (vector -1 0) (vector 0 0) (vector 0 -1))
   (list (vector -1 0) (vector 0 0) (vector 1 0) (vector 2 0))
   (list (vector 0 0) (vector 1 0) (vector 0 1) (vector 1 1))
   (list (vector 0 0) (vector 1 0) (vector 0 1) (vector 0 2))))

(define (random-shape)
  (list-ref shapes (random (length shapes))))

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define-struct block ((pos #:mutable) (shape #:mutable)))

(define (block-write block grid s)
  (for-each
   (lambda (pos)
     (grid-poke grid (+ (vector-ref (block-pos block) 0) (vector-ref pos 0))
                (+ (vector-ref (block-pos block) 1) (vector-ref pos 1)) s))
   (block-shape block)))

(define (block-rotate-ccw block)
  (set-block-shape! block
                    (map
                     (lambda (pos)
                       (vector (vector-ref pos 1) (- (vector-ref pos 0))))
                     (block-shape block))))

(define (block-rotate-cw block)
  (set-block-shape! block
                    (map
                     (lambda (pos)
                       (vector (- (vector-ref pos 1)) (vector-ref pos 0)))
                     (block-shape block))))

(define (block-move! block vec)
  (set-block-pos! block (vector (+ (vector-ref vec 0) (vector-ref (block-pos block) 0))
                                (+ (vector-ref vec 1) (vector-ref (block-pos block) 1)))))

(define (block-update block grid)
  (block-write block grid #\.) ; clear the block
  ; do rotations here...
  (when #f ; rotate ccw
    (block-rotate-ccw b)
    (when (block-check block grid)
      (block-rotate-cw b))) ; can't rotate

  (when #f ; rotate cw
    (block-rotate-cw b)
    (when (block-check block grid)
      (block-rotate-ccw b))) ; can't rotate

  (when #f ; move left
    (block-move! block (vector -1 0))
    (when (block-check block grid)
      (block-move! block (vector 1 0))))

  (when #f ; move right
    (block-move! block (vector 1 0))
    (when (block-check block grid)
      (block-move! block (vector -1 0))))
  
  (block-move! block (vector 0 1))
  
  (cond ((block-check block grid)
         ; reverse!
         (set-block-pos! block (vector (vector-ref (block-pos block) 0)
                                       (- (vector-ref (block-pos block) 1) 1)))
         (block-write block grid #\#) ; write the block
         (display "collide")(newline)
         #f)
        (else
         (block-write block grid #\#) ; write the block
         #t)))

(define (block-check block grid)
  (foldl
   (lambda (pos found)
     (if (not found)
       (eq? #\# (grid-peek grid (+ (vector-ref (block-pos block) 0) (vector-ref pos 0))
                           (+ (vector-ref (block-pos block) 1) (vector-ref pos 1))))
       #t))
   #f
   (block-shape block)))

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define g (build-grid 10 10))
(define b (make-block (vector 2 7) (car shapes)))

(for ((i (in-range 0 100)))
     (when (not (block-update b g))
         (set! b (make-block (vector (random 10) 0) (random-shape))))
     (grid-display g)
     (grid-update-top g
                      (lambda (n)
                        (printf "new line, top now:~a~n" n)))
     (newline))
