;#lang scheme
(set-searchpaths (list "../"))
(require "plant.ss")
(require "ls.ss")

(clear)
(define terrain (with-state
        (texture (load-texture "textures/ground.png"))
        (build-sphere 10 10)))

(build-plant (vector 0.001 0 0) 
     terrain (lsystem-generate 3 "A" '(("A" "FL")))))