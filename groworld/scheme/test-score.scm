(require "plant.ss")
(require "gp.ss")
(set-searchpaths (list "../"))

(clear)
;(clear-colour (vector 0.75 0.75 1))

(define light-pos (vector 0 20 0))
(light-diffuse 0 (vector 0 0 0))
(light-ambient 0 (vector 1 1 1))
(define l (make-light 'point 'free))
(light-diffuse l (vector 1 1 1))
(light-ambient l (vector 0.5 0.5 0.5))
(light-position l light-pos)
(shadow-light l)
(ambient (vector 0.2 0.2 0.2))
(fog (vector 0.75 0.75 1) 0.02 1 100)

(define terrain (with-state
        (texture (load-texture "textures/ground.png"))
        (scale (vector 1 1 1))
        (build-sphere 10 10)))

(with-primitive terrain
    (ambient (vector 0.5 0.5 0.5))
    (apply-transform))

(define plant (build-plant (vector 0.01 0 0) 
        terrain "F/[++[&&L]A][--[^^^L]A]"))

(display (plant-trace-leaves plant light-pos))(newline)
