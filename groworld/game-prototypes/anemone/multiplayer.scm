(clear)

(osc-destination "osc.udp://127.0.0.255:4002")
(osc-source "4001")

;~~~~~~

(define-struct world ((plants #:mutable) size root (id #:mutable)))

(define (build-world size)
    (make-world '() size
        (with-state
            (scale size)
            (build-sphere 20 20))
        0))

(define (world-inc-id! world)
    (set-world-id! world (+ (world-id world) 1)))

(define (world-plant world id)
    (foldl
        (lambda (plant ret)
            (if (eq? (plant-id plant) id)
                plant
                ret))
        #f
        (world-plants world)))

(define (world-choose world)
    (list-ref (world-plants world) (random (length (world-plants world)))))

(define (world-add-plant world pos col)
    (world-inc-id! world)
    (set-world-plants! world (cons 
            (build-plant world (world-id world) pos col)
            (world-plants world)))
    (world-id world))

(define (world-destroy-plant world id)
    (set-world-plants! world
        (filter
            (lambda (plant)
                (cond ((eq? (plant-id plant) id)
                        (plant-destroy plant)
                        #f)
                    (else #t)))
            (world-plants world))))

(define (world-spray world id type)
    (plant-spray (world-plant world id) type))

(define (world-update world)
    (for-each
        (lambda (plant)
            (plant-update plant world))
        (world-plants world)))

;~~~~~~

(define-struct plant (id root (spray-t #:mutable) col))

(define (build-plant world id pos col)
    (make-plant id (with-state
            (parent (world-root world))
            ;(hint-origin)
            (colour col)
            (rotate pos)
            (translate (vector 0 1.1 0))
            (scale 0.03)
            (build-torus 1 2 10 10))
        1
        col))

(define (plant-destroy plant)
    (destroy (plant-root plant)))

(define (plant-update plant world)
    (with-primitive (plant-root plant)
    (colour (plant-col plant))
    (when (> (plant-spray-t plant) 1)
        (set-plant-spray-t! plant (* (plant-spray-t plant) 0.9))
            (colour (plant-spray-t plant)))))
    
    
(define (plant-spray plant type)
    (set-plant-spray-t! plant 5))
        
;~~~~~~

(define (osc-dispatch world)
    (cond
        ((osc-msg "/add-plant")
            (printf "add plant message recieved...~n")
            (world-add-plant world 
                (vector (osc 0) (osc 1) (osc 2))
                (vector (osc 3) (osc 4) (osc 5))))
        ((osc-msg "/destroy-plant")
            (printf "destroy plant message recieved...~n")
            (world-destroy-plant world (osc 0)))
        ((osc-msg "/spray")
           ; (printf "destroy plant message recieved...~n")
            (world-spray world (osc 0) (osc 1)))))

(define (add-plant world)
    (printf "sending add plant...~n")
    (let ((pos (vmul (rndvec) 360)) (col (rndvec)))
        (osc-send "/add-plant" "ffffff" (list (vx pos) (vy pos) (vz pos)
                (vx col) (vy col) (vz col)))
        (world-add-plant world pos col)))

(define (destroy-plant world id)
    (printf "sending destroy plant...~n")
    (osc-send "/destroy-plant" "i" (list id))
    (world-destroy-plant world id))

(define (spray world id type)
;    (printf "sending destroy plant...~n")
    (osc-send "/spray" "ii" (list id type))
    (world-spray world id type))

;~~~~~~

;(set-camera-transform (mtranslate (vector 0 -10 -14)))

(define w (build-world 10))

(define (animate)
    (when (key-pressed " ") (add-plant w))
    (osc-dispatch w)
    (world-update w)
    (when (and (zero? (random 20)) (not (null? (world-plants w))) )
        (spray w (plant-id (world-choose w)) 0)))

(every-frame (animate))


