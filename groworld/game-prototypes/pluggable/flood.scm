; converts a 2D vector into an angle, with some dodgy dave maths
(define (2dvec->angle x y)
    (let ((q (/ 3.141 2)))
        (when (zero? y) (set! y 0.0001))
        (cond 
            ((>= y 0)
                (fmod (* (+ q q q (- q (atan (/ x y)))) 57.2957795) 360))
            (else
                (fmod (* (+ q (- q (atan (/ x y)))) 57.2957795) 360)))))

(define (i->pos i)
    (vector (modulo i (pixels-width)) 
        (quotient i (pixels-width)) 0))

(define (pos->i pos)
    (+ (* (round (vy pos)) (pixels-width)) (round (vx pos))))

(define (pixels-ref name pos)
    (pdata-ref name (pos->i pos)))

(define (pixels-set! name pos s)
    (pdata-set! name (pos->i pos) s))

(define (search i)
    (cond
        ((eq? i (pdata-size)) i)
        ((< (vr (pdata-ref "c" i)) 0.5) i)
        (else (search (+ i 1)))))

(define (flood pos tc av)
    (define (rec-flood pos)
        (pixels-set! "c" pos (vector 1 0 1))
        (set! tc (+ tc 1))
        (set! av (vadd av pos))
        (when (< (vr (pixels-ref "c" (vadd pos (vector -1 0 0)))) 0.5)
            (rec-flood (vadd pos (vector -1 0 0))))
        (when (< (vr (pixels-ref "c" (vadd pos (vector 1 0 0)))) 0.5)
            (rec-flood (vadd pos (vector 1 0 0))))
        (when (< (vr (pixels-ref "c" (vadd pos (vector 0 1 0)))) 0.5)
            (rec-flood (vadd pos (vector 0 1 0))))
        (when (< (vr (pixels-ref "c" (vadd pos (vector 0 -1 0)))) 0.5)
            (rec-flood (vadd pos (vector 0 -1 0)))))
    (rec-flood pos)
    (vmul av (/ 1 tc)))

(define (find-centroids pos l)
    (let ((i (search pos)))
        (cond ((eq? i (pdata-size)) l)
            (else
                (find-centroids i 
                    (cons (flood (i->pos i) 0 (vector 0 0 0)) l))))))

(define (convert-to-pos l)
    (map 
        (lambda (cp)
            (vector (- (/ (vx cp) (pixels-width)) 0.5)
                (- 1 (/ (vy cp) (pixels-height))) 0))
        l))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define-struct component (root children))

(define (build-component id children)
    (cond 
        ((null? children) 
            (let ((root (with-state
                    (hint-ignore-depth)
                    (translate (vector 0 0.5 0))
                    (rotate (vector 0 0 180))
                    (texture (load-texture (string-append "textures/comp-" id ".png")))
                    (build-plane))))
            (with-primitive root (apply-transform))
            (make-component root '())))
        (else
            (let* ((tex (load-primitive (string-append "textures/comp-cp-" id ".png")))
                    (connection-list (with-primitive tex (convert-to-pos (find-centroids 0 '()))))
                    (root (with-state
                            (translate (vector 0 0.5 0))
                            (rotate (vector 0 0 180))
                            (hint-ignore-depth)
                            (texture (load-texture (string-append "textures/comp-" id ".png")))
                            (build-plane)))
                    (comp (make-component root
                            (map
                                (lambda (child connection)
                                    (with-state                                        
                                        (parent root)
                                        (printf "~a~n" connection)
                                        (translate connection)
                                        (rotate (vector 0 0 (2dvec->angle 
                                            (vx connection) (- (vy connection) 0.5))))
                                        (rotate (vector 0 0 0))
                                        (build-component (car child) (cadr child))))
                                children
                                connection-list))))
                (with-primitive root (apply-transform))
                (destroy tex)
                comp))))

(define (component-print component)
    (printf "~a~n" (component-children component)))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define (make-random-plant)
    (let ((num-children (random 4)))
    (cond 
        ((eq? num-children 0) (list "0" (list)))
        ((eq? num-children 1) (list "1-0" (list (make-random-plant))))
        ((eq? num-children 2) (list "2-0" (list (make-random-plant)
            (make-random-plant))))
        ((eq? num-children 3) (list "2-0" (list (make-random-plant)
            (make-random-plant) (make-random-plant)))))))
        
(clear)
(clear-colour (vector 1 1 1))
(clear-geometry-cache)
(hint-unlit)

(define c (build-component "1-0" (make-random-plant)))

;    '(("1-0" (("1-0" (("3-0" (("0" ()) ("1-0" (("0" ()))) ("0" ()))))))) 
;        ("2-0" (("0" ()) ("0" ())) ))))






