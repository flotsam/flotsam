(define (make-shell n type col dist)
    (define (loop n target)
        (let ((vert (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist)))
                (tex (pdata-get "t" n)))
            (grab target)
            (pdata-set "p" n vert)
            (pdata-set "t" n (vmul tex 10)))
        (ungrab)
        (if (zero? n)
            0
            (loop (- n 1) target)))
    
    (let ((target (build-polygons (pdata-size) type)))
        
        (grab target)
        (colour col)
        (texture (load-texture "textures/hair5.png"))
        (ungrab)
        
        (loop (pdata-size) target)
        (grab target)
        (recalc-normals 1)
        (ungrab)
        target))

(define (make-shells c n dist type col l)
    (if (eq? c n)
        l
        (make-shells c (+ n 1) dist type col
            (cons (make-shell n type (vmul col (* n 2)) (* dist n)) l))))

(define (animate-shells l)
    (define (animate n)
        (let ((vary (vector-ref (pdata-get "p" n) 1)))
            (pdata-set "t" n (vadd (pdata-get "t" n) 
                    (vmul (vector (sin (+ vary (* (time) 2))) 
                            (cos (+ vary (* (time) 2))) 0) 
                        0.001))))
        (if (zero? n)
            0
            (animate (- n 1))))
    
    (define (copy n target)
        (let ((tex (pdata-get "t" n)))
            (grab target)
            (pdata-set "t" n (vadd 
                    (vmul tex 0.3)
                    (vmul (pdata-get "t" n) 0.7)))
            (ungrab))
        (if (zero? n)
            0
            (copy (- n 1) target)))
    
    (define (transfer l)
        (grab (car l))
        (copy (pdata-size) (car (cdr l)))
        (ungrab)
        (if (null? (cdr (cdr l)))
            0
            (transfer (cdr l))))
    
    (grab (car l))
    (animate (pdata-size))
    (ungrab)
    
    (transfer l))


(clear)
(colour (vector 0.0 0.2 0.0))
(define a (with-state
        (scale 10)
        (build-sphere 8 9)))

(apply-transform a)


;(light-diffuse 0 (vector 0 0 0))
(define light (make-light 'point 'free))
(light-diffuse light (vector 1 1 1))
(light-specular light (vector 1 1 1))
(light-position light (vector 100 50 0))

(parent a)
(grab a)
;(parent a)
(define shells
    (make-shells 8 0 0.06 'triangle-list (vector 0.03 1 0.02) '()))
(ungrab)
(parent 0)

(show-fps 0)

(define (animate)
    (with-primitive a (rotate (vector 0 0.1 0)))
    (animate-shells shells))

(every-frame (animate)) 