
Basic plant sensor kit which currently outputs;
 - soil humidity
 - light levels
 - human presence

Hardware interface based loosly on recollection of Martin Howse's
experiements with the Wheatstone Bridge[1] and incorporates parts of a
design published by Rob Faludi[2] which was in turn based on a design
by Forrest Mims, with nods to L. George Lawrence, Botanicalls and the
long and tangled path from Jagdish Chandra Bose.

[1] during piskel08
[2] http://www.faludi.com/2006/11/02/moisture-sensor-circuit/
