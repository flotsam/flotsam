///////////// part of the groworld HPI prototype 

#include <stdio.h>

#define sense 0  // biosensor on analog pin 0
#define power 8     // power for sensors

unsigned long then = 0; // timer which will run for < 50 days
unsigned long interval = 10; // interval between reads in seconds

///////////// set up & send

void setup()  
{ 
  pinMode(power, OUTPUT);
  pinMode(sense, INPUT);
  Serial.begin(9600); 
}


int readlevel () 
{
  int level = 0;
  level = (analogRead(sense)); 
  return level; 
}

char display[64];

void loop() 
{ 
    if (millis() - then > interval*1000) {
      then = millis(); 
      sprintf(display, "%u,%u\n", (unsigned int)then, readlevel());
      Serial.print(display);
  }
}

/////////////

