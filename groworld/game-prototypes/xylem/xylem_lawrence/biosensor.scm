
(require net/url
    fluxus-016/fluxa)

(define serial (open-input-file "/dev/tty.usbserial-A200294d"))

(define (read-serial)
    (process (read-line serial))
    (read-serial))

(define (process str)
    (let ([data (regexp-split #rx"," str)])
        (printf "biosense: ~a~n" (list-ref data 1))
        (data->noise (list-ref data 1))))


(define (data->noise l) 
    (play-now (mul (1of2 (saw 40) (sine 30))
            (string->number l))))


(define (x n)
    (cond ((not (zero? n)) 
            (play-now (mul (saw (random 600)) (adsr 1 0 (rndf) 1)))
            (sleep 0.1)
            (x (- n 1)))))

(define (1of2 x y)
    (if (> 1 (random 2)) x y ))



(x 33)    

;(read-serial)