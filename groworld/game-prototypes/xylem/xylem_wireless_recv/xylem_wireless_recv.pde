///////////// part of the groworld HPI prototype 

///////////// serial -> wireless setup

#include <SoftwareSerial.h>

#define rxPin 0
#define txPin 1

// set up a new serial port
SoftwareSerial wireless =  SoftwareSerial(rxPin, txPin);

void setup()  
{ 
  // wireless serial
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  wireless.begin(4800);

  // wired serial output
  Serial.begin(9600); 
}

void loop() 
{ 
  // read, then write
  char someChar = wireless.read();
  Serial.print(someChar);
}
