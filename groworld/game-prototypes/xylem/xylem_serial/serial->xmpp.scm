;; send sensor data via xmpp


(module serial->xmpp scheme
  
  (require (planet zzkt/xmpp))
  (provide (all-defined-out))
  
  (define serial (open-input-file "/dev/tty.usbserial-A9007Lgy"))
  
  (define (read-serial)
    (process (read-line serial))
    ;(read-serial)
    )
  
  (define (scale b t n)
    (/ (- t (/ (string->number n) 1024.0)) (- t b)))
  
  (define (process str)
    (let ([data (regexp-split #rx"," str)])
      (format "vrob light-level=n:~a soil-moisture=n:~a" 
              (scale 0.9 1 (list-ref data 1))
              (scale 0 1.0 (list-ref data 2)))))
  
  ;; send via xmpp
  
  (define (send-data)
    (with-xmpp-session "plant0000005@fo.am" "plant0000005"
                       (let sendrec ()
                         (begin (send (message "plant0000003@fo.am" (read-serial)))
                                (sleep 0.5)
                                (sendrec)))))
                       
  )
  
  
 
