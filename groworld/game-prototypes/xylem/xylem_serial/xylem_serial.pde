///////////// part of the groworld HPI prototype 

#include <stdio.h>

#define lightpin 1  // light sensor on analog pin 1
#define temppin 0  // humidity sensor on analog pin 0
#define power 8     // power for sensors

unsigned long then = 0; // timer which will run for < 50 days
unsigned long interval = 1; // interval between reads in seconds


///////////// set up & send

void setup()  
{ 
  pinMode(power, OUTPUT);
  pinMode(temppin, INPUT);
  pinMode(lightpin, INPUT);

  Serial.begin(9600); 
}



int lightlevel () 
{
  int light = 0;
  light = (analogRead(lightpin)); 
  return light; 
}

int templevel () 
{
  int temp = 0;
  temp = (analogRead(temppin)); 
  return temp; 
}



///////////// main loop. sends data to pachube and prints values to serial line

char display[64];

void loop() 
{ 
    if (millis() - then > interval*1000) {
      then = millis(); 
      sprintf(display, "%u,%u,%u\n", (unsigned int)then, lightlevel(), templevel());
      Serial.print(display);
  }
}

/////////////

