;; interface to the pachube API 


;<?xml version="1.0" encoding="UTF-8"?>
;<eeml xmlns="http://www.eeml.org/xsd/005">
;  <environment>
;    <data id="0">
;      <value>36.2</value>
;    </data>
;  </environment>
;</eeml>

(module pachube scheme
  
  (require net/url)
  
  (provide (all-defined-out))
  
  (define source (string->url "http://www.pachube.com/api/1951.xml"))
  (define api-key "")
  
  (define (format-data l m)
    (string-append "<?xml version='1.0' encoding='UTF-8'?><eeml xmlns='http://www.eeml.org/xsd/005'><environment><data id='0'><value>" l "</value></data><data id='1'><value>" m "</value></data></environment></eeml>"))
  
  (define serial (open-input-file "/dev/cu.usbserial-A6004bp0"))
  
  (define (read-serial)
    (process (read-line serial))
    (read-serial))
  
  (define (process str)
    (let ([data (regexp-split #rx"," str)])
      (printf "light: ~a, moisture: ~a~n" (list-ref data 1) (list-ref data 2))
      (send-data (list-ref data 1) (list-ref data 2))))
  
  ;; retrive data
  ;; curl --request GET --header "X-PachubeApiKey: YOUR_API_KEY" "http://www.pachube.com/api/504.csv"
  
  (define (get-test)
    (get-pure-port source (list (string-append "X-PachubeApiKey: " api-key))))
  
  ;; update the values from a feed
  ;; curl --request PUT --header "X-PachubeApiKey: YOUR_API_KEY" --data "test" "http://www.pachube.com/api/504.csv"
  
  (define (send-data l m) 
    (display-pure-port (put-impure-port 
                        source 
                        (string->bytes/utf-8 (format-data l m))
                        (list (string-append "X-PachubeApiKey: " api-key)))))
  
  
  )
