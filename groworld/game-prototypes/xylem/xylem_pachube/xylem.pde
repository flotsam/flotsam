///////////// part of the groworld HPI prototype 

#include <stdio.h>
#include <Ethernet.h>
#include "pachube_api_key.h" // local include containing API key

#define lightpin 1  // light sensor on analog pin 1
#define humidpin 0  // humidity sensor on analog pin 0
#define power 8     // power for sensors

unsigned long then = 0; // timer which will run for < 50 days
unsigned long interval = 60; // interval between reads in seconds

char display[64];

////////////// ethernet & network config

byte mac[] = {0xFE,0xEB,0xDA,0xCC,0xFE,0xED}; // MAC address
byte ip[] = {192,168,1,19};       // local ip
byte gateway[] = {192,168,1,1};   // gateway
byte subnet[] = {255,255,255,0};  // netmask
//byte server[] = {192,168,1,25}; // local listener %nc -l 80
byte server[] = {209,40,205,190}; // pachube server
Client plient(server, 80);

///////////// pachube config

#define SENSOR_FEED_ID  1951  // http://www.pachube.com/feeds/1951
char pachube_data[70];
int content_length;

///////////// set up & send

void setup()  
{ 
  pinMode(power, OUTPUT);
  pinMode(humidpin, INPUT);
  pinMode(lightpin, INPUT);

  Ethernet.begin(mac, ip, gateway, subnet);
  Serial.begin(9600); 
}


void send_data ()
{
  if (plient.connect() ) {

    // send comma-separated values to update Pachube datastreams
    
    Serial.print("connecting...\n");
    
    sprintf(pachube_data,"%d,%d", lightlevel(), moisturelevel());
    content_length = strlen(pachube_data);
    
    plient.print("PUT /api/");
    plient.print(SENSOR_FEED_ID);
    plient.print(".csv HTTP/1.1\nHost: pachube.com\nX-PachubeApiKey: ");
    plient.print(PACHUBE_API_KEY);
    
    plient.print("\nUser-Agent: Arduino (groworld)");
    plient.print("\nContent-Type: text/csv\nContent-Length: ");
    plient.print(content_length);
    plient.print("\nConnection: close\n\n");
    plient.print(pachube_data);
    
    plient.print("\n");
    plient.flush();
    plient.stop();
   }   
   else {
         Serial.print("not connected!\n");
   }
}  
  

int lightlevel () 
{
  int light = 0;
  light = (analogRead(lightpin)); 
  return light; 
}

int moisturelevel () 
{
      int moist = 0;
      digitalWrite(power, HIGH);
      moist = (analogRead(humidpin)); 
      digitalWrite(power, LOW);
      return moist; 
}



///////////// main loop. sends data to pachube and prints values to serial line

void loop() 
{ 
    if (millis() - then > interval*1000) {
      then = millis(); 
      send_data();
      sprintf(display, "%u,%u,%u\n", (unsigned int)then, lightlevel(), moisturelevel());
      Serial.print(display);
  }
}

/////////////

