#lang scheme

;; exmaple chat client

(require "xmpp.ss")
(require openssl)

(define (read-input prompt)
  (display prompt)
  (read-line (current-input-port)))

(define (chat)
  (let ((jid  (read-input "jid: "))
        (pass (read-input "password: "))
        (to   (read-input "chat with: ")))
    (with-xmpp-session jid pass 
                       (set-xmpp-handler 'message print-message)
                       (let loop ()                         
                         (let ((msg (read-line (current-input-port))))
                           (send (message to msg))
                           (loop))))))



