-module(mod_mycelium).
-author("nik gaffney <nik@fo.am>").
-behavior(gen_mod).
-include("ejabberd.hrl").

%% x.ref 
%%  -> http://www.process-one.net/en/wiki/ejabberd_module_development/
%%  -> http://anders.conbere.org/journal/building-ejabberd-modules-part-2-generic-modules/

-export([start/2, stop/1]).

start(_Host, _Opt) -> 
    ?DEBUG("MYCELIUM LOADING...", []).

stop(Host) -> 
    ?DEBUG("MYCELIUM UNLOADING...", []).

%%-define(PROCNAME, ejabberd_mycelium).
%%-define(BOTNAME, mycelium).
