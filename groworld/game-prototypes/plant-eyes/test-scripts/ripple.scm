
(define (ripple t speed wave-length)
    (define (pre-ripple p)
        (for-each
            (lambda (p)
                (with-primitive p                
                    (when (not (pdata-exists? "rip-pref"))
                        (pdata-copy "p" "rip-pref"))
                    (pre-ripple p)))
            (get-children)))
    
    
    (define (do-ripple p t)
        (for-each
            (lambda (p)
                (with-primitive p                
                    (pdata-map!
                        (lambda (p pref)
                            (vadd pref (vmul (srndvec) 
                                    (* 0.1 (+ 1 (sin (+ (* t speed) (* wave-length     
                                                        (vdist (vtransform p 
                                                                (minverse (get-transform))) 
                                                            (vector 0 0 0))))))))))
                        "p" "rip-pref")
                    (do-ripple p t)))
            (get-children)))

    (pre-ripple 1)
    (do-ripple 1 t))

(clear)
(build-cube)
(translate (rndvec))
(build-torus 0.1 2 30 30)

(every-frame (ripple (time) 5 0.001))

