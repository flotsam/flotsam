(clear)
(clear-geometry-cache)
(hint-unlit)
(colour 0)
(define p (load-primitive "meshes/spider-1.obj"))
(clear-colour 0.5)

(define (add-blendshape key model)
    (let ((b (load-primitive model))
            (pname (string-append "p" (number->string key)))
            (nname (string-append "n" (number->string key))))
        (pdata-add pname "v")
        (pdata-add nname "v")
        (pdata-index-map!
            (lambda (i p)
                (with-primitive b (pdata-ref "p" i)))
            pname)
        (pdata-index-map!
            (lambda (i n)
                (with-primitive b (pdata-ref "p" i)))
            nname)
        (destroy b)))

(define (set-blendshape key)
    (pdata-copy (string-append "p" (number->string key)) "p")
    (pdata-copy (string-append "n" (number->string key)) "n")
    (recalc-normals 0))

(with-primitive p
    (pdata-copy "p" "p0")
    (pdata-copy "n" "n0")
    (add-blendshape 1 "meshes/spider-2.obj")
    (add-blendshape 2 "meshes/spider-3.obj"))

(every-frame
    (with-primitive p
        (set-blendshape (modulo (inexact->exact (round (* (time) 5))) 3))))
