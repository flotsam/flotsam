(clear)

(texture (load-texture "textures/spark.png"))
(hint-unlit)

(define (build-pickup x y)
    (let ((p (build-ribbon 30)))
        
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                    (vector (cos (/ i x)) (sin (/ i y)) (/ i (pdata-size))))
                "p")
            (pdata-index-map!
                (lambda (i p)
                    (* 0.3 (sin (* 3.141 (/ i (pdata-size))))))
                "w")
            (pdata-map!
                (lambda (c)
                    (vector 1 1 1))
                "c"))
        
        p))

(build-pickup 5 3)