#lang scheme
(define testy%
  (class object%
    (init-field (a 0))
    
    (define/public (m) (display a)(newline))
    
    (super-new)))

(define testy2%
  (class testy%
    (inherit-field a)
    (field (b a))
    
    (define/public (mm) (display a)(display b) (newline))
    
    (super-new)))

(define t (make-object testy2% 100))
(send t m)
(send t mm)