(clear)

(clear-colour 0.5)



(define (build)     
    (let ((p (build-locator)))
        (with-state
            (colour (rndvec))
            (parent p)
            (hint-depth-sort)
            (hint-unlit)
            (backfacecull 0)
            (texture (load-texture "textures/butterfly.png"))
            (load-primitive "meshes/butterfly.obj")
            (translate (vector 0 0.001 0))
            (load-primitive "meshes/butterfly.obj"))
        p))

(define (anim)
    (let ((a (* 90 (rndf))))
        (with-primitive (car (get-children))
            (rotate (vector 0 0 a)))
        (with-primitive (cadr (get-children))
            (rotate (vector 0 0 (- a))))))

(define l (build-list 10 
    (lambda (_) 
        (with-state 
            (translate (srndvec)) 
            (rotate (vmul (rndvec) 360))
            (scale 0.5)
            (build)))))

(every-frame
    (for-each 
        (lambda (p)
            (with-primitive p
                (anim)))
        l))