(clear)
(show-axis 1)
(clear-colour 0.5)
(hint-wire)
(define p (build-ribbon 2))

(define p2 (with-state (scale (vector 0.1 0.1 1)) (build-cube)))

(every-frame 
    (let ((dir (vtransform-rot (vector 0 0 1) (minverse (get-camera-transform)))))
        (with-primitive p
            (pdata-set! "p" 1 
                dir))
        (with-primitive p2
            (identity)
            (concat (maim dir (vector 0 1 0)))
            (scale (vector 1 0.1 0.1)))))