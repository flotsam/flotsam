(define models (list 
;    "meshes/fork.obj"
    "meshes/stone1.obj"
    "meshes/stone2.obj"
    "meshes/stone3.obj"))

(define (choose l)
    (list-ref l (random (length l))))

(clear)

(for ((i (in-range 0 10)))
    (with-state
        (texture (load-texture "textures/quartz.png"))
        (translate (vmul (srndvec) 10))
        (rotate (vmul (rndvec) 360))
        (scale 0.5)
        (load-primitive (choose models))))





