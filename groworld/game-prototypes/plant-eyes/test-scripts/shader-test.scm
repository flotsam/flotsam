(clear)
(clear-shader-cache)
(clear-texture-cache)

(let ((l (make-light 'point 'free)))
    (light-diffuse 0 (vector 0 0 0))
    (light-diffuse l (vector 1 1 1))
    (light-position l (vector 10 50 -4)))


(with-primitive (with-state
    (colour (vector 0.5 1 0.4))
    (shader "shaders/twig.vert.glsl" "shaders/twig.frag.glsl")
    (multitexture 0 (load-texture "textures/cells-1.png"))
    (multitexture 1 (load-texture "textures/cells-2.png"))
    (multitexture 2 (load-texture "textures/cells-3.png"))
    (multitexture 3 (load-texture "textures/root-norm.png"))
    (build-torus 1 2 20 20))

(shader-set! (list "Maps" (list 0 1 2) "NormalMap" 3)))