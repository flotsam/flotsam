(require scheme/class)


(define dust%
    (class object%
        (field
            (rate 10)
            (next-p 0)
            (root (let ((p (with-state
                (build-particles 500))))
                (with-primitive p
                    (pdata-map! 
                        (lambda (c)
                            (vector 1 1 1))
                        "c")) p))
            (emitter (build-locator)))

    (define/public (update t d)

        (let ((emitter-pos (with-primitive emitter
            (identity)
            (concat (minverse (get-camera-transform)))
            (translate (vector 0 0 -10))
            (vtransform (vector 0 0 0) (get-transform)))))

        (with-primitive root
        (for ((i (in-range 0 rate)))
            (pdata-set! "p" next-p (vadd emitter-pos (vmul (srndvec) 10)))
            (set! next-p (+ next-p 1))))))

    (super-new)))

(clear)
(define t (make-object dust%))

(define (animate)
    (send t update (time) (delta)))

(every-frame (animate))