;; p l a n t   e y e s  [ copyright (c) 2009 foam vzw : gpl v3 ]

;#lang scheme/base
;(require fluxus-016/drflux)
(require scheme/class 
  "scripts/game-modes.ss" 
  "scripts/logic.ss" 
  "scripts/view.ss" 
  "scripts/controller.ss" 
  "scripts/client.ss" 
  "scripts/jabberer.ss" 
  "scripts/list-utils.ss")

(define world-list (let* ((f (open-input-file "world.txt"))                            
                               (o (list (read f)(read f)(read f)(read f))))
                      (close-input-port f)
                      o))

;(show-fps 1)
(clear)
(clear-shader-cache)
(clear-texture-cache)

(define mode 'gui)
(define gui (make-object gui-game-mode% (list-ref world-list 0)))
(define game (make-object main-game-mode% world-list))
(send gui setup)

;(define t 0)
;(define d 0.02)
;(define (flxtime) t)
;(define (update-time) (set! t (+ t d)))
;(define (delta) d)

(define (animate)  
  (cond 
    ((eq? mode 'gui) 
     (when (send gui update (flxtime) (delta))
       (send game setup (send gui get-player-info) (send gui get-players))
       (set! mode 'game)))
    ((eq? mode 'game) 
     (send game update (flxtime) (delta))))
  #;(update-time)
    (sleep 0.01))
    
(every-frame (animate))

;(start-framedump "pe7" "jpg")
