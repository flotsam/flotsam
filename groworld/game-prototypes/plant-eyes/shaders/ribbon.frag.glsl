varying vec2 T;
uniform sampler2D BaseMap;
varying vec3 P;
uniform vec3 Origin;

void main()
{ 
	vec4 MidColour=mix(vec4(0.171,0.273,0.336,1),
					 gl_FrontMaterial.diffuse,
					 smoothstep(-2.0,2.0,P.y+Origin.y));


    gl_FragColor = vec4(MidColour.xyz*texture2D(BaseMap, T).xyz,1);
}
