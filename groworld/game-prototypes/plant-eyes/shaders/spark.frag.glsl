varying vec2 T;
uniform sampler2D BaseMap;

void main()
{ 
    gl_FragColor = gl_FrontMaterial.diffuse*texture2D(BaseMap, T);
}
