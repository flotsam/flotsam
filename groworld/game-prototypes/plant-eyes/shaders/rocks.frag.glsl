varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec2 T;
varying vec3 P;
uniform sampler2D NormalMap;

void main()
{ 
	vec3 bump = normalize(texture2D(NormalMap,T).xyz*2.0-1.0)-vec3(0,0,1);
	vec3 n = normalize(N);
	vec3 bn = normalize(N+bump*2.0);
	vec3 l = normalize(L);
	vec3 v = normalize(V);

    float HighlightSize=0.1;
    float ShadowSize=0.2;
    float OutlineWidth=0.2;
	
	//vec4 MidColour=gl_FrontMaterial.diffuse;
	//if(P.y<0) MidColour=vec4(1,1,1,1);
		
	vec4 MidColour=mix(vec4((1.0-gl_FrontMaterial.diffuse.r)*0.2,
							(1.0-gl_FrontMaterial.diffuse.g)*0.2,
							(1.0-gl_FrontMaterial.diffuse.b)*0.2,1),
					 gl_FrontMaterial.diffuse,
					 smoothstep(-5.0,5.0,P.y));

	vec4 HighlightColour=MidColour*1.3;
	vec4 ShadowColour=MidColour*0.6;
    MidColour.a=1.0;
	HighlightColour.a=1.0;
	ShadowColour.a=1.0;
	
    float lambert = dot(l,bn);
    vec4 colour = MidColour;
    if (lambert > 1.0-HighlightSize) colour = HighlightColour;
    if (lambert < ShadowSize) colour = ShadowColour;
	//if (dot(n,v) < OutlineWidth) colour = vec4(0,0,0,1);

	gl_FragColor = vec4(colour.rgb,1);

}
