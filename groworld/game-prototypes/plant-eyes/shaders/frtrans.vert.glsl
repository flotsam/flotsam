// simple facing ratio transparency shader

varying vec3 N;
varying vec3 P;
varying vec3 V;

void main()
{
    N = normalize(gl_NormalMatrix*gl_Normal);
    P = gl_Vertex.xyz;
    V = -vec3(gl_ModelViewMatrix*gl_Vertex);
    gl_Position = ftransform();
}
