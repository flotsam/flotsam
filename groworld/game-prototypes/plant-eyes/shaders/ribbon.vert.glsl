varying vec2 T;
varying vec3 P;

void main()
{    
	T = gl_MultiTexCoord0.xy;
    P = gl_Vertex.xyz;
    gl_Position = ftransform();
}
