varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec2 T;
uniform sampler2D BaseMap;

void main()
{ 
	vec3 n = normalize(N);
	vec3 l = normalize(L);
	vec3 v = normalize(V);

    float HighlightSize=0.1;
    float ShadowSize=0.2;
    float OutlineWidth=0.4;
	
	vec4 MidColour=gl_FrontMaterial.diffuse;
	vec4 HighlightColour=MidColour*2.0;
	vec4 ShadowColour=MidColour*0.5;
	HighlightColour.a=1.0;
	ShadowColour.a=1.0;
	
    float lambert = dot(l,n);
    vec4 colour = MidColour;
    if (lambert > 1.0-HighlightSize) colour = HighlightColour;
    if (lambert < ShadowSize) colour = ShadowColour;
    if (dot(n,v) < OutlineWidth) colour = vec4(0,0,0,1);

    gl_FragColor = colour*texture2D(BaseMap, T);
}
