// simple facing ratio transparency shader

varying vec3 V;
varying vec3 N;

void main()
{
  vec3 v = normalize(V);
  vec3 n = normalize(N);
  vec3 col=vec3(gl_FrontMaterial.diffuse);
  gl_FragColor = vec4(col,1.0f-abs(dot(n,v)));
}
