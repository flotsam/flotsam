varying vec2 T;

void main()
{    
	T = gl_MultiTexCoord0.xy;
    gl_Position = ftransform();
}
