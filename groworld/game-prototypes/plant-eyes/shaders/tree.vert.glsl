// Copyright (C) 2007 Dave Griffiths
// Licence: GPLv2 (see COPYING)
varying vec3 N;
varying vec3 P;
varying vec3 V;
varying vec3 L;
varying vec2 T;

void main()
{    
    N = normalize(gl_NormalMatrix*gl_Normal);
    P = gl_Vertex.xyz;
    V = -vec3(gl_ModelViewMatrix*gl_Vertex);
	vec4 LightPos = gl_LightSource[1].position;
	L = vec3((LightPos-gl_Vertex));
	T = gl_MultiTexCoord0.xy;
    gl_Position = ftransform();
}
