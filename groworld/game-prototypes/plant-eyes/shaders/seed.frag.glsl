varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec3 P;
varying vec2 T;
uniform sampler2D Maps[3];
uniform sampler2D NormalMap;
uniform float Time;

void main()
{ 
	vec3 bump = normalize(texture2D(NormalMap,T).xyz*2.0-1.0)-vec3(0,0,1);
	vec3 n = normalize(N);
	vec3 bn = normalize(N);//+bump*2.0);
	vec3 l = normalize(L);
	vec3 v = normalize(V);

    float HighlightSize=0.1;
    float ShadowSize=0.2;
    float OutlineWidth=0.2;
	
	vec4 MidColour=mix(vec4(0.171,0.273,0.336,1),
					 gl_FrontMaterial.diffuse,
					 smoothstep(-2.0,2.0,P.y));

	vec4 HighlightColour=MidColour*1.3;
	vec4 ShadowColour=MidColour*0.6;
    MidColour.a=1.0;
	HighlightColour.a=1.0;
	ShadowColour.a=1.0;
	
	vec4 texture;
	float t = fract(Time*0.1)*3.0;
	if (t<1.0) // mix bet 0 and 1
	{
		texture = mix(texture2D(Maps[0], T*10.0),texture2D(Maps[1], T*10.0),t);
	}
	else if (t<2.0 && t>1.0) // mix bet 1 and 2
	{
		texture = mix(texture2D(Maps[1], T*10.0),texture2D(Maps[2], T*10.0),t-1.0);
	}
	else // mix bet 2 and 0
	{
		texture = mix(texture2D(Maps[2], T*10.0),texture2D(Maps[0], T*10.0),t-2.0);
	}
	
    float lambert = dot(l,bn);
    vec4 colour = MidColour;
    if (lambert > 1.0-HighlightSize) colour = HighlightColour;
    if (lambert < ShadowSize) colour = ShadowColour;
    
	if (dot(n,v) < OutlineWidth) colour = vec4(0,0,0,1);
    
	
	if (dot(n,v) < 0.0) colour = MidColour*texture*0.5;

	// add linear fog
	//float fog_factor = clamp((gl_Fog.end - gl_FogFragCoord) * gl_Fog.scale, 0.0, 1.0);
	//gl_FragColor = mix(gl_Fog.color, colour, fog_factor);
	gl_FragColor = colour;

}
