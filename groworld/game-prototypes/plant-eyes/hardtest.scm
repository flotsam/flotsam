(define p (build-torus  1 2 100 100))

(define (loop)
    (destroy p)
    (set! p (build-torus 1 2 100 100))
    (loop))

(loop)