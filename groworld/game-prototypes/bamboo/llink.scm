;; simple example of setting texture, colour and rotation of an object
;; in fluxus from pd. requires llink.pd to be running in pd.

(require mzlib/string)

(define t1 0)
(define c1 (vector 0 0 0))
(define v1 (vector 0 0 0))

(define (set-texture str)
  (set! t1 (load-texture str)))

(define (set-colour r g b)
  (set! c1 (vector r g b)))

(define (set-rotor x y z)
  (set! v1 (vector x y z)))


(define (spin)
  (push)
  (colour c1)
  (texture t1)
  (rotate v1)
  (draw-cube)
  (pop))


(define (osc-repl)
  (cond ((osc-msg "/code")
	 (printf "eval..~% ~a~%" (pd-filter (osc 0)))
	 (eval-string (pd-filter (osc 0)) osc-repl-err))
	((osc-msg "/ping")
	 (printf "ping... ~%"))))
          
(define (osc-repl-err err)
  (printf "xcrrERR: ~a~%" err))

(define (pd-filter str)
 (regexp-replace* "_" str " "))

(osc-source "88008")


(spawn-task osc-repl)
(spawn-task spin)
