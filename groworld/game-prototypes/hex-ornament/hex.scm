(define (build-ngon n)
    (let ((p (build-polygons n 'polygon)))
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                    (let ((a (* (/ i n) (* 2 3.141))))
                        (vector (cos a) (sin a) 0)))
                "p")
            (pdata-map! 
                (lambda (t p) 
                    (vsub (vmul p 0.5) (vector 0.5 0.5 0))) 
                "t" "p"))
        p))

(define (binarify n)
    (pdata-index-map!
        (lambda (i p)
            (let ((c (arithmetic-shift 1 (inexact->exact i))))
                (when (not (zero? (bitwise-and (inexact->exact n) c)))
                    (let ((pos (vtransform (vmul (vadd p (pdata-ref "p" (+ i 1))) 0.5) (get-transform))))
                        (with-state
                            (hint-none)
                            (hint-wire)
                            (translate pos)
                            (scale 0.1)
                            (build-ngon 10))))
                p))
        "p"))

(clear)

(for ((i (in-range 0 64)))
    (with-primitive (with-state
            (translate (vmul (vector (modulo i 8) (quotient i 8) 0) 2))
            (hint-none)
            (hint-wire)
            (hint-unlit)
            (build-ngon 6))
        (binarify i)))
