(define (direction-normal d) 
    (let ((a (* 60 0.017453292)))
        (vector (sin (* a d)) (cos (* a d)) 0)))


(define (build-ngon n)
    (let ((p (build-polygons n 'polygon)))
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                    (let ((a (* (/ i n) (* 2 3.141))))
                        (vector (cos a) (sin a) 0)))
                "p")
            (pdata-map! 
                (lambda (t p) 
                    (let ((p (vtransform p (mmul 
                                        (mrotate (vector 0 0 -90))
                                        (mscale (vector -1 1 1))))))
                        (vsub (vmul p 0.45) (vector 0.5 0.5 0))))
                "t" "p")
            (pdata-copy "t" "tref")
            (pdata-map! (lambda (n) (vector 0 0 1)) "n"))
        p))

(clear)
(build-ngon 6)
(for ((i (in-range 0 6)))
    (with-primitive (build-ribbon 2)
        (hint-wire)
        (pdata-set "p" 0 (vector 0 0 0))
        (pdata-set "p" 1 (vmul (direction-normal i) 2))))