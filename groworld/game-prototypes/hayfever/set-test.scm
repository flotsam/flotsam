(define (build-shells obj count dist col)
    (when (not (zero? count))
        (with-state
            (parent obj)
            (colour col)
            (let ((shell (build-copy obj)))
                (with-primitive shell                    
                    (pdata-map!
                        (lambda (p n)
                            (vadd p (vmul (vector 0 1 0) dist)))
                        "p" "n"))
                (build-shells shell (- count 1) dist (vmul col 1))))))

(define (build-shrub p n)
    (with-state
        (translate p)
        (colour (vector 0.5 0.7 0.4))
        (let ((shrub (build-ribbon (+ (random 10) 2))))
            (with-primitive shrub
                (pdata-index-map!
                    (lambda (i p)
                        (let ((j (* 0.2 (* i 0.2))))
                            (vector (* (crndf) j) (* i 0.2) (* (crndf) j))))
                    "p")
                (pdata-index-map!
                    (lambda (i w)
                        (* (/ 1 (+ i 1)) 0.2))
                    "w")))))

(define (choose l)
    (list-ref l (random (length l))))

(define (build-flower p n obj)
    (with-state 
        (backfacecull 0)
        (hint-depth-sort)
;        (hint-unlit)
        (translate p)
        (rotate (vector 0 (random 360) 0))
        (scale (+ 1 (* (rndf) 1.5)))
        (texture (load-texture (choose (list 
            "textures/bgplant-1.png"
            "textures/bgplant-2.png"
            "textures/bgplant-3.png"
            "textures/bgplant-4.png"
            "textures/bgplant-5.png"
            "textures/bgplant-6.png"
            ))))
        (let ((o (build-copy obj)))
            (with-primitive o (hide 0)))
;        (load-primitive "meshes/freeplant.obj")
))

(define (load-model tex obj)
    (with-state
        (texture (load-texture (string-append "textures/" tex)))
        (load-primitive (string-append "meshes/" obj))))

(clear)

;----------------------------------------------
; build the set

(clear-colour (vmul (vector 0.4 0.5 0.9) 0.2))
(fog (vector 0.4 0.5 0.9) 0.01 1 100)

(define l (make-light 'spot 'free))
(light-diffuse 0 (vector 0 0 0))
(light-diffuse l (vector 1 1 1))
(light-position l (vector 0 5 0))
(light-direction l (vector 0 -1 0))
(light-spot-angle l 55)
(light-spot-exponent l 1)

(define l2 (make-light 'point 'free))
(light-position l2 (vector 0 10 0))
(light-diffuse l2 (vector 0.4 0.5 0.9))

(clear-texture-cache)
(clear-geometry-cache)

(with-state
    (backfacecull 0)
    (hint-depth-sort)
    (hint-unlit)
    (texture-params 0 '(wrap-s repeat wrap-t repeat))
    (load-model "bgplant-1.png" "plant-0.obj")
    (load-model "bgplant-1.png" "plant-1.obj")
    (load-model "bgplant-1.png" "plant-2.obj")
    (load-model "bgplant-1.png" "plant-3.obj")
    (load-model "bgplant-1.png" "plant-4.obj")
    (load-model "bgplant-1.png" "plant-5.obj")
    (load-model "bgplant-1.png" "plant-6.obj")
;    (load-model "bgplant-2.png" "plant-7.obj")

)


(with-state
    (backfacecull 0)
    (load-model "car-base.png" "car.obj")
    (load-model "car-base.png" "car-2.obj")
    (load-model "telly-base.png" "telly-2.obj"))

(with-state
    (load-model "telly-base.png" "telly-0.obj")
    (load-model "telly-base2.png" "telly-1.obj")
    (load-model "telly-base.png" "telly-2.obj")
    (load-model "telly-base.png" "telly-3.obj")
    (load-model "telly-base.png" "telly-4.obj"))


(with-state
    (load-model "washer-base.png" "washer-0.obj")
    (load-model "washer-base.png" "washer-1.obj")
    (load-model "washer-base.png" "washer-2.obj")
    (load-model "washer-base.png" "washer-3.obj"))

(define terrain (with-state
        (texture (load-texture "textures/ground-base.png"))
        (load-primitive "meshes/ground.obj")))


;(define grassmap (load-primitive "textures/set-grass.png"))

(define (tx->pi tx)
    (+ (vy tx) (* (vx tx) (pixels-width))))

(define flower-obj (load-primitive "meshes/freeplant.obj"))

(with-primitive flower-obj
    (pdata-map!
        (lambda (n p)
            (vnormalise p))
        "n" "p")
    (hide 1))

(with-primitive terrain
    (poly-for-each-tri-sample    
        (lambda (indices bary)
            (let ((tc (vadd 
                            (vmul (pdata-ref "t" (list-ref indices 0)) (vx bary))
                            (vmul (pdata-ref "t" (list-ref indices 1)) (vy bary))
                            (vmul (pdata-ref "t" (list-ref indices 2)) (vz bary)))))
                (when (zero? (random 2)) #;(> (va (with-primitive grassmap (pdata-ref "c" (tx->pi tc)))) 0.5)
                    (build-flower (vadd 
                            (vmul (pdata-ref "p" (list-ref indices 0)) (vx bary))
                            (vmul (pdata-ref "p" (list-ref indices 1)) (vy bary))
                            (vmul (pdata-ref "p" (list-ref indices 2)) (vz bary))) (vector 0 1 0)
                flower-obj))))
        1))

(define shell0 (build-copy terrain))

(with-primitive shell0
    (pdata-copy "t" "t1")
    (pdata-map!
        (lambda (t)
            (vmul t 4))
        "t"))

(with-state
    (multitexture 0 (load-texture "textures/shell.png"))
    ;    (multitexture 1 (load-texture "textures/ground-grassmap.png"))
    (build-shells shell0 4 0.05 (vector 1 0.5 1)))

