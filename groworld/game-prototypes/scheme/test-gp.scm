(set-searchpaths (list "../"))

(require "gp.ss")
(require "plant.ss")
(clear)
(define target "F[/FL][+FL][-FL][\\FL]")
(define pop-size 10)
(define vocab "FL[]+-/\\^&")
(define pop (make-population pop-size "L" 50 vocab))
(define plant 0)

(define terrain (with-state
        (texture (load-texture "textures/ground.png"))
        (scale (vector 1 1 1))
        (build-sphere 10 10)))

(define (run)
    (for ((i (in-range 0 500)))
        (let ((fit (fitness-list pop string-score target)))
            (set! pop (cull pop fit 0.9))
            (set! pop (recombine-population pop pop-size))
            (set! pop (mutate-population pop 2 vocab))
            (display (car pop))(newline)
            (set! plant (build-plant (vector 0.001 0 0) terrain (car pop)))
            (destroy-plant plant))))

(thread run)
