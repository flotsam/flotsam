
; try all the rules on this character - 
; returns #f if none are matched
(define (lsys-run-rules char rules)
  (foldl
   (lambda (rule str)
     (if str ; if str is not #f
         str ; then we have already found a rule, return it
         (if (char=? char (string-ref (car rule) 0)) ; check this rule
             (cadr rule) ; return the string
             #f))) ; no match
   #f
   rules))

; runs the lsystem rules on every character in a string, 
; returns the new string
(define (lsys-search-replace str rules pos result)
  (cond 
    ((>= pos (string-length str)) result)
    (else
     (let ((ret (lsys-run-rules (string-ref str pos) rules)))
       (if (string? ret)
          (lsys-search-replace str rules (+ pos 1) 
            (string-append result ret))
          (lsys-search-replace str rules (+ pos 1) 
            (string-append result (string (string-ref str pos)))))))))

; runs the search-replace multiple (n) times on a string
(define (ls-generate n str rules)
  (cond 
    ((zero? n) str)
    (else
     (ls-generate (- n 1) 
        (lsys-search-replace str rules 0 "") rules))))

; builds objects from a string
(define (ls-build string angle branch-scale branch-col leaf-col)
  (for-each 
   (lambda (char)
     (cond 
       ((char=? #\F char)
        (with-state
		(hint-cast-shadow)

         (translate (vector 1 0 0))
         (scale (vector 1 2 2))
         (rotate (vector 0 90 0))         
         (colour branch-col)
		(load-primitive "branch.obj")
         #;(with-primitive (build-ribbon 2)
            (texture (load-texture "textures/scribble.png"))
            (hint-unlit)
            (pdata-set! "w" 0 0.1)
            (pdata-set! "w" 1 0.1)
            (pdata-set! "p" 0 (vector 0 0 1))
            (pdata-set! "p" 1 (vector 0 0 0))))
        (translate (vector 1 0 0)))
       ((char=? #\L char)
        (with-state
         (translate (vector 1 0 0))
         (scale (vector 1 0.5 0.5))
;         (rotate (vector 0 90 0))         
         (colour leaf-col)
         (texture (load-texture "textures/leaf.png"))
		 (load-primitive "leaf.obj")
         #;(build-plane))
        (translate (vector 1 0 0)))
       ((char=? #\f char)
        (translate (vector 1 0 0)))
       ((char=? #\/ char)
        (rotate (vector angle 0 0)))
       ((char=? #\\ char)
        (rotate (vector (- angle) 0 0)))
       ((char=? #\+ char)
        (rotate (vector 0 angle 0)))
       ((char=? #\- char)
        (rotate (vector 0 (- angle) 0)))
       ((char=? #\^ char)
        (rotate (vector 0 0 angle)))
       ((char=? #\& char)
        (rotate (vector 0 0 (- angle))))
       ((char=? #\| char)
        (rotate (vector 0 0 180)))
       ((char=? #\[ char)
        (push)
        (scale (vector branch-scale branch-scale branch-scale)))
       ((char=? #\] char)
        (pop))))
   (string->list string)))

(define (populate p)
    (poly-for-each-tri-sample
        (lambda (indices bary)
            (let ((p (vadd (vadd 
                (vmul (pdata-ref "p" (list-ref indices 0)) (vx bary))
                (vmul (pdata-ref "p" (list-ref indices 1)) (vy bary)))
                (vmul (pdata-ref "p" (list-ref indices 2)) (vz bary))))
                  (n (vadd (vadd 
                (vmul (pdata-ref "n" (list-ref indices 0)) (vx bary))
                (vmul (pdata-ref "n" (list-ref indices 1)) (vy bary)))
                (vmul (pdata-ref "n" (list-ref indices 2)) (vz bary)))))
            (with-state
                (translate p) 
                (concat (maim n (vector 0 1 0)))
                (scale 0.4)
                (ls-build (ls-generate (+ (random 3) 1) "F" '(("F" "F[^F][+/&FL]"))) 45 0.7
                                 ;(vector 1 0.5 0.2) 
                                 ;(vector 0.5 1 0.5)
                                 (vector 1 (rndf) (* (rndf) 0.4)) 
                                 (vector (rndf) 1 (rndf))

                                    ))))
        p))

(clear)
(light-diffuse 0 (vector 0.4 0.4 0.4))
(define l (make-light 'point 'free))
(light-diffuse l (vector 1 1 1))
(light-position l (vector 20 5 30))
(shadow-light l)
(shadow-debug 0)

(define world (with-state
    (hint-cast-shadow)
    (colour (vector 0.2 0.5 0.2))
    (build-torus 1 2 10 10)))

(with-primitive world
    (populate 1))
