(clear)
(show-axis 0)
(define p (build-polygons 12 'triangle-list))
(with-primitive p
    (colour (vector 1 1 0))
    (hint-unlit)
    (hint-wire)
    (wire-colour (vector 0 0 0))
    (pdata-set! "p" 0 (vector 0.1 0 0))
    (pdata-set! "p" 1 (vector 0 0.06 0))
    (pdata-set! "p" 2 (vector 0 -0.06 0))
    (pdata-set! "p" 3 (vector 0.1 0 1))
    (pdata-set! "p" 4 (vector 0 0.06 1))
    (pdata-set! "p" 5 (vector 0 -0.06 1))

    (pdata-set! "n" 0 (vector 1 0 0))
    (pdata-set! "n" 1 (vnormalise (vector -0.4 0.5 0)))
    (pdata-set! "n" 2 (vnormalise (vector -0.4 -0.5 0)))
    (pdata-set! "n" 3 (vector 1 0 0))
    (pdata-set! "n" 4 (vnormalise (vector -0.4 0.5 0)))
    (pdata-set! "n" 5 (vnormalise (vector -0.4 -0.5 0)))

    ;(hide 1)

    (poly-set-index (list  1 0 2  3 4 5  0 1 4  2 5 4
                           1 2 4  0 4 3  2 0 5  3 5 0))

    (save-primitive "branch.obj")
)
(clear-geometry-cache)
(with-state
    (hint-wire)
    (wire-colour (vector 0 0 0))
    (hint-unlit)
    (translate (vector 0.3 0 0))
    (with-primitive (load-primitive "branch.obj")
        (display (poly-indices))(newline)))

(define l (build-polygons 8 'quad-list))

#;(with-primitive l
    (pdata-set "p" 0 (vector 1 0 0))
    (pdata-set "p" 1 (vector 0 0.7 0))
    (pdata-set "p" 2 (vector -1 0 0))
    (pdata-set "p" 3 (vector 0 -0.7 0))

    (pdata-set "p" 4 (vector 0 -0.7 0))
    (pdata-set "p" 5 (vector -1 0 0))
    (pdata-set "p" 6 (vector 0 0.7 0))
    (pdata-set "p" 7 (vector 1 0 0))


    (pdata-set "n" 0 (vector 0 0 1))
    (pdata-set "n" 1 (vector 0 0 1))
    (pdata-set "n" 2 (vector 0 0 1))
    (pdata-set "n" 3 (vector 0 0 1))

    (pdata-set "n" 4 (vector 0 0 -1))
    (pdata-set "n" 5 (vector 0 0 -1))
    (pdata-set "n" 6 (vector 0 0 -1))
    (pdata-set "n" 7 (vector 0 0 -1))

;    (save-primitive "leaf.obj")
)




