(set-searchpaths (list "../"))
(flxseed 0)
(random-seed 1)

(require "gp.ss")
(require "plant.ss")
(require "ls.ss")
(clear)
(define pop-size 10)
(define vocab "FLA[]+-/\\^&")
(define pop (make-population pop-size "XXXXXXXX" 2 vocab)) ;"FF[^^A][&&A]/////L" 2 vocab))
(define plant 0)

(define light-pos (vector 0 5 5))
(light-diffuse 0 (vector 0 0 0))
(light-ambient 0 (vector 0.4 0.4 0.4))
(define l (make-light 'point 'free))
(light-diffuse l (vector 1 1 1))
(light-ambient l (vector 0.5 0.5 0.5))
(light-position l light-pos)
(shadow-light l)
(ambient (vector 0.2 0.2 0.2))
(define fog-col (vector 0 0 0))
(fog fog-col 0.05 0 100)
(clear-colour fog-col)

(with-state
    (colour (vector 1 1 0))
    (translate light-pos)
    (hint-unlit)
    (scale 0.3)
    (build-sphere 50 50))

(define terrain (with-state
        (texture (load-texture "textures/ground.png"))
        (scale 100)
        (translate (vector -0.5 0.01 0.5))
        (rotate (vector -90 0 0))
        (build-seg-plane 50 50)))

(with-primitive terrain
    (pdata-map!
        (lambda (t)
            (vmul t 10))
        "t"))

(with-primitive terrain (apply-transform))

(define (run-ls rule)
    (lsystem-generate 3 "A" (list (list "A" rule))))

; needs (terrain lightpos)
(define (fitness-func rule l)
    (let* ((str (run-ls rule)))
        (let*
            ((plant (build-plant (vector 0.01 0 0) (car l) str))           
                (score (plant-trace-leaves plant (cadr l))))
            (sleep 0.01)
            (destroy-plant plant)
            (if (> (string-length rule) 10) 
                (- score (* 0.1 (- (string-length rule) 10)))
                score))))

(define plant 0)
(define first-frame #t)

(define (run)
    (let ((fit (fitness-list pop fitness-func (list terrain light-pos))))
        (when (not first-frame) (destroy-plant plant))
        (set! first-frame #f)
        (set! plant (build-plant (vector 0.01 0 0) terrain 
                (run-ls (list-ref pop (fittest fit))))) ; draw the fittest from this population
        (display (list-ref pop (fittest fit)))(newline)
        (set! pop (cull pop fit 0.5))
        (set! pop (recombine-population pop pop-size))
        (set! pop (mutate-population pop 10 vocab))))

(every-frame (run))

