;(require fluxus-015/drflux)

(define light-pos (vector 0 3 1))

;; gp ------------------------------------------------

(define (random-pick str)
  (string-ref str (random (string-length str))))

(define (mutator fn str rate)
  (let ((ret ""))
    (for ((i (in-range 0 (string-length str))))
         (cond
           ((< (random 100) rate)
            (set! ret (string-append ret (fn (string-ref str i))))) ; shuffle
           (else
            (set! ret (string-append ret (string (string-ref str i)))))))
    ret))

(define (mutate-shuffle str rate)
  (mutator 
   (lambda (c)
     (string (random-pick str)))
   str rate))

(define (mutate-swap-new str rate vocab)
  (mutator 
   (lambda (c)
     (string (random-pick vocab)))
   str rate))

(define (mutate-insert-new str rate vocab)
  (mutator 
   (lambda (c)
     (string-append (string c) (string (random-pick vocab))))
   str rate))

(define (mutate-delete str rate)
  (mutator 
   (lambda (c)
     "")
   str rate))

(define (mutate-rrepeat str rate)
  (mutator 
   (lambda (c)
     (string-append (string c) (string c)))
   str rate))

(define (mutate str rate vocab)
  (set! str (mutate-shuffle str rate))
  (set! str (mutate-swap-new str rate vocab))
  (set! str (mutate-insert-new str rate vocab))
  (set! str (mutate-delete str rate))
  (set! str (mutate-rrepeat str rate))
  str)

;; l system ------------------------------------------------

; try all the rules on this character - 
; returns #f if none are matched
(define (lsys-run-rules char rules)
  (foldl
   (lambda (rule str)
     (if str ; if str is not #f
         str ; then we have already found a rule, return it
         (if (char=? char (string-ref (car rule) 0)) ; check this rule
             (cadr rule) ; return the string
             #f))) ; no match
   #f
   rules))

; runs the lsystem rules on every character in a string, 
; returns the new string
(define (lsys-search-replace str rules pos result)
  (cond 
    ((>= pos (string-length str)) result)
    (else
     (let ((ret (lsys-run-rules (string-ref str pos) rules)))
       (if (string? ret)
           (lsys-search-replace str rules (+ pos 1) 
                                (string-append result ret))
           (lsys-search-replace str rules (+ pos 1) 
                                (string-append result (string (string-ref str pos)))))))))

; runs the search-replace multiple (n) times on a string
(define (ls-generate n str rules)
  (cond 
    ((zero? n) str)
    (else
     (ls-generate (- n 1) 
                  (lsys-search-replace str rules 0 "") rules))))

; builds objects from a string
(define (ls-build string angle branch-scale branch-col leaf-col)
  (let ((d 0) (leaves '()))
    (for-each 
     (lambda (char)
       (let ((angle (+ angle (* 0 (grndf)))))
         (cond 
           ((char=? #\F char) 
            (with-state
             (hint-cast-shadow)
             (translate (vector 1 0 0))
             (scale (vector 1 1 1))
             (rotate (vector 0 90 0))         
             (colour branch-col)
             (load-primitive "branch.obj"))
            (translate (vector 1 0 0)))
           ((char=? #\L char)
            (with-state
             (hint-cast-shadow)
             (hint-normal)
             (translate (vector 0.5 0 0))
             (scale (vector 0.5 0.25 0.25))
             (rotate (vector 90 0 0))         
             (colour leaf-col)
             (set! leaves (cons (load-primitive "leaf.obj") leaves))))
            ((char=? #\f char) (translate (vector 1 0 0)))
            ((char=? #\/ char) (rotate (vector angle 0 0)))
            ((char=? #\\ char) (rotate (vector (- angle) 0 0)))
            ((char=? #\+ char) (rotate (vector 0 angle 0)))
            ((char=? #\- char) (rotate (vector 0 (- angle) 0)))
            ((char=? #\^ char) (rotate (vector 0 0 angle)))
            ((char=? #\& char) (rotate (vector 0 0 (- angle))))
            ((char=? #\| char) (rotate (vector 0 0 180)))
            ((char=? #\[ char) (set! d (+ d 1)) (push) (scale (vector branch-scale branch-scale branch-scale)))
            ((char=? #\] char) (when (> d 0) (set! d (- d 1)) (pop))))))
       (string->list string))
     (for ((i (in-range 0 d))) (pop))
     leaves))
  
  ;; plant ------------------------------------------------
  
  (define-struct plant (rules objects leaves))
  
  (define (build-plant pos terrain rule)
    ; sit on terrain
    (with-primitive terrain
                    (let ((int (line-intersect (vector (vx pos) 1000 (vz pos))
                                               (vector (vx pos) 0 (vz pos))))) 
                      (when (not (null? int)) ; otherwise fallen off world            
                        (let* ((pos (cdr (assoc "p" (car int))))
                               (rule (mutate rule 0 "FL[]+-/\\&^A")) ; mutate rule
                               (str (ls-generate 4 "A" (list (list "A" rule))))) ; gen plant
                          (with-state
                           (translate pos)
                           (rotate (vector 0 0 90))
                           (make-plant rule '() 
                                       (ls-build str 20 0.75 (vector 1 0.4 0.2) (vector 0 1 0)))))))))
  
  (define (plant-trace-leaves plant)
      (for-each
       (lambda (leaf)
         (let ((pos (with-primitive leaf (vtransform (vector 0 0 0) (get-transform))))
               (line (with-state 
                      (hint-none)(hint-unlit)(hint-wire)
                      (build-line 2))))
           
           (with-primitive line
                           (pdata-set "p" 0 pos)
                           (pdata-set "p" 1 light-pos))
            
            (with-primitive leaf

             (let* ((l (vnormalise (vsub light-pos pos)))
                    (score1 (vdot l (pdata-ref "n" 0)))
                    (score2 (vdot l (pdata-ref "n" 4)))
                    (score (+ (if (< score1 0) 0 score1)
                              (if (< score2 0) 0 score2))))
    
            
            (with-state             
                (hint-unlit)
                (hint-depth-sort)
                (translate pos)
                (scale 0.2)
                (rotate (vector 0 90 0))
                (texture (load-texture "font.png"))
                (build-text (number->string (* (round (* score 10)) 0.1))))))))
       (plant-leaves plant)))
  
  ;; scene ------------------------------------------------
  
  (clear)
  (clear-colour (vector 0.75 0.75 1))
  
  (light-diffuse 0 (vector 0 0 0))
  (light-ambient 0 (vector 1 1 1))
  (define l (make-light 'point 'free))
  (light-diffuse l (vector 1 1 1))
  (light-ambient l (vector 0.5 0.5 0.5))
  (light-position l light-pos)
  (shadow-light l)
  (ambient (vector 0.2 0.2 0.2))
  (fog (vector 0.75 0.75 1) 0.02 1 100)
  
  (define terrain (with-state
                   (texture (load-texture "textures/ground.png"))
                   ;        (colour (vector 0 0.5 0))
                   (scale (vector 1 1 1))
                   ; (load-primitive "terrain.obj")
                   (build-sphere 10 10)))
  
  (with-primitive terrain
                  (ambient (vector 0.5 0.5 0.5))
                  #;(pdata-map!
                     (lambda (t)
                       (vmul t 10))
                     "t")
                  (apply-transform))
  
  (define plant (build-plant (vector 0.001 0 0) 
                               terrain "F/[++[&&&L]A][--[^^^L]A]"))
  
  (plant-trace-leaves plant)
  
