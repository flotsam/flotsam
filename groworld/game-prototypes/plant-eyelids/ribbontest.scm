(clear)
(clear-colour 0.5)
(define p (build-ribbon 20))

(with-primitive p
    (translate (vector 1 0 0))
    (pdata-map!
        (lambda (p)
            (srndvec))
        "p")
    (pdata-map!
        (lambda (w)
            0.01)
        "w"))