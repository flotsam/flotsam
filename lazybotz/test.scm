(require "lazybotz.scm")

(go-laziness)

#;(bot 'fred #t "tex.png"
    (x 0
        (x 0 (x 1) (x 0 (x 1)
                (x 0 (x 0
                        (lambda () (* 45 (sin (time))))  (x 1)))))
        (x 0 (x 0 
                (lambda () (cos (* (time) 0.3)))
                (x 0) (x 0 (lambda () (* 1 (sin (* (time) 3)))) )))))

(texture (load-texture "green.png"))

(define (a)
    (* 45 (sin (time))))

(bot 'twanky (x 2 (x 1 a)))


;(bot 'twanky2 (x 4 (x 3 a) (x 4 a (x 2 a))))

