#include <spiralcore/Tuna.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
	if (argc<3)
	{
		cerr<<"Usage: intune scalafile.scl outputlut.scm"<<endl;
		return 1;
	}
	
	Tuna tuna;
	tuna.Open(argv[1]);
	
	ofstream f(argv[2]);
	f<<"(";
	for (int n=0; n<256; n++)
	{
		f<<tuna.GetNote(n)<<" ";
	}
	f<<")";
	f.close();
}
