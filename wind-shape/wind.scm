(define (inside? pos)
   (let ((p (vtransform pos (get-transform))))
      (odd? (length (geo/line-intersect (vector -100 1 -1000) p)))))


(define (random-triangle)
    (* (random (/ (pdata-size) 3)) 3))

(define (make-windshape num-particles col objfile dir)
    (let* ((oshape (load-primitive objfile))
           (shape (with-state 
                (backfacecull 1) 
                (hint-depth-sort)
                (hint-none) 
                (hint-cull-ccw)
                (hint-wire) 
                (opacity 0.2)
                (poly-build-triangulate oshape))))

        (destroy oshape)

    ;    (with-primitive shape (hide 1))

        (let ((particles (build-particles num-particles)))

            (with-primitive particles    
                (blend-mode 'src-alpha 'one)
                (texture (load-texture "pollen.png"))
               ;(hint-none)
                (hint-nozwrite)
                
               ; (hint-points) (hint-anti-alias)
                (point-width 3)
                (pdata-map! (lambda (c) col) "c")
                (pdata-map! (lambda (p) (vector 100 0 0)) "p")
                (pdata-map! (lambda (w) (let ((s (* 0.4 (rndf)))) (vector s s 0))) "s")
                (pdata-add "vel" "v")
                (pdata-map! (lambda (vel) (vmul (vadd dir (vmul (srndvec) 0.8)) 0.05)) "vel"))
                
            (list particles shape))))
            
(define (windshape-particles ws)
    (car ws))    

(define (windshape-shape ws)
    (cadr ws))    

(define (windshape-update ws)
    (with-primitive (windshape-particles ws)
        (pdata-op "+" "p" "vel")
        (pdata-map! 
            (lambda (p)
                (with-primitive (windshape-shape ws)
                    (if (not (inside? (vtransform p (minverse (get-transform)))))
                        (let* ((tri (random-triangle))
                              (b (rndbary)))
                            (vadd
                                (vmul (pdata-ref "p" tri) (vx b))
                                (vmul (pdata-ref "p" (+ tri 1)) (vy b))
                                (vmul (pdata-ref "p" (+ tri 2)) (vz b))))
                         p)))
            "p")))
      
(clear)

(fog (vector 0 0 0) 0.1 1 100)

(clear-colour (vector 0 0 0))
(define nparticles 1000)

(define ws-list
    (list 
        (with-state (translate (vector 1 2 4)) 
            (make-windshape nparticles (vector 0 0.4 0.7 0.5) "meshes/l.obj" (vector 1 0 0)))
        (with-state (translate (vector 0 5 3)) 
                    (rotate (vector 45 0 0))
            (make-windshape nparticles (vector 0.3 0.3 1.0 0.5) "meshes/s.obj" (vector -1 0 0)))
        (with-state (translate (vector 0 -2 2)) 
            (make-windshape nparticles (vector 1 0.5 1 0.5) "meshes/w.obj" (vector -0.5 0.5 0)))))

#;(define ws-list
    (list 
       (make-windshape nparticles (vector 1 0 0 1) "meshes/torus.obj" (vector 1 0 0))))
      
(every-frame 
    (for-each
        (lambda (ws)
            (windshape-update ws))
    ws-list))
      
      
(start-framedump "wind-" "jpg")      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
