#include <stdio.h>
#include <iostream>
#include "scheme.h"
#include "init.h"
#include <string.h>
using namespace std;

int main()
{
    scheme *sc=scheme_init_new();
    scheme_set_output_port_file(sc, stdout);
    //scheme_load_string(sc,"  \n  (define a 10)");
    //   cerr<<strlen("(begin\n (define a 10))")<<endl;
    //cerr<<initcode<<endl;

    //FILE *inf=fopen("init.scm","r");
    //scheme_load_file(sc,inf);
    //fclose(inf);

    scheme_load_string(sc,initcode);
    scheme_load_string(sc,"(define (a n) (when (not (zero? n)) (display n) (a (- n 1)))) (a 19)");
    return 0;
}
