class fluxus_renderer
{
public:
    fluxus_renderer();
    ~fluxus_renderer();

    fluxus_renderer *get();
 
    int build_cube();
    
private:

    static fluxus_renderer *m_renderer;

}
