(module pickled-feet (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  (define (lbp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para (* width 3) text)))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  
  (slide/center
   (page-para* "Fluxus: Scheme Livecoding") 
   (page-para* (text "Dave Griffiths" main-font 24)))
  
  
  (slide/title/center
   "Overview"
   (hc-append 
    (vl-append
     gap
     (bp "What is fluxus?")
     (bp "Livecoding")
     (bp "Scheme")
     )))
  
  
  (slide/title/center
   "What is fluxus?"
   (hc-append 
    (scale (vl-append
            gap
            (bp "Framework for various things:")
            (bbp "Playing/learning about graphics")
            (bbp "Workshops")
            (bbp "Performances")
            (bbp "Art installations")
            (bp "Game engine at heart...")
            (bp "With a livecoding editor")
            (bp "Source released under GPL")
            (bp "4 or 5 developers working on it")
            (bp "Works on Linux OSX and Windows (ish)")
            (bp "Uses the Scheme language")) 0.8)
    (scale (bitmap (build-path "images/fluxus.jpg")) 0.6)))
	
 (slide/title/center
   "Boring Feature List"
   (hc-append 
    (scale (vl-append
            gap
            (bp "Immediate mode drawing")
            (bp "Scenegraph")
            (bp "Primitives")
            (bbp "Polys")
            (bbp "Particles")
            (bbp "NURBS patches")
            (bbp "Blobbies (implicit surfaces)")
            (bbp "Pixels (procedural texture access)")
            (bp "Unified access to primitive data (vertex arrays, texture data)")
            (bp "More advanced stuff")
            (bbp "GLSL Hardware shading")
            (bbp "ODE physics")
            (bbp "Shadows")
            (bbp "Skinning/Skeletons")
            (bp "Audio synthesis")
            ) 0.725)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
		  
  (slide/center
   (page-para* (bt "I use fluxus for...")))
  
  (slide/center
   (scale (bitmap (build-path "images/slubfave.jpg")) 0.5)
   (page-para* (t "Live coding graphics, using live audio input")))
  
  
  (slide/center
   (scale (bitmap (build-path "images/stroke3.jpg")) 1)
   (page-para* (t "Live coding graphics and audio at the same time")))
  
  (slide/center
   (hc-append 
    gap
    (scale (bitmap (build-path "images/bbb.jpg")) 1)
    (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 1))
   (page-para* (t "Make installations and performances")))
 
  (slide/center
   (scale (bitmap (build-path "images/sonarama-toplap.jpg")) 1)
   (page-para* (t "slub")))

  (slide/center
   (hc-append 
    gap
    (scale (bitmap (build-path "images/treetris4.png")) 0.6)
    (scale (bitmap (build-path "images/5-0.png")) 0.6))
   (page-para* (t "Games prototyping"))) 
    
	  (slide/center
   (page-para* "Livecoding"))
  
  (slide/title/center
   "Livecoding"
   (hc-append 
    (vl-append
     gap
     (bp "Performance programming")
     (bp "Comes from a musical background")
     (bp "Reaction against the normal laptop performance")
     (bp "Improvisation")
     (bp "Showing the audience what you're doing")
     (bp "Thinking out loud")
     )
    (scale (bitmap (build-path "images/slub.jpg")) 1)))
  
  (slide/title/center
   "TOPLAP"
   (hc-append 
    (vl-append
     gap
     (bp "Formed Feburary 2004 in a smokey Hamburg bar")
     (bp "Now grown to 100's of livecoders")
     (bp "Role is to promote live coding as a unique art form")
     (bp "Currently planning a Uk Planetarium Livecoding Tour")
     )
    (scale (bitmap (build-path "images/toplap.png")) 1)))
  
  (slide
   (scale (vl-append
           10
           (page-para* "TOPLAP MANEFESTO")
           (para 5000 "")
           (para 5000 "We demand:")
           (lbp "Give us access to the performer's mind, to the whole human instrument.")
           (lbp "Obscurantism is dangerous. Show us your screens.")
           (lbp "Programs are instruments that can change themselves.")
           (lbp "The program is to be transcended - Artificial language is the way.")
           (lbp "Code should be seen as well as heard, underlying algorithms viewed as well as their visual outcome.")
           (lbp "Live coding is not about tools. Algorithms are thoughts. Chainsaws are tools. That's why algorithms are sometimes harder to notice than chainsaws.")
           
           (para 5000 "")
           (para 5000 "We recognise continuums of interaction and profundity, but prefer:")
           (lbp "Insight into algorithms")
           (lbp "The skillful extemporisation of algorithm as an expressive/impressive display of mental dexterity")
           (lbp "No backup (minidisc, DVD, safety net computer)")
           
           (para 5000 "")
           (para 5000 "We acknowledge that:")
           (lbp "It is not necessary for a lay audience to understand the code to appreciate it, much as it is not necessary to know how to play guitar in order to appreciate watching a guitar performance.")
           (lbp "Live coding may be accompanied by an impressive display of manual dexterity and the glorification of the typing interface.")
           (lbp "Performance involves continuums of interaction, covering perhaps the scope of controls with respect to the parameter space of the artwork, or gestural content, particularly directness of expressive detail. Whilst the traditional haptic rate timing deviations of expressivity in instrumental music are not approximated in code, why repeat the past? No doubt the writing of code and expression of thought will develop its own nuances and customs.")
           ) 0.55))
    
  (slide/title/center
   "Livecoding & Fluxus"   
   (hc-append 
    (vl-append
     gap
     (bp "Fluxus is part of the livecoding movement")
     (bp "People using it for performance ('no copy paste' from Budapest)")
     (bp "Fluxus/Supercollider Workshop at the first Livecoding festival in Sheffield")
     (bp "The movement has greatly influenced fluxus development")
     )
    (scale (bitmap (build-path "images/nocopy.png")) 1)))	
	
  (slide/center
   (page-para* "Some other livecoding systems"))
  
  (slide/title/center
   "Impromptu"   
   (scale (bitmap (build-path "images/impromptu.jpg")) 1))
  
  (slide/title/center
   "SuperCollider"   
   (scale (bitmap (build-path "images/supercollider.jpg")) 0.7))
  
  (slide/title/center
   "ChucK"   
   (scale (bitmap (build-path "images/chuck.jpg")) 1))
 
  (slide/title/center
   "Gamepad Livecoding"   
   (hc-append 
    (vl-append
     gap
     (bp "Live coding doesn't have to be about text editors")
     (bp "Live coding doesn't have to be hard")
     (bp "Making fun, simplified languages"))
    (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 1)))
 	 
  (slide/center
   (page-para* "About Scheme"))
   
    (slide/title/center
   "Scheme"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Invented in 1975 by Jerald J. Sussman and Guy L. Steel Jr.")
     (bp "A simplified dialect of Lisp")
     (bp "A \"high level\" language")
     (bp "A language for learning programming")
     (bp "Influences modern languages such as Python and C#")
     (scale 
       (para 600 "... if you are used to a C based language it can seem very strange")
      0.75) 
      
     )
    (frame (scale (code (define (factorial n)
                   (if (zero? n)
                       1
                       (* n (factorial (- n 1)))))) 0.75))))

(slide/title/center
   "Scheme is good for live coding"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Functional")
     (bp "Minimal syntax")
     (bp "Maximum complexity out of minimum code")   
     )
    (scale (bitmap (build-path "images/shadows.jpg")) 0.75)))

	
  #;(slide/title/center
 	 "Fluxus philosophy"
   (hc-append 
    (scale (vl-append
     (* gap 2)
     (bp "Grunt work in C++")
     (bbp "Fast (close to the metal)")
     (bp "'Creative' code in Scheme")
     (bbp "Few keypresses needed to get interesting results")
     (bbp "More about thinking than machines")
     ) 0.75)
    (scale (bitmap (build-path "images/city.png")) 0.5)))
	
  )
