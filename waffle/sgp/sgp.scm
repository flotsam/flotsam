#lang slideshow
(require slideshow/code)

(slide
 (t "The abuse of computer programming for Art"))

(slide
 #:title "Overview"
 (item "Looking at computer code differently")
 (item "Esoteric programming languages")
 (item "Programming as gameplay")
 (item "Gamepad livecoding"))

(slide
 (t "Looking at code differently...")
 (t "Programming can be more than a means to an end"))

(slide
 #:title "London.pl"
 (item "A Perl poem which transcribes William Blake's 19th century poem 'London' into program code.")
 (item "By Graham Harwood")
 (htl-append
  (scale (bitmap (build-path "images/Blake_London.jpg")) 0.8)
  (scale (bitmap (build-path "images/london.jpg")) 0.4)))

(slide
 (scale (bitmap (build-path "images/london2.jpg")) 0.4))

(slide
 #:title "dismap"
 (item "By Ben Fry")
 (item "Visualisations of the flow of old NES games")
 (item "Chosen for their relative simplicity")
 (scale (bitmap (build-path "images/dismap.jpg")) 1))

(slide
 (scale (bitmap (build-path "images/pacman.png")) 1))

(slide
 (t "Esoteric programming languages"))

(slide
 #:title "INTERCAL"
 (item "The first esoteric programming language")
 (item "Deliberately designed to be perverse")
 (item "Insists on polite programmers")
 (scale
  (vl-append
   (para #:align 'left "DO ,1 <- #13")
   (para #:align 'left  "PLEASE DO ,1 SUB #1 <- #234")
   (para #:align 'left  "DO ,1 SUB #2 <- #112")
   (para #:align 'left  "DO ,1 SUB #3 <- #112")
   (para #:align 'left  "DO ,1 SUB #4 <- #0")
   (para #:align 'left  "DO ,1 SUB #5 <- #64")
   (para #:align 'left  "DO ,1 SUB #6 <- #194")
   (para #:align 'left  "DO ,1 SUB #7 <- #48")
   (para #:align 'left  "PLEASE DO ,1 SUB #8 <- #22")
   (para #:align 'left  "DO ,1 SUB #9 <- #248")
   (para #:align 'left  "DO ,1 SUB #10 <- #168")
   (para #:align 'left  "DO ,1 SUB #11 <- #24")
   (para #:align 'left  "DO ,1 SUB #12 <- #16")
   (para #:align 'left  "DO ,1 SUB #13 <- #214")
   (para #:align 'left  "PLEASE READ OUT ,1")
   (para #:align 'left  "PLEASE GIVE UP")) 0.5))

(slide
 #:title "Opus-2 "
 (item "By Chris Pressey")
 (item "An abstract artlang")
 (scale (bitmap (build-path "images/white.jpg")) 0.5)
 (scale
  (hc-append
   (scale (bitmap (build-path "images/white.jpg")) 0.5)
   (vl-append
    (it "pale green")
    (it "Eb, trombone, forte")
    (it "leaning 40 degrees left (sudden)")
    (it "C, tubular bells, piano")
    (it "mothballs (gentle whiff)"))) 1))

(slide
 #:title "Xigxag" 
 (para #:align 'left "><<")
 (para #:align 'left "<<>><")
 (para #:align 'left "<><<<<>>")
 (para #:align 'left "<<<<>><><><<><<<><<<>"))

(slide
 #:title "Piet"
 (item "By David Morgan-Mar")
 (item "Programming with colour")
 (item "Designed to look like abstract paintings")
 (htl-append 
  (vc-append 
   (bitmap (build-path "images/Piet_hello_big.png"))
   (t "hello world"))
  (bitmap (build-path "images/white.jpg"))
  (vc-append 
   (bitmap (build-path "images/hw2-11.gif"))
   (t "pretty hello world"))
  (bitmap (build-path "images/white.jpg"))
  (vc-append 
   (bitmap (build-path "images/piet-primetest.png"))
   (t "prime numbers"))))


(slide
 (t "Programming as gameplay"))

(slide
 #:title "Carnage Heart"
 (item "By ArtDink")
 (item "Released in Japan in 1995")
 (item "Playstation")
 (bitmap (build-path "images/CarnageHeart.png")))

(slide
 #:title "Marionette Handler"
 (item "By MicroNet")
 (item "Released in 1999 for the Dreamcast") 
 (bitmap (build-path "images/marionette.jpg")))

(slide
 (bitmap (build-path "images/marionette2.jpg")))

(slide
 (t "Gamepad Livecoding")
 (bitmap (build-path "images/bbb.jpg")))

(slide
 #:title "Gamepad Livecoding"   
 (item "Make watching live coding a bit more accessible")
 (item "Live coding doesn't have to be hard")
 (item "Making fun, simplified languages")
 (scale 
  (htl-append
   (bitmap (build-path "images/aljazari-dev16.jpg"))
   (scale (bitmap (build-path "images/d2.png")) 0.5)) 0.75))

