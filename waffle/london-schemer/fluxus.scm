(module fluxus (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (slide/center
   (page-para* "Fluxus: Scheme Livecoding") 
   (page-para* (text "Dave Griffiths" main-font 24)))
  

 (slide/title/center
   "Overview"
   (hc-append 
    (vl-append
     gap
     (bp "Introduction")
     (bp "Technical bits")
     (bp "Fluxus philosophy")
     (bp "Livecoding")
     (bp "Some code")
     (bp "Functional Reactive Programming")
     (bp "Gamepad programming")

   )
  (scale (bitmap (build-path "images/gamepad1.jpg")) 0.8)))

  (slide/title/center
   "About me"
   (hc-append 
    (vl-append
     gap
     (bp "Free Software developer")
     (bp "Part of slub - a livecoding band")
     (bp "'Software Artist'")
     (bp "Job - film effects, computer games")
     (bp "Education: computer animation"))))
 
  (slide/title/center
   "What is fluxus?"
   (hc-append 
    (vl-append
     gap

 (bp "Game engine at heart")
 (bp "Framework for various things:")
  (bbp "Playing/learning")
  (bbp "Workshops")
  (bbp "Performances")
  (bbp "Installations")
 (bp "Livecoding editor")
 (bp "Uses PLT Scheme")
 (bp "GPL")
 (bp "Linux and sometimes OSX"))))
  
  (slide/center
   (page-para* (bt "DEMO!")))
  
  (slide/center
   (page-para* (bt "I use fluxus for...")))

  (slide/center
   (scale (bitmap (build-path "images/slubfave.jpg")) 0.5)
   (page-para* (t "Live coding graphics, using live audio input")))

 
  (slide/center
   (scale (bitmap (build-path "images/stroke3.jpg")) 1)
   (page-para* (t "Live coding graphics and audio at the same time")))
 
  (slide/center
   (hc-append 
    10
    (scale (bitmap (build-path "images/bbb.jpg")) 1)
    (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 1))
   (page-para* (t "As a framework for developing new livecoding languages")))
   
  (slide/center
   (scale (bitmap (build-path "images/sonarama-toplap.jpg")) 1)
   (page-para* (t "slub")))


  (slide/center
   (page-para* (bt "Technical Details")))
  

(slide/title/center
   "Boring Feature List"
   (hc-append 
    (scale (vl-append
     10
     (bp "Immediate mode drawing")
     (bp "Scenegraph")
     (bp "Primitives")
     (bbp "Polys")
     (bbp "Particles")
     (bbp "NURBS patches")
     (bbp "Blobbies (implicit surfaces)")
     (bbp "Pixels (procedural texture access)")
     (bp "Unified access to primitive data (vertex arrays, texture data)")
     (bp "More advanced stuff")
     (bbp "GLSL Hardware shading")
     (bbp "ODE physics")
     (bbp "Shadows")
     (bbp "Skinning/Skeletons")
     (bp "Audio synthesis")
      ) 0.75)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
  
   (slide/title/center
   "Architecture"
   (hc-append 
    (scale (vl-append
     10
     (bp "Scratchpad")
	 (bbp "Text editor in an OpenGL window")
	 (bbp "Livecoding application")
     (bbp "mzscheme interpreter")
     (bp "fluxus-engine")
	 (bbp "mzscheme extension")
	 (bp "libfluxus")
	 (bp "OpenGL")
      ) 0.75)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
 
   (slide/title/center
   "DrScheme integration"
   (hc-append 
    (scale (vl-append
     10
     (bp "DrScheme")
     (bp "fluxus-engine")
	 (bbp "mzscheme extensions")
	 (bp "libfluxus")
	 (bp "OpenGL")
      ) 0.75)
    (scale (bitmap (build-path "images/drscheme.png")) 0.4)))

   (slide/title/center
   "DrScheme integration"
   (hc-append 
    (scale (vl-append
     10
     (bp "Much better editor")
     (bp "Debugger")
     (bp "Profiler")
     (bp "Syntax highlighting")
     (bp "Less control over OpenGL")
     (bbp "No GLSL shader support")
     (bbp "No stencilmap shadows")
     (bbp "Slower")) 0.75)
    (scale (bitmap (build-path "images/drscheme.png")) 0.4)))

   (slide/title/center
   "Philosophy"
   (hc-append 
    (scale (vl-append
     10
     (bp "Creative code in Scheme")
	 (bp "OpenGL grunt work in C++")
	 (bp "Expose enough detail to Scheme")
	 (bbp "Nice blocks to build with")
	 (bbp "But not too many (not every OpenGL call)")
      ) 0.75)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
	
   (slide/title/center
   "Creative code - Scheme"
   (hc-append 
    (scale (vl-append
     10
     (bp "Scheme is good for exploratory programming")
     (bp "Few keypresses needed to get interesting results")
     (bbp "Functional roots")
     (bbp "Lack of syntax")
     (bbp "Use of macros to make shape the languages")
	 (bp "Lots of research going on with Scheme")
	) 0.75)
    (scale (bitmap (build-path "images/drscheme.png")) 0.4)))
	
   (slide/title/center
   "Livecoding"
   (hc-append 
    (scale (vl-append
     10
     (bp "")
     (bp "Few keypresses needed to get interesting results")
     (bbp "Lack of syntax")
     (bbp "Use of macros to make shape the languages")
	 (bp "Lots of research going on")
     (bbp "Functional reactive programming")
	) 0.75)
    (scale (bitmap (build-path "images/drscheme.png")) 0.4)))

  
   )
