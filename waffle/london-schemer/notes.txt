
demos w livecoding

1. tree.scm - animate

2. scenegraph : car.scm - rotate
   pdata : particles.scm - change to polys?
   collide-seq.scm (need fluxa running)

3.   
   
4. aljazari.scm

-----

blurb

Fluxus: livecoding and making scheme do graphics

Fluxus is a rapid prototyping, livecoding and playing/learning environment for 3D
graphics and games. Extends PLT Scheme with graphical commands and can be used
within it's own livecoding environment or from within the DrScheme IDE. Builds for
Linux (and sometimes OSX), and released under the GPL licence.

Bio:

Dave has been writing programs to make noises, pictures and animations using a
variety of languages for many years. He is the author of lots of free software
projects exploring these areas, and uses much of it in performances and workshops
around europe. He is part of the Openlab free software artists 
collective, the TOPLAP live coding community and slub, the live coding band. He
lives in London where he makes film effects software and computer games.


--------------

schemers talk

me
--

* free software developer
* part of slub - a livecoding band
* 'software artist'
* work for large games company
* computer graphics

fluxus intro
------------

* game engine at heart
* framework for various things:
   - playing/learning
   - workshops
   - performances
   - installations
* livecoding editor
* uses plt scheme
* gpl
* linux and sometimes OSX

what I use it for
-----------------

...

graphics engine
---------------

* scenegraph
* 3D physics engine (ode)
* hardware shaders
* skinning
* shadows
* polys, NURBS, implicit surfaces, particles
* procedural texturing

architecture
------------

scratchpad [text editor in opengl window - application]
mzscheme [scheme interpreter]
fluxus-engine [plt extension]
libfluxus [graphics engine]
OpenGL 

drscheme integration
--------------------

got bored with writing text editors...

drscheme [scheme IDE]
mred [opengl canvas]
fluxus-engine [plt extension]

much better editor
debugging
profiling
syntax highlighting
...
less control over OpenGL - so lack 
   - stencil buffer shadows
   - GLSL shader support
   - bit slower

philosophy
----------

scheme is good for creative, exploritory programming
  few keypresses needed to get interesting results with scheme
    - lack of syntax
    - use macros to make domain specific languages
  a lot of research going on with scheme

C++ is fast and good for 'grunt work'

the trick is to expose enough detail, and not too much 
   (eg binding each opengl function)

livecoding
----------

comes from computer music
...

other livecoding languages
--------------------------

supercollider (smalltalk)
chuck (domain specific)
impromtu (scheme)

toplap manefesto
----------------

...

livecoding fluxus
-----------------

The original use for fluxus was to livecode visuals to music.
Run fft on incoming sound to convert it to frequency bands
Plug into animation parameters
...

[TREE ANIMATE DEMO TO VOICE]

scene description
-----------------

scene description:

(translate (vector 0 1 0))
(scale (vector 0.1 0.1 2))
(rotate (vector 45 0 0))
(colour (vector 1 0 0))
(draw-cube)

[mutable state machine by design - see later]

(colour car-col)
(translate car-pos)
(with-state
  (colour wheel-col)
  (with-state
    (translate (vector 1 -0.5 1))
    (draw-cylinder))
  (with-state
    (translate (vector -1 -0.5 1))
    (draw-cylinder))
  (with-state
    (translate (vector -1 -0.5 -1))
    (draw-cylinder))
  (with-state
    (translate (vector 1 -0.5 -1))
    (draw-cylinder)))

primitive data mapping
----------------------

primitives can also be stored in a scenegraph, and manipulated

(with-primitive myparticles
  (pdata-map! vadd "p" "vel"))

adds velocity to all the positions for a particle system

(with-primitive mymesh
  (pdata-map! 
    (lambda (n)
      (vector (- (vector-ref n 0))
              (- (vector-ref n 1))
              (- (vector-ref n 2))))
    "n"))

inverts the normals on a mesh

[DEMO OF PARTICLES OR SOMETHING]

mutation
--------

this is all great - but something was wrong
* lots of mutable state
* feels like there should be another option

functional reactive programming
-------------------------------

...

[FRISBEE DEMO]

Back to livecoding
------------------

normal gamepad livecoding talk...
betablocker
al-jazari












what I use it for
-----------------

livecoding to audio (for vjing)
livecoding audio and visual at the same time
for making livecoding specific playgrounds
    betablocker
    aljazari
    daisychain

system
------

------------------------------------------
fluxus scratchpad    |     drscheme
scratchpad.ss        |     drflux.ss
==========================================
fluxus-engine | fluxus-audio | fluxus-osc
libfluxus     | libfftw      | liblo
OpenGL        | jackd        
------------------------------------------

low level is C++, high level is scheme

why scheme
----------

difference between 'creative code' and 'grunt work' code
scheme is good for exploritory code


demo al jazari
--------------



