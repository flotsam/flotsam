(module fluxus (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (slide/center
   (page-para* (t "fluxus & Scheme")) 
   (page-para* (text "Dave Griffiths" main-font 24)))
  

 (slide/title/center
   "Fluxus facts"
   (hc-append 
    (vl-append
     gap
     (bp "A 3D rendering engine")
     (bp "Livecoding interpreter")
     (bp "Uses mzscheme and OpenGL")
     (bp "Free Software")
     (bp "Works on Linux and sometimes OSX")
     (bp "Version 0.1 released Tuesday, August 5th 2003 17:29 (courtesy of freshmeat)")
     (bp "Current stable version 0.12")
      
   )
  (scale (bitmap (build-path "images/paintbomb1.png")) 0.5)))

  (slide/title/center
   "Boring Feature List"
   (hc-append 
    (scale (vl-append
     10
     (bp "Immediate mode drawing")
     (bp "Scenegraph")
     (bp "Primitives")
     (bbp "Polys")
     (bbp "Particles")
     (bbp "NURBS patches")
     (bbp "Blobbies (implicit surfaces)")
     (bbp "Pixels (procedural texture access)")
     (bp "Unified access to primitive data (vertex arrays, texture data)")
     (bp "More advanced stuff")
     (bbp "GLSL Hardware shading")
     (bbp "ODE physics")
     (bbp "Shadows")
     (bbp "Skinning/Skeletons")
      ) 0.75)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
  
  (slide/center
   (page-para* (bt "I use fluxus for...")))

  (slide/center
   (scale (bitmap (build-path "images/slubfave.jpg")) 0.5)
   (page-para* (t "Live coding graphics, using live audio input")))
 
  (slide/center
   (scale (bitmap (build-path "images/stroke3.jpg")) 1)
   (page-para* (t "Live coding graphics and audio at the same time")))
 
  (slide/center
   (hc-append 
    10
    (scale (bitmap (build-path "images/bbb.jpg")) 1)
    (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 1))
   (page-para* (t "As a framework for developing new livecoding languages")))


  

 (slide/title/center
   "Scheme"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Invented in 1975 by Jerald J. Sussman and Guy L. Steel Jr.")
     (bp "A simplified dialect of Lisp")
     (bp "A \"high level\" language")
     (bp "A language for learning programming")
     (bp "Influences modern languages such as Python and C#")
     (scale 
       (para 600 "... if you are used to a C based language it can seem very strange")
      0.75) 
      
     )
    (frame (scale (code (define (factorial n)
                   (if (zero? n)
                       1
                       (* n (factorial (- n 1)))))) 0.75))))

(slide/title/center
   "Scheme is good for live coding"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Minimal syntax")
     (bp "Functional")
     (bp "Minimum code, maximum complexity")   
      
     )
    (scale (bitmap (build-path "images/shadows.jpg")) 0.75)))
	
(slide/title/center
   "but can take a bit of getting used to..."
    (scale (code
(define !
  (lambda (n)
    ((lambda (n)
       ((n (lambda (x) (+ x 1))) 0))
     ((lambda (n)
        ((lambda (p)
           (p (lambda (x)
                (lambda (y)
                  y))))
         ((n (lambda (p)
               (((lambda (x)
                   (lambda (y)
                     (lambda (fun)
                       ((fun x) y))))
                 ((lambda (n)
                    (lambda (f)
                      (lambda (x)
                        (f ((n f) x)))))
                  ((lambda (p)
                     (p (lambda (x)
                          (lambda (y)
                            x)))) p)))
                (((lambda (x)
                    (lambda (y)
                      ((y ((lambda (x)
                             (lambda (y)
                               ((y (lambda (n)
                                     (lambda (f)
                                       (lambda (x)
                                         (f ((n f) x)))))) x))) x))
                       (lambda (f)
                         (lambda (x)
                           x)))))
                  ((lambda (p)
                     (p (lambda (x)
                          (lambda (y)
                            x)))) p))
                 ((lambda (p)
                    (p (lambda (x)
                         (lambda (y)
                           y)))) p)))))
          (((lambda (x)
              (lambda (y)
                (lambda (fun)
                  ((fun x) y))))
            ((lambda (n)
               (lambda (f)
                 (lambda (x)
                   (f ((n f) x)))))
             (lambda (f)
               (lambda (x)
                 x))))
           ((lambda (n)
              (lambda (f)
                (lambda (x)
                  (f ((n f) x)))))
            (lambda (f)
              (lambda (x)
                x)))))))
      (((lambda (i-n)
          (lambda (n)
            (i-n i-n n)))
        (lambda (loop n)
          (if (zero? n)
              (lambda (f)
                (lambda (x)
                  x))
              ((lambda (n)
                 (lambda (f)
                   (lambda (x)
                     (f ((n f) x)))))
               (loop loop (- n 1)))))) n))))) ) 0.4))


(slide/title/center
   "Recursion"
    (scale (bitmap (build-path "images/fish_and_scales.gif")) 1)
 	(scale (t "\"Fishes and Scales\" M.C. Escher") 0.5)
	(bp "Recursion is the only way to loop")
	(bp "Elegant but hard to explain"))

   
   )

