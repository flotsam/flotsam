
(clear)

(define (setup n)
    (define (loop n angle)
        (pdata-set "p" n (vadd (vector 1.5 1.5 1.5) 
            (vector (sin (* angle n)) (cos (* angle n)) 0)))
        (pdata-set "c" n (vmul (vector (flxrnd) (flxrnd) (flxrnd)) 0.1))
        (pdata-set "s" n (* 0.04 (+ 1 n)))
        (if (zero? n)
            0
            (loop (- n 1) angle)))
    (loop n (/ 6.6 n)))
    

(define (animate n)
    (pdata-set "p" n (vadd (pdata-get "p" n) 
            (vector 0 0 (* (sin (+ n (time))) 0.03))))
    (if (zero? n)
        0
        (animate (- n 1))))

    
(hint-vertcols)
(shinyness 50)
(specular (vector 0.5 0.5 0.5))
(define a (build-blobby 7 (vector 20 20 20) (vector 3 3 3)))

(grab a)
(setup (pdata-size))
(ungrab)

(define (render)
    (grab a)
    (animate (pdata-size))
    (ungrab))

(every-frame (render))