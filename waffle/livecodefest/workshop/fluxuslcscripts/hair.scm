(clear)

; clear the pixel primitive
(define (clear-pixels n)
    (pdata-set "c" n (vector 0 0 0 0))
    (if (zero? n)
        0
        (clear-pixels (- n 1))))

; set a random pixel to a random colour
(define (spray n)
    (pdata-set "c" (random (pdata-size)) 
        (vector (flxrnd) (flxrnd) (flxrnd)))
    (if (zero? n)
        0
        (spray (- n 1))))

; make a shell
(define (expand n dst dist)
    (let ((v (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist))))
    (grab dst)
    (pdata-set "p" n v)
    (ungrab))
    (if (zero? n)
        0
        (expand (- n 1) dst dist)))

; make a load of shells
(define (shell n dist incr)
    (expand (pdata-size) (build-cube) dist)
    (if (zero? n)
        0
        (shell (- n 1) (+ dist incr) incr)))

; make the pixels prim (100X100 pixels)
(define p (build-pixels 100 100))
(grab p)
(clear-pixels (pdata-size))
(spray 2000)
(upload-pixels) ; upload to graphics card ready to use as a texture
(ungrab)

(scale (vector 3 3 3))
(define ob (build-cube))

(push)
(texture (pixels->texture p)) ; set the pixel prim texture
(hint-unlit)
(grab ob)
(shell 30 0 0.003) ; make the shells
(ungrab)
(pop)

