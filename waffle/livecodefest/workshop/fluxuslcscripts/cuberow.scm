(define (row n)
    (translate (vector 2 0 0))
    (draw-cube)
    (if (zero? n)
        0
        (row (- n 1))))


(every-frame (row 10))