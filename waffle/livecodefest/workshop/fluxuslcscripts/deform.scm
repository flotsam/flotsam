
; returns a random vector with each element between -0.5 and 0.5
(define (rndvec)
    (vsub (vector (flxrnd) (flxrnd) (flxrnd)) (vector 0.5 0.5 0.5)))

; deform the vertex positions
(define (deform n)
    ; add a small random vector to p
    (pdata-set "p" n (vadd (pdata-get "p" n) (vmul (rndvec) 0.01)))
    (if (zero? n)
        0
        (deform (- n 1))))


(clear)
; make a sphere
(define s (build-sphere 10 10))

(define (animate)
    (grab s)
    (deform (pdata-size))
    ; recalculate lighting normals
    (recalc-normals 0)
    (ungrab))

(every-frame (animate))