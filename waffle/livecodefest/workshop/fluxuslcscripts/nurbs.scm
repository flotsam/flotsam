
; deform the contol vertex positions
(define (deform n)
    ; get the distance of the vert to the centre, and add time
    (let ((v (+ (time) (* 10 (vmag (pdata-get "pref" n))))))

    (pdata-set "p" n (vadd (pdata-get "pref" n)
        (vector 0 (* 0.1 (+ (sin v) (cos v))) 0))))

    (if (zero? n)
        0
        (deform (- n 1))))


(clear)
(push)
; set some surface parameters
(specular (vector 1 1 1))
(shinyness 20)
(colour (vector 0.5 0.5 0.5))
(hint-points) ; show the control vertices
(scale (vector 2 2 2))
; move so we are centred around the origin
(translate (vector -0.5 0 -0.5)) 
; make a nurbs sphere
(define s (build-nurbs-plane 20 20))
(pop)
(apply-transform s)

(grab s)
(pdata-copy "p" "pref")
(ungrab)

(define (animate)
    (grab s)
    (deform (pdata-size))
    ; recalculate the lighting normals
    (recalc-normals 1) 
    (ungrab))

(every-frame (animate))