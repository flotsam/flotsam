; hide the code with ctrl-h
; then use keys w a s d to control the player

; return a random vector
(define (rndvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) (vector 0.5 0.5 0.5)))

; make some random blocks
(define (make-terrain n)
    (push)
    (texture (load-texture "peel.png"))
    (rotate (vector 0 (* 360 (flxrnd)) 0))
    (translate (vector (* 100 (- (flxrnd) 0.5)) 0 (* 100 (- (flxrnd) 0.5))))
    (scale (vector (* 10 (flxrnd)) (* 10 (flxrnd)) (* 10 (flxrnd))))
    (build-cube)
    (pop)
    (if (zero? n)
        0
        (make-terrain (- n 1))))

; update the player
(define (player-update)
    (if (key-pressed "a")
        (set! player-angle (- player-angle 1)))
    (if (key-pressed "d")
        (set! player-angle (+ player-angle 1)))
    (if (key-pressed "w")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 -0.1) tx)))))
    (if (key-pressed "s")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 0.1) tx)))))

    (grab player)
    (identity)
    (translate player-pos)
    (rotate (vector 0 player-angle 0))
    (ungrab))

(clear)
(clear-colour (vector 0.5 0.5 0.6))
(fog (vector 0.5 0.5 0.6) 0.03 1 100)

; turn off the camera light
(light-diffuse 0 (vector 0 0 0))

; make some new static lights
(define light (make-light "free" "point"))
(light-diffuse light (vector 1 1 1))
(light-position light (vector 100 100 10))

(define light2 (make-light "free" "point"))
(light-diffuse light2 (vector 0.6 0.7 1))
(light-position light2 (vector -100 100 -5))

; make the terrain
(make-terrain 100)

; make the floor
(push)
(rotate (vector 90 0 0))
(scale (vector 100 100 100))
(hint-unlit)
(texture (load-texture "peel.png"))
(define floor (build-plane))
(pop)

; make the player
(push)
(translate (vector 0 0.5 0))
(define player (build-cube))
(define player-pos (vector 0 0 0))
(define player-angle 0)
(pop)
    
; lock the camera to the player
(lock-camera player)
; and setup the camera transform (turns off the mouse camera)
(set-camera-transform (mtranslate (vector 0 -2 -1)))

; go!
(define (animate)
    (player-update))

(every-frame (animate)) 