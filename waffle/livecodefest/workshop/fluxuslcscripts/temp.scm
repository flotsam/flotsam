(define (render n)

    (translate (vector 0 2 0))
    (draw-cube)

    (cond 
        ((zero? n)
            0)
        (else

            (push)
            (rotate (vector 45 0 0))
            (render (- n 1))
            (pop)

            (push)
            (rotate (vector -45 0 0))
            (render (- n 1))
            (pop)

            )))

(every-frame (render 5))