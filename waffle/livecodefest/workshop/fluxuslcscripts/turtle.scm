; use the turtle to deform the polygons
(define (splat n)
    (turtle-move (+ 1 (sin (* n 10))))
    (turtle-turn (vector (* 10 (sin (* 0.1 (time)))) 
                         (cos (* 0.3 (time))) 10))
    (turtle-vert)
    (if (zero? n)
        0
        (splat (- n 1))))


(clear)
(backfacecull 0)
(hint-wire)
(wire-colour (vector 1 1 1))
(line-width 2)

; make some raw polygons
(define shape (build-polygons 200 0))
; attach the turtle 
(turtle-attach shape)

(define (animate)
    (turtle-reset)
    (grab shape)
    (splat (pdata-size))
    (ungrab))

(blur 0.1)

(every-frame (animate))