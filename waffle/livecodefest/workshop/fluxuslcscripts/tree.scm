(define (render n)

    (translate (vector 0 2 0))
    (push)
    (scale (vector 0.2 2 0.2))
    (draw-cube)
    (pop)

    (cond 
        ((zero? n)
            0)
        (else

            (push)
            (rotate (vector 25 
                (* 45 (sin (* (time) 0.1))) (gh n)))
            (render (- n 1))
            (pop)

            (push)
            (rotate (vector -25 
                (* 45 (sin (time))) 0))
            (render (- n 1))
            (pop)

            )))

(every-frame (render 10))