(module losslivecode (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (slide/center
   (page-para* "Programming with a gamepad") 
   (page-para* (text "Dave Griffiths" main-font 24)))
  

 (slide/title/center
   "Overview"
   (hc-append 
    (vl-append
     gap
     (bp "2 music live coding environments")
     (bp "Common stuff they share")
     (bbp "Gamepads")
     (bbp "Ringmenus")
     (bbp "Uncrashable languages")
     (bp "Betablocker")
     (bbp "Demo")
     (bp "Al-Jazari")
     (bbp "Demo")
     (bp "Thoughts")
   )
  (scale (bitmap (build-path "images/gamepad1.jpg")) 0.8)))

  (slide/center
   (page-para* (bt "Common stuff")))
  
  (slide/title/center
   "Vague themes"
   (hc-append 
    (vl-append
     gap
     (bp "Make watching live coding a bit more accessible")
     (bp "Live coding doesn't have to be about text editors")
     (bp "Live coding doesn't have to be hard"))
    (vl-append 10 (scale (bitmap (build-path "images/bbb.jpg")) 0.8)
               (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 0.8))))
      
  (slide/title/center
   "Gamepads"
   (hc-append 
    (vl-append
     gap
     (bp "Analogue sticks")
     (bp "Directional pad")
     (bp "Right hand buttons")
     (bp "Shoulder buttons")
     (bp "Force feedback")
     (bp "Analogue buttons"))
      (scale (bitmap (build-path "images/p2600.jpg")) 1)))
  
  (slide/center
   (page-para* (bt "Ring Menus")))

  (slide/title
  "Ring menus"
   (scale (bitmap (build-path "images/ringmenu-1.png")) 1)
   (page-para* "Values from analogue stick"))
 
  (slide/title
  "Ring menus"
   (scale (bitmap (build-path "images/ringmenu-2.png")) 1)
   (page-para* "Convert to vector"))
  
  (slide/title
   "Ring menus"
   (scale (bitmap (build-path "images/ringmenu-3.png")) 1)
   (page-para* "Use magnitude to activate menu"))
  
  (slide/title
   "Ring menus"
   (scale (bitmap (build-path "images/ringmenu-4.png")) 1)
   (page-para* "Use angle to pick from items"))
  
  (slide/title/center
   "Ring menus"
   (hc-append 
    (vl-append
     10
     (bp "Nice tactile feel")
     (bp "Good use of muscle memory")
     (bp "You can pick from quite a large range")
     (bp "Seems easier than mouse based \'gestures\'"))

    (scale (bitmap (build-path "images/aj-ring.jpg")) 1)))

  (slide/center
   (page-para* (bt "Uncrashable Languages")))
  
  (slide/title/center
   "Uncrashable Languages"
   (hc-append 
    (vl-append
     10
     (bp "Design away crashes")
     (bbp "Divide by 0 returns 0")
     (bbp "Invalid memory read returns 0")
     (bp "Genetic programming: Tierra")
     (bp "Not good for general purposes")
     (bbp "Crashes stop unintended code from executing")
     (bbp "Removing them makes bugs very hard to find"))
    (scale (bitmap (build-path "images/bsod.png")) 1)))
 
  (slide/title/center
   "Uncrashable Livecoding"
   (hc-append 
    (vl-append
     10
     (bp "Good for live coding:")
     (bbp "Less chance of embarrassment!")
     (bbp "Things can run out of control")
     (bbp "Changes how you program")
     (bbp "Malleable/brittle")
     (bp "All possible programs are executable"))
    (scale (bitmap (build-path "images/ravsav.jpg")) 0.5)))
  
  (slide/center
   (page-para* (bt "Betablocker")))

  (slide/title/center
   "Betablocker influences"   
   (hc-append 
    (vl-append
     gap
     (bp "TOPLAP discussion on virtual machines (\"Hello (& Chaos)\" 2004)")
     (bp "Corewars (Redcode)") 
     (bp "Forth")
     (bp "Mr Driller"))
    
    (scale (bitmap (build-path "images/r_driller01.jpg")) 1)))

  
  (slide/title/center
   "Betablocker"   
   (hc-append 
    (vl-append
     gap
     (bp "First attempt at gamepad coding")
     (bp "Virtual machine")
     (bp "Multithreaded")
     (bp "No memory protection")
     (bp "All memory is visible")
     (bp "The rhythm is a direct result of the instruction count")
	 (bp "Very unpredictable"))
    (scale (bitmap (build-path "images/bb.jpg")) 1)))

  (slide/center
   (page-para* (bt "Demo")))
 
  (slide/center
   (page-para* (bt "Al-Jazari")))
  
    (slide/title/center
   "Al-Jazari"   
   (hc-append 
    (vl-append
     gap
     (bp "13th century scientist and inventor")
     (bbp "First recorded use of gears")
     (bbp "Crankshaft")
     (bbp "Mechanical clock")
     (bbp "Combination lock")
     (bp "Programmable humanoid robot")
     (bbp "Robot musicians for royal drinking parties"))
    (scale (bitmap (build-path "images/Al-jazari_elephant_clock.png")) 1)))

  (slide/center
   (page-para* (bt "Al-Jazari"))
   (page-para* (t "(live coding for royal drinking parties)")))
 
  (slide/title/center
   "Al-Jazari influences"   
   (hc-append 
    (vl-append
     gap
     (bp "Influences")
     (bbp "The Sims")
     (bbp "Gullibloon's Army of Darkness")
     (bbp "Simon Yuill's Spring Alpha (live coding game)")
     (bp "Musical robot livecoding")
     (bp "Code in \'thought bubbles\'")
     (bp "Indirect musical triggers")
     (bp "No text"))
    (vl-append
     gap
     (scale (bitmap (build-path "images/TheSims.jpg")) 1)
     (scale (bitmap (build-path "images/army.jpg")) 0.5))))
 
 (slide/center
   (page-para* (bt "Demo")))
	 
  (slide/title/center
   "Thoughts"   
   (hc-append 
    (vl-append
     gap
     (bp "Simple languages")
     (bbp "No abstraction, so hard/impossible to build layers")
     (bbp "Fun to play with")
     (bbp "More practice will influence the languages")
     (bp "Using a gamepad")
     (bbp "More restricted than a keyboard")
     (bbp "People don't think you are programming")
     (bbp "People want to have a go")
     (bbp "Detaches you from the laptop"))
  (scale (bitmap (build-path "images/gamepad1.jpg")) 0.8)))
   
 
  
   )
