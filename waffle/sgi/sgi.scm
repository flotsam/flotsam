(module sgi (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))

  (define (nbp text)
     (para width text))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  (define (lbp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para (* width 3) text)))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  
  (slide/center
   (page-para* "Games, Art and Livecoding") 
   (page-para* (text "Dave Griffiths" main-font 24)))

  (slide/title/center
   "Overview"
   (hc-append 
    (vl-append
     gap
     (bp "My background")
     (bp "Livecoding")
     (bp "Fluxus, a livecode-able game engine")
	 (bp "Some current projects")
     (bbp "Lirec")
     (bbp "Groworld")
	 (bbp "Al Jazari installation")
     )))
  
  (slide/title/center
   "My background"
   (hc-append 
    10
    (scale (bitmap (build-path "../propaganda/evolva1.jpg")) 1.2)
    (scale (bitmap (build-path "../propaganda/thething1.jpg")) 1.2))
  (scale (bitmap (build-path "../propaganda/EyeToy.jpg")) 1)
  (page-para* (scale (t "Computer games - Evolva (PC), The Thing (PC/XBox/PS2), EyeToy (PS3)") 0.75)))

  (slide/title/center
   "My background"
   (vc-append 
    10
    (scale (bitmap (build-path "../propaganda/troy1.jpg")) 1)
    (scale (bitmap (build-path "../propaganda/kingdom1.jpg")) 1))
  (page-para* (t "Film (crowds) - Troy and Kingdom of Heaven")))

 (slide/center
   (page-para* "Livecoding & fluxus"))
      
  (slide/title/center
   "Livecoding"
   (hc-append 
    (vl-append
     gap
	 (scale (bitmap (build-path "../propaganda/sonarama-toplap.jpg")) 1)
     (bp "Programming in front of an audience")
     (bp "Project your screens")
     (bp "Comes from the computer music scene")
	 (bp "Promoted by TOPLAP (www.toplap.org)")
     (bp "Visualisation of a thought process")
     )))  
  
 (slide/title/center
   "Fluxus"
   (hc-append 
    (vl-append
     gap
     (bp "My contribution to livecoding")
     (bp "Livecoding system/3D graphics engine for art")
     (bp "Free Software")
     (bp "Scheme programming language")
     (bp "Originally for livecoded VJ performances")
     (bp "Demo...")
     )
	 (scale (bitmap (build-path "../propaganda/slubfave06c.jpg")) 0.35)))  

(slide/title/center
   "Livecoding"
   (hc-append 
    (vl-append
     gap
     (bp "The livecoding focus leads to interesting places...")
     (bp "- Rapid prototyping")
     (bp "- Teaching")
     (bp "Want to explore these areas further")
	 (bp "Different direction to normal games engines")
     )
	 (scale (bitmap (build-path "../propaganda/stroke3.jpg")) 0.6)))
	 
(slide/title/center
   "Realtime Animation Workshops"
   (hc-append 
    (vl-append
     gap
     (bp "Teach programming and 3D graphics")
	 (bp "Fit it in to the long history of abstract animation (rather than games)")
     (bp "Adults/students/teenagers, artists and first time programmers")
	 (bp "Looking for more places to do workshops")
     )(scale (bitmap (build-path "../propaganda/loss5.jpeg")) 0.6)))
 
  (slide/center
   (page-para* "Some current projects"))
 
(slide/title/center
   "Foam"
   (vc-append
    (scale (bitmap (build-path "../propaganda/foam2.jpg")) 0.5)
   (hc-append
    (scale (bitmap (build-path "../propaganda/foam3.jpg")) 0.35)
	(scale (bitmap (build-path "../propaganda/foam4.jpg")) 0.35))

    (vl-append
     gap
     (bp "'grow your own worlds' http://fo.am")
	 (bp "Based in Brussels")
	 (bp "Working with them on 2 projects")
     )))
	 
(slide/title/center
   "Lirec"
   (hc-append 
    (vl-append
     gap
     (bp "Long term artificial companions")
     (bp "Multiple platforms")
	 (bbp "Graphical system (PC)")
	 (bbp "Hand held devices (PDA/Phone)")
	 (bbp "Mobile or static robots")
	 (bp "EC funded project")
	 (bp "Research partners:")
	 (scale (vl-append
	 (nbp "Queen Mary's")
	 (nbp "University of Hertfordshire")
	 (nbp "Heriot Watt")
	 (nbp "Swedish Institute of Computer Science")
	 (nbp "INESC-ID (portugal)")
	 (nbp "University of Banberg")
	 (nbp "Wroclaw University of Technology")
	 (nbp "Eotovo University (Hungary)")
	 (nbp "Cnotinfor (Portugal)")) 0.75))
	 (scale (bitmap (build-path "../propaganda/pleo.jpg")) 0.7)
))
	 

(slide/title/center
   "Groworld"
   (hc-append 
    (vl-append
     gap
     (bp "All about plants")
	 (bbp "Real gardens")
	 (bbp "Online multiplayer plant game")
	 (bbp "Events")
	 (bbp "Installations")
	 (bp "Prototype phase for the rest of this year")
	 (bp "Looking for more people to work with for the production phase")
     )
	 	 (scale (bitmap (build-path "../propaganda/groworld.jpg")) 0.5)
))


(slide/title/center
   "Groworld"
   (hc-append 
    (vl-append
     gap
	 (bp "Visualisation of the vegetal state")
	 (bp "Plant communication")
	 (bp "Augmented reality")
     (bp "Bring people closer to the plants around them")
	)	 	 (scale (bitmap (build-path "../propaganda/groworld2.jpg")) 0.5)
))
	 
(slide/title/center
   "Tale of Tales"
   (hc-append 
    (vl-append
     gap
	 (bp "Games as art")
	 (bp "http://tale-of-tales.com/")
	 (bp "Endless forest")
	 (bp "The Graveyard (Independent Games Festival Award finalist)")
	 (bp "The Path")
     )
	 (vl-append
	 	 (scale (bitmap (build-path "../propaganda/graveyard.jpg")) 0.75)
	 	 (scale (bitmap (build-path "../propaganda/thepath.jpg")) 0.75))
	 ))

(slide/title/center
   "Groworld game prototypes"
   (hc-append 
    (vl-append
     gap
     (bp "Use fluxus to churn out game prototypes")
     (bp "Allocating a couple of days for each prototype")
     (bp "These are very rough and ready!")
     (bp "Demo...")
     ) (scale (bitmap (build-path "../propaganda/groworld-games.png")) 0.4)))

(slide/center
   (page-para* "Something finished..."))

 (slide/title/center
   "Al-Jazari"   
   (hc-append 
    (vl-append
     gap
     (bp "13th century scientist and inventor")
     (bbp "First recorded use of gears")
     (bbp "Crankshaft")
     (bbp "Mechanical clock")
     (bbp "Combination lock")
     (bp "Programmable humanoid robot")
     (bbp "Robot musicians for royal drinking parties"))
    (scale (bitmap (build-path "../propaganda/aljazari.jpg")) 1)))
	
(slide/title/center
   "Livecoding for royal drinking parties"
   (hc-append 
    (vl-append
     gap
     (bp "Gamepad livecoding")
     (bp "Music performance 'game' written in fluxus")
     (bp "Installation commissioned by the Seville Biennale")
     (bp "The Palace of Charles V at the Alhambra, Granada, Spain")	 
     (bp "45,307 people were recorded visiting the exhibition")
	 (bp "Finish on one last demo...")
    )
	(scale (bitmap (build-path "../propaganda/granada.jpg")) 0.4)))






)













