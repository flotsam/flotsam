(module fluxus (lib "slideshow.ss" "slideshow")
  (require (lib "code.ss" "slideshow"))
  (current-title-color "black")
  (set-margin! 10)
  (define gap 20)
  (define width 500)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define (bp text)
    (htl-append
     (/ gap-size 2)
     bullet
     (para width text)))
  
  (define (bbp text)
    (inset (htl-append
            (/ gap-size 2)
            o-bullet
            (para width text))
           (* 2 gap-size) 0 0 0))
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (slide/center
   (page-para* (t "fluxus & Scheme")) 
   (page-para* (text "Dave Griffiths" main-font 24)))
  

 (slide/title/center
   "Fluxus facts"
   (hc-append 
    (vl-append
     gap
     (bp "A 3D rendering engine")
     (bp "Scheme interpreter")
     (bp "Uses mzscheme and OpenGL")
     (bp "Free Software")
     (bp "Works on Linux and sometimes OSX")
     (bp "Version 0.1 released Tuesday, August 5th 2003 17:29 (courtesy of freshmeat)")
      
   )
  (scale (bitmap (build-path "images/paintbomb1.png")) 0.5)))

  (slide/title/center
   "Boring Feature List"
   (hc-append 
    (scale (vl-append
     10
     (bp "Immediate mode drawing")
     (bp "Scenegraph")
     (bp "Primitives")
     (bbp "Polys")
     (bbp "Particles")
     (bbp "NURBS patches")
     (bbp "Blobbies (implicit surfaces)")
     (bbp "Pixels (procedural texture access)")
     (bp "Unified access to primitive data (vertex arrays, texture data)")
     (bp "More advanced stuff")
     (bbp "GLSL Hardware shading")
     (bbp "ODE physics")
     (bbp "Shadows")
     (bbp "Skinning/Skeletons")
      ) 0.75)
    (scale (bitmap (build-path "images/worm1.png")) 1)))
  
  (slide/center
   (page-para* (bt "I use fluxus for...")))

  (slide/center
   (scale (bitmap (build-path "images/slubfave.jpg")) 0.5)
   (page-para* (t "Live coding graphics, using live audio input")))
 
  (slide/center
   (scale (bitmap (build-path "images/stroke3.jpg")) 1)
   (page-para* (t "Live coding graphics and audio at the same time")))
 
  (slide/center
   (hc-append 
    10
    (scale (bitmap (build-path "images/bbb.jpg")) 1)
    (scale (bitmap (build-path "images/aljazari-dev16.jpg")) 1))
   (page-para* (t "As a framework for developing new livecoding languages")))


 (slide/title/center
   "Scheme"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Invented in 1975 by Jerald J. Sussman and Guy L. Steel Jr.")
     (bp "A simplified dialect of Lisp")
     (bp "A \"high level\" language")
     (bp "A language for learning programming")
     (bp "Influences modern languages such as Python and C#")
     (scale 
       (para 600 "... if you are used to a C based language it can seem very strange")
      0.75)
      
     )
    (frame (scale (code (define (factorial n)
                       (if (zero? n)
                           1
                           (* n (factorial (- n 1)))))) 0.75))))

(slide/title/center
   "Scheme is good for live coding"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Minimal syntax")
     (bp "Functional")
     (bp "Minimum code, maximum complexity")   
      
     )
    (scale (bitmap (build-path "images/shadows.jpg")) 0.75)))
	
(slide/title/center
   "Livecoding"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Performance programming")
     (bp "Mainly a musical field")
     (bp "Reaction against the normal laptop performance")   
     (bp "Showing the audience what you're doing")         
     )
    (scale (bitmap (build-path "images/slub.jpg")) 0.75)))
	
(slide/title/center
   "TOPLAP"
   (hc-append 
    (vl-append
     (* gap 2)
     (bp "Formed in 2003")
	 (bp "Promote Livecoding")
	 (bp "Has an Important Manefesto")        
     )
    (scale (bitmap (build-path "images/toplap.png")) 0.75)))
   
   )

