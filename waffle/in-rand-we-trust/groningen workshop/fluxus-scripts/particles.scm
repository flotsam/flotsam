; setup the initial values of colour and the velocity
(define (init n)
    (pdata-set "c" n (vector 1 1 1))
    (pdata-set "vel" n (vsub (vector (flxrnd) (flxrnd) (flxrnd))
                             (vector 0.5 0.5 0.5))) ; makes the range -0.5 -> 0.5
    (if (zero? n)
        0
        (init (- n 1))))

; add the velocity to each particle position
(define (animate n)
    (pdata-set "p" n (vadd (pdata-get "p" n) (pdata-get "vel" n)))
    (if (zero? n)
        0
        (animate (- n 1))))

(clear)

; build some particles
(push)
(hint-none)        ; turns off the poly particle rendering
(hint-points)      ; turn on point rendering
(point-width 5)    ; set the size of the points
(hint-anti-alias)  ; turns on round points
(define p (build-particles 1000))
(pop)

; set up the particles
(grab p)
(pdata-add "vel" "v")
(init (pdata-size))
(ungrab)

(define (render)
    (grab p)
    (animate (pdata-size))
    (ungrab))

(every-frame (render))