; so far we've only looked at polygonal primitives - fluxus also contains other
; forms of primitive, including particles, which are only usable via modification
; of pdata

; a procedure to setup a particle primitive for animation
(define (particles-setup particles)

    ; local procedures can be declared inside other procedures
    ; just the random vector util again
    (define (rndvec)
        (vadd (vector (flxrnd) (flxrnd) (flxrnd)) 
            (vector -0.5 -0.5 -0.5)))

    ; a procedure to loop over particles setting them up
    (define (init-particles n)
        (pdata-set "p" n (vector 0 0 0))         ; set the position to the origin
        (pdata-set "c" n (vector 1 1 1))         ; set the colour to white
        (pdata-set "vel" n (vmul (rndvec) 0.1))  ; set the velocity to random
        (if (zero? n)
            0
            (init-particles (- n 1))))

    (grab particles)               ; grab the particles for working on
    (hint-none)                    ; turn of polygonal solid mode
    (hint-points)                  ; turn on fast hw opengl points
    (point-width 5)                ; make them more visible
    (hint-anti-alias)              ; this makes them round (on most gfx cards)
    (pdata-add "vel" "v")          ; add a new user pdata vector type called vel
    (init-particles (pdata-size))  ; init them individually
    (ungrab))
    
; the procedure called every frame for animation
(define (particles-animate particles)
    (grab particles)               ; grab the particles for working on
    ; pdata-op is a special command to speed up simple operations on pdata. 
    ; all we need to do here to animate the particles is add the velocity onto the 
    ; position pdata - so we can do this with one instruction, which is much faster
    ; than looping throught them in scheme
    (pdata-op "+" "p" "vel")       ; add all of the velocities to all the positions
    (ungrab))                      

(clear)
(define myparticles (build-particles 100)) ; make a particle prim with 100 particles
(particles-setup myparticles) ; set them up

(define (render)
    (particles-animate myparticles)) ; animate them

(every-frame (render))
