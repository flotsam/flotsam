; a simple script for applying a texture to an object

(clear)

(define (render)
    (texture (load-texture "textures/peel.png")) ; load a png as a texture and set 
                                                 ; it as the current texture
    (draw-sphere))                               ; draw a sphere (bored of cubes :) )

(every-frame (render))