; this script uses recursion to draw a row of cubes
; recursion is the only form of looping in scheme, so we use it 
; everywhere for tasks like this

(clear)

(define (draw-row count)         ; make a procedure that takes an argument "count" 

    (translate (vector 1.2 0 0)) ; move a little in x
    (draw-cube)                  ; draw a cube

    (if (zero? count)            ; if the count argument is 0
        0                        ; return 0 (doesn't matter what we return) & exit
        (draw-row (- count 1)))) ; otherwise call render again with count - 1 & exit 


(every-frame (draw-row 10))