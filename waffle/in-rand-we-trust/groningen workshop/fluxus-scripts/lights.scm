(clear)

; light zero is the default camera light - set to a low level
(light-diffuse 0 (vector 0 0 0))
(light-specular 0 (vector 0 0 0))

; make a big fat key light
(define key (make-light "spot" "free"))
(light-position key (vector 5 5 0))
(light-diffuse key (vector 1 0.95 0.8))
(light-specular key (vector 0.6 0.3 0.1))
(light-spot-angle key 22)
(light-spot-exponent key 100)
(light-direction key (vector -1 -1 0))

; make a fill light
(define fill (make-light "spot" "free"))
(light-position fill (vector -7 7 12))
(light-diffuse fill (vector 0.5 0.3 0.1))
(light-specular fill (vector 0.5 0.3 0.05))
(light-spot-angle fill 12)
(light-spot-exponent fill 100)
(light-direction fill (vector 0.6 -0.6 -1))

; make a rim light
(define rim (make-light "spot" "free"))
(light-position rim (vector 0.5 7 -12))
(light-diffuse rim (vector 0 0.3 0.5))
(light-specular rim (vector 0.4 0.6 1))
(light-spot-angle rim 12)
(light-spot-exponent rim 100)
(light-direction rim (vector 0 -0.6 1))

; set a shiny material
(specular (vector 1 1 1))
(shinyness 80)

; make some spheres
(define (make-spheres n)
    (translate (vector 1 0 0))
    (rotate (vector 0 (* n 0.5) 0))
    (scale (vector 0.9 0.9 0.9))
    (build-sphere 14 14)
    (if (zero? n)
        0
        (make-spheres (- n 1))))

(push)
(translate (vector 0 0.5 -1))
(make-spheres 50)
(pop)

