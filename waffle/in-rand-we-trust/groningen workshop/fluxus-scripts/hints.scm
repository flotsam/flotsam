(clear)

(define (render)
    (push)
    
    ; hints tell fluxus to change the way it renders something. what these
    ; commands do is not enforced, and varies depending on the type of the 
    ; primitive, hence they are called hints
    (hint-normal)      
    (hint-points)
    (hint-wire)
    
    ; some functions to change the way that lines and points are drawn - all 
    ; these functions operate on the current stack
    (line-width 2)
    (point-width 10)
    (wire-colour (vector 0 0.5 1))
    
    ; some commands to set the material of the sphere
    (shinyness 10)
    (specular (vector 1 1 1))
    (colour (vector 0 0 0.5))
    
    (draw-sphere)
    (pop)

    ; draw another sphere using the default appearance
    (translate (vector 3 0 0))
    (draw-sphere))

(every-frame (render))