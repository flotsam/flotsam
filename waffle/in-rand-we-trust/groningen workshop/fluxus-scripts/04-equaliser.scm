; a cube row that reacts to sound = a graphic equaliser
; I use this to test the audio input, as it displays all 
; the harmonics using (gh)

(clear)

; initialise the audio 
(start-audio "alsa_pcm:capture_1" 2048 44100)

(define (bar c)                   ; draw one bar - given a harmonic number
    (push)                        ; push a state for this bar
    (colour (vector 1 0 (gh c)))  ; set the colour using the current harmonic 
    (scale (vector 1 (+ 0.1 (* 5 (gh c))) 1)) ; scale using the current harmonic
    (draw-cube)                   ; draw the bar
    (pop))                        ; pop out of this state as we've finished with it

(define (bars c)                  ; a function to loop over all harmonics
    (bar c)                       ; draw one bar with the current harmonic
    (translate (vector 1 0 0))    ; move 
    (if (zero? c)                 ; if 0 then we've finished drawing bars
        0                         ; exit by returning 0
        (bars (- c 1))))          ; otherwise, draw the next bar along, and then exit

(define (render) 
    (bars 16))   ; render all 16 harmonics

(every-frame (render))

