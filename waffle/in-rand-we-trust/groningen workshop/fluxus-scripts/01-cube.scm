; this script illustrates one of the easiest ways to use fluxus - 
; with the draw functions, which render a simple shape for one frame,
; so need to be called repeatedly using every-frame

(clear) ; clear the scene (not strictly required but good to get in the habit)




(define (render)  ; make a new procedure called render
    (draw-cube))  ; which just draws a cube

(every-frame (render)) ; calls the (render) procedure every frame 