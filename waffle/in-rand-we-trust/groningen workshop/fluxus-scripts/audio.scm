; audio test
(define (bars c)
    (push)
    (colour (vector 1 0 (gh c)))
    (translate (vector c 0 0))
    (scale (vector 1 (+ 0.1 (* 5 (gh c))) 1))
    (draw-cube)
    (pop)
    (if (zero? c)
        0
        (bars (- c 1))))


(define (render)
    (bars 16))

(start-audio "alsa_pcm:capture_1" 1024 44100)
(every-frame (render))

