; this simple script is just to illustrate the nature of the fluxus state stack
; the state in fluxus contains all the current information on drawing, ie colour,
; transform, material properties etc. push and pop are used to store and recall
; states - this is a useful way of organising the descriptions of objects as it
; minimises the amount of code you have to write

(clear)

(define (render)

    (colour (vector 1 0 0))      ; set the current colour to red

    (push)                       ; push a new state onto the stack
        (colour (vector 0 1 0))  ; set the colour for this current state to green
        (draw-cube)              ; draw the first cube
    (pop)                        ; lose this state, and return to the old one

    (translate (vector 1.1 0 0)) ; move a bit
    (draw-cube))                 ; draw the second cube, which uses the current 
                                 ; state colour (which is now back to red)

(every-frame (render)) 