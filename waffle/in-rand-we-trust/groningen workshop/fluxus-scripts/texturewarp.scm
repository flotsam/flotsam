; change the texture coordinates over time
(define (warp n)
    ; just add a small vector to the existing coords
    (pdata-set "t" n (vadd (pdata-get "t" n)
        (vector 0.001 0.002 0)))
    (if (zero? n)
        0
        (warp (- n 1))))

(clear)
(texture (load-texture "fish.png"))
(define p (build-sphere 15 15))

(define (render)
    (grab p)
    (warp (pdata-size))
    (ungrab))

(every-frame (render))