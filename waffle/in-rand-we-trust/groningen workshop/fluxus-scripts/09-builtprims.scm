; so far we've been using "draw" functions to render premade primitives.
; however to do more interesting things, and to speed things up a little we use
; "build" commands to create objects in fluxus's scenegraph. a scenegraph is a kind
; of database for storing objects in a heirachy. fluxus takes care of drawing them 
; every frame and and allows you to edit their properties directly - but you have to 
; record the object scenegraph id in order to access them later on

(clear)

; make a cube with build - rather than the draw procedure
(define myobject (build-cube)) 

(display myobject)(newline) ; we can print out the object

; objects in the scenegraph can be "grabbed" and changed 
; later on, for example:

(define (change-colour obj)
    (grab obj) ; sends all following commands to this primitive
    (colour (vector (flxrnd) (flxrnd) (flxrnd)))
    (ungrab))  ; turns off the grabbed mode (important to remember to do this)

(define (update)
    (if (mouse-button 1) ; if mouse button 1 is currently pressed
        (change-colour myobject)))

(every-frame (update)) 