; this script is an enhancement of the cube row, and shows how you can use
; recursion and state to describe complex procedural forms

(clear)

(show-axis 1) ; show the origin (helpful to see what's going on)

(define (draw-tree depth)
    (translate (vector 0 2 0))             ; move a little
    (draw-cube)                            ; draw a cube
    (cond 
        ((zero? depth)                     ; if depth is 0 
            0)                             ; return 0 and exit this procedure
        (else                              ; otherwise
            (push)                         ; push a new state
                (rotate (vector 30 0 0))   ; turn
                (draw-tree (- depth 1))    ; draw a new tree, take one off the depth
            (pop)                          ; pop this state
            (push)                         ; push a new one
                (rotate (vector -30 0 0))  ; turn the other direction
                (draw-tree (- depth 1))    ; draw another tree
            (pop))))                       ; pop this state and exit procedure

(every-frame (draw-tree 5)) 