; built primitives can be changed in more complex ways by editing the information 
; that makes up the primitive - usually vertex information such as position, colour, 
; texture coordinates or lighting normals. in fluxus, this data is called primitive 
; data - or "pdata" for short

(clear)

; a util function to return a random vector
(define (rndvec)
    (vmul (vsub (vector (flxrnd) (flxrnd) (flxrnd)) 
        (vector 0.5 0.5 0.5)) 0.1))

; a procedure to loop over all the vertices in an object
(define (deform vert)

    ; try changing "p" to "t" or "n"
    (pdata-set "t" vert (vadd (pdata-get "t" vert) ; add a vector onto the 
                              (rndvec)))           ; position of the vertex

    (if (zero? vert)                               ; end the recursion
        0
        (deform (- vert 1))))                      ; recurse...


; turn some stuff on to help see whats going on when we deform
(backfacecull 0)                        ; makes all polys visible
(hint-normal)                           ; turn this on to see the normals
(hint-wire)                             ; show wireframe
(line-width 3)                          ; make wire and normals thicker
(wire-colour (vector 1 1 1))            ; change the colour of the wireframe lines
(texture (load-texture "textures/peel.png"))     ; set a texture
(define myobject (build-cube))          ; make an object in the scenegraph

(define (render)
    (grab myobject)                     ; grab it - which sets it current
    (deform (pdata-size))               ; call deform with the num verts
    (ungrab))                           ; ungrab it again

(every-frame (render))