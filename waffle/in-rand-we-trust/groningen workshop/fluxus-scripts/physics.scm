; (press the left mouse button)

(define (render)
    (if (mouse-button 1)            ; if the mouse button has been pressed
        (active-box (build-cube)))) ; make a new cube and make it active in
                                    ; the physics system as a box
 
(clear)

(ground-plane (vector 0 1 0) 0) ; define an infinite ground plane
(collisions 1)                  ; turn collisions on

(every-frame (render))