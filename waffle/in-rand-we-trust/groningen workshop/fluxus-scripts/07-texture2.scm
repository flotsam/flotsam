; an example of how texturing can add a lot of visual detail without much work

(clear)

(define rot 0) ; a global variable is used to store the current rotation for all cogs

; a procedure to draw one cog
(define (draw-cog texfilename)
    (push)                                   ; push a new state to work in
    (texture (load-texture texfilename))     ; set the current texture 
    (hint-unlit)                             ; turn opengl lighting off
    (hint-ignore-depth)                      ; needs to be on for transparency (ask)
    (translate (vmul (vector (flxrnd) (flxrnd) 0) 3)) ; move to a random position
    (rotate (vector 0 0 (* rot (* 10 (- (flxrnd) 0.5))))) ; rotate and make the speed 
                                             ; different for each cog
    (draw-plane)                             ; draw the plane
    (pop))                                   ; pop this state
   

(define (draw-all-cogs)
    (draw-cog "textures/mech/1.png") ; draw cogs for all the textures...
    (draw-cog "textures/mech/2.png")
    (draw-cog "textures/mech/3.png")
    (draw-cog "textures/mech/4.png")
    (draw-cog "textures/mech/5.png")
    (draw-cog "textures/mech/6.png")
    (draw-cog "textures/mech/7.png")
    (draw-cog "textures/mech/8.png")
    (draw-cog "textures/mech/9.png")
    (draw-cog "textures/mech/10.png")
    (draw-cog "textures/mech/11.png")
    (draw-cog "textures/mech/12.png")
    (draw-cog "textures/mech/13.png")
    (draw-cog "textures/mech/14.png")
    (draw-cog "textures/mech/15.png"))
    
(define (render)
    (flxseed 1) ; seed the random number generator, so we get the same sequence
                ; of random numbers every frame

    (push)                        ; make a new state for all the cogs
    (wire-colour (vector 1 1 1))
;    (hint-wire)                  ; uncomment to see the polygons
    (scale (vector 5 5 5))        ; make it bigger so we can zoom in
    (draw-all-cogs)               ; draw all the cogs a few times
    (draw-all-cogs)
    (draw-all-cogs)
    (draw-all-cogs)
    (pop)
    (set! rot (+ rot 1)))         ; increment the global rotation counter

(every-frame (render))