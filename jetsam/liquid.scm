; elastic ngons

(define (liquidise strength viscosity)
    (define (loop n)
        (pdata-set "vel" n (vmul (vadd (vmul (vsub (pdata-get "pref" n) 
                                             (pdata-get "p" n)) strength)
                                 (vmul (pdata-get "vel" n) (- 1 strength))) viscosity))
        (if (zero? n)
            0
            (loop (- n 1))))
    (loop (pdata-size))
    (pdata-op "+" "p" "vel"))


(define (perturb strength)
    (define (loop n)
        (pdata-set "vel" n (vmul (vadd (vector (flxrnd) (flxrnd) (flxrnd)) 
            (vector -0.5 -0.5 -0.5))
             strength))
        (if (zero? n)
            0
            (loop (- n 1))))
    (loop (pdata-size)))


(clear)
;(hint-unlit)
(clear-colour (vector 0.5 0.5 0.5))
;(hint-none)
;(hint-wire)
(line-width 5)
(backfacecull 0)
(turtle-reset)
(gain 100)

(define o (build-sphere 10 10))

(grab o)
(recalc-normals 1)
(pdata-copy "p" "pref")
(pdata-add "vel" "v")
(perturb 0.5)
(ungrab)

(define (animate)
    (grab o)
    (liquidise 0.1 1.1)
    (recalc-normals 1)
    (ungrab))

(every-frame (animate))
