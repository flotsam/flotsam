(clear)

(define obs '())


(texture (load-texture "tex/paint.png"))

(push)
(hint-unlit)
(scale (vector -100 -100 -100))
(build-sphere 10 10)
(pop)

(hint-wire)
(line-width 4)

(define (build n)
    (translate (vector 2.1 0 0))
    (push)
    (shinyness 100)
    (specular (vector 1 1 1))
    (scale (vector 2 2 2))
    (set! obs (cons (build-cube) obs))
    (pop)
    (if (zero? n)
        0
        (build (- n 1))))

(define (meta-build x y)
    (translate (vector 0 2.1 0))
    (push)
    (build x)
    (pop)
    (if (zero? y)
        0
        (meta-build x (- y 1))))

(define (init-tex n a)
    (pdata-set "t" n (vmul (pdata-get "t" n) 0.03))
    (if (zero? n)
        0
        (init-tex (- n 1) a)))


(define (tex n a)
    (pdata-set "t" n (vadd (pdata-get "t" n) (vmul (vector (gh a) (gh (+ a 4)) 0) 0.001)))
    (if (zero? n)
        0
        (tex (- n 1) a)))


(define (animate l n)
    (grab (car l))
    (rotate (vector (gh n) (gh (+ n 5)) 0))
    (tex (pdata-size) n)
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (animate (cdr l) (+ n 1))))

(define (init l n)
    (grab (car l))
    (init-tex (pdata-size) n)
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (init (cdr l) (+ n 1))))


(meta-build 10 10)

(init obs 0)

(gain 100)

(let ((l (make-light "point" "free")))
(light-position l (vector 10 100 40)))

(every-frame (animate obs 0))    
