(define (make-shell n type col dist)
    (define (loop n target)
        (let ((vert (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist)))
               (tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "p" n vert)
        (pdata-set "t" n tex))
        (ungrab)
        (if (zero? n)
            0
            (loop (- n 1) target)))
    
    (let ((target (build-polygons (pdata-size) type)))

    (grab target)
    (colour col)
    (texture (force-load-texture "hair5.png"))
    (ungrab)

    (loop (pdata-size) target)
    (grab target)
    (recalc-normals 1)
    (ungrab)
    target))

(define (make-shells c n dist type col l)
    (if (eq? c n)
        l
        (make-shells c (+ n 1) dist type col
            (cons (make-shell n type (vmul col (* n 2)) (* dist n)) l))))

(define (animate-shells l)
    (define (animate n)
        (let ((vary (f32vector-ref (pdata-get "p" n) 1)))
        (pdata-set "t" n (vadd (pdata-get "t" n) 
            (vmul (vector (sin (+ vary (* (time) 2))) 
                          (cos (+ vary (* (time) 2))) 0) 
                0.001))))
        (if (zero? n)
            0
            (animate (- n 1))))

    (define (copy n target)
        (let ((tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "t" n (vadd 
            (vmul tex 0.3)
            (vmul (pdata-get "t" n) 0.7)))
        (ungrab))
        (if (zero? n)
            0
            (copy (- n 1) target)))

    (define (transfer l)
        (grab (car l))
        (copy (pdata-size) (car (cdr l)))
        (ungrab)
        (if (null? (cdr (cdr l)))
            0
            (transfer (cdr l))))

    (grab (car l))
    (animate (pdata-size))
    (ungrab)

    (transfer l))


(define (deform n)
    (pdata-set "t" n (vmul (pdata-get "t" n) 8))
    (let ((val (sin (* 0.1 (vdist (pdata-get "p" n) (vector 10 0 5))))))
    (pdata-set "p" n (vadd (pdata-get "p" n) (vector 0 (* val 5) 0))))
    (if (zero? n)
        0
        (deform (- n 1))))

(clear)
(clear-colour (vector 0 0.5 1))
(colour (vector 0.0 0.2 0.0))
(push)
(scale (vector 50 50 50))
(rotate (vector 270 0 0))
(define a (build-seg-plane 5 5))
(pop)
(apply-transform a)


(light-diffuse 0 (vector 0 0 0))
(define light (make-light "point" "free"))
(light-diffuse light (vector 1 1 1))
(light-specular light (vector 1 1 1))
(light-position light (vector 100 50 0))

;(hint-wire)
;(wire-colour (vector 1 1 1))

(grab a)
(deform (pdata-size))
(recalc-normals 1)
(define shells (make-shells 8 0 0.06 1 (vector 0.03 0.1 0.02) '()))
(ungrab)

(show-fps 1)

(define (animate)
    (animate-shells shells))

(every-frame (animate)) 