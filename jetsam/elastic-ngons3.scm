; elastic ngons

(define bm-src '(zero one dst-color one-minus-dst-color src-alpha
    one-minus-src-alpha dst-alpha one-minus-dst-alpha src-alpha-saturate))

(define bm-dst '(zero one dst-color one-minus-dst-color src-alpha
    one-minus-src-alpha dst-alpha one-minus-dst-alpha))

(define ngon-cols (list (vector 1 1 0) (vector 0 1 0) (vector 0 0 1)))

(define (ngon n)
    (define (gon n angle)
        (turtle-vert)
        (turtle-turn (vector 0 0 angle))
        (turtle-move 1)
        (if (zero? n)
            0
            (gon (- n 1) angle)))
    (turtle-prim 4)
    (opacity 0.2)
    (blend-mode (list-ref bm-src (random (length bm-src)))
        (list-ref bm-dst (random (length bm-dst))))

    (colour (list-ref ngon-cols (modulo n (length ngon-cols))))
    (gon n (/ 360 n))
    (turtle-turn (vector 0 0 180))
    (let ((o (turtle-build)))
    (grab o)
    (recalc-normals 1)
    (ungrab)
    o))
    
(define (elastic)
    (define (update-mass n)
        (let ((v (vmul (vsub (pdata-get "pref" n) (pdata-get "p" n)) 0.1)))
        (pdata-set "vel" n (vmul (vadd (pdata-get "vel" n) v) 0.9)))
        (if (zero? n)
            0
            (update-mass (- n 1))))

    (update-mass (pdata-size))
    (pdata-op "+" "p" "vel"))

(define (perturb strength)
    (define (loop n)
        (pdata-set "vel" n (vadd (pdata-get "vel" n) (vmul (vadd (vector (flxrnd) (flxrnd) 0.5) 
            (vector -0.5 -0.5 -0.5)) strength)))
        (if (zero? n)
            0
            (loop (- n 1))))
    (loop (pdata-size)))


(define (make-obs)
    (list (list)))

(define (obs-get-list obs)
    (list-ref obs 0))

(define (obs-set-list! obs s)
    (list-set! obs 0 s))

(define (obs-add obs ob)
    (grab ob)
    (pdata-copy "p" "pref")
    (pdata-add "vel" "v")
    (ungrab)
    (obs-set-list! obs (append (obs-get-list obs) (list ob))))

(define (obs-animate obs)
    (define (animate obs n)
        (grab (car obs))
        (perturb (gh n))
        (elastic)
        (ungrab)
        (if (null? (cdr obs))
            0
            (animate (cdr obs) (+ n 1))))
    (animate (obs-get-list obs) 0))

(clear)
;(hint-unlit)
(clear-colour (vector 0.5 0.5 0.5))
;(hint-normal)
;(hint-wire)
(line-width 5)
(backfacecull 0)
(turtle-reset)

(blur 0)
(define obs (make-obs))

(define (vz obs n) 
    (obs-add obs (ngon n))
    (cond 
        ((> n 3)
            (turtle-push) 
            (vz obs (- n 1))
            (turtle-pop) 
            (turtle-turn (vector 1 0 90))        
            (turtle-turn (vector 0 90 0))        
            (turtle-move 0.05)        
            (turtle-turn (vector 0 -90 0))        
            (turtle-move 2)        
            (turtle-push) 
            (vz obs (- n 1))
            (turtle-pop))))

(vz obs 8)

(define (animate)
    (obs-animate obs))

(every-frame (animate))
