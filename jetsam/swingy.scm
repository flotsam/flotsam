(define swingy '())

(blur 0.01)

(clear)
(texture (load-texture "tex/transp.png"))

;(hint-none)
(hint-unlit)
(line-width 5)
(define joint-pos (vector 0 1 0))

(push)
(scale (vector 6 1 6))
(define anchor (build-cube))
(active-box anchor)
(set-mass anchor 5)
(grab anchor)
(hide 1)
(ungrab)
(pop)

(define (add-pendulum)
    (define pendulum 0)
    (push)
    (colour (vector (flxrnd) (* (flxrnd) 0.5) (* (flxrnd) 0.1)))
    (translate (vector 10 10 0))
    (scale (vector 15 10 0.1))
    (set! pendulum (build-cube))
    (active-box pendulum)
    (set-mass pendulum 0.005)
    (pop)
    (set! swingy (cons pendulum swingy))
    (build-hingejoint pendulum anchor joint-pos (vector 0 1 0) ))

(collisions 1)
(ground-plane (vector 0 1 0) -2.5)

(push)
(translate joint-pos)
(scale (vector 0.1 0.1 0.1))
(build-sphere 5 5 )
(pop)

(show-axis 1)

(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)
(add-pendulum)

(define (animate l n)
    (twist (car l) (vmul (vector 0 1 0) (* 1 (sin (* (gh n) 10000)))))
    (if (eq? (cdr l) '())
        0
        (animate (cdr l) (+ n 1))))

(every-frame "(animate swingy 0)")



