
(clear)
(ortho)

(pattern-init)
(push)

(pattern-voice 1 "sub" '("octavea" 0 "typea" 0 "typeb" 0 "cutoff" 1 
    "finefreqa" 1.5 "decaya" 0.2 "decayb" 0.3 "mainvolume" 3 "resonance" 0 "poly" 4))

(pattern 1 2 2 (list "!<1" (list (list "1" "o.o.----o."))))


(pop)
