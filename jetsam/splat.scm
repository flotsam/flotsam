(define (splat n)
    (cond ((not (zero? n))
            (turtle-move (+ 1 (sin (+ (time) (* n 0.1)))))
            (turtle-turn (vector (* 0.3 (sin (time))) (* 4 (cos (* 0.3 (time)))) 10))
            (turtle-vert)
            (splat (- n 1)))))


(clear)
(backfacecull 0)
(hint-wire)
(wire-colour (vector 1 1 1))
(line-width 2)
(define shape (build-polygons 2000 'triangle-strip))
(turtle-attach shape)

(every-frame
    (with-primitive shape
        (turtle-reset)
        (splat (pdata-size))))

(every-frame (animate))