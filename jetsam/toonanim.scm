(gain 0.1)
(clear)
(define dirlight1 (vtransform (vector 1 0 0) (mrotate (vector 0 0 0))))

(texture (force-load-texture "tex/gradient.png"))
;(show-axis 1)
(hint-unlit)
;(hint-wire)
;(line-width 2)

(define (toon-light n)
    (let ((lighting (vdot (pdata-get "n" n) dirlight1)))
        (if (< lighting 0) (set! lighting 0.1)) 
        (if (> lighting 0.95) (set! lighting 0.95)) 
        (pdata-set "t" n (vector lighting 1 0)))
    (if (< n 1)
        0
        (toon-light (- n 1))))

(define (deform n)
     (let ((v (gh n)))
         (if (> v 1) (set! v 1))    
         (pdata-set "p" n (vadd (pdata-get "pref" n) 
             (vmul (pdata-get "nref" n) v))))
    (if (< n 1)
        0
        (deform (- n 1))))


(colour (vector 0.9 0.5 1))
(define s (build-nurbs-sphere 12 19))
(grab s)
(pdata-copy "p" "pref")
(pdata-copy "n" "nref")
(ungrab)

(define (render)
    (grab s)
    (pdata-copy "pref" "p")
    (deform (pdata-size))
    (recalc-normals 1)
    (toon-light (pdata-size))
    (ungrab))

(every-frame (render)) 