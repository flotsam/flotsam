
(clear)
(pattern-init)
(load (full-path "stroke.scm"))

(pattern-samples 1 "kip")

(pattern-voice 2 "drum" '("kickfreqdecay" 0.05 "kickdecay" 0.3 "kickfreqvolume" 4 
        "kickfreq" 0.5
        "hat1decay" 0.05 "hat1volume" 2 "hat1cutoff" 0.5 "hat1resonance" 0.3
        "hat2decay" 0.05 "hat2volume" 2 "hat2cutoff" 0.9 "hat2resonance" 0.4
        "snaredecay" 0.01 "snarevolume" 2 "distort" 0.4 
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.6 "snareresonance" 0.3
        "crushfreq" 0.2 "crushbits" 4 "poly" 1 "mainvolume" 0.2))

(pattern-voice 3 "fm" 
      '("freq" 0.25 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.5 "sustain" 0.3 
       "release" 0.3 "modtype" 0 "modattack" 0.2 "moddecay" 0.2 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 0.2 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 2 "fbvolume" 0.2 "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 0.2 "distort" 0.5 "crushfreq" 0
        "crushbits" 2))

(pattern-voice 4 "sub" '("freqa" 0.5 "freqb" 0.2501 "cutoff" 0.1 "resonance" 0.45 
    "ftype" 1 "typea" 1 "decaya" 0.2 "sustaina" 0 "releasea" 0.5 
    "volumea" 1 "typeb" 1 "attackb" 0 "decayb" 0.2 "sustainb" 0 "releaseb" 0.5 
    "volumeb" 1 "attackf" 0.4 "decayf" 0.6 "sustainf" 0 "releasef" 0 "volumef" 0.1
    "lfodepth" 0.03 "lfofreq" 0.1 "distort" 0.5 "poly" 3 "mainvolume" 0.5))

(pattern-voice 5 "fm"
      '("freq" 0.125 "modfreq" 0.5 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.3 
       "release" 0.1 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.6 "fbattack" 0.2 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 7 "fbvolume" 5 "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 0.2))
      
(pattern-bpm 100)
(pattern-sample-volume 1.5)
(pattern-sample-pitch 0.5)

(pattern 1 4 1 (list "!<111+++1" (list (list "1" "[o.+o...+o...+o...+o...]"))))

(pattern 2 4 4 (list "!<111+++1" 
    (list 
        (list "1" "21[++12]")
        (list "2" "[o..+o.++o.+o.+++o.+o..]"))))

(pattern 3 1 1 
    (list "!<1" 
        (list (list "1" "----o.O.---o.O."))))

(pattern 4 4 1 (list "!<1" 
    (list 
        (list "1" "[..o.O.O..O..]"))))

(pattern 5 4 1 (list "!<1" 
    (list 
        (list "1" "[o.O.O..O..]"))))

(pattern-cascade '(1 2 3 4))
