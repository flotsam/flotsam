(require fluxus-016/fluxa)

(clear)
(max-synths 10)
(clear-colour (vector 1 1 1))
(define-struct ob (id prim 
    (rot #:mutable) 
    (spike #:mutable)
    (freq #:mutable)
    (col #:mutable)))

(define (make i)
    (let ((p (build-torus 0.05 i 4 10))) 
        (with-primitive p
            (texture (load-texture "textures/hole.png"))
            (poly-convert-to-indexed)
            (pdata-copy "p" "pref")
            (pdata-copy "t" "tref"))
        (make-ob i p (vector 0 0 0) 0 0 (vector 1 1 1))))

(define (anim l)
    (with-primitive (ob-prim l)

        (set-ob-spike! l (* 0.9 (ob-spike l)))
        (set-ob-freq! l (* 0.95 (ob-freq l)))
        (set-ob-col! l (vmix (ob-col l) (vector 1 1 1) 0.9))
        (set-ob-rot! l (vmul (ob-rot l) 0.9))
        (colour (ob-col l))
        (rotate (ob-rot l))
        (pdata-index-map!
            (lambda (i p pref n)
                (vadd pref (vmul n (* (ob-spike l)
                                      (abs (sin (+ (* (ob-freq l) i)
                                       (* (ob-id l) 0.1 (time)))))))))
            "p" "pref" "n")
        
        (pdata-map!
            (lambda (t n)
                (vadd t (vmul n (* (ob-id l) 0.01))))
            "t" "n")
        (recalc-normals 0)
        ))

(define l (build-list 10 (lambda (i) (make i))))

(define (wrap l n)
    (list-ref l (modulo n (length l))))

(fluxa-init)
(max-synths 50)
(seq  
    (lambda (time clock)
        (when (zmod (+ 1 clock) 1)
            (play time (mul (sine 2000) (adsr 0 (* (modulo clock 5) 0.01) 0 0)))
            (at time (lambda (_) (set-ob-spike! (list-ref l (modulo clock 5))
                (* 0.1 (modulo clock 4)))) 0))
            
        #;(when (zmod clock 8)
            (play time (mul (white 5) (adsr 0 (* (rndf) 0.1) 0.01 1)))
                (at time (lambda (_) (set-ob-col! (list-ref l (modulo clock (length l))) 
                    (vector 0 0 10))) 0))

        #;(when (or (zmod (+ clock 2) 16) (zmod (+ clock 2) 8))
            (play time (crush (mul 4 (mul 
                (sine (mul (random 200) (adsr 0 0.04 0.5 1)))
                (adsr 0 0.1 0.2 1))) 8 (* 0.01 (modulo clock 50))))
            (at time (lambda (_) 
            (set-ob-rot! (list-ref l (modulo clock (length l)))
                (vector 0 0 (* (modulo clock 100) 0.1)))) 0))


        #;(when (wrap (list #t #f #f #f #t #t) clock)
            (let ((n (* 2 (+ 5 (modulo clock 7)))))
            (play time (mul (mooglp (saw (note n)) 
                    (* (random 10) 0.01) 0.3) (adsr 0 0.2 0.1 2)))
                (at time (lambda (_) (set-ob-spike! 
                    (list-ref l (modulo n (length l))) 
                        (* 1 (modulo clock 3)))
                    (set-ob-freq! 
                    (list-ref l (modulo n (length l))) 
                        (* 0.01 (modulo clock 30)))) 0)))

        #;(when (or (zmod clock 6) (zmod clock 5))
            (let ((n (+ 2 (modulo clock 8))))
            (play time (mul 0.1 (mul (add (saw (note n)) (saw (+ 0.1 (note n))) )
                     (adsr 0 0.2 0.5 0))))
                (at time (lambda (_)  
                    (with-primitive
                        (ob-prim (list-ref l (modulo n (length l))))
                        (pdata-copy "tref" "t")
                        #;(hint-wire))) 0)))

        #;(when (zmod clock 3)
            (let ((n (+ 30 (modulo clock 10))))
            (play time (mul (mooglp (saw (add (mul (mul 10000 
                    (adsr (rndf) 0 0 0)) (saw (* (note n) 0.25))) (note n)))
                    (* (random 10) 0.1) 0.1) (adsr 0 0.2 0.1 0.2)))
            (at time (lambda (_) 
                (set-ob-rot! (list-ref l (modulo n (length l))) 
                    (vector 0 (* n 0.1) 0))) 0)))

        #;(when (zmod clock 1)
            (let ((n (+ 30 (modulo clock 10))))
            (play time (mul (mooglp (saw (add (mul (mul 100 
                    (adsr 1 0 0 0)) (saw (* (note n) 0.25))) (note n)))
                    (* (random 10) 0.1) 0.1) (adsr 0 0.1 0.1 0.2)))
            (at time (lambda (_) 
                (set-ob-rot! (list-ref l (modulo n (length l))) 
                    (vector 0 (* n 0.1) 0))) 0)))

       (if (odd? clock) 0.15 0.1))
    )

(every-frame 
    (for-each
        (lambda (ob)
            (anim ob))
        l))