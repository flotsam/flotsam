(clear)
(texture (load-texture "rhod.png"))
(clear-colour (vector 0.5 0.7 0.6))


(define (move-tx n speed)
    (pdata-set "t" n (vadd (pdata-get "t" n) (vector speed 0 0)))
    (if (zero? n)
        0
        (move-tx (- n 1) speed)))

(hint-unlit)
(rotate (vector 0 0 180))
(scale (vector 160 100 100))
(define a (build-plane))
(translate (vector 0 0 0.1))
(define b (build-plane))


(define (render)
    (grab a)
    (move-tx (pdata-size) 0.0005)
    (ungrab)
    (grab b)
    (move-tx (pdata-size) 0.001)
    (ungrab))

(every-frame (render))



