
(clear)
(ortho)

(load (full-path "stroke.scm"))

(pattern-init)

(pattern-voice 1 "sub" '("decaya" 0.1 "decayb" 0.1 "typea" 2 "typeb" 2 "freqa" 0.5
        "freqb" 0.2501 "ftype" 3 "cutoff" 0.5 "slidea" 0 "slideb" 0 "volumef" 1
        "poly" 1 "decayf" 0.9 "lfodepth" 0.5 "resonance" 0 "lfofreq" 0.5 "ring" 0 
        "volumea" 1 "volumeb" 1 "mainvolume" 5))

(pattern 1 4 2 (list "!<1" 
    (list 
        (list "1" "1--[2O.o.+++o.---O.]++1")
        (list "2" "++++++12"))))

(pattern-voice 2 "sub" '("decaya" 0.1 "decayb" 0.1 "typea" 2 "typeb" 2 "freqa" 0.5
        "freqb" 0.2501 "ftype" 0 "cutoff" 0.2 "slidea" 0 "slideb" 0 "volumef" 1
        "poly" 1 "decayf" 0.9 "lfodepth" 0.01 "resonance" 0.2 "crushfreq" 20 
        "crushbits" 50 "lfofreq" 0.5 "ring" 0 "volumea" 1 "volumeb" 1 "mainvolume" 1))

(pattern 2 4 2 (list "!<1" 
    (list 
        (list "1" "1--[2O.o..+++o.---O.]++1")
        (list "2" "++++++12"))))

(pattern-voice 3 "sub" '("decaya" 0.6 "decayb" 0.6 "typea" 0 "typeb" 0 "freqa" 0.25
        "freqb" 0.12501 "ftype" 0 "cutoff" 0.6 "slidea" 0 "slideb" 0 "volumef" 0.2
        "poly" 1 "decayf" 0.9 "lfodepth" 0.1 "resonance" 0 "crushfreq" 0.1
        "crushbits" 5 "lfofreq" 0.5 "ring" 0.5 "volumea" 1 "volumeb" 1 "mainvolume" 1))

(pattern 3 4 2 (list "!<1" 
    (list 
        (list "1" "1--[2O....o..]++1")
        (list "2" "++++++++++++12"))))

(pattern-samples 4 "rip")
(pattern-sample-volume 1)
(pattern-sample-pitch 1)

(pattern 4 4 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[o..+o.++o.++o..+o..]"))))


(pattern-cascade '(1 2 3 4)) 