(clear-colour (vector 0.5 0.5 0.5))

(define (build n)
    (turtle-reset)
    (turtle-prim 4)
    (turtle-push)
    (build-loop n n)
    (turtle-pop)
    (turtle-build))

(define (build-loop n t)
    (turtle-turn (vector 0 (/ 360 t) 0))
    (turtle-move 1)
    (turtle-vert)
    (if (< n 1)
        0
        (build-loop (- n 1) t)))

    
(backfacecull 0)    
(clear)
(hint-unlit)
(hint-wire)
(line-width 4)

(build 12)