(clear)
(define dirlight1 (vtransform (vector 1 0 0) (mrotate (vector 45 0 90))))
(source "scm/objimport.scm")


(hint-multitex)
(multitexture 0 (force-load-texture "tex/gradient.png"))
(multitexture 1 (force-load-texture "tex/outline.png"))

(clear-colour (vector 0 0.5 1))

;(show-axis 1)
(hint-unlit)
;(hint-wire)
;(line-width 2)

(define (toon-outline n camerapos obpos)
    (let ((v (vadd obpos (pdata-get "p" n))))                           ; find the vertex in worldspace 
        (let ((i (vnormalise (vsub v camerapos))))                      ; incident direction (normalised)
            (pdata-set "t1" n (vector (* 1 (vdot i (pdata-get "n" n))) 0 0)))) ; set s to the facing ratio (i dot n) 
    (if (< n 0)
        0
        (toon-outline (- n 1) camerapos obpos)))    

(define (toon-light n)
    (let ((lighting (vdot (pdata-get "n" n) dirlight1)))
        (if (< lighting 0) (set! lighting 0.1)) 
        (if (> lighting 0.95) (set! lighting 0.95)) 
        (pdata-set "t" n (vector lighting 1 0)))
    (if (< n 1)
        0
        (toon-light (- n 1))))

(define (deform n)
    (pdata-set "p" n (vadd (pdata-get "pref" n) 
        (vmul (pdata-get "nref" n) 
                (* (sin (+ (time) (* 7 (vector-ref (pdata-get "p" n) 1))))
                    0.4))))
    (if (< n 1)
        0
        (deform (- n 1))))


(colour (vector 0.9 0.5 1))
(define s (obj-make (obj-load "meshes/bunny-1500.obj")))
(display s)(newline)
(grab s)
(pdata-copy "p" "pref")
(pdata-copy "n" "nref")
(pdata-copy "t" "t1")
(ungrab)

(define (render)
    (grab s)
    (pdata-copy "pref" "p")
    (deform (pdata-size))
    (recalc-normals)
    (toon-light (pdata-size))
    (toon-outline (pdata-size) 
        (vtransform (vector 0 0 0) (get-camera-transform)) 
        (vtransform (vector 0 0 0) (get-transform)))
    (ungrab))

(every-frame "(render)") 
