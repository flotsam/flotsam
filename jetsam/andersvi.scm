(clear)

(define (pickl lista)
  (list-ref lista (random (length lista))))

(define (rand-cent y)
  (- y (random (* 2 y))))

(define (rand-vect y)
  (vector (rand-cent y) (rand-cent y) (rand-cent y)))


;; map operator gjennom liste og akkumuler resultat:

(define (akkumuler-liste liste op init)
  (if (null? (cdr liste))
      (cons (op (car liste) init) '())
      (let ((verdi (op (car liste) init)))
        (cons verdi
              (akkumuler-liste (cdr liste) op verdi)))))


(searchpaths '("/home/andersvi/prosjekter/CIKADA/Fluxus/"))

(define pi 3.141592654)

;; liste av fingerlengder:
(define fingerlengder '(0.7 1.0 1.06 1.0 0.7))

(define fingerleddlengder '(3 1.3 1.0 0.8))

;;(define fingerleddoffsets '(0 2.0 3.1 4.1 5.0))
(define fingerleddoffsets
  (akkumuler-liste fingerleddlengder + 0))


(define finger-avstand 2.5)

(define finger-avstand 0.9)

(define finger-hoyde 0.5)
(define hand-kurve -5)
(define finger-spredning 5)



(define (fingerleddparametre j)
  (joint-param j "LoStop" -0.5) 
  (joint-param j "HiStop" 0.0)     
  (joint-param j "FMax" 3) 
  (joint-param j "FudgeFactor" 0))

(define (fingerleddparametre j)
  (joint-param j "LoStop" -0.5) 
  (joint-param j "HiStop" 0.0)     
  (joint-param j "FMax" 3)
  (joint-param j "FudgeFactor" 0.9)
  (joint-param j "Bounce" 0.1)
  (joint-param j "CFM" 0.5)
  (joint-param j "StopERP" 0.8)
  (joint-param j "StopCFM" 0)
  )

(define fingerkoblinger '())




(define teksturer '("teksturer/hud1.png"
                    "teksturer/hud2.png"
                    "teksturer/hud3.png"
                    "teksturer/hud4.png"))

(define (lag-finger ledd n-finger)
  (let ((fingerledd (make-vector ledd))
        (lengde-fingerledd (list-ref fingerlengder n-finger)))
    (do ((n 0 (+ n 1)))
        ((>= n (vector-length fingerledd)) fingerledd)
      (push)
      (translate (vector (* (+ n 1) lengde-fingerledd)
                         0 0))
      (scale (vector (* lengde-fingerledd (if (= n 0) 1.3 1))
                     ;;lengde-fingerledd ;;(* n;;(if (= n 0) 2 1) lengde-fingerledd)
                     finger-hoyde
                     finger-hoyde))
      (texture (load-texture (pickl teksturer)))
      (vector-set! fingerledd n (build-sphere 12 12))
      (active-sphere (vector-ref fingerledd n))
      (pop)
      )

    (do ((n 1 (+ n 1)))
        ((>= n (vector-length fingerledd))  fingerledd)
      (let ((l1 (vector-ref fingerledd (- n 1)))
            (l2 (vector-ref fingerledd n))
            (hinge (vector 0 1 0))
            (pos (vector (- (* (+ n 2) lengde-fingerledd)
                            (* 0.5 lengde-fingerledd))
                         (* n-finger finger-avstand)
                         -0.2)))
        (set! fingerkoblinger
              (cons (build-hingejoint l1 l2
                                      pos
                                      hinge)
                    fingerkoblinger))
        (fingerleddparametre (car fingerkoblinger))))
    
    fingerledd))

(define (lag-finger ledd n-finger)
  (let ((fingerledd (make-vector ledd))
        (lengde-fingerledd (list-ref fingerlengder n-finger)))
    (do ((n 0 (+ n 1)))
        ((>= n (vector-length fingerledd)) fingerledd)
      (push)
      (translate (vector (* (list-ref fingerleddoffsets n) lengde-fingerledd)
                         0 0))
      (scale (vector (* lengde-fingerledd (if (= n 0) 1.0 1))
                     ;;lengde-fingerledd ;;(* n;;(if (= n 0) 2 1) lengde-fingerledd)
                     finger-hoyde
                     finger-hoyde))
      (texture (load-texture (pickl teksturer)))
      (vector-set! fingerledd n (build-sphere 12 12))
      (active-sphere (vector-ref fingerledd n))
      (pop)
      )

    (do ((n 1 (+ n 1)))
        ((>= n (vector-length fingerledd))  fingerledd)
      (let ((l1 (vector-ref fingerledd (- n 1)))
            (l2 (vector-ref fingerledd n))
            (hinge (vector 0 1 0))
            (pos (vector (- (* (list-ref fingerleddoffsets n) lengde-fingerledd)
                            (* 0.5 lengde-fingerledd))
                         (* n-finger finger-avstand)
                         -0.2)))
        (set! fingerkoblinger
              (cons (build-hingejoint l1 l2
                                      pos
                                      hinge)
                    fingerkoblinger))
        (fingerleddparametre (car fingerkoblinger))))
    
    fingerledd))


(define handleddskoblinger '())

(define v-handledd #f)
(define h-handledd #f)

(define (lag-venstre-hand)

  
  (let ((hand '())
    (handledd-hinge (vector 1 0 0)))

    (push)
    (translate (vector -2 (* finger-avstand 2)  0))
    (scale (vector 4.4 2.5 1))
    (texture (load-texture "teksturer/aids.png"))
    
    (set! v-handledd (build-sphere 20 20))
    ;;(build-fixedjoint v-handledd)
    ;;(active-sphere v-handledd)
    (pop)

    (push)

    (scale (vector 1 0.8 1.0))
    (set! hand (cons (lag-finger 3 0) hand)) ; lille petter spillemann

    (pop)
    
    
    
    (translate (vector 0 finger-avstand 0))
    ;;(rotate (vector (* 1 hand-kurve) 0 (* 3 finger-spredning)))
    (set! hand (cons (lag-finger 3 1) hand)) ; gulbrand

    

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                              (vector-ref (car (cdr hand)) 0)
                              (vector 0 (* 0.5 finger-avstand) 0)
                              handledd-hinge)
        handleddskoblinger))

    
    (translate (vector 0 finger-avstand 0))
    ;;(rotate (vector hand-kurve 0 finger-spredning))
    (set! hand (cons (lag-finger 3 2) hand)) ; langemann

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 1.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))
    

    (translate (vector 0 finger-avstand 0))
;   (rotate (vector hand-kurve 0 finger-spredning))
    (set! hand (cons (lag-finger 3 3) hand)) ; slikkepott

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 2.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))

    (translate (vector 0 finger-avstand 0))

    (push)
    (rotate (vector hand-kurve 0 (* finger-spredning 4)))
    (set! hand (cons (lag-finger 2 4) hand)) ; tommeltott

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 3.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))
    (pop)
    ;; returnere handa


    hand)
  
  )

(define (lag-hoyre-hand)

  
  (let ((hand '())
    (handledd-hinge (vector 1 0 0)))
    (set! hand (cons (lag-finger 3 0) hand)) ; tommeltott

    (translate (vector 0 finger-avstand 0))
    ;;(rotate (vector (* 1 hand-kurve) 0 (* 3 finger-spredning)))
    (set! hand (cons (lag-finger 3 1) hand)) ; slikkepott
    
    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 0.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))

    
    (translate (vector 0 finger-avstand 0))
    ;;(rotate (vector hand-kurve 0 finger-spredning))
    (set! hand (cons (lag-finger 3 2) hand)) ; langemann

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 1.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))
    

    (translate (vector 0 finger-avstand 0))
;   (rotate (vector hand-kurve 0 finger-spredning))
    (set! hand (cons (lag-finger 3 3) hand)) ; gulbrand

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 2.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))

    (translate (vector 0 finger-avstand 0))
;   (rotate (vector hand-kurve 0 finger-spredning))
    (set! hand (cons (lag-finger 3 4) hand)) ; lille petter spillemann

    (set! handleddskoblinger
      (cons (build-hingejoint (vector-ref (car hand) 0)
                  (vector-ref (car (cdr hand)) 0)
                  (vector 0 (* 3.5 finger-avstand) 0)
                  handledd-hinge)
        handleddskoblinger))
    ;; returnere handa
    
    hand)
  
  )






;;(hint-none)
;;(hint-wire)
;;(hint-normal)
;;(hint-points)
;;(hint-anti-alias)
;;(hint-unlit)
;;(hint-box)
;;(hint-vertcols)

;;(show-axis 1)
(collisions 1)
(desiredfps 15)
(define size 5)


(ground-plane (vector 0  1 0) (- size))
(ground-plane (vector 0 -1 0) (- size))
(ground-plane (vector -1 0 0) (- size))
(ground-plane (vector 1  0 0) (- size))
(ground-plane (vector 0  0 1) (- size))
(ground-plane (vector 0 0 -1) (- size))



(define (festhand hand)
  (do ((index 0 (+ index 1)))
      ((>= index (length hand)) #f)
    (let* ((finger (list-ref hand index))
       (fingerfeste (vector-ref finger 0)))
      (build-fixedjoint fingerfeste))))


(define venstre-hand (lag-venstre-hand))

(festhand venstre-hand)


(gravity (vector 0 -1 0))


(define (tuppsleng hand)
  (do ((index 0 (+ index 1)))
      ((>= index (length hand)) #f)
    (let* ((finger (list-ref hand index))
       (fingertupp (vector-ref finger (- (vector-length finger) 1)))
       (fingerledd2 (vector-ref finger (- (vector-length finger) 2)))
       (kraft (vector (rand-cent 2.0) (rand-cent 2.0) (* (- 1 (random 2)) -12 (expt
(flxrnd) 7)))))
      (kick fingertupp kraft)
      (kick fingerledd2 (vmul kraft 3)))))

;;(clear-lights)




(define (fingerkick hand)
  (do ((index 0 (+ index 1)))
      ((>= index (length hand)) #f)
    (if (zero? (random 10))
        (let* ((finger (list-ref hand index))
               (ledd (vector-ref finger (random (vector-length finger))))
               (kraft
                (vector (rand-cent 1.0)
                        (rand-cent 20.0)
                        (* (- 1.8 (random 2.0)) -80 (expt (flxrnd) 4)))))
          (kick ledd kraft))
        #f)))

(define fingerfarger
  (vector
    (vector (random 1.0) (random 1.0) (random 1.0))
    (vector (random 1.0) (random 1.0) (random 1.0))
    (vector (random 1.0) (random 1.0) (random 1.0))
    (vector (random 1.0) (random 1.0) (random 1.0))
    (vector (random 1.0) (random 1.0) (random 1.0))
    (vector (random 1.0) (random 1.0) (random 1.0))))



(define (fingerkickcolor hand)
  (do ((index 0 (+ index 1)))
      ((>= index (length hand)) #f)
    (let ((finger (list-ref hand index)))
      (do ((fingerleddn 0 (+ fingerleddn 1)))
          ((>= fingerleddn (vector-length finger)) #t)
        (grab (vector-ref finger fingerleddn))
        (colour (vadd (vector-ref fingerfarger index)
                      (vmul (vector (gh fingerleddn)
                                    (gh (+ index fingerleddn))
                                    (gh (+ index fingerleddn)))
                            0.1)))
        
        (ungrab)))
    (if (zero? (random 10))
        (let* ((finger (list-ref hand index))
               (ledd (vector-ref finger (random (vector-length finger))))
               (kraft
                (vector (rand-cent 1.0)
                        (rand-cent 20.0)
                        (* (- 1.8 (random 2.0)) -80 (expt (flxrnd) 4)))))
          (kick ledd kraft))
        #f)))

(define l1 (make-light 0))
;;(light-position l1 (vector 0 10 10))

(define (oppdater)
  ;;(fingerkick venstre-hand)
  (fingerkickcolor venstre-hand)
  ;;(osc-send "/hello" "fis" '(1 2 "three"))
  ;;(light-position l1 (vector (sin (time)) (cos (time)) (cos (* (time) 0.7))))
  )


(osc-destination "4444")


;;(blur 0.14)
;;(shinyness 0.0)


;;(every-frame "(tuppsleng venstre-hand)")
(every-frame "(oppdater)")
