(define robo-objs `())
(define robo-joints `())
(define robo-rootstack '(0))
(define robo-joints->objs `())

(define (robo-clear)
    (set! robo-objs `())
    (set! robo-joints `())
    (set! robo-rootstack '(0)))

(define (robo-world size)
    (ground-plane (vector 0  1 0) (- size))
    (ground-plane (vector 0 -1 0) (- size))
    (ground-plane (vector -1 0 0) (- size))
    (ground-plane (vector 1  0 0) (- size))    
    (ground-plane (vector 0  0 1) (- size))
    (ground-plane (vector 0 0 -1) (- size))

    (collisions 1)
    (push)
    (hint-wire)
    (wire-colour (vector 1 1 1))
    (scale (vmul (vector (- size) (- size) (- size)) 2))
    (let ((box (build-cube)))
    (grab box)
    (selectable 0)
    (ungrab)
    (pop)))

(define (robo-joint)
    (let ((s (robo-build (vector 1 0.2 0.2))))
    (robo-connect (vtransform (vector -0.5 0 0) s) (vtransform-rot (vector 0 1 0) s) (vtransform-rot (vector 1 0 0) s)))
    (set-car! robo-rootstack (car robo-objs))
    (car robo-joints))

(define (robo-block)
    (robo-build (vector 1 1 1))
    (set-car! robo-rootstack (car robo-objs))
    (car robo-objs))

(define (robo-build sc)
    (translate (vector 1.1 0 0))
    (push)
    (identity)
    (scale sc)
    (set! robo-objs (cons (build-cube) robo-objs))
    (pop)
    (apply-transform (car robo-objs))
    
    ; get the state transform
    (let ((tx (get-transform)))
    
    (grab (car robo-objs))
    (concat tx)
    ; have to return the matrix before activating, as the physics wipes it
    (let ((temp (get-transform)))
        (ungrab)    
        (active-box (car robo-objs))
        temp)))

(define (robo-push)
    (push)
    (set! robo-rootstack (cons (car robo-rootstack) robo-rootstack)))

(define (robo-pop)
    (pop)
    (set! robo-rootstack (cdr robo-rootstack)))
       
(define (robo-connect pos axis axis2)
    (set! robo-joints (cons (build-hingejoint 
        (car robo-objs) (car robo-rootstack) pos axis) robo-joints))
    (robo-initjoint (car robo-joints))
    ; record this joint, and the objects it connects
    (set! robo-joints->objs (cons (list (car robo-joints) 
                                (list (car robo-objs) (car robo-rootstack)))
                                robo-joints->objs)))

(define (robo-initjoint j)
    (joint-param j "LoStop" -2) 
    (joint-param j "HiStop" 2)     
    (joint-param j "FMax" 100) 
    (joint-param j "FudgeFactor" 0))
 
(define (robo-angle-all a)
    (robo-angle-all-walk a robo-joints))

(define (robo-angle-all-walk a l)
    (joint-angle (car l) 20 a)
    (if (eq? (cdr l) '())
        0
        (robo-angle-all-walk a (cdr l))))

(define (robo-test-all)
    (robo-test-all-walk robo-joints 0))

(define (robo-test-all-walk l n)
    (joint-angle (car l) 1 (sin (+ n (time))))
    (if (eq? (cdr l) '())
        0
        (robo-test-all-walk (cdr l) (+ n 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(clear)
(hint-none)
(hint-wire)
(wire-colour (vector 1 1 1))
(blur 0.1)
(clear-frame 1)
(robo-clear)
(robo-world 10)
(gravity (vector 0 -10 0))

(define (chain)
        (robo-push)
        (rotate (vector (* (flxrnd) 180) (* (flxrnd) 180) (* (flxrnd) 180)))
        (robo-joint)
        (robo-joint)
        (if (eq? (random 2) 0) (chain))
        (robo-joint)
        (robo-joint)
        (robo-pop))

(push)

(push)
(scale (vector 2 0.1 1))
(define root (robo-block))
(pop)

(translate (vector 3 0 0))

(chain)
(pop)

(define (animate)
    (cond ((eq? (random 100) 0) 
        (grab root)
        (let ((tx (get-transform)))
        (ungrab)
        (push)
        (concat tx)
        (chain)
        (pop))))
    (robo-test-all))

(every-frame (animate))
