(desiredfps 10000)

(define (rv) 
    (vadd (vector (flxrnd) (flxrnd) (flxrnd)) (vector -0.5 -0.5 -0.5)))

(define (make-line)
    (let ((a (build-line 500)))
    (grab a)
    (pdata-add "vel" "v")
    (set-line (pdata-size))
    (ungrab)
    a))

(define (set-line n)
    (pdata-set "p" n (vector (* n 0.1) 0 0))
    (pdata-set "vel" n (vector 0.01 0.1 0))
    (pdata-set "c" n (vector 1 1 1))
    (if (zero? n)
        0
        (set-line (- n 1))))

(define (line-jitter line)
    (define (jitter n)
        (let ((h (inexact->exact (round (* (/ n (pdata-size)) 16)))))
        (pdata-set "p" n  (vadd (pdata-get "p" n) 
            (vmul (vector (gh h) (gh (+ h 1)) (gh (+ h 2))) 0.01))))
        (if (zero? n)
            0
            (jitter (- n 1))))
    (grab line)
    (jitter (pdata-size))
    (ungrab))

(define (line-anim line)
    (define (update-mass n)
        (let ((v (vmul (vnormalise (vsub (pdata-get "p" (+ n 1))
                    (pdata-get "p" n))) 5)))

        (set! v (vadd v (vmul (vsub (vector 0 0 0) (pdata-get "p" n))  0.1)))

        (pdata-set "vel" n (vmul (vadd (pdata-get "vel" n) v) 0.2)))
       
        (if (zero? n)
            0
            (update-mass (- n 1))))

    (grab line)
    (update-mass (pdata-size))
    (pdata-op "+" "p" "vel")
    (ungrab))

(clear)
(gain 100)
(blur 0)
(clear-colour (vector 0 0 0))
(hint-none)
(hint-unlit)
(hint-wire)
(line-width 2)
(wire-colour (vector 1 1 1))
(define test (make-line))
(define (render)
    (line-anim test)
    (line-jitter test))

(every-frame (render))
