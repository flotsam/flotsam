(clear)

(define (build char)
    (cond 
        ((char=? char #\+)
            (rotate (vector 45 0 0)))
        ((char=? char #\-)
            (rotate (vector -45 0 0)))
        ((char=? char #\f)
            (scale (vector 0.9 0.9 0.9))
            (translate (vector 0 0.5 0))
            (push)
            (scale (vector 0.1 1 0.1))
            (build-cube)
            (pop)
            (translate (vector 0 0.5 0)))
        ((char=? char #\[)
            (push))
        ((char=? char #\])
            (pop))))

(define (list-build strlist)
    (build (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (list-build (cdr strlist))))

(osc-source "4444")

(define (render)
    (if (osc-msg "/build")
        (begin
            (clear)
            (list-build (string->list (osc 0))))))

(every-frame "(render)")

