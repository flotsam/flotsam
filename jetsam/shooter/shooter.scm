;(require (lib "drflux.ss" "fluxus-0.14"))

(clear)

(define gun-position (vector 0 1 0))
(define bullets '())
(define terrain '())

(define ship 
  (with-state
    (hint-cast-shadow)
    (translate (vector 0 -5 0))
    (build-cube)))

(define ground
  (with-state
    (translate (vector 0 0 -4))
    (scale (vector 100 100 100))
    (build-plane)))

(define (get-position)
    (vtransform (vector 0 0 0) (get-transform)))

(define (add-bullet position)
    (set! bullets (cons 
        (with-state      
            (colour (vector 1 0 0))
            (translate position)
            (scale (vector 0.1 0.1 0.1))
            (build-cube))
        bullets)))

(define (add-bullet-spray position)
    (for-each
        (lambda (d)
            (set! bullets (cons 
                (with-state      
                    (hint-unlit)
                    (opacity 0.3)
                    (colour (vector 1 1 0))
                    (translate position)
                    (rotate (vector 0 
                        (* 90 (sin (* 3 (time)))) 
                        (* d 4)))
                    (scale (vector 0.1 0.1 0.1))
                    (build-cube))
                bullets)))
    '(-2 -1 0 1 2)))


(define (add-building)
    (set! terrain (cons  
        (with-state 
            (texture (load-texture "/home/dave/play/tex/flake.png"))
            (hint-cast-shadow)          
            (colour (vector 1 (flxrnd) (flxrnd)))
            (rotate (vmul (vector 0 0 (- (flxrnd) 0.5)) 45))
            (scale (vmul (vector (flxrnd) 
                (* (flxrnd) 5) (* 2 (flxrnd))) 6))
            (let ((p (build-cube)))
                (with-primitive p
                    (apply-transform p)
                     (translate (vector (* 20 (- (flxrnd) 0.5)) 50 -3))
                    p)))
        terrain)))

(define (update-bullets)
    (for-each
        (lambda (bullet)
            (with-primitive bullet
                (translate (vector 0 3 0))))
        bullets)
    (set! bullets 
        (filter
            (lambda (bullet)
                (with-primitive bullet
                    (let ((position (get-position)))
                        (cond 
                            ((> (vector-ref position 1) 20)
                                (destroy bullet)
                                #f)
                            (else #t)))))        bullets)))

(define (update-terrain)
    (if (eq? (random 20) 0)
        (add-building))

    (for-each
        (lambda (building)
            (with-primitive building
                (translate (vector 0 -0.5 0))))
        terrain)

    (set! terrain 
        (filter
            (lambda (building)                
                (with-primitive building
                    (let ((position (get-position)))
                        (cond 
                            ((< (vector-ref position 1) -25)
                                (destroy building)
                                #f)
                            (else #t)))))
            terrain)))


(define (update-ship)
  (with-primitive 
   ship
   (let ((position (get-position)))
       (if (key-special-pressed 100)
           (translate (vector -0.2 0 0)))
       (if (key-special-pressed 102)
           (translate (vector 0.2 0 0)))
       (if (key-special-pressed 103)
           (translate (vector 0 0 -0.2)))
       (if (key-special-pressed 101)
           (translate (vector 0 0 0.2)))
       (if (key-pressed " ")
           (add-bullet (vadd gun-position position)))
       (if (key-pressed "x")
           (add-bullet-spray (vadd gun-position position)))

       ;; clamp the position
       (if (> (vector-ref position 0) 10) 
           (translate (vector -0.2 0 0)))
       (if (< (vector-ref position 0) -10) 
           (translate (vector 0.2 0 0)))
       (if (> (vector-ref position 2) 10) 
           (translate (vector 0 0 -0.2)))
       (if (< (vector-ref position 2) -3.5) 
           (translate (vector 0 0 0.2))))))


(define (update)
  (update-terrain)
  (update-bullets)
  (update-ship))

(define light (make-light 'point 'free))
(light-position light (vector 400 -1000 500))
(light-diffuse light (vector 1 1 1))
(light-diffuse 0 (vector 0 0 0))
(light-specular 0 (vector 0 0 0))
(shadow-light light)
(lock-camera ship)
(clear-colour (vector 0 0 0))
(fog (vector 1 1 1) 0.0 1 100)

(every-frame (update))
