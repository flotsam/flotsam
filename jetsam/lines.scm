; a line following a line following a line following ...
; naturally resolves to either a loop or figure of 8 if left long enough
; (may take several hours with this many points)

(clear)
(hint-wire)
(hint-unlit)
(define lines (build-line 2000)) ; make a line with 2000 points

(with-primitive lines
    (pdata-add "vel" "v") ; make velocity pdata
    (pdata-map!           
        (lambda (p)
            (srndvec))    ; randomise initial positions
        "p")
    (pdata-map!
        (lambda (vel)
            (srndvec))    ; randomise initial velocity
        "vel"))

(every-frame                       ; animate
    (with-primitive lines          ; the line 
        (pdata-index-map!          ; for every velocity
            (lambda (i vel)
                (vmul (vnormalise  ; clamp the magnitude
                    (vadd vel      ; add to previous velocity
                        (vmul (vsub (pdata-ref "p" i)  ; follow the next position
                                    (pdata-ref "p" (+ i 1))) -0.01)))
                    0.01))
            "vel")
        (pdata-op "+" "p" "vel"))) ; finally, add all the velocities to all the positions
                     