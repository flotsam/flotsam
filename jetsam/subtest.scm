
(clear)
(line-width 3)
(pattern-init)

(pattern-voice 1 "sub" (list "cutoff" 0.8 "octaveA" 0 "typea" 4 "octaveb" 2 
                             "finefreqb" 1.0001 "typeb" 3
                             "attacka" 0 "decaya" 0.1 "sustaina" 0.2 "releasea" 0.7
                             "attackb" 0 "decayb" 0.1 "sustainb" 0.2 "releaseb" 0.7
                             "resonance" 0 "attackf" 0.0 "decayf" 0.6 "sustainf" 0.0
                             "volumef" 0.2 "volumea" 1 "volumeb" 1 "poly" 4))


(pattern-bpm 120)

(pattern 1 2 3 (list "!<1" 
        (list 
            (list "1" "21-----21+++o.++O.")
            (list "2" "[o.++o.--o...+++++++o...]"))))


(pattern-voice-volume 2)

(pattern-cascade '(1 2 3 4))









