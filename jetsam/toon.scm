(clear)
(define dirlight1 (vtransform (vector 1 0 0) (mrotate (vector 45 0 90))))
(hint-unlit)
;(load-from-path "objimport.scm")

(define (toon-light n)
    (let ((lighting (vdot (pdata-get "n" n) dirlight1)))
        (if (< lighting 0) (set! lighting 0.1)) 
        (if (> lighting 0.95) (set! lighting 0.95)) 
        (pdata-set "t" n (vector lighting 1 0)))
    (if (< n 1)
        0
        (toon-light (- n 1))))

(define (toon-outline n camerapos obpos)
    (let ((v (vadd obpos (pdata-get "p" n))))                           ; find the vertex in worldspace 
        (let ((i (vnormalise (vsub camerapos v))))                      ; incident direction (normalised)
            (pdata-set "t1" n (vector (vdot i (pdata-get "n" n)) 0 0)))) ; set s to the facing ratio (i dot n) 
    (if (< n 0)
        0
        (toon-outline (- n 1) camerapos obpos)))    

(define (deform n a)
    (pdata-set "p" n (vadd (pdata-get "pref" n) 
        (vmul (pdata-get "nref" n) 
                (* (sin (+ (time) (* 7 (f32vector-ref (pdata-get "p" n) 1))))
                    a))))
    (if (< n 1)
        0
        (deform (- n 1) a)))

(hint-multitex)
(multitexture 0 (force-load-texture "gradient.png"))
(multitexture 1 (force-load-texture "outline.png"))
(clear-colour (vector 0 0.5 1))

(colour (vector 0.9 0.5 1))
(define s (build-sphere 20 20))

;(scale (vector 10 10 10))
;(define s (obj-make (obj-load "/home/dave/code/fluxus/examples/meshes/bunny-1500.obj")))

(grab s)
(pdata-copy "p" "pref")
(pdata-copy "n" "nref")
(pdata-copy "t" "t1")
(ungrab)


(define (render)
    (grab s)
    (pdata-copy "pref" "p")
    (deform (pdata-size) 0.4)
    (recalc-normals 1)
    (toon-light (pdata-size))
    (toon-outline (pdata-size) 
        (vtransform (vector 0 0 0) (get-camera-transform)) 
        (vtransform (vector 0 0 0) (get-transform)))
    (ungrab))

(every-frame (render)) 
