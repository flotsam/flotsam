(clear)

(define speed 0.00001)
(define rotspeed 0.001)
(define rot (vector 0.0 0.0 0.0))

(define (init-particles n)
    (pdata-set "s" n (vector 30 30 30))
    (pdata-set "p" n (vmul (vsub (vector (flxrnd) (flxrnd) (flxrnd)) 
                            (vector 0.5 0.5 0.5)) 20000))
    (pdata-set "c" n (vector (flxrnd) (flxrnd) (flxrnd)))
    (if (zero? n)
        0
        (init-particles (- n 1))))

(define (build-world)
    (texture (load-texture "world.png"))
    (let ((p (build-particles 10000)))
    (grab p)
    (init-particles (pdata-size))
    (ungrab)))


(push)
(scale (vector 1 1 2))
(define s (build-cube))
(pop)

(apply-transform s)
(lock-camera s)

(define vel (vector 0 0 0))

(build-world)

(define (update)
    (if (key-pressed "a")
        (set! rot (vadd rot (vector rotspeed 0 0))))
    (if (key-pressed "q")
        (set! rot (vadd rot (vector (- rotspeed) 0 0))))
    (if (key-pressed "p")
        (set! rot (vadd rot (vector 0 rotspeed 0))))
    (if (key-pressed "o")
        (set! rot (vadd rot (vector 0 (- rotspeed) 0))))
    (if (key-pressed " ")
        (set! speed (+ speed 0.01)))
    (grab s)
    (rotate rot)
    (display rot)(newline)
    (translate (vector 0 0 speed))
    (ungrab)
    (set! rot (vmul rot 0.999))
    (set! speed (* speed 0.999)))


(every-frame "(update)")

