(define (render n x)
    (cond
        ((not (zero? n))
            (colour (vmul (vector n x (sin (time))) 0.1))
            (rotate (vector 
                (+ (* x 2) (* 20 (sin (time)))) 
                0 (* 30 (cos (time)))
                ))
            (translate (vector 2.2 0 0))
            (push)
            (scale (vector 1 1 1))
            (draw-torus)
            (pop)
            (render (- n 1) x))))

(define (render2 n)
    (cond
        ((not (zero? n))
            (translate (vector 0 2 0))
            (push)
            (render 20 n)
            (pop)
            (render2 (- n 1)))))

(opacity 0.2)
(every-frame (render2 10))

(blur 0.01)