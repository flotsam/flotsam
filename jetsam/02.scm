
(clear)
(ortho)
(pattern-init)
(line-width 4)
(pattern-samples 1 "rip")
(pattern-samples 2 "attk")

(pattern-sample-volume 5)
(pattern-sample-pitch 1)

(pattern 1 2 1 (list "!<111+++1" (list (list "1" "[---o.o.++o...o...o...]"))))
(pattern 2 4 1 (list "!<11111+++1" (list (list "1" "[o.++o.--o.]"))))

(pattern-voice 3 "sub" '("typea" 2 "typeb" 3 "cutoff" 0.2 
    "freqa" 0.2501 "freqb" 0.25 "resonance" 0.4 "decaya" 0.3 "decayb" 0.3 
    "mainvolume" 2 "crushbits" 5 "crushfreq" 0.2))

(pattern 3 4 2 (list "!<1" 
    (list
        (list "1" "1+++2+++1") 
        (list "2" "[O..o..O..o.]"))))

(pattern-voice 4 "sub" '("typea" 2 "typeb" 3 "cutoff" 0.5 
    "freqa" 0.5001 "resonance" 0.2 "decaya" 0.1 "decayb" 0.2 "mainvolume" 2))

(pattern 4 4 3 (list "!<1" 
    (list
        (list "1" "1---1+++2") 
        (list "2" "[O..o..]"))))
