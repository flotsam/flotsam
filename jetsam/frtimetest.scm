(require (all-except (lib "frp-core.ss" "frtime") undefined?))

(define mouse-left (event-receiver))

(define (read-inputs)
  (cond ((zero? (random 10)) 
      (printf "testing...~n")
      (send-event mouse-left #t))))

; syncronising rendering with frtime
(define (loop)
  ;; emit the next pulse
  (read-inputs)
  ;; wait for frtime to update
  (do-in-manager-after (void))
  (loop))

(thread loop)  

(define test 
  (hold 
   (map-e 
    (lambda (e) 
      (printf "!!!~n")) 
    mouse-left)))