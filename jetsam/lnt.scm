(start-audio "noiseweapon:Out0" 1024 44100)
(osc-source "4004")
(desiredfps 25)
(clear)
(hint-none)
(hint-wire)
(clear-colour (vector 0 0.5 1))
(define angle 10)

(define depth 0)

(define obs '())
(define current-ob 0)
(define current-pattern 0)

(define (build ch)
    (cond 
        ((char=? ch #\+)
            (rotate (vector 0 0 angle)))
        ((char=? ch #\-)
            (rotate (vector 0 0 (- angle))))
        ((char=? ch #\/)
            (rotate (vector 0 angle 0)))
        ((char=? ch #\\)
            (rotate (vector 0 (- angle) 0)))
        ((or (char=? ch #\o) (char=? ch #\O))
            (scale (vector 0.9 0.9 0.9))
            (translate (vector 0 0.5 0))
            (push)
            (scale (vector 0.1 1 0.1))
            (set! obs (cons (build-cube) obs))
            (pop)
            (translate (vector 0 0.5 0)))
        ((char=? ch #\[)
            (set! depth (+ depth 1))
            (push))
        ((char=? ch #\])
            (cond 
                ((> depth 0)
                    (set! depth (- depth 1))
                    (pop))))))

(define (list-build strlist)
    (build (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (list-build (cdr strlist))))

(define (destroy-all l)
    (destroy (car l))
    (if (eq? (cdr l) '())
        0
        (destroy-all (cdr l))))


(define (fix-stack)
    (if (< depth 1)
        0 
        (begin 
            (pop)
            (set! depth (- depth 1))
            (fix-stack)))) 

(define (highlight)
    (cond
        ((< current-ob (length obs))
            (grab current-ob)
            (hint-solid)
            (colour (vector 1 0 0))
            (ungrab))))

(define (unhighlight)
    (cond
        ((< current-ob (length obs))
            (grab current-ob)
            (hint-none)
            (hint-wire)
            (colour (vector 1 1 1))
            (ungrab))))

(define (render)
    (if (osc-msg "/modifystr")
        (begin
            (if (not (eq? obs '())) (destroy-all obs))
            (list-build (string->list (osc 2)))
            (fix-stack)
            (set! current-pattern (osc 0))))
    (if (and (osc-msg "/play") (eqv? (osc 2) current-pattern))
                (begin
                    (unhighlight)
                    (set! current-ob (osc 8))
                    (display (osc 8))(newline)
                    (highlight))))

(every-frame "(render)")

