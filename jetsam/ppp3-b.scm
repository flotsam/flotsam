;(clear)

(push)
(define pattern-display 0)
(pattern 1 2 2 
    (list "!<111+++1" 
        (list 
            (list "1" "21[+++21]")
            (list "2" "//[o..o..o.+++.o.+++++o.]"))))

(translate (vector 2 0 0))
(pattern 2 2 3 
    (list "!<1" 
        (list 
            (list "1" "[O.o..+++++++++++O.]----1++++1"))))

(translate (vector 2 0 0))

(pattern 3 2 3 
    (list "!<111+++++1" 
        (list 
            (list "1" "2222222[+++2]")
            (list "2" "[o..+++o..]"))))

(translate (vector 2 0 0))
(pattern 4 1 5 (list "!<1" (list (list "1" "O..o..O.++o[1].+++o.."))))
(translate (vector 2 0 0))

(pattern 5 1 4 (list "!<1" (list (list "1" "O..o..O.++o[1].+++o.."))))
(translate (vector 2 0 0))

(pattern 6 1 6 (list "!<1" (list (list "1" "O..o..O.++o[1]..+++O."))))
(translate (vector 2 0 0))

(pattern 7 1 5 (list "!<1" (list (list "1" "O..o.O.++o[1].+++o..."))))
(translate (vector 2 0 0))


(pattern-cascade '(3 4 6 7))
(pop)