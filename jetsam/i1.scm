(clear)
(ortho)
(load (full-path "stroke.scm"))
(line-width 3)
(pattern-init)

(pattern-voice 1 "sub" (list "cutoff" 0.8 "typea" 4  
                             "freqb" 0.5001 "typeb" 3
                             "attacka" 0 "decaya" 0.1 "sustaina" 0.2 "releasea" 0.7
                             "attackb" 0 "decayb" 0.1 "sustainb" 0.2 "releaseb" 0.7
                             "resonance" 0 "attackf" 0.0 "decayf" 0.6 "sustainf" 0.0
                             "volumef" 0.2 "volumea" 1 "volumeb" 1 "poly" 4))


(pattern-voice 2 "fm" (list  "freq" 0.5 "type" 0 "modfreq" 0.501
                             "modtype" 3 "attack" 0 "decay" 0.1 "sustain" 0.2 
                             "release" 0.2 "volume" 0.2 "moddecay" 0.5 
                             "modvolume" 0.2 "poly" 2))


(pattern-voice 3 "sub" (list "cutoff" 0.3 "typea" 7 "freqb" 0.250001 "typeb" 0 
                             "attacka" 1 "attackb" 1
                             "ftype" 0 "decaya" 20.2 "decayb" 20.2 "resonance" 0.1
                             "attackf" 0.6 "decayf" 0.1 "sustainf" 0.0 "volumef" 0.2
                             "volumea" 2 "volumeb" 2 "lfodepth" 0.2 "lfofreq" 0.1
                             "poly" 4 "distort" 0))

(pattern-bpm 100)

(define p '(
            ("1" "2++1")
            ("2" "[o.--O.]")))

(pattern 1 2 2 (list "!<1" p))

(pattern 2 2 2 (list "!<..1" p))

(pattern 3 2 2 (list "!<....1" p))


(pattern-voice-volume 2)

(pattern-cascade '(1 2 3))
































