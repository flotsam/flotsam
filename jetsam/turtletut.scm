
(define (strip n)
    (define (row c)            ; simply make a row of vertices
        (turtle-vert)
        (turtle-move 1)
        (turtle-skip 1)        ; and skip one, so we set every other vert
        (if (equal? c 1)       ; until c is 1
            0
            (row (- c 1))))

    (turtle-push)    ; record the turtle position
    (row n)          ; do a row
    (turtle-pop)     ; reset the position as before

    (turtle-turn (vector 0 90 0))   ; turn 90 degrees
    (turtle-move 1)                 ; move the width of the strip
    (turtle-turn (vector 0 -90 0))  ; turn back to face the same direction as before

    (turtle-skip (- (- (* n 2) 1))) ; rewind the current vert to the start + 1
    
    (turtle-push)   
    (row n)          ; do the second row
    (turtle-pop))

(define (ring n radius)
    (define (circle c angle)              ; make a circle of vertices
        (turtle-move radius)              ; move out to the edge
        (turtle-vert)                     ; make our vertex
        (turtle-move (- radius))          ; move back again
        (turtle-turn (vector 0 0 angle))  ; turn to face the new direction
        (turtle-skip 1)                   ; and skip one, so we set every other vert
        (if (equal? c 1)                  ; until c is 1
            0
            (circle (- c 1) angle)))

    (let ((angle (/ 360 (- n 1))))  ; figure out the angle per section

    (turtle-push)                   ; record the turtle position
    (circle n angle)                ; do a circle
    (turtle-pop)                    ; reset the position as before

    (turtle-turn (vector 0 90 0))   ; turn 90 degrees
    (turtle-move 1)                 ; move the width of the strip
    (turtle-turn (vector 0 -90 0))  ; turn back to face the same direction as before

    (turtle-skip (- (- (* n 2) 1))) ; rewind the current vert to the start + 1
    
    (turtle-push)   
    (circle n angle)                ; do a circle
    (turtle-pop)

    (turtle-skip -1)))              ; hide the degenerate triangle

(define (torus n radius1 n2 radius2)
    (define (circle c angle)              ; make a circle of vertices
        (turtle-move radius2)              ; move out to the edge
        (turtle-vert)                     ; make our vertex
        (turtle-move (- radius2))          ; move back again
        (turtle-turn (vector 0 angle 0))  ; turn to face the new direction
        (turtle-skip 1)                   ; and skip one, so we set every other vert
        (if (equal? c 1)                  ; until c is 1
            0
            (circle (- c 1) angle)))

    (define (section c angle)              ; make a circle of vertices
        (turtle-move radius1)              ; move out to the edge
        (turtle-push)
        (turtle-turn (vector 0 0 90))  ; turn to face the new direction
        (circle n2 (/ 360 n2))                     ; make our vertex
        (turtle-pop)
        (turtle-move (- radius1))          ; move back again

        (turtle-turn (vector 0 0 angle))  ; turn to face the new direction
        (turtle-skip (- (- (* c 2) 1))) ; rewind the current vert to the start + 1


        (turtle-move radius1)              ; move out to the edge
        (turtle-push)
        (turtle-turn (vector 0 0 90))  ; turn to face the new direction
        (circle n2 (/ 360 n2))                     ; make our vertex
        (turtle-pop)
        (turtle-move (- radius1))              ; move out to the edge

        (turtle-skip -1) ; rewind the current vert to the start + 1

        (if (equal? c 1)                  ; until c is 1
            0
            (section (- c 1) angle)))

    (let ((angle (/ 360 (- n 1))))  ; figure out the angle per section
    (section n angle)))

(define (torus2 n radius n2 radius2)

    (define (ring n radius)
        (define (circle c angle)              ; make a circle of vertices
            (turtle-move radius)              ; move out to the edge
            (turtle-vert)                     ; make our vertex
            (turtle-move (- radius))          ; move back again
            (turtle-turn (vector 0 0 angle))  ; turn to face the new direction
            (turtle-skip 1)                   ; and skip one, so we set every other vert
            (if (equal? c 1)                  ; until c is 1
                0
                (circle (- c 1) angle)))

        (let ((angle (/ 360 (- n 1))))  ; figure out the angle per section

        (turtle-push)                   ; record the turtle position
        (circle n angle)                ; do a circle
        (turtle-pop)                    ; reset the position as before
    
        (turtle-turn (vector 0 90 0))   ; turn 90 degrees
        (turtle-move 1)                 ; move the width of the strip
        (turtle-turn (vector 0 -90 0))  ; turn back to face the same direction as before
    
        (turtle-skip (- (- (* n 2) 1))) ; rewind the current vert to the start + 1
        
        (turtle-push)   
        (circle n angle)                ; do a circle
        (turtle-pop)
    
        (turtle-skip -1)))              ; hide the degenerate triangle

    (ring n2 radius2))
    
    
(clear)
(hint-none)
(hint-wire)
(wire-colour (vector 0 1 0))
(backfacecull 0)
(turtle-reset)
(show-axis 1)
(define size 400)
(define ob (build-polygons (* size 1) 0))
(turtle-attach ob)
;(strip size)

;(ring size 5)
;(ring size 5)

(torus2 10 10 10 1)