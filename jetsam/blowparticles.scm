; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define (rndvec)
    (vadd (vector (flxrnd) (flxrnd) (flxrnd)) (vector -0.5 -0.5 -0.5)))

(define (anivec)
    (vector (sin (* (time) 20)) (cos (* (time) 20)) 0))

(define (make-particle-system pos)
    (define (init-particles n pos)
        (pdata-set "s" n (vector 1 1 1))
        (pdata-set "p" n pos)
        (pdata-set "age" n (vector (flxrnd) 0 0))
        (pdata-set "c" n (vector 1 1 1))
        (pdata-set "vel" n (vmul (rndvec) 2))
        (if (zero? n)
            0
            (init-particles (- n 1) pos)))
    (let ((p (build-particles 500)))
    (grab p)
    (hint-none)
    (hint-points)
    (point-width 5)
    (hint-anti-alias)
    (pdata-add "vel" "v")
    (pdata-add "age" "v")
    (init-particles (pdata-size) pos)
    (ungrab)
    p))

(define (particle-system-mod ps dir)
    (grab ps)
    (pdata-op "+" "vel" dir)
    (ungrab))
    

(define (particle-system-update ps amount)
    (define (update-particles n)
        (cond 
            ((< (f32vector-ref (pdata-get "age" n) 0) 0.01)
                (pdata-set "p" n (vector 0 0 0))
                (pdata-set "vel" n (vmul (anivec) 0.1))
                (pdata-set "age" n (vector (flxrnd) 0 0))))

        (pdata-set "vel" n (vadd (pdata-get "vel" n) (vmul (rndvec) amount)))

        (if (zero? n)
            0
            (update-particles (- n 1))))
    (grab ps)
    (update-particles (pdata-size))
    (pdata-op "+" "vel" (vector 0 -0.001 0))
    (pdata-op "+" "p" "vel")
    (pdata-op "*" "age" 0.99)
    (ungrab))

(clear)

(hint-none)
(hint-wire)
(wire-colour (vector 1 1 1))
(line-width 5)

(define ps (make-particle-system (vector 0 0 0)))

(define dir (vector 0 0 0))

(define (render n)

    (particle-system-update ps (gh 9))
    (set! dir (vmul dir 0.9)))

(every-frame (render 10))
