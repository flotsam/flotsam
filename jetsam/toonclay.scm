(clear)

(load "clay.scm")

(colour (vector 0.5 0.9 1))
(define shape (build-polygons 1002 2))
(toon-setup shape)

(gain 0)
(backfacecull 0)
(blur 0)

(define width-vec (make-vector 18 0.1))


(define (build n) 
    (if (not (zero? n))
        (vector-set! width-vec n (vector-ref width-vec (- n 1))))
    (extrude-push)
    (extrude-radius-set (vector-ref width-vec n))
    (turtle-turn (vector 0 (* 0 (sin (time))) 0))
    (extrude-pop)
    (if (zero? n)
        0
        (build (- n 1))))

(clear-colour (vector 0 0.3 0.5))

(show-fps 1)
(extrude-begin shape)
(build 17)
(extrude-end)

(grab shape)
;(poly-convert-to-indexed)
(ungrab)


(define (render)
    (vector-set! width-vec 0 (+ 0.3 (* 0.5 (sin (gh 1)))))
    (extrude-begin shape)
    (build 17)
    (extrude-end)
    (toon-shade shape))

(every-frame (render))
