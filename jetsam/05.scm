(clear)
(ortho)
;(pattern-reset)
(pattern-init)
(push)

(pattern-voice 1 "sub" '("typea" 2 "typeb" 2 "cutoff" 0.6  
    "freqa" 1.01 "decaya" 0.1 "decayb" 0.1 "sustaina" 0.2 "sustainb" 0.2 
        "mainvolume" 2 "resonance" 0.4 "poly" 2 "slidea" 0.2 "slideb" 0.2))

(pattern 1 4 3 (list "!<1" (list (list "1" "o..1O.---1---o..[++++++++++++o]++++++."))))

(pattern-voice 2 "sub" '("typea" 2 "typeb" 2 "cutoff" 0.2 "ring" 1 "ftype" 3 
    "freqa" 0.501 "freqb" 1.5 "decaya" 0.5 "decayb" 0.5 "sustaina" 0.0 "sustainb" 0.0 
        "mainvolume" 5 "resonance" 0.4 "poly" 1 "slidea" 0.2 "slideb" 0.2))

(pattern 2 4 3 (list "!<1" (list (list "1" "o.1O.---1--o.[++++++++++++o]+++++."))))

(pattern-samples 3 "rip")
(pattern-sample-volume 2)
(pattern 3 4 1 (list "!<111[++2]111[++2]111[++2]111[++2]" 
    (list (list "1" "[++o..++o..]")
          (list "2" "[+o...o.+o..o..]"))))

(pattern-cascade '(3 2 1))


(pop)
