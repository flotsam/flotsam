; press spacebar to destroy a sphere

(define (rndvec)
    (vsub (vector (flxrnd) (flxrnd) (flxrnd))
          (vector 0.5 0.5 0.5)))

(define (init n)
    (pdata-set "c" n (vector 1 1 1))
    (if (zero? n)
        0
        (init (- n 1))))

(define (explode n)
    (let ((m (* (+ 0.5 (/ (vector-ref (pdata-get "p" n) 1) 2)) 0.5)))
    (set! m (* m m m))
    (pdata-set "p" n (vadd (pdata-get "p" n) (vmul (rndvec) m)))
    (pdata-set "c" n (vadd (vector (vector-ref (pdata-get "c" n) 0)
                                   (vector-ref (pdata-get "c" n) 1)
                                   (vector-ref (pdata-get "c" n) 2))
                                 (vmul (rndvec)
                                     (* m 20))))
    (pdata-set "t" n (vadd (pdata-get "t" n) (vmul (rndvec) m)))
    (if (zero? n)
        0
        (explode (- n 1)))))

(clear)
(clear-colour (vector 1 1 1))
(backfacecull 0)
(push)
(hint-vertcols)
;(hint-unlit)
(texture (load-texture "robo.png"))
(define ob (build-sphere 30 30))
(pop)
(grab ob)
(init (pdata-size))
(ungrab) 

(define (animate)
    (grab ob)
    (when (key-pressed " ") 
        (explode (pdata-size)))
    (ungrab))

(every-frame (animate))
