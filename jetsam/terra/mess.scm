(clear)
(backfacecull 0)
(hint-unlit)
(scale (vector 10 10 10))
(texture (load-texture "ah.png"))
(define shape (build-sphere 20 20))
(texture (load-texture "ag.png"))
(translate (vector 0 2 0))
(define shape2 (build-sphere 20 20))


(define (animate)
    (with-primitive shape    
        (pdata-index-map! 
            (lambda (i p)
                (vmul (vector
                    (sin (+ i (* 0.1 (time))))
                    (cos (+ i (* 0.1 (time))))
                    (* i 0.001))
                  (* 1 (sin (+ (* i 0.1 (cos (* (time) 0.002))) (* (time) 0.3))))))
            "p"))
    (with-primitive shape2    
        (pdata-index-map! 
            (lambda (i p)
                (vmul (vector
                    (sin (+ i (* 0.01 (time))))
                    (cos (+ i (* 0.01 (time))))
                    (* i 0.001))
                  (sin (+ (* i 0.1 (cos (* (time) 0.02))) (* (time) 0.3)))))
            "p")))

(every-frame (animate))