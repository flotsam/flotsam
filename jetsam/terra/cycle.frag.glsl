uniform float Time;
uniform vec3 Centre;
uniform sampler2D Gradient;
uniform int PaletteSize;
uniform float Gap;

varying vec3 P;
varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec4 S;

void main()
{ 
	float d = floor(distance(P,Centre)/Gap+floor(Time))/PaletteSize;
	gl_FragColor = texture2D(Gradient,vec2(d,0));
}
