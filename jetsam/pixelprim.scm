(clear)

(define (clear-pixels n)
    (pdata-set "c" n (vector 0 0 0 0))
    (if (zero? n)
        0
        (clear-pixels (- n 1))))

(define (spray n)
    (pdata-set "c" (random (pdata-size)) 
        (vector (flxrnd) (flxrnd) (flxrnd)))
    (if (zero? n)
        0
        (spray (- n 1))))

(define (expand n dst dist)
    (let ((v (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist))))
    (grab dst)
    (pdata-set "p" n v)
    (ungrab))
    (if (zero? n)
        0
        (expand (- n 1) dst dist)))

(define (shell n dist incr)
    (expand (pdata-size) (build-cube) dist)
    (if (zero? n)
        0
        (shell (- n 1) (+ dist incr) incr)))

(define p (build-pixels 100 100))
(grab p)
(clear-pixels (pdata-size))
(spray 2000)
(upload-pixels)
(ungrab)

(scale (vector 3 3 3))
(define ob (build-cube))

(push)
(texture (pixels->texture p))
(hint-unlit)
(grab ob)
(shell 30 0 0.003)
(ungrab)
(pop)

(clear-colour (vector 1 1 1))
