(clear)
(define a (build-cube))
(active-box a)

(push)
(translate (vector 2 0 0))
(define b (build-cube))
(active-box b)
(pop)

(define j (build-hingejoint a b (vector 1 0 0) (vector 0 1 0)))

(ground-plane (vector 0 1 0) 0)
(collisions 1)
(show-axis 1)

(joint-angle j 1 10)