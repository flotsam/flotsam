(gain 0.1)
(clear)

(colour (vector 0.5 0.5 0.5))
;(hint-normal)
(define dirlight1 (vtransform (vector 1 0 0) (mrotate (vector 0 0 45))))

(texture (force-load-texture "tex/paint.png"))
(hint-unlit)
(hint-wire)
(line-width 4)

(push)
(scale (vector -100 -100 -100))
;(build-sphere 10 10)
(pop)

(define (toon-init n)
    (pdata-set "t" n (vadd (vmul (pdata-get "t" n) 0.1) (vector 0 (* (flxrnd) 0.5) 0)))
    (if (< n 1)
        0
        (toon-init (- n 1))))

(define (toon-light n)
    (let ((lighting (vdot (vtransform-rot (pdata-get "n" n) (get-transform)) dirlight1)))
        (if (< lighting 0) (set! lighting 0)) 
        (if (> lighting 0.9) (set! lighting 0.9)) 
        (pdata-set "t" n (vadd (pdata-get "tref" n) (vector lighting 0 0))))
    (if (< n 1)
        0
        (toon-light (- n 1))))

(ground-plane (vector 0 1 0) 0)
(collisions 1)

(define obs '())

(define (make)
    (push)
    (scale (vector 0.9 0.9 0.9))
    (if (not (eq? obs '())) (parent (car obs)))
    (if (< 1 (flxrnd))
        (set! obs (cons (build-sphere 5 5) obs))
        (set! obs (cons (build-cube) obs)))

    (pop)
        
    (grab (car obs))
    (toon-init (pdata-size))
    (pdata-copy "t" "tref")
    (ungrab))

(define (make-line n)
    (push)
    (translate (vector 1 0 0))
    (rotate (vector 0 0 45))
    (make)
    (pop)
    (if (zero? n)
        0
        (make-line (- n 1))))

(make-line 20)
(ortho)
(gain 10)

(define (animate l n)
    (grab (car l))
    (rotate (vector (gh n) (gh (+ n 2)) 0))
    (toon-light (pdata-size))
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (animate (cdr l) (+ n 1))))

(blur 0.01)

(every-frame (animate obs 0)) 