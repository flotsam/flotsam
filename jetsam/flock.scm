
(define l '())

(define (make-agent)
    (push)
    (colour (vector (flxrnd)(flxrnd)(flxrnd)))
    (set! l (cons (vector (vmul (vector (flxrnd) 0 (flxrnd)) 0.1) (build-cube)) l))
    (pop))


(define (find-closest-agent agent)
    (grab (vector-ref agent 1))
    (let ((pos (vtransform (vector 0 0 0) (get-transform))))
        (ungrab)
        (find-closest-agent-walk (vector-ref agent 1) pos 0 99999 l)))

(define (find-closest-agent-walk ob pos closest dist l)
    (cond 
        ((not (eq? ob (vector-ref (car l) 1)))
            (grab (vector-ref (car l) 1))
            (let ((cpos (vtransform (vector 0 0 0) (get-transform))))
                (let ((newdist (vdist pos cpos)))        
                    (cond 
                        ((< newdist dist)
                            (set! dist newdist)
                            (set! closest (car l))))))
            (ungrab)))
    (if (eq? (cdr l) '())
        closest
        (find-closest-agent-walk ob pos closest dist (cdr l))))

(define (update-agents l)
    (grab (vector-ref (car l) 1))
    (let ((pos (vtransform (vector 0 0 0) (get-transform))))
        (ungrab)
        (let ((closest (find-closest-agent (car l))))
             (grab (vector-ref closest 1))
             (let ((closestpos (vtransform (vector 0 0 0) (get-transform))))
                 (ungrab)
                 (let ((closestdir (vmul (vnormalise (vsub pos closestpos)) 0.1)))
                     (vector-set! (car l) 0 (vadd (vector-ref (car l) 0) closestdir))                     
                     (let ((centre (vmul (vnormalise (vsub (vector 0 0 0) pos)) 0.1)))
                         (vector-set! (car l) 0 (vadd (vector-ref (car l) 0) centre)))                    
                     (vector-set! (car l) 0 (vmul (vector-ref (car l) 0) 0.95))                        
                     ))))

    (grab (vector-ref (car l) 1))
    (translate (vector-ref (car l) 0))
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (update-agents (cdr l))))


(define (make-flock n)
    (push)
    (translate (vector (flxrnd) (flxrnd) (flxrnd)))
    (make-agent)
    (pop)
    (if (< n 1)
        0
        (make-flock (- n 1))))
    

(clear)
(hint-wire)
(line-width 4)
(clear-colour (vector 0.6 0.6 1))
(make-flock 10)
(blur 0)
(define (render)
    (update-agents l))

(desiredfps 10000)
(show-fps 1)
(every-frame (render))


