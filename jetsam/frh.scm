(clear)

(define scale "o.---o.[!++O].-o.")

(pattern 1 2 6 (list "!<111++1" 
    (list 
        (list "1" "222[+++++2]")
        (list "2" "//[o.++++o.+++o.++++o.]"))))


(pattern 2 2 2 (list "!1<11++1@" 
    (list 
        (list "1" "22[++22]")
        (list "2" scale))))

(pattern 3 2 2 (list "!<11<1++1@" 
    (list 
        (list "1" "22.[++22]")
        (list "2" scale))))

(pattern 4 2 2 (list "!11<1++1@" 
    (list 
        (list "1" "2[++2...2]")
        (list "2" scale))))


(pattern 5 2 2 (list "!<111<++1@" 
    (list 
        (list "1" "22[++22]")
        (list "2" "//[o.+o..+o.+o..]"))))

(pattern-cascade '(1 5 2 3 4))
