(load-extension "/usr/local/lib/plt/collects/fluxus-0.12/extensions/fluxus-osc.so")
(require fluxus-osc)

(osc-destination "osc.udp://127.0.0.1:1234")

(osc-send "/show-code" "sif" '("testing..." 0 5))
(osc-send "/start-video" "s" '("/home/dave/pixels/movies/Swim.ogg"))