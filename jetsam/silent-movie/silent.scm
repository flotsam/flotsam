;;----------------------------------------------------------------------
;; osc interface:
;; /start-video s [path-to-ogg-theora-video]
;; /show-code sif [code-string, card-back-num, time-to-display-seconds]
;;----------------------------------------------------------------------

;; requires the video texture extension
(load-extension (string-append fluxus-collects-location "fluxus-" fluxus-version
    "/extensions/fluxus-video-texture.so"))
(require fluxus-video-texture)

(clear)

(osc-source "1234") ; port to listen to

; add more textures to this vector
(define back-textures (vector "back1.png" "back2.png" "back3.png"))

; video aspect ratio
(define aspect-ratio 0.75)

; video fps (todo - not exact)
(desiredfps 25)

(define (make-card)
    (define back 0)
    (define grain 0)
    (define text 0)
    (define hide-time 0)
    (define hidden #f)
    
    (define (build)
        ; make the background card
        (hint-unlit)
        (push)
        (scale (vector 3 -2.5 1))
        (translate (vector 0 0 -0.01))
        (set! back (build-plane))
        (pop)

        ; make the film grain
        (push)
        (hint-depth-sort)
        (scale (vector 3 3 1))
        (colour (vector 0.2 0.2 0.2))
        (texture (load-texture "grain1.png"))
        (set! grain (build-plane))
        (pop)

        (set! text (build-text "hello"))

        ; squash the texture coords a little to make the grain finer
        (grab grain)
        (pdata-set "t" 0 (vector 0 0 0))
        (pdata-set "t" 1 (vector 3 0 0))
        (pdata-set "t" 2 (vector 3 1 0))
        (pdata-set "t" 3 (vector 0 1 0))
        (ungrab)

        (hide-card))

    (define (show t)
        (grab back)
        (hide 0)
        (ungrab)
        (grab grain)
        (hide 0)
        (ungrab)
        (grab text)
        (hide 0)
        (ungrab)
        (set! hidden #f)
        (set! hide-time (+ (time) t)))

    (define (hide-card)
        (grab back)
        (hide 1)
        (ungrab)
        (grab grain)
        (hide 1)
        (ungrab)
        (grab text)
        (hide 1)
        (ungrab)
        (set! hidden #t))


    (define (set-text! str)
        ; build the text
        (if (not (zero? text)) (destroy text))
        (push)
        (colour (vector 0.7 0.7 0.7))
        (scale (vector 0.15 0.15 0.15))
        (translate (vector -5 4 0))
        (texture (load-texture "font.png"))
        (set! text (build-text str))
        (pop))

    (define (set-back! texnum)
        (grab back)
        ; wrap the texture number to prevent errors
        (texture (load-texture (vector-ref back-textures
            (modulo texnum (vector-length back-textures)))))
        (ungrab))

    (define (animate)

        ; apply an offset to animate the film grain
        (define (anim-grain n x y)
            (pdata-set "t" n (vadd (pdata-get "t" n) (vector x y 0)))
            (if (zero? n)
                0
                (anim-grain (- n 1) x y)))

        (cond 
            ((not hidden)

                (grab grain)
                ; jitter the grain left and right while moving it down fast
                (anim-grain (- (pdata-size) 1) (* (- (flxrnd) 0.5) 0.1) 0.09)
                (ungrab)
        
                ; see if it's time to hide again
                (if (> (time) hide-time)
                    (hide-card)))))
    
    ; diy oo
    (define (dispatch m)
        (cond
            ((eq? m 'build) build)
            ((eq? m 'set-text!) set-text!)
            ((eq? m 'set-back!) set-back!)
            ((eq? m 'animate) animate)
            ((eq? m 'show) show)
              (else (error "unknown method" m))))
    dispatch)

;;------------------------------------------------------
;; video stuff

(define video-texture 0)
(push)
(hint-unlit)
(colour (vector 0 0 0.3))
(translate (vector 0 0 -10))
(scale (vector 3 (- (* 3 aspect-ratio)) 1))
(define video-plane (build-plane))
(pop)

(define (start-video filename)
    (clear-video-textures)
    (set! video-texture (load-video-texture filename))
    (grab video-plane)
    (colour (vector 1 1 1))
    (texture video-texture)
    (ungrab))

;;------------------------------------------------------

(define card (make-card))
((card 'build))

; fix the camera
(set-camera-transform (mtranslate (vector 0 0 -1)))
(ortho)

(define (update)
    (cond
        ((osc-msg "/show-code")
            ((card 'set-text!) (osc 0))
            ((card 'set-back!) (osc 1))
            ((card 'show) (osc 2)))
        ((osc-msg "/start-video")
            (start-video (osc 0))))
    (if (not (zero? video-texture))
        (update-video-texture video-texture))
    ((card 'animate)))

(every-frame (update))
