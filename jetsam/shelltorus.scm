(define tex (list
    (load-texture "shell1.png")
    (load-texture "shell2.png")
    (load-texture "shell3.png")
    (load-texture "shell4.png")
    (load-texture "shell5.png")))

(define (make-shell n type col dist)
    (define (loop n target)
        (let ((vert (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist)))
               (tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "p" n vert)
        (pdata-set "t" n tex))
        (ungrab)
        (if (zero? n)
            0
            (loop (- n 1) target)))
    
    (let ((target (build-polygons (pdata-size) type)))

    (grab target)
    (colour col)
    (texture (load-texture "hair5.png"))
 ;   (texture (list-ref tex n))
    (ungrab)

    (loop (pdata-size) target)
    (grab target)
    (recalc-normals 1)
    (ungrab)
    target))

(define (make-shells c n dist type col l)
    (if (eq? c n)
        l
        (make-shells c (+ n 1) dist type col
            (cons (make-shell n type (vmul col (* n 2)) (* dist n)) l))))

(define (animate-shells l)
    (define (animate n)
        (let ((vary (f32vector-ref (pdata-get "p" n) 1)))
        (pdata-set "t" n (vadd (pdata-get "t" n) 
            (vmul (vector (sin (+ vary (* (time) 2))) 
                          (cos (+ vary (* (time) 2))) 0) 
                0.001))))
        (if (zero? n)
            0
            (animate (- n 1))))

    (define (copy n target)
        (let ((tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "t" n (vadd 
            (vmul tex 0.3)
            (vmul (pdata-get "t" n) 0.7)))
        (ungrab))
        (if (zero? n)
            0
            (copy (- n 1) target)))

    (define (transfer l)
        (grab (car l))
        (copy (pdata-size) (car (cdr l)))
        (ungrab)
        (if (null? (cdr (cdr l)))
            0
            (transfer (cdr l))))

    (grab (car l))
    (animate (pdata-size))
    (ungrab)

    (transfer l))


(define (circle n radius s)
    (define (loop i angle)
        (turtle-move radius)
        (turtle-turn (vector 0 angle 0))
        (turtle-vert)
        (pdata-set "t" (turtle-position) (vmul (vector (/ n i) (* s 0.1) 0) 1))
        (turtle-skip 1)
        (if (zero? (- i 1))
            0
            (loop (- i 1) angle)))

    (turtle-push)
    (loop n (/ 360 n))
    (turtle-pop))

(define lastwidth 1)
(define turnx 0)
(define turny 0)

(define (shape c n)

    (circle c lastwidth n)
    (turtle-skip (- (- (* c 2) 1)))
    (turtle-turn (vector 0 0 90))
    (turtle-turn (vector 12 0 0))
    (turtle-move 2)
    (turtle-turn (vector 12 0 0))
    (turtle-turn (vector 0 0 -90))
    (set! lastwidth (+ 0.1 (abs (* 0.9 (sin (+ (* 0.3 n) (time)))))))

    (circle c lastwidth (+ n 1))
    (turtle-skip -1)
    (if (zero? n)
        0
        (shape c (- n 1))))

(clear)
(backfacecull 0)
(clear-colour (vector 0 0 0))
;(hint-wire)
(define o (build-polygons 271 0))
(turtle-attach o)
(turtle-reset)

(define nexttime 0)

(grab o)
(shape 9 14)
(recalc-normals 0)
(define shells (make-shells 20 0 0.06 0 (vector 0.02 0 0.01) '()))
(ungrab)

(define (update)
    (animate-shells shells)
        (cond ((< nexttime (time))
            (set! nexttime (+ (time) 0.1))
            (turtle-reset)
            (grab o)    
            ;(shape 9 14)
            ;(recalc-normals 0)
            (ungrab))))

(every-frame (update))
