(define (circle n radius s)
    (define (loop i angle)
        (turtle-move radius)
        (turtle-turn (vector 0 angle 0))
        (turtle-vert)
        (pdata-set "t" (turtle-position) (vector (/ n i) (* s 0.1) 0))
        (turtle-skip 1)
        (if (zero? (- i 1))
            0
            (loop (- i 1) angle)))

    (turtle-push)
    (loop n (/ 360 n))
    (turtle-pop))

(define lastwidth 1)
(define turnx 0)
(define turny 0)

(define (shape c n)

    (circle c lastwidth n)
    (turtle-skip (- (- (* c 2) 1)))
    (turtle-turn (vector 0 0 90))
    (turtle-turn (vector 12 0 0))
    (turtle-move 2)
    (turtle-turn (vector 12 0 0))
    (turtle-turn (vector 0 0 -90))
    (set! lastwidth (+ 0.1 (abs (* 0.9 (sin (+ (* 0.3 n) (time)))))))

    (circle c lastwidth (+ n 1))
    (turtle-skip -1)
    (if (zero? n)
        0
        (shape c (- n 1))))

(clear)
(backfacecull 0)
(clear-colour (vector 0 0 0))
(hint-wire)
(define o (build-polygons 271 'triangle-list))
(turtle-attach o)
(turtle-reset)

(define nexttime 0)

(define (update)

        (cond ((< nexttime (time))
            (set! nexttime (+ (time) 0.001))
            (turtle-reset)
            (grab o)    
            (shape 9 14)
            (recalc-normals 0)
            (ungrab))))

(every-frame (update))
