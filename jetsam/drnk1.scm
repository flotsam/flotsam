(clear)
(hint-unlit)

(pattern 1 2 5 (list "!111++1<" (list (list "1" "//[O....+o...++o...+o...++o...]1"))))

(translate (vector 5 0 0))

(pattern 2 2 5 (list "!<1111......@" (list (list "1" "O..o.[-----O.1]..++++O.1"))))

(translate (vector 5 0 0))

(pattern 3 4 5 (list "!1<11++1@" (list (list "1" "[1O..+o..+++o+..o..]"))))

(translate (vector 5 0 0))

(pattern 4 2 5 (list "!1<1@1" (list (list "1" "O..---.o..[++++o.1]..o."))))

(translate (vector 5 0 0))

(pattern 5 2 5 (list "!1<1@" (list (list "1" "O..o.[++++o.1]..o."))))

(translate (vector 5 0 0))

(pattern 6 2 5 (list "!1<1@" (list (list "1" "O1..o+++++1.[o++++.1].o---1...."))))

(translate (vector 7 0 0))

(pattern 7 2 2 (list "!1<1......11" (list (list "1" "O1.++++o1.[++++o.1].-----o1."))))


(pattern-cascade '(7 1 2 3 4 5 6))
