(clear)
(gain 1)
(clear-colour (vector 0 0 0))
(hint-unlit)
(blur 0)
(persp)
(opacity 0.5)

(gain 1)

(define (shape n)
    (turtle-move (+ 1 (gh n)))
    (turtle-turn (vector 
        (* 45 (cos (time))) 
        (* 45 (sin (time))) 0))
    (turtle-vert)
    (if (eq? n 0)
        1
        (shape (- n 1))))

(define (meta-shape nn)
    (turtle-turn (vector 0 40 0))
    (turtle-push)
    (shape 16)
    (turtle-pop)
    (if (eq? nn 0)
        1
        (meta-shape (- nn 1))))

(turtle-reset)
(backfacecull 0)
(hint-wire)
(line-width 4)

(define ob (build-cube))

(show-axis 0)
(gain 10)

(define (render)
    (destroy ob)
    (turtle-prim 0) 
    (meta-shape 10)
    (set! ob (turtle-build)))

(every-frame (render))
  

