(clear)
(load-from-path "fluxus/cubes.scm")
(load-from-path "fluxus/lsys.scm")

(blur 0)
(camera-lag 0.001)
(ortho)
(pattern-init)

(pattern-clock 0 0)

(pattern-osc-destination pattern-osc-noisepattern)
(osc-send "/loadtuning" "s" '("/home/dave/noiz/scales/scl/iran_diat.scl"))

(pattern-voice 1 "fm" '("freq" 0.5 "modfreq" 1.3 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.1 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1))

(pattern-voice 2 "fm" '("freq" 0.5 "modfreq" 0.5 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.2 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1))

(pattern-voice 3 "fm" '("freq" 0.25 "modfreq" 0.00125 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 2.6 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1))







(define a ".")

(define lsa (ls-gen 1 "!1" (list (cons "1" "1--2++2") (cons "2" a))))
(define lsb (ls-gen 1 "!1" (list (cons "1" "2++1--1") (cons "2" a))))

(pattern 1 2 lsa)
(pattern 2 2 (string-append "..." lsb))
(pattern 3 1 lsb)































