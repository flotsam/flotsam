
(clear)

(hint-unlit)

(define scale "[o---.O-------o.o--.]")

(pattern 1 0.5 4 (list "!111.1<" 
    (list 
        (list "1" (string-append "1++" scale)))))

(translate (vector 1 0 0 ))

(pattern 2 1 5 (list "!<1<1@" 
        (list 
            (list "1" "2++.[21].--1")
            (list "2" scale))))

(translate (vector 1 0 0 ))

(pattern 3 0.5 5 (list "!1<1++1<1@" 
    (list 
        (list "1" "o1....2")
        (list "2" scale))))

(translate (vector 1 0 0 ))

(pattern 4 1 5 (list "!11++1<1@" 
    (list 
        (list "1" "o1.+++++.2")
        (list "2" scale))))
