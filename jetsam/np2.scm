(source "noisepattern.scm")
(clear)
;(np-init)

(hint-wire)
(line-width 4)

(define bpm 240)

(np 1 (* bpm 1) 1 
    '("!<1" 
        (("1" "2\\\3[-----21]+++++3[1+++++2]")
         ("2" "O.")
         ("3" "o.1"))))

(translate (vector 2 0 0))

(np 2 (* bpm 2) 2 
    '("!<1" 
        (("1" "2\\\3[-----21]+++++3[1+++++2]")
         ("2" "o.-----2")
         ("3" "o."))))


(translate (vector 2 0 0))

(np 3 (* bpm 2) 5 
    '("!<1" 
        (("1" "2")
         ("2" "[1]O.-----3")
         ("3" "[++o.2]"))))


