(clear)

(define (make-ob n)
    (let ((p (with-state
                    (translate (vector (* 0.3 n) 0 0))
                    (texture-params 0 (list 'mag 'nearest 'min 'nearest))
                    (texture (load-texture "grad.png"))
                    (shader "cycle.vert.glsl" "cycle.frag.glsl")
                    (build-torus 1 2 20 20))))
        
        (with-primitive p
            (shader-set! 
                (list 
                    "Gradient" 0
                    "Centre" (vmul (grndvec) 10)
                    "PaletteSize" (random 32)
                    "Gap" (rndf)
                    )))
    p))

(define (make-obs n l)
    (cond ((zero? n) l)
        (else
            (cons (make-ob n) (make-obs (- n 1) l)))))

(define (update-obs l)
    (cond ((not (null? l))
        (with-primitive (car l)
            (rotate (vector 0 1 0))
            (shader-set! 
                (list "Time" (* 10 (time)))))
        (update-obs (cdr l)))))

(define l (make-obs 100 '()))

(every-frame (update-obs l))
