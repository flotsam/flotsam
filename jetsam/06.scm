
(clear)
(ortho)
(pattern-init)

(push)

(pattern-voice 1 "sub" '("typea" 2 "typeb" 2 "cutoff" 0.6 
    "freqa" 2.01 "freqb" 4.01 "decaya" 0.1 "decayb" 0.1 "sustaina" 0.2 "sustainb" 0.2 
        "mainvolume" 2 "resonance" 0.4 "poly" 2 "slidea" 0.2 "slideb" 0.2))

(pattern 1 4 3 (list "!<1" (list (list "1" "-----o..[+++++++++++++++++O].O..1"))))

(pattern-voice 2 "sub" '("typea" 1 "typeb" 2 "cutoff" 0.9 
    "freqa" 0.501 "freqb" 0.2501 "decaya" 0.5 "decayb" 0.1 "sustaina" 0.0 
        "sustainb" 0.0 "mainvolume" 2 "resonance" 0.4 "poly" 1 
        "distortion" 0.1 "slidea" 0.0 "slideb" 0.0))

(pattern 2 4 3 (list "!<1" (list (list "1" "-----o.[+++++++++++++++++O].O.1"))))

(pattern-samples 3 "kip")
(pattern-sample-volume 2)
(pattern 3 4 1 (list "!<111++++++1" 
    (list (list "1" "[o..+o..+o..+o..]")
          (list "2" "[+o.o.+o..]"))))

(pattern-samples 4 "rip")
(pattern 4 4 1 (list "!<111+++1" 
    (list (list "1" "[o..+o.+o..+o.]"))))

(pattern-cascade '(3 4 2 1))

(pop)
