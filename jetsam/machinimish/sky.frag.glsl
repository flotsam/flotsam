uniform vec4 Colour1;
uniform vec4 Colour2;
uniform vec4 Colour3;
uniform vec4 Colour4;
uniform float Blob1;
uniform float Blob2;
uniform float Blob3;
uniform float Blob4;
uniform float Time;
uniform float foo;

varying vec3 P;
varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec4 S;

void main()
{ 
	vec3 n = normalize(N);
	vec3 l = normalize(L);
	vec3 v = normalize(V);
	vec3 s = vec3(S)/S.w;
	float t = Time;

    vec4 colour = mix(Colour1,Colour2,1+sin(s.y*Blob1+t)*cos(s.x*Blob2+t));
	colour += mix(Colour1,Colour3,1+sin(s.y*Blob2+t)*cos(s.x*Blob4+t)*0.9);
	gl_FragColor = colour;
}
