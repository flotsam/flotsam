; missile command

(require fluxus-015/fluxa)

;--------------------------------------------

(define-struct city (
        root
        pos))

(define (add-city cities pos)
    (let ((root (with-state
                    (translate pos)
                    (build-locator))))
        (with-state
            (parent root)
            (wire-colour (vector 0 0 0))
            (hint-wire)
            
            (for ((i (in-range 0 10)))
                (with-state
                    (colour (vmul (vector 0.5 0.5 0.5) (abs (grndf))))
                    (translate (vmul (vector (abs (grndf)) 0 (abs (grndf)))
                            0.4))
                    (scale (vmul (vector (abs (grndf)) 
                                (* 30 (abs (grndf))) 
                                (abs (grndf)))
                            0.2))
                    (build-cube))))
        (cons (make-city root pos) cities)))

(define (update-cities cities explosions)
    (filter
        (lambda (city)                    
            (with-primitive (city-root city)
                (rotate (vector 0 3 0)))
            (foldl
                (lambda (explosion ret)
                    (cond ((and ret (> (explosion-radius explosion)
                                    (vdist (explosion-pos explosion)
                                        (city-pos city))))
                            (play-now (mul (adsr 0.2 0.3 0 0)
                                    (saw (add 3 (mul 1000 (sine 50))))))
                            (destroy (city-root city))
                            #f)
                        (else ret)))                               
                #t explosions))
        cities))

;--------------------------------------------

(define-struct bomb (
        root
        (pos #:mutable)
        (vel #:mutable)
        split))

(define (add-bomb bombs pos)
    (cons (make-bomb 
            (with-state 
                (colour (vector 1 0 0))
                (translate pos) 
                (scale (vector 0.1 0.1 0.1)) 
                (build-sphere 5 5)) pos 
            (vadd (vector 0 -0.01 0) 
                (vmul (vector (grndf) (- (abs (grndf))) 0) 0.1))
            (* (rndf) 10)) bombs))

(define (update-bombs bombs explosions)
    (filter
        (lambda (bomb)      
              
            (with-primitive (bomb-root bomb)
                (rotate (vector 0 0 5))
                (translate (vmul (bomb-vel bomb) 50))
                (set-bomb-pos! bomb (vtransform (vector 0 0 0) (get-transform))))
            
            (cond ((< (vy (bomb-pos bomb)) 0)
                    (add-bomb-explosion (bomb-pos bomb))
                    (destroy (bomb-root bomb))
                    #f)
                (else 
                    (foldl
                        (lambda (explosion ret)
                            (cond ((and ret (> (explosion-radius explosion)
                                            (vdist (explosion-pos explosion)
                                                (bomb-pos bomb))))
                                    (play-now (mul (adsr 0.2 0.3 0 0)
                                            (saw (add 880 (mul 1000 (sine 50))))))
                                    (destroy (bomb-root bomb))
                                    #f)
                                (else ret)))                               
                        #t explosions))))
        bombs))

;--------------------------------------------

(define-struct explosion (
        root
        pos 
        (radius #:mutable)))

(define (update-explosions explosions)
    (filter 
        (lambda (explosion)
            (with-primitive (explosion-root explosion)
                (identity) ; want to scale linearly, so have to reset trasform
                (translate (explosion-pos explosion))            
                (scale (explosion-radius explosion)))
            (set-explosion-radius! explosion (+ (explosion-radius explosion) (* 10 (delta))))
            (cond ((> (explosion-radius explosion) 30)
                    (destroy (explosion-root explosion)) ; kill the explosion
                    #f)
                (else #t)))                    
        explosions))

;--------------------------------------------

(define-struct player (
        pos 
        missiles 
        (next-missile #:mutable) 
        num-missiles))

(define (build-player pos num-missiles)
    (let ((p (build-particles num-missiles)))
        (with-primitive p
            (pdata-add "vel" "v")
            (pdata-add "dest" "v")
            (pdata-map!
                (lambda (p)
                    pos)
                "p")
            (pdata-map!
                (lambda (c)
                    (rndvec))
                "c")
            (pdata-map!
                (lambda (c)
                    (vmul (rndvec) 1))
                "s")
            (pdata-map!
                (lambda (c)
                    (vmul (rndvec) 10))
                "vel")
            (pdata-map!
                (lambda (dest)
                    (vector 1 1000 1)) ; make the destination far away so we 
                "dest"))               ; don't explode in the silo
        (make-player pos p 0 num-missiles)))

(define (get-pos-from-mouse)
    (let* ((ndcpos (vector (* (- (/ (mouse-x) (vx (get-screen-size))) 0.5) 20)
                    (* (- (- (/ (mouse-y) (vy (get-screen-size))) 0.5)) 14) -10))
            (scrpos (vtransform ndcpos (minverse (get-camera-transform)))))
        scrpos))

(define (get-vel-from-mouse player)
    (vmul (vnormalise (vsub (get-pos-from-mouse) (player-pos player))) 3))

(define (launch-missile player)
    (play-now (mul (white (mul 10 (adsr 0 0.5 0 0))) (adsr 0 0.3 0 0)))
    (with-primitive (player-missiles player)
        (pdata-set! "vel" (player-next-missile player) (get-vel-from-mouse player))
        (pdata-set! "p" (player-next-missile player) (player-pos player))
        (pdata-set! "dest" (player-next-missile player) (get-pos-from-mouse)))
    (set-player-next-missile! player
        (modulo (+ (player-next-missile player) 1) 
            (player-num-missiles player))))


(define (update-player player)
    (when (mouse-button 1)
        (launch-missile player))
    
    (with-primitive (player-missiles player)
        (pdata-op "+" "p" "vel")
        (pdata-op "+" "vel" (vector 0 -0.01 0))
        (pdata-index-map! 
            (lambda (i p dest)
                (when (< (vy p) 0)
                    (pdata-set! "vel" i (vmul (pdata-get "vel" i) -0.9))
                    (play-now (mul (adsr 0.01 0.01 0.1 4) (saw (mul (mul 100 (adsr 0 0.1 0.1 4)) (note (* 5 i)))))))
                (cond ((< (vdist p dest) 0.4) ; time to explode!
                        (pdata-set! "vel" i (vector 0 0 0))
                        (add-player-explosion p)
                        (pdata-set! "dest" i (vector 0 1000 0)) ; stop exploding
                        (player-pos player)) ; reset position
                    (else p)))
            "p" "dest")))

;---------------------------------------

(define player-explosions '())
(define bomb-explosions '())
(define bombs '())
(define cities '())

(define (add-player-explosion pos)
    (play-now (mul (white 4) (adsr 0 0.3 0 0)))
    (let ((obj (with-state
                    (colour (vector 0 0 1))
                    (translate pos)
                    (scale 0.01)
                    (build-sphere 10 10))))
        (set! player-explosions (cons (make-explosion obj pos 0) player-explosions))))

(define (add-bomb-explosion pos)
    (play-now (mul (saw (* (vx pos) 100)) (adsr 0 5 0 0)))
    (let ((obj (with-state
                    (colour (vector 1 0 0))
                    (translate pos)
                    (scale 0.01)
                    (build-sphere 10 10))))
        (set! bomb-explosions (cons (make-explosion obj pos 0) bomb-explosions))))


; main ---------------------------------

(clear)
(clear-colour (vector 0.1 0.1 0.2))
(blur 0.1)
;(hint-unlit)
;(hint-none)
(hint-wire)
(opacity 0.2)
;(set-camera-transform (mtranslate (vector 0 -6 -10)))

(with-state
    (colour (vector 0.1 0.2 0.1))
    (scale (vector 50 1 15))
    (rotate (vector 90 0 0))
    (build-plane))

(for ((i (in-range 0 10)))
    (with-state
        (colour 0.2)
        (translate (vector (* 25 (crndf)) 0 -10))
        (rotate (vector 45 0 (+ (* (grndf) 10) 45)))
        (scale (vmul (rndvec) 10))
        (build-cube)))

(define myplayer (build-player (vector 0 0 0) 10))

(for ((i (in-range 0 50)))
    (set! cities (add-city cities (vector (* (crndf) 10) 0 (* (crndf) 2)))))

(define (animate)
    (when (zero? (random 10))
        (set! bombs (add-bomb bombs (vector (* (crndf) 5) 13 0)))
        (play-now (mul (adsr 0.5 0 0.5 5) (saw (add 100 (mul 100 (adsr 0 0.5 0.3 5)))))))
    (update-player myplayer)
    (set! player-explosions (update-explosions player-explosions))
    (set! bomb-explosions (update-explosions bomb-explosions))
    (set! bombs (update-bombs bombs player-explosions))
    (set! cities (update-cities cities bomb-explosions)))

(every-frame (animate))
