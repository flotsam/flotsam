(clear)
(require fluxus-015/fluxa)

(define p (build-particles 20))

(with-primitive p
    (pdata-add "vel" "v")
    (pdata-map! 
        (lambda (c)
            (vector 1 1 1))
        "c")
    
    (pdata-index-map! 
        (lambda (i vel)
            (vmul (vector (sin i) 1 (cos i)) (* (cos (* i 0.04)) 0.06)))
        "vel")
    
    (pdata-map! 
        (lambda (c)
            (vector 0 10 0))
        "p"))

(every-frame (with-primitive p
        (pdata-op "+" "vel" (vector 0 -0.01 0))
        (pdata-op "+" "p" "vel")
        (pdata-index-map!
            (lambda (i vel p)
                (cond ((< (vy p) 0)
                        (pdata-set! "p" i (vector (vx p) 0 (vz p)))
                        (cond ((> (vmag vel) 0.01)
                                (play-now (mul (sine 
                            (mul (mul (sine 3) (* 100 (vmag vel))) 
                                (note (+ 50 (* i 2)))))
            ;(add (note (+ i 10)) (mul 1000 (saw (* (vmag vel) 5000)))))
                                        (adsr 0 0.05 0.1 0)))
                                (with-state 
                                    (colour (vector (abs (vx vel)) (abs (vy vel)) 1))
                                    (translate p)
                                    (scale (vector (* 100 (abs (vx vel))) 1 1))
                                    (draw-cube))
                                (vadd
                                    (vmul (vector (vx vel) (- (vy vel)) (vz vel)) 0.5)
                                    (vmul (srndvec) 0.1)))
                            (else
                                (pdata-set! "p" i (vector 0 10 0))
                                (vmul vel 10))))
                    (else
                        vel)))
            "vel" "p")))