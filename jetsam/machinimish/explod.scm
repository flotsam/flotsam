(clear)
(clear-colour (vector 0 0.3 0.4))
(fluxus-init)
(clear-shader-cache)

(define p (with-state
        (scale -10) 
        (shader "sky.vert.glsl" "sky.frag.glsl")
        (build-sphere 10 10)))


(with-primitive p
    (pdata-map! 
        (lambda (n)
            (vmul n -1))
        "n")
    (shader-set! 
        (list 
            "Blob1" 30.2
            "Blob2" 3.0
            "Blob3" 5.0
            "Blob4" 2.0
            "Colour1" (vector 0 0 0 1)
            "Colour2" (vector 0.1 0.04 0.02 1)
            "Colour3" (vector 0.1 0.02 0.06 1)
            )))

(every-frame
    (with-primitive p
        (shader-set! 
            (list "Time" (* 1 (time))))
        
        ;(identity)
        ;        (scale (* (+ 1 (sin (time))) 1))
        ))
