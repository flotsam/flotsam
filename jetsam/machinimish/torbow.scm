(clear)
(define p (build-torus 1 2 10 10))

(define l (with-state
    (hint-none)
    (hint-wire)
    (build-line 2)))

(every-frame
    (with-primitive l
        (pdata-set "p" 0 (vector 0 0 0))
        (pdata-set "p" 0 (vector 1 0 0))))