uniform vec4 Colour1;
uniform vec4 Colour2;
uniform vec4 Colour3;
uniform vec4 Colour4;
uniform float Blob1;
uniform float Blob2;
uniform float Blob3;
uniform float Time;
uniform float foo;

varying vec3 P;
varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec4 S;

void main()
{ 
	vec3 n = normalize(N);
	vec3 l = normalize(L);
	vec3 v = normalize(V);
	vec3 s = vec3(S);
	float t = Time;
    float lambert = dot(l,n);
    vec4 colour = Colour1;
	
	colour = Colour1;
	
    if (dot(n,v)<0.95+(sin(n.y*Blob1+t)+cos(n.z*Blob1+t))*0.1) colour = Colour2;
    if (dot(n,v)<0.7+(sin(n.y*Blob2+t)+cos(n.z*Blob2+t))*0.1) colour = Colour3;
    if (dot(n,v)<0.5+(sin(n.y*Blob3+t)+cos(n.z+Blob3+t))*0.1) colour = Colour4;

    gl_FragColor = colour;
}
