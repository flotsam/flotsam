(start-audio "" 1024 44100)

(define (render n)
    (cond
        ((zero? n) 0)
        (else
            (translate (vector 1 0 0))
            (push)
            (scale (vector 1 (gh n) 1))
            (draw-cube)
            (pop)
            (render (- n 1)))))

(every-frame (render 16))