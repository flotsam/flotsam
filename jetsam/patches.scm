
(clear)
(ortho)

(load (full-path "stroke.scm"))

(pattern-reset)
(pattern-init)

(define fm-pp1 '("freq" 2 "modfreq" 0.01 
    "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.1 "release" 0.5 
"modtype" 0 "modattack" 0.1 "moddecay" 0.2 "modsustain" 0.1 "modrelease" 0.5 
"fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
"volume" 1 "modvolume" 1 "fbvolume" 1 "crushfreq" 0 "crushbits" 0
        "slide" 0 "modslide" 0 
        "poly" 3 "mainvolume" 1))

(define fm-pp2 '("freq" 2 "modfreq" 1 
    "type" 2 "attack" 0.5 "decay" 0.1 "sustain" 0.1 "release" 0.5 
"modtype" 0 "modattack" 0 "moddecay" 0.6 "modsustain" 0.1 "modrelease" 0.5 
"fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
"volume" 1 "modvolume" 0.2 "fbvolume" 0 
        "slide" 0 "modslide" 0 
        "poly" 3 "mainvolume" 1))

(define sub-pp1 '("freqa" 2 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2
    "typea" 2 "attacka" 0 "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "volumea" 1 
    "typeb" 2 "attackb" 0 "decayb" 0.1 "sustainb" 0.1 "releaseb" 0.5 "volumeb" 1
    "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 "releasef" 0.5 "volumef" 0.2
    "lfodepth" 0.5 "lfofreq" 0.1 "crushfreq" 0 "crushbits" 0
        "slidea" 0.02 "slideb" 0.05 "distort" 0.7
        "poly" 3 "mainvolume" 1))

(define sub-pp2 '("freqa" 1 "freqb" 0.501 "cutoff" 0.2 "resonance" 0.1 "ftype" 2
    "typea" 8 "attacka" 0 "decaya" 0.5 "sustaina" 0 "releasea" 0 "volumea" 1 
    "typeb" 8 "attackb" 0 "decayb" 0.2 "sustainb" 0 "releaseb" 0 "volumeb" 1
    "attackf" 0 "decayf" 0.6 "sustainf" 0 "releasef" 0 "volumef" 1
    "lfodepth" 0 "lfofreq" 5 "crushfreq" 0 "crushbits" 0
        "slidea" 0 "slideb" 0.2 "distort" 0.7
        "poly" 3 "mainvolume" 1))


(define drum-pp1 '("kickfreqdecay" 0.1 "kickdecay" 0.5 "kickfreqvolume" 2 "kickfreq" 0.1
        "hat1decay" 0.02 "hat1volume" 2 "hat1cutoff" 0.01 "hat1resonance" 0.4
        "hat2decay" 0.02 "hat2volume" 2 "hat2cutoff" 0.4 "hat2resonance" 0.1
        "snaredecay" 0.01 "snarevolume" 2 "distort" 0.5 
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.2 "snareresonance" 0.4
        "crushfreq" 2 "crushbits" 3.5 "poly" 1 "mainvolume" 1))

(pattern-voice 1 "fm" fm-pp1)

(pattern 1 4 5 (list "!<1" 
    (list 
        (list "1" "[2.o].O++..2")
        (list "2" "-----1---2"))))

(pattern-voice 2 "fm" fm-pp2)

(pattern 2 1 5 (list "!<1" 
    (list 
        (list "1" "[2.o].O++..2")
        (list "2" "---1---2"))))

(pattern-voice 3 "sub" sub-pp1)

(pattern 3 4 5 (list "!<1" 
    (list 
        (list "1" "[2,o],.O++.2")
        (list "2" "-----1---2"))))

(pattern-voice 4 "sub" sub-pp2)

(pattern 4 4 3 (list "!<1" 
    (list 
        (list "1" "1O..o.[2.o.2.]")
        (list "2" "---1-----2"))))

(pattern-voice 5 "drum" drum-pp1)

(pattern 5 2 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[o.o...++o...+++o...++o...]"))))

;(pattern-voice 6 "drum" drum-pp1)

(pattern 6 4 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[+++o.o.o.o.++o.]"))))

(pattern-cascade '(1 2 3 4 5 6))