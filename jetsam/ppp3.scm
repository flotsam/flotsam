
;(clear)

(push)
(define pattern-display 0)
(pattern 1 2 2 
    (list "!<111+++1" 
        (list 
            (list "1" "21[+++21]")
            (list "2" "//[o..o..o.+++.o.+++++o.]"))))

(translate (vector 2 0 0))
(pattern 2 2 3 
    (list "!<1" 
        (list 
            (list "1" "[O.o..+++++++++++O.]----1++++1"))))

(translate (vector 2 0 0))

(pattern 3 2 3 
    (list "!<111+++++1" 
        (list 
            (list "1" "[o.++++o.+++o.+++o..++o..]")
            (list "2" "[o..+++o..]"))))

(translate (vector 2 0 0))
(pattern 4 1 5 (list "!<1" (list (list "1" "O..o..O.++o[1].+++o.."))))
(translate (vector 2 0 0))

(pattern 5 0.25 5 
    (list "!<1" 
        (list 
            (list "1" "O.[++++++++O]..[++1]--1o..O."))))

(translate (vector 2 0 0))

(pattern 6 1 3 
    (list "!<1" 
        (list 
            (list "1" "O.[++++++++O]..[++1]--1o..O."))))

(translate (vector 2 0 0))
(pattern 7 2 5 
    (list "!%<-----------1" 
        (list 
            (list "1" "2++1--1")
            (list "2" "o..O.."))))

(pattern-cascade '(1 3 2 4 6 7 5))
(pop)