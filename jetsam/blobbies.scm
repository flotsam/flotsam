(clear)
(gain 1)
;(hint-wire)
(hint-vertcols)
(shinyness 50)
(specular (vector 0.5 0.5 0.5))
(colour (vector 0 0.5 0))
(clear-colour (vector 0 0 0))

(show-fps 1)

(define a (build-blobby 7 (vector 30 10 30) (vector 5 5 5)))

(define (setup n)
    (define (loop n angle)
        (pdata-set "p" n (vadd (vector 1.5 1.5 1) 
            (vector (sin (* angle n)) (cos (* angle n)) 0 )))
        (pdata-set "c" n (vmul (vector (flxrnd) (flxrnd) (flxrnd)) 0.1))
        (pdata-set "s" n 0.1)
        (if (zero? n)
            0
            (loop (- n 1) angle)))
    (loop n (/ 6.6 n)))
    

(define (animate n)
    (pdata-set "p" n (vadd (vmul (pdata-get "p" n) 0.9) 
                     (vmul (vadd (vmul (vector (sin n) 0 (cos n)) (gh n))
                           (vector 1 1 1)) 0.1)))
    (if (zero? n)
        0
        (animate (- n 1))))
    

(grab a)
(setup (pdata-size))
(ungrab)

(light-diffuse 0 (vector 0 0 0))
(light-specular 0 (vector 0 0 0))

(define l (make-light "point" "free"))
(light-diffuse l (vector 1 0 0))
(light-specular 1 (vector 1 1 1))

(define l2 (make-light "point" "free"))
(light-position l2 (vector 1 -5 0))
(light-diffuse l2 (vector 1 1 0))
(light-specular 12 (vector 1 1 1))

(define (render)
    (grab a)
    (animate (pdata-size))
    (ungrab))

(every-frame (render))