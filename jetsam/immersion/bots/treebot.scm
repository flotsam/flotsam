(define (tree n)
    (robo-joint)
	(if (zero? n)
		0
		(begin
			(scale (vector 0.8 0.8 0.8))
			
			(robo-push)
    		(rotate (vector 0 0 45))
			(tree (- n 1))
			(robo-pop)
		
			(robo-push)
    		(rotate (vector 0 0 -45))
			(tree (- n 1))
			(robo-pop))))

(push)

(rotate (vector 0 0 90))
(scale (vector 2 2 2))
(push)
(robo-block)
(pop)

(translate (vector 1 0 0))
(tree 5)

(pop)
