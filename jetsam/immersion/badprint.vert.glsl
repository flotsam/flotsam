// Copyright (C) 2007 Dave Griffiths
// Licence: GPLv2 (see COPYING)
// Fluxus Shader Library
// ---------------------
// BadPrint NPR Shader
// This shader tries to emulate the effect
// of a bad printing process. Can be controlled
// with different settings for RGB

varying vec3 N;
varying vec3 P;
varying vec4 S;
varying vec3 L;
varying vec4 C;

void main()
{    
    N = normalize(gl_NormalMatrix*gl_Normal);
    P = gl_Vertex.xyz;
    gl_Position = ftransform();
	//L = vec3(gl_LightSource[0].position-gl_Position);
    S = gl_ProjectionMatrix*gl_Position;
	C = gl_FrontMaterial.diffuse;
	vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);
	L = vec3(gl_LightSource[0].position.xyz - vVertex);
}
