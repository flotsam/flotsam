// Copyright (C) 2007 Dave Griffiths
// Licence: GPLv2 (see COPYING)
// Fluxus Shader Library
// ---------------------
// BadPrint NPR Shader
// This shader tries to emulate the effect
// of a bad printing process. Can be controlled
// with different settings for RGB

varying vec3 N;
varying vec3 P;
varying vec4 S;
varying vec3 L;
varying vec4 C;

void main()
{ 
    vec3 l = normalize(L);
    vec3 n = normalize(N);
    vec2 p = S.xy;
    vec2 s = p*50;
    float diffuse = dot(l,n);
    float c = (sin(s.x)+cos(s.y))*0.1+diffuse;
	vec4 col = C*step(0.5,c);
    gl_FragColor = vec4(col.r,col.g,col.b,C.a);
}
