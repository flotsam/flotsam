(clear)
(gain 0.1)
(define (render n)
    (cond ((not (zero? n))
        (translate (vector 1.1 0 0))
        (with-state
            (scale (vector 1 (+ 0.1 (gh n)) 1))
            (draw-cube))
        (render (- n 1)))))

(every-frame (render 16))