// Copyright (C) 2007 Dave Griffiths
// Licence: GPLv2 (see COPYING)
// Fluxus Shader Library
// ---------------------
// Toon (Cel) NPR Shader
// Standard toon model with configurable
// colours for the different shading areas

varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec4 C;

void main()
{ 
	vec3 n = normalize(N);
	vec3 v = normalize(V);
    float fr = dot(n,v);
    vec4 colour = vec4(0,0,0,0);
	
    if (fr<0.9) colour = C*(1-fr);
	else colour = vec4(0,0,0,0);
    gl_FragColor = colour;
}
