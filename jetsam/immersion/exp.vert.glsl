// Copyright (C) 2007 Dave Griffiths
// Licence: GPLv2 (see COPYING)
// Fluxus Shader Library
// ---------------------
// Toon (Cel) NPR Shader
// Standard toon model with configurable
// colours for the different shading areas

varying vec3 N;
varying vec3 P;
varying vec3 V;
varying vec3 L;
varying vec4 C;

void main()
{    
    N = normalize(gl_NormalMatrix*gl_Normal);
    P = gl_Vertex.xyz;
    V = -vec3(gl_ModelViewMatrix*gl_Vertex);
	C = gl_FrontMaterial.diffuse;
    gl_Position = ftransform();
}
