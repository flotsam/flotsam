(clear)
(fluxus-init)
(desiredfps 10000)
(define ls '())
;(blur 0)
(show-fps 1)
;(shader "exp.vert.glsl" "exp.frag.glsl")
(shader "badprint.vert.glsl" "badprint.frag.glsl")

(define (render)

    (for-each
        (lambda (p)
            (with-primitive p
                (rotate (vector 2 0 0))
;                (translate (vmul (crndvec) 0.1))
                (scale (vector 1.01 1.01 1.01))))
        ls)

    (cond 
        ((zero? (random 8))
            (with-state
                (shader "exp.vert.glsl" "exp.frag.glsl")
                (rotate (vmul (rndvec) 360))
                (colour (rndvec))
                (opacity 0.4)
                (translate (vmul (crndvec) 2))
                (scale (vector 0.01 0.01 0.01))
                (hint-depth-sort)
                (set! ls (cons (build-torus 0.1 0.2 10 10) ls)))))
    (set! ls (max-list ls 80)))

(every-frame (render))

