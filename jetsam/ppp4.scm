(clear)
(line-width 4)
(pattern 1 2 1 
    (list "!<[111+1]111+1" 
        (list 
            (list "1" "/[o..++o.++o..++o.]///o..o.o..o."))))

(translate (vector 2 0 0))

(pattern 2 2 4 
    (list "!%<111+1" 
        (list 
            (list "1" "/1O..+1o.[--1O]..+1o."))))

(scale (vector 5 5 5))
(translate (vector 2 0 0))

(pattern 3 4 1 
    (list "!<111+1" 
        (list 
            (list "1" "////[o..++++o..+++++o..+++++o..]"))))

(translate (vector 2 0 0))

(pattern 6 2 1 
    (list "!111+1" 
        (list 
            (list "1" "/1O.+++++1o.-----[-----1O.+++++1o.]"))))

(translate (vector 2 0 0))

(pattern 5 0.5 1 
    (list "!<111+1" 
        (list 
            (list "1" "/1O.+++++1o.-----[-----1O.+++++1o.]"))))

(pattern-cascade '(1 3 2 6 5))