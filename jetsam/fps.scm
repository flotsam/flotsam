(clear)

(define (rndvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) (vector 0.5 0.5 0.5)))

(define (make-terrain n)
    (push)
    (texture (load-texture "pave1.png"))
    (rotate (vector 0 (* 360 (flxrnd)) 0))
    (translate (vector (* 100 (- (flxrnd) 0.5)) 0 (* 100 (- (flxrnd) 0.5))))
    (scale (vector (* 10 (flxrnd)) (* 10 (flxrnd)) (* 10 (flxrnd))))
    (build-cube)
    (pop)
    (if (zero? n)
        0
        (make-terrain (- n 1))))

(ambient (vector 0.1 0.5 0.1))
(specular (vector 0 0 0))
(make-terrain 100)
(clear-colour (vector 0.5 0.5 0.6))
(fog (vector 0.5 0.5 0.6) 0.03 1 100)

(light-diffuse 0 (vector 0 0 0))
(light-specular 0 (vector 0 0 0))

(define light (make-light "free" "point"))
(light-diffuse light (vector 1 1 1))
(light-position light (vector 100 100 10))
(light-specular light (vector 0 0 0))

(define light2 (make-light "free" "point"))
(light-diffuse light2 (vector 0.6 0.7 1))
(light-position light2 (vector -100 100 -5))
(light-specular light2 (vector 0 0 0))


(push)
(rotate (vector 90 0 0))
(scale (vector 100 100 100))
(hint-unlit)
(texture (load-texture "pave1.png"))
(define floor (build-plane))
(pop)

(push)
(translate (vector 0 0.5 0))
(define player (build-cube))
(define player-pos (vector 0 0 0))
(define player-angle 0)
(pop)


(define (player-update)
    (if (key-pressed "a")
        (set! player-angle (- player-angle 1)))
    (if (key-pressed "d")
        (set! player-angle (+ player-angle 1)))
    (if (key-pressed "w")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 -0.1) tx)))))
    (if (key-pressed "s")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 0.1) tx)))))

    (grab player)
    (identity)
    (translate player-pos)
    (rotate (vector 0 player-angle 0))
    (ungrab))
    
(lock-camera player)
(set-camera-transform (mtranslate (vector 0 -2 -1)))

(define (animate)
    (player-update))

(every-frame (animate)) 