(desiredfps 10000)

(define (rndvec)
    (vector (flxrnd)(flxrnd)(flxrnd)))

(define (rndcvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) (vector 0.5 0.5 0.5)))

(define leaves '())

(define (make-leaf)
    (with-state
        (scale (vector 0.1 8 0.1))
        (let ((c (build-cube)))
            (active-box c)
            (twist c (vmul (rndcvec) 0.1))
            (set! leaves (cons c leaves)))))

(define (animate)
    (for-each
        (lambda (leaf)
            (if (zero? (random 500))
                (with-primitive leaf
                    (kick leaf (vmul (vnormalise (vsub (vector 0 0 0)
                            (vtransform (vector 0 0 0) 
                        (get-transform)))) (* 0.1 (flxrnd)))))))
        leaves))

(clear)
(collisions 1)
(gravity (vector 0 0 0))

(let loop ((c 100))
    (with-state
        (translate (vmul (rndcvec) 50))
        (make-leaf))
    (if (not (zero? c)) (loop (- c 1))))

(every-frame (animate))