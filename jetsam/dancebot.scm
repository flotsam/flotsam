;;; dance dance robot revolution ;;;

;; todo
; delete objects properly to fix stack problem
; new robot on enter, not "t"
; more robots

(load-from-path "robo.scm")

(define NONE 1)
(define NORTH 2)
(define NORTHEAST 3)
(define EAST 4)
(define SOUTHEAST 5)
(define SOUTH 6)
(define SOUTHWEST 7)
(define WEST 8)
(define NORTHWEST 9)

(clear-colour (vector 1 1 0))
(gravity (vector 0 -0.5 0))

(define (build-seq seqcount size)
    (define (loop n l)
        (translate (vector 1.2 0 0))
        (if (zero? n)
            l
            (loop (- n 1) (append l (list (build-plane))))))
    (define (make-sequences n l)
        (if (zero? n)
            l
            (make-sequences (- n 1) (cons (make-list size 0) l))))
    (push)
    (hint-unlit)
    (colour (vector 1 1 0))
    (texture (load-texture "dancebot/none.png"))
    (translate (vector -5.4 -2.5 5))
    (let ((objs (loop size '())))
    (pop)
    (list objs 0 1 0 (time) (make-sequences seqcount '()) 0
        (list 
            (load-texture "dancebot/none.png")
            (load-texture "dancebot/none.png")
            (load-texture "dancebot/north.png")
            (load-texture "dancebot/northeast.png")
            (load-texture "dancebot/east.png")
            (load-texture "dancebot/southeast.png")
            (load-texture "dancebot/south.png")
            (load-texture "dancebot/southwest.png")
            (load-texture "dancebot/west.png")
            (load-texture "dancebot/northwest.png")))))

(define (seq-get-objs seq)
    (list-ref seq 0))

(define (seq-get-pos seq)
    (list-ref seq 1))

(define (seq-inc-pos seq)
    (list-set! seq 1 (modulo (+ (list-ref seq 1) 1) (length (seq-get-objs seq)))))

(define (seq-get-gap seq)
    (list-ref seq 2))

(define (seq-set-gap seq gap)
    (list-set! seq 2 gap))

(define (seq-get-laststep seq)
    (list-ref seq 3))

(define (seq-set-laststep seq time)
    (list-set! seq 3 time))

(define (seq-get-nexttime seq)
    (list-ref seq 4))

(define (seq-set-nexttime seq time)
    (list-set! seq 4 time))

(define (seq-get-sequences seq)
    (list-ref seq 5))

(define (seq-get-sequence seq id)
    (list-ref (seq-get-sequences seq) id))
    
(define (seq-set-current-sequence-id seq s)
    (list-set! seq 6 s)
    (seq-update-icons seq))

(define (seq-update-icons seq)
    (define (loop l n)
        (grab (car l))
        (texture (list-ref (seq-get-icons seq) (list-ref (seq-get-current-sequence seq) n)))
        (ungrab)
        (if (null? (cdr l))
            0
            (loop (cdr l) (+ n 1))))
    (loop (seq-get-objs seq) 0))
    
(define (seq-get-current-sequence-id seq)
    (list-ref seq 6))

(define (seq-get-current-sequence seq)
    (seq-get-sequence seq (seq-get-current-sequence-id seq)))

(define (seq-get-value seq id)
    (list-ref (seq-get-sequence seq id) (seq-get-pos seq)))

(define (seq-set-value seq value)
    (list-set! (seq-get-current-sequence seq) (seq-get-pos seq) value)
    (grab (list-ref (seq-get-objs seq) (seq-get-pos seq)))
    (texture (list-ref (seq-get-icons seq) value))
    (ungrab))

(define (seq-get-icons seq)
    (list-ref seq 7))

(define (seq-update seq step input)

    (if (not (zero? input))
        (seq-set-value seq input))
    
    (cond 
        (step
            (if (< (seq-get-gap seq) 100)
                ; average the gap
                (seq-set-gap seq (* 0.5 
                    (+ (- (time) (seq-get-laststep seq))
                       (seq-get-gap seq))))
                ; set the gap - first time
                (seq-set-gap seq (- (time) (seq-get-laststep seq))))

            (seq-set-laststep seq (time))
            (seq-set-nexttime seq (time))))

    (cond 
        ((< (seq-get-nexttime seq) (time))
            (seq-set-nexttime seq (+ (seq-get-nexttime seq) (seq-get-gap seq)))

            (grab (list-ref (seq-get-objs seq) (seq-get-pos seq)))
            (colour (vector 1 1 0))
            (ungrab)
            (seq-inc-pos seq)
            (grab (list-ref (seq-get-objs seq) (seq-get-pos seq)))
            (colour (vector 1 0 0))
            (ungrab))))

(define (make-world)
    (push)
    (rotate (vector -20 35 -20))
    (translate (vector 0 -10 5))
    (let ((camera (build-locator)))
    (lock-camera camera)
    (set-camera-transform (mtranslate (vector 0 0 10)))
    (pop)

    (list 0 camera 0)))

(define (world-get-camera world)
    (list-ref world 1))

(define (world-get-joint-id world)
    (list-ref world 2))

(define (world-inc-joint-id world)
    (list-set! world 2 (modulo (+ (world-get-joint-id world) 1)
        (length robo-joints))))

(define (world-dec-joint-id world)
    (list-set! world 2 (modulo (- (world-get-joint-id world) 1)
        (length robo-joints))))

(define (world-get-joint world)
    (list-ref robo-joints (world-get-joint-id world)))

(define (world-get-joint-objs world)
    (cadr (assq (world-get-joint world) robo-joints->objs)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (files->list path)
    (define (dir-loop dir l)
        (let ((item (readdir dir)))
        (cond 
            ((eof-object? item)
                l)
            (else
                ; don't record files beginning with a .
                (if (char=? (car (string->list item)) #\.)
                    (dir-loop dir l)
                    (dir-loop dir (append l (list item))))))))
                    

    (let ((dir (opendir path)))
    (cond 
        ((directory-stream? dir)
            (let ((ret (dir-loop dir '())))
            (closedir dir)
            ret))
        (else 
            '()))))

(define (load-robot filename)
    (clear)
    (robo-clear)
    (texture (load-texture "robo.png"))

    (push)
    (hint-unlit)
    (robo-world 10)
    (pop)
    
    (push)
    (load-from-path filename)
    (pop)
    
    (set! world (make-world))
    (push)
    (parent (world-get-camera world))
    (set! seq (build-seq (length robo-joints) 8))
    (every-frame (update))
    (pop))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define world 0)
(define seq 0)
(define bots (files->list "scheme/bots/"))
(define currentbot 0)

(load-robot "bots/spiderbot.scm")

(define debounce #t)

(define (update)
    
    (define (hilight col)
        (let ((obs (world-get-joint-objs world)))
        (grab (car obs))
        (colour col)
        (ungrab)
        (grab (cadr obs))
        (colour col)
        (ungrab)))
    
    (define (update-joints n)
        (cond
            ((eq? (seq-get-value seq n) NORTH)
                (joint-angle (list-ref robo-joints n) 2 -1.7))
            ((eq? (seq-get-value seq n) SOUTH)
                (joint-angle (list-ref robo-joints n) 2 1.7))
            (else
                (joint-angle (list-ref robo-joints n) 2 0)))
        (if (zero? n)
            0
            (update-joints (- n 1))))
                
    (cond
        ((key-special-pressed 103)
            (cond (debounce
                    (seq-update seq #f NORTH)
                    (set! debounce #f))))

        ((key-special-pressed 101)
            (cond (debounce
                    (seq-update seq #f SOUTH)
                    (set! debounce #f))))
 
       ((key-pressed "q")
            (cond (debounce
                    (hilight (vector 1 1 1))
                    (world-inc-joint-id world)
                    (hilight (vector 1 0 0))
                    (seq-set-current-sequence-id seq (world-get-joint-id world))
                    (set! debounce #f))))

        ((key-pressed "c")
            (cond (debounce
                    (hilight (vector 1 1 1))
                    (world-dec-joint-id world)
                    (hilight (vector 1 0 0))
                    (seq-set-current-sequence-id seq (world-get-joint-id world))
                    (set! debounce #f))))

        ((key-pressed "s")
            (cond (debounce
                    (seq-update seq #f 1)
                    (set! debounce #f))))

        ((key-special-pressed 102)
            (cond (debounce
                    (seq-update seq #t 0)
                    (set! debounce #f))))
       
       ((key-pressed "t")
            (cond (debounce
                    (set! currentbot (modulo (+ currentbot 1) (length bots)))
                    (load-robot (string-append "bots/" (list-ref bots currentbot)))
                    (set! debounce #f))))
            
        (else
            (set! debounce #t)))
            
        (seq-update seq #f 0)
     
         (update-joints (- (length robo-joints) 1)))


(every-frame (update))
