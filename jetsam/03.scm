
(clear)
(ortho)

;(load (full-path "cubes.scm"))
;(pattern-reset)
(pattern-init)
(push)

(pattern-voice 1 "sub" '("typea" 0 "typeb" 0 "cutoff" 1 
    "freqa" 1.5 "decaya" 0.2 "decayb" 0.3 "mainvolume" 3 "resonance" 0 "poly" 4))

(pattern 1 2 2 (list "!<o" (list (list "o" "o..++++o.----o"))))

(pattern-voice 2 "sub" '("typea" 0 "typeb" 0 "cutoff" 1 
    "freqa" 0.75 "freqb" 0.5 "decaya" 0.2 "decayb" 0.3 "sustaina" 0.2 "sustainb" 0.3 
        "mainvolume" 3 "resonance" 0 "poly" 4))

(pattern 2 2 2 (list "!<o" (list (list "o" "o.++++++++o..-------o-"))))


(pop)
