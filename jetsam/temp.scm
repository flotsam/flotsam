(clear)
(fluxus-init)
(desiredfps 100000)
;(shader "badprint.vert.glsl" "badprint.frag.glsl")
(shader-set! 
    (list "LightPos" (vector 0 40 40)
          "Size" (vector 40 40 40)
          "Scale" (vector 0.1 0.1 0.1)
          "Offset" 0.1))

(define (render n)
    (cond ((not (zero? n))
        (rotate (vector 0 0 20))
        (translate (vector 2 0 0))
        (draw-cube)
        (scale (vector 0.75 0.75 0.75))
        (with-state
            (rotate (vector 0 (* 45 (sin (time))) 45))
            (render (- n 1)))
        (with-state
            (rotate (vector 0 (* 45 (cos (time))) -45))
            (render (- n 1))))))

(every-frame (render 10))