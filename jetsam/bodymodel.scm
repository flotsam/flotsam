(define params (list 0 0 0 0))
(define velocities (list 0 0 0 0))

(define (circle n radius s)
    (define (loop i angle)
        (turtle-move radius)
        (turtle-turn (vector 0 angle 0))
        (let ((tx (vector s (/ i n) 0)))
        (pdata-set "t" (turtle-position) tx))
        (turtle-vert)
        (turtle-skip 1)
        (if (zero? (- i 1))
            0
            (loop (- i 1) angle)))

    (turtle-push)
    (loop n (/ 360 n))
    (turtle-pop))

(define (shape c n)
    (circle c (+ 1 (sin (* n (list-ref params 0)))) (* n 0.1))
    (turtle-skip (- (- (* c 2) 1)))
    (turtle-turn (vector 0 0 90))
    (turtle-turn (vector 20 0 0))

    (turtle-move 2)

    (turtle-turn (vector (* 10 (sin (list-ref params 3))) (* 20 (cos (list-ref params 2))) 0))

    (turtle-turn (vector 0 0 -90))
    (circle c (+ 1 (sin (* (- n (cos (list-ref params 1))) (list-ref params 0))))
        (* (- n 1) 0.1))
    (turtle-skip -1)
    (if (zero? n)
        0
        (shape c (- n 1))))

(clear-colour (vector 0 0 0))

(clear)
;(hint-none)
;(hint-wire)
;(hint-unlit)
(wire-colour (vector 0 0 0))
(line-width 4)
(show-axis 0)
(backfacecull 0)

(shinyness 100)
(specular (vector 1 1 1))

(texture (load-texture "../../play/tex/robo.png"))
(define o (build-polygons 816 0))

(turtle-attach o)
;(turtle-reset)

;(shape 8 50)
(display (turtle-position))(newline)

(define (update-params n)
    (list-set! params n (+ (list-ref params n) 
        (* (list-ref velocities n) 0.001)))
    (list-set! velocities n (* 0.95 (list-ref velocities n)))
    (if (zero? n)
        0
        (update-params (- n 1))))

(define (render)
    ;(if (key-pressed "s") (list-set! velocities 0 (+ (list-ref velocities 0) 0.1)))
    (if (key-pressed "c") (list-set! velocities 0 (+ (list-ref velocities 0) 0.1))) 
    (if (key-pressed "q") (list-set! velocities 0 (- (list-ref velocities 0) 0.1))) 
    (if (key-pressed "e") (list-set! velocities 1 (+ (list-ref velocities 1) 1))) 
    (if (key-pressed "z") (list-set! velocities 1 (- (list-ref velocities 1) 1))) 
    (if (key-special-pressed 100) (list-set! velocities 2 (+ (list-ref velocities 2) 1)))
    (if (key-special-pressed 102) (list-set! velocities 2 (- (list-ref velocities 2) 1)))
    (if (key-special-pressed 101) (list-set! velocities 3 (+ (list-ref velocities 3) 0.1)))
    (if (key-special-pressed 103) (list-set! velocities 3 (- (list-ref velocities 3) 0.1)))

    (update-params 3)

    (turtle-reset)
    (grab o)
    (shape 8 50)
    (recalc-normals 1)
    (ungrab))

(every-frame (render))
