(clear)
(pattern-reset)
(load (full-path "stroke.scm"))

(pattern-sample-pitch 1)
(pattern-samples 1 "hrs")
(pattern-samples 2 "syn")

(pattern-voice 3 "sub" '("freqa" 0.125 "freqb" 0.25 "distort" 0.4 
    "crushfreq" 0.4 "crushbits" 4 "typea" 2 "decaya" 0.5 "decayb" 0.5
    "mainvolume" 0.2))

(pattern-voice 4 "sub" '("freqa" 0.25 "freqb" 0.25 "distort" 0.4 
    "crushfreq" 0 "crushbits" 4 "typea" 2 "decaya" 0.1 "decayb" 0.1
    "mainvolume" 1 "cutoff" 0.1 "resonance" 0.4))

(pattern-voice 5 "sub" '("freqa" 4 "freqb" 2.5 "distort" 0.4 
    "crushfreq" 0.2 "crushbits" 8 "typea" 7 "typeb" 7 "decaya" 0.6 "decayb" 0.6
    "mainvolume" 0.4 "slidea" 0.1 "slideb" 0.1 "cutoff" 0.5 "lfodepth" 0.5 
    "lfofreq" 0.1 "resonance" 0.4 
    "poly" 1))

(pattern 1 4 1 (list "!<111+++1" (list (list "1" "o..++o.++o.++o...+o..."))))
(pattern 2 4 1 (list "!11<1+++1" (list (list "1" "[o.+++o.+o].++o."))))
(pattern 3 2 1 (list "!1<11@" (list (list "1" "O...O...."))))
(pattern 4 4 1 (list "!1111<1@" (list (list "1" "o.++++++++++O----------.O."))))
(pattern 5 4 1 (list "!1111@" (list (list "1" "...o.---O.O."))))

(pattern-cascade '(1 2))

