
(clear)
(ortho)

(pattern-bpm 120)

(load (full-path "stroke.scm"))

(pattern-reset)

(pattern-voice 1 "sub" '("decaya" 0.1 "decayb" 0.1 "typea" 2 "typeb" 2 "freqa" 0.5
        "freqb" 0.2501 "ftype" 3 "cutoff" 0.5 "slidea" 0 "slideb" 0 "volumef" 1
        "poly" 1 "decayf" 0.9 "lfodepth" 0.5 "resonance" 0 "lfofreq" 0.5 "ring" 0 
        "volumea" 1 "volumeb" 1 "mainvolume" 2))


(pattern-voice 2 "sub" '("decaya" 0.1 "decayb" 0.5 "typea" 2 "typeb" 2 "freqa" 1.5
        "freqb" 0.251 "ftype" 1 "cutoff" 0.1 "slidea" 0 "slideb" 0 "volumef" 1
        "poly" 1 "decayf" 0.1 "lfodepth" 0.01 "resonance" 0.2 "crushfreq" 20 
        "crushbits" 50 "lfofreq" 0.5 "ring" 1 "volumea" 1 "volumeb" 1 "mainvolume" 0.5))


(pattern-voice 3 "sub" '("decaya" 0.1 "decayb" 0.1 "typea" 2 "typeb" 2 "freqa" 0.25
        "freqb" 0.501 "ftype" 0 "cutoff" 0.2 "slidea" 0 "slideb" 0 "volumef" 0.02
        "poly" 1 "decayf" 0.9 "lfodepth" 0.1 "resonance" 0.4 "crushfreq" 0
        "crushbits" 16 "lfofreq" 2 "ring" 0.5 "volumea" 1 "volumeb" 1 
        "mainvolume" 1))

(pattern-samples 4 "rip")
(pattern-samples 5 "808")
(pattern-sample-volume 1)
(pattern-sample-pitch 2)

(pattern 1 4 2 (list "!11<" 
    (list 
        (list "1" "1--[2O..o..+++o..---O..]")
        (list "2" "++++++12"))))

(pattern 2 4 2 (list "!11<" 
    (list 
        (list "1" "1--[2O..o..+++o..---O..]")
        (list "2" "++++++12"))))

(pattern 3 4 2 (list "!11<" 
    (list 
        (list "1" "1--[2O..o..+++o..---O..]")
        (list "2" "++++++12"))))

(pattern 4 4 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[o.+o...++o...++o...+o...]"))))

(pattern 5 4 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[--o.+o..+o..+o..]"))))

(pattern-cascade '(1 2 3)) 