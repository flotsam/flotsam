

(start-osc "88000")
(define speed -50)
(define rotsp 200)
(collisions 0)

(define (spawn)
    (push)
        (opacity 1)
        (colour (vector (osc 1) (osc 2) (osc 3)))
        (scale (vector (osc 1) (osc 2) 1 ))
        (let ((ob (build-cube)))    
            (apply ob)
            (pop)
            (active-box ob)
            (kick ob (vector (*(osc 2)speed) (*(osc 3)speed) (*(osc 4)speed) ))
            (twist ob (vector (osc 2) (osc 2) (osc 3)))
            (push)
            (hint-unlit)
            (scale (vector 5 5 5))
            (texture (load-texture "font2.png"))
            (parent ob)
            (build-text (osc 0))
            (pop)))

(set-max-physical 20)

(define (run-loop)    
     (if (osc-msg "/trigger") (spawn)))


(gravity (vector 0 0 0 ))
(line-width 5)
(clear)
(ground-plane (vector 0 1 0) 0)    
(every-frame "(run-loop)")

(blur 0)
