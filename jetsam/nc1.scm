(clear)
(pattern 1 3 5 '("!^1" (("1" "o.[1][-----1]"))))
(translate (vector 10 0 0))
(pattern 2 3 5 '("!1" (("1" "o.[+++++1].O.[-----1]"))))
(translate (vector 10 0 0))
(pattern 3 3 5 '("!1" (
    ("1" "o.[+++++2].[-----2]")
    ("2" "O..o+++.o.1"))))

