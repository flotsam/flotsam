(clear)
(searchpaths '("/home/dave/work/scm/" "/home/dave/work/tex/"))
(desiredfps 25)
;(source "objimport.scm")
(load (full-path "noisepattern.scm"))
(pattern-init)

(osc-send "/loadtuning" "s" '("/home/dave/noiz/scales/scl/ji_12.scl"))

(pattern-bpm 200)
(backfacecull 0)
(pattern-delay 0)
(set! pattern-type 0)

(pattern 1 1 1 (list "!1" (list (list "1" ""))))
(pattern 2 1 1 (list "!1" (list (list "1" ""))))
(pattern 3 1 1 (list "!1" (list (list "1" ""))))
(pattern 4 1 1 (list "!1" (list (list "1" ""))))
(pattern 5 1 1 (list "!1" (list (list "1" ""))))
(pattern 6 1 1 (list "!1" (list (list "1" ""))))
(pattern 7 1 1 (list "!1" (list (list "1" ""))))

(osc-destination "osc.udp://localhost:4000")


(pattern-cascade '(1 2 3 4 5 6 7))

(pattern 1 1 1 (list "!<1" (list (list "1" "O.o.o.o."))))
