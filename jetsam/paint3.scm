
(clear)
(gain 100)
(define dirlight1 (vtransform (vector 1 0 0) (mrotate (vector 0 0 45))))

(texture (force-load-texture "tex/paint.png"))
(hint-unlit)

(push)
(scale (vector -100 -100 -100))
(build-sphere 10 10)
(pop)

(define (toon-init n)
    (pdata-set "t" n (vadd (vmul (pdata-get "t" n) 0.1) (vector 0 (* (flxrnd) 0.3) 0)))
    (if (< n 1)
        0
        (toon-init (- n 1))))

(define (toon-light n)
    (let ((lighting (vdot (pdata-get "n" n) dirlight1)))
        (if (< lighting 0) (set! lighting 0)) 
        (if (> lighting 0.9) (set! lighting 0.9)) 
        (pdata-set "t" n (vadd (pdata-get "tref" n) (vector lighting 0 0))))
    (if (< n 1)
        0
        (toon-light (- n 1))))

(define (deform n)
    (pdata-set "p" n (vadd (pdata-get "pref" n) 
        (vmul (pdata-get "nref" n) (* 0.3 (sin (+ (* n 1) (gh n)))))))
    (if (zero? n)
        0
        (deform (- n 1))))

(define obs '())

(define (make)
    (push)
    (set! obs (cons (build-nurbs-sphere 20 20) obs))   
    (pop)
        
    (grab (car obs))
    (toon-init (pdata-size))
    (pdata-copy "p" "pref")
    (pdata-copy "t" "tref")
    (pdata-copy "n" "nref")
    (ungrab))

(backfacecull 0)
(make)

(define (animate l n)
    (grab (car l))
    ;(rotate (vector 0.1 0.1 0))
    (deform (pdata-size))
    (recalc-normals 1)
    (toon-light (pdata-size))
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (animate (cdr l) (+ n 1))))

(blur 0)

(every-frame (animate obs 0)) 