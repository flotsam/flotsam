
(define obj (build-sphere 10 10))

(define (animate)
    (grab obj)
    (rotate (vector 0 (* (/ (delta) 86400) 360) 0)) ; 86400 seconds in a day
    (ungrab))

(every-frame (animate))
