
(clear)
(load (full-path "stroke.scm"))
;(pattern-reset)
(pattern-init)

;(load (full-path "cubes.scm"))

(pattern-sample-unmap)
(pattern-samples 1 "latino")
(pattern-samples 2 "rip")
(pattern-samples 3 "kip")
(pattern-sample-volume 1)

(pattern 1 4 1 (list "!<111+++1" (list (list "1" "[-----o.+++o.+++o+.o.]"))))

(pattern 2 4 1 (list "!<111+1" (list (list "1" "[o.++o+...o+...++o...++o...]"))))

(pattern 3 4 1 (list "!<111+++1" (list (list "1" "[o.++++++++o...o+...+o...o...]"))))

(pattern-voice 4 "sub" '("freqb" 0.5 "decaya" 0.1 "decayb" 0.1 "typea" 1 
    "typeb" 2 "cutoff" 0.05 "resonance" 0.4))

(pattern 4 4 3 (list "!<1" (list (list "1" "[--O.+++o.------O].---1+++1"))))

(pattern-voice 5 "sub" '("freqa" 1.001 "decaya" 0.5 "decayb" 0.5 "typea" 2 "typeb" 2 
             "cutoff" 0.9 "resonance" 0.1 "poly" 5 "mainvolume" 2))

(pattern 5 2 3 (list "!<1" (list (list "1" "[--O+++o----O]...---1+++1"))))

(pattern-cascade '(1 3 5 4))