(osc-source "4444")

(clear)

(define (make-finger digit)
    (push)
    (scale (vector 2 1 1))
    (translate (vector digit 0 0))
    (let ((ret (list (build-cube) 0 digit)))
    ret))

(define (finger-get-obj finger)
    (list-ref finger 0))

(define (finger-get-rot finger)
    (list-ref finger 1))

(define (finger-set-rot finger rot)
    (list-set! finger 1 rot))

(define (finger-get-digit finger)
    (list-ref finger 2))


(define (finger-update finger)
    (grab (finger-get-obj finger))
    (identity)
    
    (let ((length 3))

    (cond 
        ((eq? (finger-get-digit finger) 4)
            (translate (vector -1 0 -2))
            (rotate (vector 0 90 0))
            (set! length 3))
        (else 
            (translate (vector (finger-get-digit finger) 0 0))))

    (cond 
        ((osc-msg (string-append "/p5glove/finger/" 
            (number->string (finger-get-digit finger))))
         
            (finger-set-rot finger (* (osc 0) 90))))

    (rotate (vector (finger-get-rot finger) 0 0))
    (translate (vector 0 0 1))
    (scale (vector 0.6 0.6 length))
    (ungrab)))

(define (make-hand)
    (push)
    (translate (vector 1.5 0 -2))
    (scale (vector 4 1 4))
    (let ((palm (build-cube)))
    (pop)
    (apply-transform palm)
    (push)
    (parent palm)
    (let ((fingers         
         (list 
            (make-finger 0)
            (make-finger 1)
            (make-finger 2)
            (make-finger 3)
            (make-finger 4))))
    (pop)
    (list fingers palm (vector 0 0 0) (mident)))))

(define (hand-get-fingers hand)
    (list-ref hand 0))

(define (hand-get-palm hand)
    (list-ref hand 1))

(define (hand-get-translate hand)
    (list-ref hand 2))

(define (hand-set-translate hand trans)
    (list-set! hand 2 trans))

(define (hand-get-rotate hand)
    (list-ref hand 3))

(define (hand-set-rotate hand rot)
    (list-set! hand 3 rot))


(define (hand-update hand) 
    (define (loop l)
        (finger-update (car l))
        (if (null? (cdr l))
            0
            (loop (cdr l))))


    (push)
    (cond 
        ((osc-msg "/p5glove/position")
            (hand-set-translate hand (vmul (vector (osc 0) (osc 1) (osc 2)) 100)))

        ((osc-msg "/p5glove/rotation")
            (hand-set-rotate hand (qtomatrix 
                (qaxisangle (vector (osc 0) (osc 1) (osc 2)) (osc 3))))))

    (grab (hand-get-palm hand))
    (identity)
    (translate (hand-get-translate hand))
    ;(rotate (vector 0 0 90))
     (concat (hand-get-rotate hand))
    
    (ungrab)
            
    (loop (hand-get-fingers hand))
    (pop))

(define hand (make-hand))

(define (render)
    (hand-update hand))

(every-frame (render))
