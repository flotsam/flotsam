(define particle-count 20000)

(gain 1)
(define (init n)
    (pdata-set "p" n (vector 0 0 0))
    (pdata-set "vel" n (vmul (vector (sin (gh n)) 
                                     (cos (gh (+ 5 4))) 
                                     (sin (gh (+ 5 6)))) 1))
    (pdata-set "c" n (vector (gh 4) (gh 4) (gh 4))))

(define (initsome n)
    (init (* particle-count (flxrnd)))
    (if (< n 0)
        0
        (initsome (- n 1))))

(define (initall n)
    (init n)
    (if (< n 0)
        0
        (initall (- n 1))))

(define (update)
    (pdata-op "+" "vel" (vmul (vsub (vnormalise (vector (gh 1) (gh 5) (gh 8))) 
        (vector 0.5 0.5 0.5)) 0.1))
    (pdata-op "+" "p" "vel")
    (pdata-op "*" "vel" 0.99))

(define (render)
    (grab ob)
    (initsome 100)    
    (update)
    (ungrab))

(clear)
(hint-none)
(hint-points)
(hint-anti-alias)
(show-fps 0)
(point-width 5)
(hint-anti-alias)

(define ob (build-particles particle-count))

(grab ob)
(pdata-add "vel" "v")
(initall (pdata-size))
(ungrab)
(blur 0.1)

(every-frame "(render)")
