

(define (build-section count fn rfn)
    (define (build n)
        (turtle-push)
        (turtle-turn (rfn n count))
        (turtle-move (fn n count))
        (turtle-vert)
        (turtle-pop)

        (turtle-move 5)

        (turtle-push)
        (turtle-turn (rfn n count))
        (turtle-move (fn n (+ count 1)))
        (turtle-vert)
        (turtle-pop)
    
        (turtle-move -5)
        (turtle-turn (vector 0 -90 0))
        (turtle-move 3)
        (turtle-turn (vector 0 90 0))


        (if (zero? n)
            0
            (build (- n 1))))

;    (colour (vector (flxrnd) 1 1))

    (turtle-push)
    (turtle-prim 0)
    (build 10)
    (turtle-pop)
;    (turtle-move 0.5)
    (let ((ob (turtle-build)))
    (grab ob)
    (recalc-normals 1)
    (ungrab)
    ob))
        
(define (make-track)
    (list (list) 0 0))

(define (track-get-obs track)
    (list-ref track 0))

(define (track-set-obs! track obs)
    (list-set! track 0 obs))

(define (track-get-front track)
    (list-ref track 1))

(define (track-set-front! track front)
    (list-set! track 1 front))

(define (track-get-counter track)
    (list-ref track 2))

(define (track-set-counter! track counter)
    (list-set! track 2 counter))

(define (track-inc-counter! track)
    (track-set-counter! track (+ (track-get-counter track) 1)))

(define (build-track track size)
    (track-append-section track 
        (lambda (n count) 0) 
        (lambda (n count) (vector 0 0 90)))
    (if (zero? size)
        0
        (build-track track (- size 1))))

(define (track-append-section track fn rfn)
    (track-set-front! track (build-section (track-get-counter track) fn rfn))
    (track-inc-counter! track)
    (track-set-obs! track (append (track-get-obs track) 
        (list (track-get-front track)))))

(define (track-update track fn rfn)
    (define (move track)
        (grab (car track))
        (translate (vector (* (delta) -40) 0 0))
        (ungrab)
        (if (null? (cdr track))
            0
            (move (cdr track))))
    (move (track-get-obs track))
    (grab (track-get-front track))

    (let ((dist (f32vector-ref (vtransform (vector 0 0 0) (get-transform)) 0)))
    (ungrab)
    (cond 
        ((< dist -5)
            (push)
            (translate (vector (- dist -5) 0 0))
            (track-append-section track fn rfn)
            (pop)
            (destroy (car (track-get-obs track)))
            (track-set-obs! track (cdr (track-get-obs track)))))))


(clear)
(turtle-reset)
(show-axis 1)
(wire-colour (vector 0 0 0.5))
(line-width 2)
;(hint-none)
(hint-wire)
(hint-normal)

(define track (make-track))
(build-track track 30)

(display track)(newline)

(define (track-fn n count)
    (* (cos (* n 0.2)) (sin (* count 0.1)) (sin (* count 0.54)) 10))

;(define (track-fn n count)
;    (sin count))

(define (track-rfn n count)
    (vector 0 0 90))

(fog (vector 0 0 0) 0.02 0 1000)

(define (render)
    (track-update track track-fn track-rfn))

(every-frame (render))
