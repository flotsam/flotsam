(clear)

(define a 0)

(define (make)
    (set! a (build-cube))
    (active-box a))

(collisions 1)

(ground-plane (vector 0 1 0) -5)


(define (update)
    (if (key-pressed "q")
        (make)))


(every-frame "(update)")