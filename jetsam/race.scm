
(clear)
(define samplenames '("sine/005_sine6.wav" "drumtracks/006_DT Snare.wav" "three.wav"))
(define objects '())
(start-osc "88000")
(collisions 1)
(clear-colour (vector 0 0 1))
(desiredfps 50)

; build an object for every sample in the list
(define (build-objects samplelist n)
     (push)
     (colour (vector (flxrnd) (flxrnd) (flxrnd)))
     (scale (vector (flxrnd) (flxrnd) (flxrnd)))
     (translate (vector (* n 2) 5 0))
     (set! objects (cons (build-cube) objects))
     (active-box (car objects))
     (pop)
     ; build the text name
     (push)
     (scale (vector 0.1 0.1 0.1))
     (texture (load-texture "font2.png"))
     (translate (vector 0 10 0))
     (parent (car objects))
     (build-text (car samplelist))
     (pop)
     (if (eq? (cdr samplelist) '())
          0
          (build-objects (cdr samplelist) (+ n 1))))
     
; check each one to see if the sample has been triggered
(define (render-walk objlist samplelist)
     (if (osc-msg "/trigger")
        (begin
            (display (osc 0))(newline)
            (if (string=? (osc 0) (car samplelist))
                (begin
                    ;(twist (car objlist) (vector 0 (osc 2) 0))
                    (kick (car objlist) (vector 0 1 (* 1000 (osc 2))))
                    ))))
     (if (eq? (cdr objlist) '())
          0
          (render-walk (cdr objlist) (cdr samplelist))))
                    
(ground-plane (vector 0 1 0) 0.5)
(ground-plane (vector 0 -1 0) -6.5)
(build-objects samplenames 0)
(every-frame "(render-walk objects samplenames)")
