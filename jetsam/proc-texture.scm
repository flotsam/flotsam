(define p (build-pixels 100 100))

(with-primitive p
    (pdata-map! 
        (lambda (colour)
            (vector (flxrnd) 1 1 1))
        "c")
    (pixels-upload))
