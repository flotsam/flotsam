
(clear)

(pattern 1 4 4 
    (list "!^111++1" 
        (list 
            (list "1" "21[++++12]")
            (list "2" "[o..+o..++o..++o..]"))))

(translate (vector 2 0 0))
(pattern 2 2 5 
    (list "!------1<..1<@" 
        (list 
            (list "1" "1O..+++[1o---.1o.]"))))

(translate (vector 2 0 0))
(pattern 3 2 1 (list "!11<1+++1" 
     (list (list "1" "[O..++o.++++o..+++++o.]1"))))

(translate (vector 2 0 0))
(pattern 4 2 6
    (list "!<1@" 
        (list 
            (list "1" "2+++++O..[1-----1]")
            (list "2" "o."))))


(translate (vector 2 0 0))
(pattern 5 0.25 6
    (list "!<1@" 
        (list 
            (list "1" "2[+++1---1]")
            (list "2" ".O---..o"))))


(pattern-cascade '(1 3 4 2 5 6))









