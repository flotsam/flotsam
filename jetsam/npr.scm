(clear)
(texture (load-texture "tex/paint.png"))

(push)
(hint-unlit)
(scale (vector -100 -100 -100))
(build-sphere 10 10)
(pop)

(define (init-tex n)
    (pdata-set "t" n (vmul (pdata-get "t" n) 0.03))
    (if (zero? n)
        0
        (init-tex (- n 1))))

(define (tex n)
    (pdata-set "t" n (vadd (pdata-get "tref" n)
        (vector (vdot (vtransform (pdata-get "n" n) (get-transform)) (vector 0 1 0)) 0 0)))
    (if (zero? n)
        0
        (tex (- n 1))))

(push)
(hint-unlit)
(define ob (build-cube))
(pop)

(grab ob)
(pdata-add "tref" "v")
(init-tex (pdata-size))
(pdata-copy "t" "tref")
(ungrab)

(define (animate)
    (grab ob)
    (rotate (vector 0.1 0.1 0))
    (tex (pdata-size))
    (ungrab))

(every-frame (animate))    
