(clear)
(line-width 3)
(pattern-init)

(pattern-voice 1 "sub" (list "cutoff" 0.8 "typea" 4  
                             "freqb" 0.5001 "typeb" 3
                             "attacka" 0 "decaya" 0.1 "sustaina" 0.2 "releasea" 0.7
                             "attackb" 0 "decayb" 0.1 "sustainb" 0.2 "releaseb" 0.7
                             "resonance" 0 "attackf" 0.0 "decayf" 0.6 "sustainf" 0.0
                             "volumef" 0.2 "volumea" 1 "volumeb" 1 "poly" 4))

(pattern-voice 2 "sub" (list "cutoff" 0.3 "typea" 7 "freqb" 1.0001 "typeb" 0 
                             "attacka" 1 "attackb" 1
                             "ftype" 0 "decaya" 20.2 "decayb" 20.2 "resonance" 0.1
                             "attackf" 0.6 "decayf" 0.1 "sustainf" 0.0 "volumef" 0.2
                             "volumea" 2 "volumeb" 2 "lfodepth" 0.2 "lfofreq" 0.1
                             "poly" 4 "distort" 0))

(pattern-voice 3 "sub" (list "cutoff" 0.3 "typea" 3
                             "freqb" 0.5001 "freqa" 0.25 "resonance" 0.4 "typeb" 3
                             "attacka" 0 "decaya" 0.2 "sustaina" 0.2 "releasea" 0.7
                             "attackb" 0 "decayb" 0.2 "sustainb" 0.2 "releaseb" 0.7
                             "volumea" 0.6 "volumeb" 0.6 "volumef" 0 "lfodepth" 0.2
                             "lfofreq" 0.1 "poly" 1))

(pattern-voice 4 "fm" (list  "octavea" 0 "type" 0 "modfreq" 0.501
                             "modtype" 3 "attack" 0 "decay" 0.1 "sustain" 0.2 
                             "release" 0.2 "volume" 0.05 "moddecay" 0.5 
                             "modvolume" 0.2 "poly" 2))



(pattern-bpm 100)

(pattern 1 2 3 (list "!<1" 
        (list 
            (list "1" "21-----21+++o.++O.")
            (list "2" "[o.++o.--o...+++++++o...]"))))

(pattern 2 1 1 (list "!1<@" 
        (list 
            (list "1" "[2++2+++++2++2]")
            (list "2" "o..."))))

(pattern 3 4 8 (list "!1" 
        (list 
            (list "1" "2")
            (list "2" "[o.--o.-----o.--o.]"))))

(pattern 4 4 2 (list "<!-----1" 
        (list 
            (list "1" "21-----21+++O.++o.")
            (list "2" "[o..++o..--o.+++++++o...]"))))


(pattern-voice-volume 2)

(pattern-cascade '(1 2 3 4))









