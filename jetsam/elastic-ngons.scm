; elastic ngons

(define ngon-cols (list (vector 1 1 0) (vector 0 1 0) (vector 0 0 1)))

(define (ngon n)
    (define (gon n angle)
        (turtle-vert)
        (turtle-turn (vector 0 0 angle))
        (turtle-move 1)
        (if (zero? n)
            0
            (gon (- n 1) angle)))
    (turtle-prim 4)
    (opacity 0.5)
    (colour (list-ref ngon-cols (modulo n (length ngon-cols))))
    (gon n (/ 360 n))
    (turtle-turn (vector 0 0 180))
    (let ((o (turtle-build)))
    (grab o)
    (recalc-normals 1)
    (ungrab)
    o))

(define (elastic strength)
    (define (loop n)
        (pdata-set "p" n (vadd (vmul (pdata-get "p" n) (- 1 strength))
                               (vmul (pdata-get "pref" n) strength)))
        (if (zero? n)
            0
            (loop (- n 1))))
    (loop (pdata-size)))

(define (perturb strength)
    (define (loop n)
        (pdata-set "p" n (vadd (pdata-get "p" n) 
            (vmul (vadd (vector (flxrnd) (flxrnd) 0.5) (vector -0.5 -0.5 -0.5))
             strength)))
        (if (zero? n)
            0
            (loop (- n 1))))
    (loop (pdata-size)))


(define (make-obs)
    (list (list)))

(define (obs-get-list obs)
    (list-ref obs 0))

(define (obs-set-list! obs s)
    (list-set! obs 0 s))

(define (obs-add obs ob)
    (grab ob)
    (pdata-copy "p" "pref")
    (ungrab)
    (obs-set-list! obs (append (obs-get-list obs) (list ob))))

(define (obs-animate obs)
    (define (animate obs n)
        (grab (car obs))
        (perturb (gh n))
        (elastic 0.2)
        (ungrab)
        (if (null? (cdr obs))
            0
            (animate (cdr obs) (+ n 1))))
    (animate (obs-get-list obs) 0))

(clear)
;(hint-unlit)
(clear-colour (vector 0.5 0.5 0.5))
;(hint-normal)
(hint-wire)
(line-width 5)
(backfacecull 0)
(turtle-reset)
(gain 1)

(define obs (make-obs))

(define (vz obs n) 
    (obs-add obs (ngon n))
    (cond 
        ((> n 3)
            (turtle-push) 
            (vz obs (- n 1))
            (turtle-pop) 
            (turtle-turn (vector 0 90 0))        
            (turtle-move 5)        
            (turtle-push) 
            (vz obs (- n 1))
            (turtle-pop))))

(vz obs 10)

(define (animate)
    (obs-animate obs))

(every-frame (animate))
