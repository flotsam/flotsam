(gain 1)

(opacity 0.2)

(define (render n)
    (rotate (vector 10 0 0))
    (push)
    (translate (vector (* n 10) 0 0))
    (scale (vector 0.1 5 0.1))
    (colour (vector (gh 4) (gh 5) (gh 6)))
    (rotate (vmul (vector (gh (+ n 1)) (gh (+ n 2)) (gh 3)) 45))
    (draw-cube)
    (pop)
    (if (< n 0)
        0
        (begin
            (render (- n 1))
            (rotate (vector  0 (* 45 (gh n)) 0))
            (render (- n 1)))))

(blur 0.01)

(every-frame "(render 4)")