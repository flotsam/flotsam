(define out 0)
(texture (load-texture "font2.png"))

(define (osctest)
    (destroy out)
    (set! out (build-text (osc-peek))))

(start-osc "88000")

(every-frame "(osctest)")
