
(clear)
(ortho)

(load (full-path "stroke.scm"))

(pattern-reset)
(pattern-init)

(define drum-pp1 '("kickfreqdecay" 0.1 "kickdecay" 0.5 "kickfreqvolume" 2 "kickfreq" 0.1
        "hat1decay" 0.02 "hat1volume" 2 "hat1cutoff" 0.01 "hat1resonance" 0.4
        "hat2decay" 0.02 "hat2volume" 2 "hat2cutoff" 0.4 "hat2resonance" 0.1
        "snaredecay" 0.01 "snarevolume" 2 "distort" 0.5 
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.2 "snareresonance" 0.4
        "crushfreq" 2 "crushbits" 3.5 "poly" 1 "mainvolume" 1))

(pattern-voice 1 "drum" drum-pp1)

(pattern 1 2 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[o.o...++o...+++o...++o...]"))))

(pattern-voice 2 "drum" drum-pp1)

(pattern 2 4 2 (list "!<111+++1" 
    (list 
        (list "1" "2")
        (list "2" "[+++o.o.o.o.++o.]"))))

(pattern-cascade '(1 2 3 4 5 6))