(define tex '("weather/19.png"))

(define obs '())

(clear)
(hint-unlit)

(clear-colour (vector 0.38 0.71 0.90))

(define (build l)
    (translate (vector 0 0 0.01))
    (push)
    (translate (vmul (vector (flxrnd) (flxrnd) 0) 10))
    (rotate (vector 0 180 180))
    (scale (vector 1 1 1))
    (texture (load-texture (car l)))
    (set! obs (cons (build-plane) obs))
    (pop)
    (if (eq? (cdr l) '())
        0
        (build (cdr l))))

(define (animate l n)
    (grab (car l))
;    (rotate (vector 0 0 (gh n)))
    (translate (vmul (vector (sin (+ (time) n)) (cos (+ (time) n)) 0) 
        0.01));(* 0.01 (gh (+ n 19)))))
    (ungrab)
    (if (eq? (cdr l) '())
        0
        (animate (cdr l) (+ n 1))))

;(blur 0.05)

(define (build-many n)
    (build tex) 
    (if (zero? n)
        0
        (build-many (- n 1))))

(build-many 50)

(every-frame (animate obs 0))