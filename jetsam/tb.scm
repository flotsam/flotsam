(define (build n) 
    (turtle-reset) 
    (turtle-prim 0) 
    (build-loop n n) 
    (turtle-build))

(desiredfps 1000)

(gain 0)
(blur 0.0)
(define (build-loop n t) 
    (turtle-turn (vector 0 (/ 360 t) 0)) 
    (turtle-move 1) 
    (turtle-vert) 
    (if (< n 1) 0 
        (begin 
            (build-loop (- n 1) t) ; add another call to the recursion 
            (turtle-move (cos (time)))
            (turtle-turn (vector (* 10 (sin (time))) (gh 9) (+ 45 (gh 14)))) ; twist a bit 
            (build-loop (- n 1) t))))

(backfacecull 0) 
(clear) 
(hint-unlit) 
(hint-wire) 
(line-width 3)

(define o 0)

(define (render)
    (destroy o)
    (set! o (build 6)))

(every-frame "(render)")
