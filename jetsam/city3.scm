(clear)
(define l (list (build-cube)))
(define camob (car l))

(define (add-to-list n)
    (apply-transform n)
    (grab n)
    (ungrab)
    (set! l (append l (list n)))
    (cond 
        ((> (length l) 1000)
            (destroy (car l))
            (if (eq? (car l) camob)
                (begin
                    (set! camob n)
                    (lock-camera camob)))
            (set! l (cdr l)))))


(define (render)
    (if (> (gh 10) 1)
        (begin
            (push)
            ;(translate (vector (* 5 (gh 1)) (* 30 (gh 2)) (* 100 (gh 3)))) 
            (colour (vadd (vector 0.1 0.1 0.1) (vmul (vector (gh 3) (gh 6) (gh 9)) 0.01)))
            (scale (vadd (vector 1 1 1) (vmul (vector (gh 5) (gh 8) (gh 6)) 0.1)))
            (add-to-list (build-cube))
            (pop)))
    (animate 0 l))

(define (animate n l)
    (grab (car l))    
    ;(rotate (vector 0 (* 1 (gh n)) 0)) 
    (translate (vector (* 0.01 (gh n)) 0 (* 0.06 (gh (+ 11 n))))) 
    (ungrab)
    (if (eq? (cdr l) '())
        '()
        (animate (+ n 1) (cdr l))))

(push)
(colour (vector 0 1 0))
(scale (vector 100 1 100))
(rotate (vector 90 0 0))
(pop)
(line-width 5)

(camera-lag 0.1)

(blur 1)
(gain 10000)
(every-frame (render))
