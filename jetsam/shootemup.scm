; hide the code with ctrl-h
; then use keys w a s d to control the player space to fire

; return a random vector
(define (rndvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) (vector 0.5 0.5 0.5)))

(define cur-bullet 0)

(define (setup-bullets bullets)
    (grab bullets)
    (pdata-add "vel" "v")
    (let loop ((n (pdata-size)))
        (cond ((not (zero? n))
            (pdata-set "p" n (vector 0 100 0))
            (pdata-set "s" n (vector 1 1 1))
            (pdata-set "c" n (vector 1 0 0))
            (pdata-set "vel" n (vector 0 0 0))
            (loop (- n 1)))))
    (ungrab))
            

(define (shoot bullets pos dir)
    (grab bullets)
    (pdata-set "p" cur-bullet pos)
    (pdata-set "vel" cur-bullet dir)
    (set! cur-bullet (+ cur-bullet 1))
    (when (> cur-bullet (pdata-size))
        (set! cur-bullet 0))
    (ungrab))

(define (bullets-update bullets)
    (grab bullets)
    (pdata-op "+" "p" "vel")
    (ungrab))

(define enemies '())

(define (add-enemy pos)
    (push)
    (translate pos)    
    (colour (vector 0 1 0))
    (set! enemies (cons (build-cylinder 10 10) enemies))
    (pop))

(define (enemies-update enemies bullets)
    (for-each
        (lambda (enemy)
            (grab enemy)
            (colour (vector 0 (flxrnd) 0))
            (let ((enemy-pos (vtransform (vector 0 0 0) 
                (get-transform))))
            (ungrab)
            
            (grab bullets)
            (let loop ((n (pdata-size)))
                (cond ((not (zero? n))
                    (cond ((< (vdist enemy-pos (pdata-get "p" n)) 2)
                        ;(destroy enemy)
                        (grab enemy)
                        (scale (vector 0.5 0.5 0.5))
                        (ungrab)))
                    (loop (- n 1)))))
            (ungrab)))
        enemies))

; update the player
(define (player-update)
    (when (key-pressed "a")
        (set! player-angle (- player-angle 1)))
    (when (key-pressed "d")
        (set! player-angle (+ player-angle 1)))
    (when (key-pressed "w")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 -0.1) tx)))))
    (when (key-pressed "s")
        (let ((tx (mrotate (vector 0 player-angle 0))))
        (set! player-pos (vadd player-pos (vtransform (vector 0 0 0.1) tx)))))
    (when (key-pressed " ")
        (shoot bullets player-pos (vtransform (vector 0 0 -0.5) 
            (mrotate (vector 0 player-angle 0)))))

    (grab player)
    (identity)
    (translate player-pos)
    (rotate (vector 0 player-angle 0))
    (ungrab))

(clear)
(clear-colour (vector 0.5 0.5 0.6))
(fog (vector 0.5 0.5 0.6) 0.03 1 100)

; turn off the camera light
(light-diffuse 0 (vector 0 0 0))

; make some new static lights
(define light (make-light 'free 'point))
(light-diffuse light (vector 1 1 1))
(light-position light (vector 100 100 10))

(define light2 (make-light 'free 'point))
(light-diffuse light2 (vector 0.6 0.7 1))
(light-position light2 (vector -100 100 -5))

; make some bullets
(define bullets (build-particles 100))
(setup-bullets bullets)

; make the floor
(push)
(rotate (vector 90 0 0))
(scale (vector 100 100 1))
(translate (vector 0 0 1))
(hint-unlit)
(texture (load-texture "peel.png"))
(define floor (build-plane))
(pop)

; make the player
(push)
(translate (vector 0 0.5 0))
(define player (build-cube))
(define player-pos (vector 0 0 0))
(define player-angle 0)
(pop)

(map add-enemy 
    (list (vector 0 0 -10) (vector 10 0 -10) (vector -10 0 -10) (vector 10 0 0)))
    
; lock the camera to the player
(lock-camera player)
; and setup the camera transform (turns off the mouse camera)
(set-camera-transform (mtranslate (vector 0 -1 -1)))

; go!
(define (animate)
    (bullets-update bullets)
    (enemies-update enemies bullets)
    (player-update))

(every-frame (animate)) 