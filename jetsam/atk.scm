(save-name "atk.scm")

(clear)
(pattern-init)

(load (full-path "stroke.scm"))

(pattern-samples 1 "atk")

(pattern-bpm 180)

(pattern 1 4 2 (list "!<111+1" 
    (list 
        (list "1" "2o..o..o..o..")
        (list "2" "[o..+o..+o..+o..]"))))

(pattern-samples 2 "808")

(pattern 2 2 2 (list "!<111+1" (list (list "1" "[o..+o.+o..+o.]"))))


(pattern-samples 3 "hrs")

(pattern 3 2 2 (list "!<111++++1" 
    (list 
        (list "1" "2[++1]")
        (list "2" "[o.+++o.+++o.+++o.]"))))

(pattern-sample-pitch 1)

(pattern-voice 4 "sub" '("freqa" 0.5 "freqb" 1.5 "typea" 3 "cutoff" 0.2 "resonance" 0.4
        "lfodepth" 0.1 "lfofreq" 1 "decaya" 0.5 "decayb" 0.5
        "slidea" 0.1 "slideb" 0.1
        ))

(pattern 4 2 4 (list "!<1" 
    (list 
        (list "1" "O.o2++.O.")
        (list "2" "[o-----.21]"))))

(pattern-voice 5 "sub" '("freqa" 2 "freqb" 0.5001 "typea" 3 "typeb" 3 "cutoff" 0.5 "resonance" 0.2
        "lfodepth" 0.1 "lfofreq" 1 "attacka" 0.1 "attackb" 0.2 "decaya" 0.7 "decayb" 0.7
        "slidea" 0 "slideb" 0 "poly" 4 "mainvolume" 2
        ))

(pattern 5 2 4 (list "!<++1" 
    (list 
        (list "1" "O.o2++O...")
        (list "2" "[o-----21]"))))

(pattern-voice 6 "sub" '("freqa" 0.25 "freqb" 0.25 "typea" 3 "cutoff" 0.1 
        "resonance" 0.45 "decayf" 0.5
        "lfodepth" 0 "lfofreq" 1 "decaya" 0.5 "decayb" 0.5 "volumef" 0.1
        "slidea" 0 "slideb" 0 "crushfreq" 5 "crushbits" 6
        ))

(pattern 6 2 4 (list "!<1" 
    (list 
        (list "1" "O.O.2")
        (list "2" "[o-----.21]"))))

(pattern-cascade '(1 2 3 4 5 6))