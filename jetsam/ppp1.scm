
(clear)

(define phr1 ".O.o.O")
(define phr2 "[o..++++o..+++++++o..++o..]")

(pattern 1 2 3 
    (list "!^1" 
        (list 
            (list "1" "2[++1][-----1.]")
            (list "2" phr1))))

(translate (vector 2 0 0))

(pattern 2 2 3 
    (list "!1" 
        (list 
            (list "1" "2[++1][-----1]")
            (list "2" phr1))))

(translate (vector 2 0 0))

(pattern 3 4 3 
    (list "!1" 
        (list 
            (list "1" "2[++1]..[-----1]")
            (list "2" phr1))))

(translate (vector 2 0 0))

(pattern 4 4 3 
    (list "!111+++1" 
        (list 
            (list "1" "[o..o..o..++++++o..]")
            (list "2" phr1))))


(translate (vector 2 0 0))

(pattern 5 4 3 
    (list "!111+++1" 
        (list 
            (list "1" "[o.++o.++++o.++++++o.]")
            (list "2" phr1))))


