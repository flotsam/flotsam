(clear)

(define rhyth "2..2.2..2.")

(pattern 1 1 1 
    (list "!<1111++11" 
        (list
            (list "1" rhyth)
            (list "2" "//o++++"))))