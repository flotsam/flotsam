
(clear)
(define pattern-display 1)
(point-width 10)
(define pattern-type 5)

(pattern 1 4 1 
    (list "!<111++1" 
        (list 
            (list "1" "12[+++12]+++o.o.o.o.")
            (list "2" "///[o..+o..+o..+o..]"))))


(translate (vector 2 0 0))

(pattern 2 2 2 
    (list "!<%1" 
        (list 
            (list "1" "//O1..++o1.--[--o1..++o1.]"))))


(translate (vector 2 0 0))

(pattern 3 4 1 
    (list "!<111++1" 
        (list 
            (list "1" "12[++++++12]")
            (list "2" "///[o..+++o..+o..+o..]"))))


(translate (vector 2 0 0))

(pattern 4 2 2 
    (list "!%1" 
        (list 
            (list "1" "//O.o..o.[----O....1]++o.o.o.o."))))

(translate (vector 2 0 0))

(pattern 5 0.5 5 
    (list "!<%1" 
        (list 
            (list "1" "//O.[----o1.++o1]"))))

(translate (vector 2 0 0))

(pattern 6 2 7 
    (list "!<%1" 
        (list 
            (list "1" "O.[--o1].[++++1]"))))

(translate (vector 2 0 0))

(pattern 7 2 7 
    (list "!<%1" 
        (list 
            (list "1" "O..[----1]++++o."))))

(pattern-cascade '(4 5 2))
