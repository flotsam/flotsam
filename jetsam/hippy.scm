
(clear)
(opacity 0.5)
(define (render n)
    (push)
    (rotate (vector 0 (* 360 (sin (time))) 0))
    (translate (vector (* 1 n) (gh n) 0))
    (colour (vector (gh n) (gh (+ 1 n)) (gh 3)))
    (rotate (vmul (vector (gh 1) (gh 4) (gh 8)) 90))
    (scale (vmul (vector 0.2 0.2 (gh (+ n 50))) 1))
    (draw-cube)
    (pop)
    (if (< n 0)
        0
        (begin
        (render (- n 1))
        (rotate (vector 10 45 0))
        (render (- n 1)))))

(blur 0.01)

(every-frame (render 8))