(define vocab
  ('one 'string '(number number))
  ('two 'number '(string number))
  ('three 'number '()))

(define (make-code-tree depth func)
  (list
   (car func)
   