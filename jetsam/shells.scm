
(define (make-shell n type col dist)
    (define (loop n target)
        (let ((vert (vadd (pdata-get "p" n) (vmul (pdata-get "n" n) dist)))
               (tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "p" n vert)
        (pdata-set "t" n tex))
        (ungrab)
        (if (zero? n)
            0
            (loop (- n 1) target)))
    
    (let ((target (build-polygons (pdata-size) type)))

    (grab target)
    (colour col)
    (hint-unlit)
    ;(hint-ignore-depth)
    ;(blend-mode "one" "one")
    (shinyness 10)
    (specular (vector 1 0.1 0.1))
    (texture (load-texture "drawn.png"))
 ;   (texture (list-ref tex n))
    (ungrab)

    (loop (pdata-size) target)
    (grab target)
    (recalc-normals 1)
    (ungrab)
    target))


(define (make-shells c n dist type col l)
    (if (eq? c n)
        l
        (make-shells c (+ n 1) dist type col
            (cons (make-shell n type (vmul col (* n 2)) (* dist n)) l))))

(define (animate-shells l)
    (define (animate n)
        (let ((vary (f32vector-ref (pdata-get "p" n) 1)))
        (pdata-set "t" n (vadd (pdata-get "t" n) 
            (vmul (vector (sin (+ vary (* (time) 2))) 
                          (cos (+ vary (* (time) 2))) 0) 
                0.01))))
        (if (zero? n)
            0
            (animate (- n 1))))

    (define (copy n target)
        (let ((tex (pdata-get "t" n)))
        (grab target)
        (pdata-set "t" n (vadd 
            (vmul tex 0.7)
            (vmul (pdata-get "t" n) 0.3)))
        (ungrab))
        (if (zero? n)
            0
            (copy (- n 1) target)))

    (define (transfer l)
        (grab (car l))
        (copy (pdata-size) (car (cdr l)))
        (ungrab)
        (if (null? (cdr (cdr l)))
            0
            (transfer (cdr l))))

    (grab (car l))
    (animate (pdata-size))
    (ungrab)

    (transfer l))


(define (circle n radius s)
    (define (loop i angle)
        (turtle-move radius)
        (turtle-turn (vector 0 angle 0))
        (turtle-vert)
        (pdata-set "t" (turtle-position) (vmul (vector (/ n i) (* s 0.1) 0) 10))
        (turtle-skip 1)
        (if (zero? (- i 1))
            0
            (loop (- i 1) angle)))

    (turtle-push)
    (loop n (/ 360 n))
    (turtle-pop))

(define lastwidth 1)
(define turnx 0)
(define turny 0)

(define (shape c n)

    (circle c lastwidth n)
    (turtle-skip (- (- (* c 2) 1)))
    (turtle-turn (vector 0 0 90))
    (turtle-turn (vector 10 0 0))
    (turtle-move 3)
    (turtle-turn (vector 10 0 0))
    (turtle-turn (vector 0 0 -90))
    (set! lastwidth (+ 0.1 (abs (* 0.9 (sin (+ (* 0.3 n) (time)))))))

    (circle c lastwidth (+ n 1))
    (turtle-skip -1)
    (if (zero? n)
        0
        (shape c (- n 1))))

(clear)
(show-axis 1)
;(scale (vector 10 10 10))
(backfacecull 0)
(backfacecull 0)
(clear-colour (vector 0 0 0))
;(hint-wire)
(define o (build-polygons 200 0))
;(define o (build-sphere 20 20))
(turtle-attach o)
(turtle-reset)

(define nexttime 0)

(grab o)
(hide 0)
(shape 18 20)
(recalc-normals 0)
(define shells (make-shells 20 0 0.02 0 (vector 0.1 0.01 0.01) '()))
(ungrab)
(clear-colour (vector 0 0 0))

(define (update)
    (animate-shells shells))

(every-frame (update))
