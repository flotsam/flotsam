(require 
    (lib "clay.ss" "fluxus-0.12"))

(clear)
(load "lights.scm")

(desiredfps 10000)

(wire-colour (vector 0.5 0.5 1))

(define (my-shape-func deg dist pos)
    (turtle-turn (vector 0 (* deg pos) 0))
    (turtle-move 1))

;(extrude-shape-func-set extrude-circle-shape-func)
(extrude-shape-func-set my-shape-func)

(gain 100)

(define (make n)
  (extrude-push)
  (extrude-radius-set (* (+ 5 (sin (+ (+ (gh n) n) (* (time) 0.1)))) 0.1))
  (turtle-turn (vector (* 30 (sin (+ (* n 0.1) (* 0.1 (time))))) 0 0)) 
  (extrude-pop)
  (if (zero? n)
      0
      (make (- n 1))))    

(define o (build-polygons 5000 2))

(define (animate)
  (extrude-begin o)
  (make 80)
  (extrude-end))

(every-frame (animate))

