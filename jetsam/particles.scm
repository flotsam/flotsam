(desiredfps 100000)
(define speed 10)
(define rotsp 2)

(collisions 0)

(define (spawn)
    (push)
        (opacity 1)
        (translate (vector (* (flxrnd) 1) 5 (* (flxrnd) 1)))
        (printf "~a ~a~n" (gh 4) (gh 9))
        ;(scale (vector (*(gh 4)10) 0.1 (*(gh 9)10)))
        (scale (vector 0 1 1))
        (shinyness 1)
        (specular (vector 0 0 0))
        ;(colour (vector (gh 4) (gh 6) (gh 8)))
        (let ((ob (build-cube)))    
            (active-box ob)
            (kick ob (vector (*(gh 5)speed) (*(gh 7)speed) (*(gh 9)speed) ))
            (twist ob (vector (* (- (gh 13) 0.5) rotsp) 
                              (* (- (gh 14) 0.5) rotsp) 
                              (* (- (gh 15) 0.5) rotsp) )))
    (pop))

(ground-plane (vector 0 1 0) 0)
(set-max-physical 20)
(blur 0)

(define (run-loop)    
     (spawn))
    ;(if (> (gh 10) 0.2) (spawn)))

(line-width 5)
(clear)
(every-frame (run-loop))

(show-axis 0)
(clear-colour (vector 0 0 0))
(blur 0)
