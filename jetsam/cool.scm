(start-audio "alsa_pcm:capture_1" 1024 44100)

(define particle-count 10000)

(define (init n)
    (pdata-set "p" n (vector (* 1 (flxrnd)) (* 1 (flxrnd)) (* 1 (flxrnd))))
    (pdata-set "vel" n (vmul (vsub (vector (flxrnd) (flxrnd) (flxrnd)) 
                             (vector 0.5 0.5 0.5)) 0.1))
    (pdata-set "c" n (vector (/ (gh 2) 8) (/ (gh 8) 8) (/ (gh 12) 8))))

(define (initsome n)
    (init (round (inexact->exact (* particle-count (flxrnd)))))
    (if (< n 0)
         0
        (initsome (- n 1))))

(define (initall n)
    (init n)
    (if (< n 0)
            0
            (initall (- n 1))))

(define (update)
    (pdata-op "+" "vel" (vector 0 -0.0015 0))
    (pdata-op "+" "p" "vel"))

(define (render)
    (grab ob)
    (initsome 100)    
    (update)
    (ungrab))

(clear)
(show-fps 0)
;(hint-none)
;(hint-points)
(point-width 2)
(hint-anti-alias)

(define ob (build-particles particle-count))

(grab ob)
(pdata-add "vel" "v")
(initall (pdata-size))
(ungrab)
(blur 0.1)

(every-frame (render))