

(clear)
(ortho)
(pattern-init)
(push)

(pattern-voice 1 "sub" '("typea" 1 "typeb" 1 "cutoff" 0.8 
    "freqa" 1.5001 "decaya" 0.1 "decayb" 0.1 "sustaina" 0.2 "sustainb" 0.2 
        "mainvolume" 3 "resonance" 0.3 "poly" 4 "slidea" 0.1 "slideb" 0))

(pattern 1 2 4 (list "!<1" (list (list "1" "1.--o[++o+++O]O1++."))))

(pattern-voice 2 "sub" '("typea" 0 "typeb" 2 "cutoff" 0.1 "ring" 0
    "freqa" 1.5 "freqb" 2 "decaya" 0.1 "decayb" 0.1 "sustaina" 0.0 "sustainb" 0.0 
        "decayf" 0.5 "attackf" 2 
        "mainvolume" 0.5 "resonance" 0.4 "poly" 1 
        "distort" 0.5 "slidea" 0.0 "slideb" 0.0))

(pattern 2 4 4 (list "!<1" (list (list "1" "1.--O.[++o.+++o].o1++"))))


;(pattern 2 4 3 (list "!<-----1" 
;    (list 
;        (list "1" "1o.O.O.[2O.O.o.o.].1")
;        (list "2" "++++++1+++++2"))))

(pattern-samples 3 "kip")
(pattern-sample-volume 2)
(pattern 3 4 2 (list "!<111++++++1" 
    (list (list "1" "21[+++21]")
          (list "2" "[o..++o.+o..++o.]"))))

(pattern-samples 4 "rip")
(pattern 4 4 2 (list "!<111+++1"
    (list (list "1" "1212") 
        (list "2" "[o.+o.+o.+o.]"))))


(pattern-cascade '(1 2 3 4))

(pop)
