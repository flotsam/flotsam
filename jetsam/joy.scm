(osc-source "4444")

(clear)


(define x 1)
(define y 0)
(define z 1)
(define w 1)
(define c (vector 1 1 1))

(opacity 0.1)
(blur 0.001)
(define a (build-cube))

(define (test)
    (cond 
        ((osc-msg "/joystick_button")
            (cond
                ((eq? (osc 0) 0) 
                    (set! c (vector (flxrnd) (flxrnd) (flxrnd))))))

        ((osc-msg "/joystick_axis")
            (cond 
                ((eq? (osc 0) 1) (set! x (* 180 (osc 1))))
                ((eq? (osc 0) 0) (set! y (* 180 (osc 1))))
                ((eq? (osc 0) 4) (set! z (+ 1 (osc 1))))
                ((eq? (osc 0) 5) (set! w (+ 1 (osc 1)))))))
            
        (grab a)
        (identity)
        (colour c)
        (rotate (vector 0 y (- x)))
        (scale (vector 1 (* 5 w) (* 5 z)))
        (ungrab))


(every-frame (test))