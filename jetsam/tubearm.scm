(osc-source "9999")

(define (circle n radius s)
    (define (loop i angle)
        (turtle-move radius)
        (turtle-turn (vector 0 angle 0))
        (turtle-vert)
        (pdata-set "t" (turtle-position) (vector (/ n i) (* s 0.1) 0))
        (turtle-skip 1)
        (if (zero? (- i 1))
            0
            (loop (- i 1) angle)))

    (turtle-push)
    (loop n (/ 360 n))
    (turtle-pop))

(define lastwidth 1)
(define turnx 0)
(define turny 0)

(define (shape c n)
   (define (drain-upper)
        (cond
            ((osc-msg "/bodydata/1/upper")
                (display (osc 0))(newline)
                (set! turny (* (gh 0) 90))
                (drain-upper))))

    (define (drain-lower)
        (cond
            ((osc-msg "/bodydata/1/lower")
                (display (osc 0))(newline)
   
                (set! turnx (* (osc 0) 90))
                (drain-lower))))
    (drain-upper)    
    (drain-lower)

    (if (> turnx 90) (set! turnx 90))
    (if (> turny 90) (set! turny 90))

    (circle c lastwidth n)
    (turtle-skip (- (- (* c 2) 1)))
    (turtle-turn (vector 0 0 90))
    (turtle-turn (vmul (vector turnx turny 0) 0.1))
    (turtle-move 2)
    (turtle-turn (vmul (vector turnx turny 0) 0.1))
    (turtle-turn (vector 0 0 -90))
    (set! lastwidth (+ (* lastwidth 0.9) (* (gh 8) 0.001)))

    (circle c lastwidth (+ n 1))
    (turtle-skip -1)
    (if (zero? n)
        0
        (shape c (- n 1))))

(clear)
(backfacecull 0)
(clear-colour (vector 0.5 0.5 0.5))
;(hint-wire)
;(hint-unlit)
(texture (load-texture "rhod.png"))
(define o (build-polygons 5000 0))
(turtle-attach o)
(turtle-reset)

(gain 100000)

(define camera (build-locator))
(lock-camera camera)
(camera-lag 0.01)

(define nexttime 0)

(define (update)

        (cond ((< nexttime (time))

            (set! nexttime (+ (time) 0.1))

            (grab o)

            (let ((t (pdata-get "p" (modulo (- (turtle-position) 1) (pdata-size)))))
            (grab camera)
            (identity)
            (translate t)
            (ungrab))
    
            (shape 12 1)
            (recalc-normals 0)
            (ungrab))))

(every-frame (update))
