;(require fluxus-017/drscheme)
(require "graph.ss")
(require "aigraph.ss")
(require "net.ss")

(clear-geometry-cache)

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define size 32)

(define (index x y)
    (inexact->exact (round (+ x (* y size)))))

(define (sample p pos)    
    (with-primitive p 
        (let ((r (pdata-ref "c" (pixels-index pos))))
            (pdata-set "c" (pixels-index pos) (vmul r 0.99))
            (pixels-upload)
            r)))

(define (test-sample p)
    (for ((i (in-range 0 1000)))
        (let ((pos (let ((v (crndvec))) (vector (vx v) 0 (vz v))))
                #;(pos (vector -14 0 -15)))
            (with-state
                (hint-wire)
                (wire-colour 0)
                (hint-unlit)
                (translate pos)
                (colour (sample p pos))
                (scale 0.5)
                (build-cube)))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define g (make-graph 
        (list 
            (make-ainode 1 (vector 18 0 19) 0.1 1))
        (list)))

(clear)
(flxseed 3)
(random-seed 3)
(clear-colour 0)
(define n (net '() '()))
(define t 0)

(light-diffuse 0 (vector 0.4 0.4 0.4))
(let ((l (make-light 'point 'free)))
    (light-diffuse l (vector 1 1 1))
    (light-position l (vector 100 40 10))
    #;(shadow-light l))

(define tp (with-state
        ;    (colour (vector 0 0.2 0.5))
        (backfacecull 0)
        (rotate (vector 90 0 0))
        (scale 32)
        (translate (vector -0 -0 0.001))
        (load-primitive "textures/resources.png")))

;(with-primitive tp (pixels-download))

;(test-sample tp)

(set-suck (lambda (pos) 
        (let ((pos (vdiv pos 32)))
            (display pos)(newline)
            (* 0.1 (vz (sample tp (vector (vx pos) (vz pos) 0)))))))



(every-frame
    (cond 
        ((< t 2000)
            ;        (display t)(newline)
            (set! g (aigraph-update g))
            ;        (graph-display-nodes g)
            (set! n (net-update n g))
            (set! t (+ t 1)))))

;(start-framedump "tec1-" "jpg")
