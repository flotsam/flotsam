(clear)

(define (build-plate size roughness)
    (let ((p (build-polygons size 'polygon)))
        (with-primitive p
            (hint-unlit)
            (backfacecull 0)
            (colour (vector 0.4 0.2 0))
            (hint-wire)
            (line-width 2)
            (wire-colour (vector 0 0 0))
            (pdata-index-map!
                (lambda (i p)
                    (vadd (vector (sin (* 2 3.141 (/ i size)))
                            (cos (* 2 3.141 (/ i size))) 0)
                        (vmul (vector (crndf) (crndf) 0) roughness)))
                "p")) p))

(define (debug-line from to)
    (with-primitive (build-ribbon 2)
        (hint-none)
        (hint-wire)
        (hint-unlit)
        (wire-opacity 0.3)
        (wire-colour (vector 1 1 0))
        (pdata-set! "p" 0 from)
        (pdata-set! "p" 1 to)))

(define (v4->v3 v)
    (vector (vx v) (vy v) (vz v)))

(define (project-plate plate world dir)
    (with-primitive plate
        (let* ((plate-tx (get-transform))
                (inv-plate-tx (minverse (get-transform)))
                (size (pdata-size))
                (r (with-state 
                        (hint-unlit)
                        (backfacecull 0)
                        (colour (vmul (vector 0.4 0.2 0) (rndf)))
                        ;(hint-wire)
                        (line-width 2)
                        (wire-colour (vector 0 0 0))
                        (build-polygons size 'polygon))))
            (pdata-index-map!
                (lambda (i p)
                    (with-primitive world                    
                        (let* ((tp (vtransform p plate-tx))
                                (in (geo/line-intersect tp (vadd tp (vmul dir 4)))))
                            #;(debug-line tp (vadd tp (vmul dir 4)))
                            (with-primitive r 
                                (pdata-set! "p" i
                                    (cond 
                                        ((not (null? in))
                                            #;(with-state 
                                                (translate (cdr (assoc "p" (car in))))
                                                (scale 0.02)
                                                (colour (vector 1 1 0))
                                                (build-sphere 5 5))
                                            (v4->v3 (cdr (assoc "p" (car in)))))
                                        (else (printf "no intersection~n") p))))
                            p)))
                "p") r)))

;=====================

(light-diffuse 0 (vector 0.1 0.1 0.1))
(let ((l (make-light 'point 'free)))
    (light-diffuse l (vector 1 1 1))
    (light-position l (vector 10 100 10)))

(define world (with-state
        ;    (hint-none)
  ;      (hint-wire)
        (wire-colour (vector 0 0.3 0.6))
        (hint-depth-sort)
        (opacity 0.7)
;        (colour (vector 0 0.8 1))
        (build-sphere 20 20)))

(with-primitive world (hide 1))

(define (do-plate)
    (let ((plate 
                (with-state 
                    (translate (vector (* (crndf) 0.5) (* (crndf) 0.5) -4))
                    (scale (* (rndf) 0.4))
                    (build-plate 20 0.1))))
        (let ((r (project-plate plate world (vector 0 0 1))))
            (destroy plate) r)))

(clear-colour (vector 0.5 0.3 0.2))

(define l (build-list 20 (lambda (_) (list (vmul (srndvec) (rndf)) (do-plate)))))

(every-frame
    (for-each
        (lambda (plate)
            (with-primitive (cadr plate)
            (rotate (car plate))))
        l))

;(start-framedump "plates-" "jpg") 
