(clear)
(define o (load-primitive "k2.obj"))

(define l '())

(with-primitive o
    (poly-convert-to-indexed)
    (poly-for-each-triangle
        (lambda (indices)
            (let ((n (vadd (vmul (pdata-ref "n" (list-ref indices 0)) 0.3333)
                            (vmul (pdata-ref "n" (list-ref indices 1)) 0.3333)
                            (vmul (pdata-ref "n" (list-ref indices 2)) 0.3333))))
                
                (when (> (vdot (vector 0 1 0) n) 0.8)
                    (set! l (cons (pdata-ref "p" (list-ref indices 0)) l))
                    (set! l (cons (pdata-ref "p" (list-ref indices 1)) l))
                    (set! l (cons (pdata-ref "p" (list-ref indices 2)) l)))))))
                

(destroy o)

(define no (build-polygons (* (length l) 3) 'triangle-list))

(with-primitive no
    (hint-wire)
    (pdata-index-map!
        (lambda (i p)
            (list-ref l i))
        "p"))

 