(clear-texture-cache)

(define-struct plant (root shape widths (start-time #:mutable)))

(define (choose l)
    (list-ref l (random (length l))))

(define (build-plant size)
    (make-plant
        (with-state 
            ;(hint-none)
            ;(hint-wire)
            (hint-unlit)
            (texture (load-texture (choose (list
                "textures/leaf-01.png"
                "textures/leaf-02.png"
                "textures/leaf-03.png"))))
            (colour (vmul (vector (* 0.75 (rndf)) 1 (* 0.75 (rndf))) 1))
            (build-ribbon size))
        (let ((r (* 20 (crndf)))
                (d (vmul (vector 0 1 0.2) (* (rndf) 1)))
                (p (vector 0 0 0)))
            (build-list size 
                (lambda (i) 
                    (set! d (vtransform d (mrotate (vector 0 0 (* r i 0.1)))))
                    (set! d (vmul d 0.9))
                    (set! p (vadd p d))
                    p)))
        (build-list size
            (lambda (i)
                (* (/ i size) (/ i size) 0.5)))
        (time)))

(define (reset-plant plant pos)
    (with-primitive (plant-root plant)
        (identity)
        (translate pos)
        (pdata-map!
            (lambda (p)
                (vector 0 0 0))
            "p")
        (pdata-index-map!
            (lambda (i w)
                (list-ref (plant-widths plant) i))
            "w"))
    (set-plant-start-time! plant (time)))

(define (plant-update plant)
    (let ((t (* (- (time) (plant-start-time plant)) 10)))
        (with-primitive (plant-root plant)
            (when (< t (pdata-size))
                (pdata-index-map!
                    (lambda (i p)
                        (let ((t (- t i)))
                            (if (> t 0) 
                                (vmix p (list-ref (plant-shape plant) (inexact->exact (floor t))) 0.9)
                                p)))
                    "p")
                #;(pdata-index-map!
                    (lambda (i w)
                        (lerp w (list-ref (plant-widths plant) i) 0.9))
                    "w")))))


(define (plant-destroy plant)
    (destroy (plant-root plant)))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(clear)
(clear-colour (vector 0.6 0.7 0.8))
(define plants (build-list 200 (lambda (_) (build-plant 20))))
(define cur 0)

(define cam (build-locator))
(lock-camera cam)
(with-state 
    (rotate (vector 90 0 0))
    (colour (vector 0.2 0.6 0.2))
    (scale 11)
    (build-plane))

(define (plants-cons plants)
    (reset-plant (list-ref plants cur) (vmul (vector (crndf) 0 (crndf)) 5))
    (set! cur (modulo (+ cur 1) (length plants))))

(define t 0)
(define (time) t)
(define (update-time) 
    (set! t (+ t 0.02)))

(define (plants-update)
    (with-primitive cam (rotate (vector 0 0.1 0)))
    (when (zero? (random 10))
        (plants-cons plants))
    (for-each
        (lambda (plant)
            (plant-update plant))
        plants)
    (update-time))

(every-frame (plants-update))

(start-framedump "grass-" "jpg") 