fastbreeder notes : (C) 2006 Dave Griffiths : dave at pawfal dot org : www.pawfal.org

fastbreeder - a genetic programming synthesiser

generates a lisp style function in gp python code, and uploads it using an osc 
message it to a realtime synth engine for playback via the jack engine. 

in it's previous incarnation (as wigwamjam) this program used to generate samples
that were played back by a simple sampler.

reasons for using a sample:

* ephasis on open ended evolution means that cpu time is uncertain for realtime
* dealing with pitch is easier (just pitch the sample)

reasons for not using a sample:

* pitch can be used more interestingly (via the gp)
* much cleaner design (no dsp code in the scripting language)

ideas:

gp prefiltering 
do some checks on the resultant programs to increase usefulness, check for:
* all zero
* constant
* not centred on zero?
dump all programs not corresponding to this and remutate 
