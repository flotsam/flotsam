// Copyright (C) 2006 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Types.h"
#include "OSCServer.h"
#include "Time.h"
#include "EventQueue.h"
#include "Synth.h"

#ifndef FASTBREEDER
#define FASTBREEDER

struct OutBuffer
{
	Sample Left;
	Sample Right;
};

class FastBreeder 
{
public:
	FastBreeder(OSCServer *server, unsigned int Samplerate);
	virtual ~FastBreeder();

private:
		
	// audiothread side functions
	void NewOutputBuffer();
	static void Run(void *RunContext, uint32 BufSize);
    virtual void Process(uint32 BufSize);
	void ProcessCommands();
	
	int m_CurrentSynth;
	vector<Synth*> m_SynthVec;
	
	vector<OutBuffer*> m_BufferVec;
	unsigned long m_SampleRate;
	int m_Left, m_Right;
	OSCServer *m_Server;
	
};

#endif //  FastBreeder
