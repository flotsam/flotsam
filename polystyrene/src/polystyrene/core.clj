(ns polystyrene.core
  (:use clojure.contrib.shell-out)
  (:import 
   org.jgap.Chromosome
   org.jgap.FitnessFunction
   org.jgap.Genotype
   org.jgap.impl.DefaultConfiguration
   org.jgap.impl.IntegerGene
   ))

(println "starting")

(defn make-csv [l]
  (reduce
   (fn [r i]
     (str r "," i))
   "" l))

(defn parse-number [s]
  (try (Integer/parseInt (.trim s))
       (catch NumberFormatException e nil)))

(defn opcode [c]
  (nth
   (list "nop" "org" "equ" "jmp" "jmpz" "pshl"
         "psh" "pshi" "pop" "popi" "add" "sub"
         "inc" "dec" "and" "or" "xor" "not" "ror"
         "rol" "pip" "pdp" "dup" "note" "vox")
   c))
 
(defn operand? [c]
  (nth
   (list false false false true true true
         true true true true false false
         false false false false false false true
         true true true false false false)
   c))

(defn safe-nth [l i]
  (if (< i (count l))
    (nth l i)
    0))

(defn disassemble [code]
  (defn _ [r pos]
    (let [op (safe-nth code pos)]
      (cond
       (>= pos (count code)) r
       (< op 25)
       (if (operand? op)
         (_ (str r (opcode op) " " (safe-nth code (+ pos 1)) "\n")
            (+ pos 2))
         (_ (str r (opcode op) "\n")
            (+ pos 1)))
       :else
       (_ (str r op "\n") (+ pos 1)))))
  (_ "" 0))
                  
(defn run-bblocker [cycles code]
  (map
   parse-number
   (.split
    (sh "./poly"
        (str cycles)
        :in (make-csv code))
    " ")))

(defn chromo->list [chromo]
  (defn _ [c]
    (cond
     (= (.size chromo) c) '()
     :else (cons (.getAllele (.getGene chromo c))
                 (_ (+ c 1)))))
  (_ 0))

(defn loop-evolve [population count]
  (.evolve population)
  (let [fittest (.getFittestChromosome population)] 
    (println fittest)
    (println (disassemble (chromo->list fittest)))
    (println (run-bblocker 100 (chromo->list fittest))))
  (when (> count 0)
    (recur population (- count 1))))

(defn repetition [a]
  (defn _ [r c]
    (cond
     (= c 1) r
     :else (_ (+ r (Math/abs (- (nth a c) (nth a (+ c 1)))))
              (- c 1))))
  (if (< (count a) 3)
    0
    (_ 0 (- (count a) 2))))

(defn num-repeats [a v]
  (cond
   (= (count a) 0) 0
   (= (first a) v) (+ 1 (num-repeats (rest a) v))
   :else (recur (rest a) v)))
  
(defn hist [a]
  (reduce
   (fn [r v]
     (+ r (- (count a) (num-repeats a v))))
   0
   a))

(defn list-contains? [l i]
  (cond
   (empty? l) false
   (= (first l) i) true
   :else (recur (rest l) i)))

(defn num-unique [a]
  (reduce
   (fn [r v]
     (if (not (list-contains? r v))
       (cons v r)
       r))
   '()
   a))

(defn deriv [l]
  (cond
   (empty? l) '()
   (empty? (rest l)) '()
   :else (cons (- (first (rest l)) (first l))
               (deriv (rest l)))))


(defn freq [l n]
  (defn _ [c]
    (cond
     (>= (+ c n) (count l)) 0
     (= (nth l c) (nth l (+ c n))) (+ 1 (_ (+ c 1)))
     :else (_ (+ c 1))))
  (_ 0))

(defn freqs [l]
  (defn _ [c]
    (cond
     (>= c (count l)) '()
     :else (cons
            (freq l c)
            (_ (+ c 1)))))
  (_ 0))

(defn add-fitness [c a b]
  (cond
   (= c 0) 0
   :else (+ (- 255 (Math/abs (- (nth a c) (nth b c))))
            (add-fitness (- c 1) a b))))
   
(defn fitness [res target]
  (+ (* 50 (count (num-unique res)))
  ;   (count (num-unique (deriv res)))
     ;(reduce + 0 (deriv res))
     ;(min (count res) 20)
     
     (freq res 4)
     (freq res 6)
;     (/ (cond
;      (= (count res) 0) 0
;      (< (count res) (count target))
;      (add-fitness (- (count res) 1) res target)
;      :else
;      (add-fitness (- (count target) 1) res target))
;        10)
     
     ))

(defn go []
  (let [target [0 1 1 2 3 5 8 13 21 34 55 89 144]
        conf (DefaultConfiguration.)
        myFunc (proxy [FitnessFunction] []
                 (evaluate [chromo]
                           (let [res (run-bblocker 100 (chromo->list chromo))]


                             (if (and (> (count res) 0)
                                      (not (nil? (first res))))
                               ;(- 255 (Math/abs (- (first res) 99)))
                               (+ 1 (fitness res target))
                               0)
                           )))
        sampleGenes (IntegerGene. conf 0 256)
        sampleChromosome (Chromosome. conf sampleGenes 16)]
    
    (.setFitnessFunction conf myFunc)
    (.setSampleChromosome conf sampleChromosome)
    (.setPopulationSize conf 500)
    
    (let [population (Genotype/randomInitialGenotype conf)]
      (loop-evolve population 10000))))

(go)
