;-------------------------------------------------------------
; One bit pic drumsynth - (c) copyleft nebogeo noise research
;-------------------------------------------------------------

	Processor 16F84
	include "/usr/local/share/gputils/header/p16f84.inc"

#define RANDOM		0c
#define SYNTHSTATE	0d
	; 0 output 
	; 1 playing bit
	; 2 osc state
	
#define INSTRUMENT	0e
#define OSCFREQ		0f 
#define OSCPULSETIME	10
#define TEMPVAR		11

#define OHIHAT 		00
#define CHIHAT 		01
#define KICK   		02
#define SNARE  		03

;--------------------------------------------------------

start
	bsf 	STATUS, RP0
	movlw	D'1'
	movwf	TRISA		;set port a bits as inputs
	movlw	D'0'
	movwf	TRISB		;set port b bits as outputs
	bcf 	STATUS, RP0

	movlw	3		
	movwf	OSCFREQ		; set initial frequency
	movwf	OSCPULSETIME	; setup osc time
	movlw	B'00000010'	
	movwf	SYNTHSTATE	; initialise state (as playing)
loop
	clrwdt			; clear watchdog timer
	btfss	SYNTHSTATE,1	; if we are playing
	goto	loop
	call	osc

	btfss	SYNTHSTATE,0	; drive the output
	goto	outputset
	bcf	PORTB,0
	goto	loop
outputset
	bsf	PORTB,0
	goto	loop

;--------------------------------------------------------

hihat

;---------------------------------------------------------

kick
		
;--------------------------------------------------------

osc
	decf	OSCPULSETIME
	movf	OSCPULSETIME,W
	btfsc	STATUS,2	; is it time to flip? (time zero)
	call	flip		; yes
	retlw	0	
flip
	btfss	SYNTHSTATE,0	; flip output bit
	goto	flipset
	bcf	SYNTHSTATE,0
flipend
	movf	OSCFREQ,W	; reset time
	movwf	OSCPULSETIME
	retlw	0
flipset
	bsf	SYNTHSTATE,0
	goto	flipend

;---------------------------------------------------------

noise
	call	rnd
	movlw	RANDOM
	movwf	OSCFREQ	; use a random frequency on the osc
	call	osc
	retlw	0

;---------------------------------------------------------

rnd	
	movlw	01DH
	clrc	
	rlf	RANDOM
	skpnc
	xorwf   RANDOM
	retlw   0

end