	processor 16f84
	include	  <p16f84.inc>
	__config  _WDT_OFF

temp	EQU	H'10'
count	EQU	H'11'
current	EQU	H'12'

; clock at 4MHz = 1MHz cycles
; prescaler at 256 = 256us per rtcc 

;--------------------------
main:
	movlw	B'10000111' ; timer prescaler /256
	option	
	movlw	B'00000000' ; output
	tris	PORTA

;--------------------------
begin:
	movlw	D'62' ; about a sec?
	call	wait

	incf	current,f

	; wrap at 9
	movf	current,w	; load current
	sublw	D'9'		; subtract 9 from current
	btfss	STATUS,C	; if carry is set, skip
	clrf	current		; set current to 0

	movf	current,w
	call	sevenseg
	movwf	PORTA 
	goto 	begin

;---------------------------
; waits by value set in w * 16ms
wait:
	clrf	TMR0
	movwf	count
waitloop:
	btfss	TMR0,6	; rtcc bits 64*256us = 16ms
	goto	$-1
	clrf	TMR0
	decfsz	count,f
	goto	waitloop
	retlw	0

;---------------------------
; 7 seg display lookup
sevenseg:
	addwf	02,f
	retlw	B'0111111' ; 0
	retlw	B'0001001' ; 1
	retlw	B'1011110' ; 2
	retlw	B'1011011' ; 3
	retlw	B'1101001' ; 4
	retlw	B'1110011' ; 5
	retlw	B'1110111' ; 6
	retlw	B'0011001' ; 7
	retlw	B'1111111' ; 8
	retlw	B'1111001' ; 9

end
	