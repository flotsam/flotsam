; Assembly code for a 10MHz PIC16F84 microcontroller
; Modified from work by Ross Bencina

temp	equ	H'1F'
xmit	equ	H'1D'
i	equ	H'1C'
j	equ	H'1B'
k	equ	H'1A'

; sendmidi transmits one midi byte on RA2
; at 10mhz there are 80 instructions per midi bit
; xmit contains byte to send
; * this should be rewritten to support variable delays for
; * different clock speeds

sendmidi:			
	
startb:	bcf	PORTA, 0x02	; start bit

	movlw	D'24'		; delay 73 clocks: 2 + (23 * 3 + 1 * 2)
	movwf	temp		; |
loop1:	decfsz	temp,f		; |	
	goto	loop1		; end delay

	movlw	D'8'		
	movwf	j

sendloop:			; executes 5 instuctions before setting bit
	rrf	xmit,f
	btfsc	STATUS, C
	goto	send1
; remember midi bits are opposite from our representation
send0:	nop
	bcf	PORTA, 0x02	;send a 0 bit
	goto	endloop

send1:	bsf	PORTA, 0x02	;send a 1 bit
	nop
	nop

endloop:			;
			
	movlw	D'23'		;delay 70 instructions 2 + (22 * 3 + 1 * 2)
	movwf	temp		; |
loop2:	decfsz	temp,f		; |
	goto	loop2		; end delay

	decfsz	j,f		;
	goto	sendloop

stopb:
	nop
	nop
	nop
	nop
	nop
	bsf	PORTA, 0x02	; stop bit
	movlw	D'26'		; delay 79 clocks: 2 + (25 * 3 + 1 * 2)
	movwf	temp		; |
loop3:	decfsz	temp,f		; |
	goto	loop3		; end delay

	return
