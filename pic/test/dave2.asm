	Processor 16F84
	include "/usr/local/share/gputils/header/p16f84.inc"

#define RANDOM 0f

start
	bsf STATUS, RP0
	movlw	D'0'
	movwf	TRISA	;set port a bits as outputs
	movlw	D'0'
	movwf	TRISB	;set port b bits as outputs
	bcf STATUS, RP0
	movlw	00
	movwf	0d		; 0d is location of count var

	movlw	01
	movwf	TRISA	; set ports b to debug

loop
	clrwdt			; clear watchdog timer
	movlw	00
	movwf	TRISB	; set all ports b low
	movlw	0f
	incf	0d,1	; inc count 
	movf	0d,0	; copy to w
	movwf	TRISB	; send count to ports b	
	call	waitforabit	; stop for a bit
	goto	loop
waitforabit
	movlw	10
	movwf	0e
waitloop	
	decfsz	0e
	goto	waitloop
	return
rnd	
	movlw	01DH
	clrc	
	rlf		RANDOM
	skpnc
	xorwf   RANDOM
	retlw   0
	
	END	
