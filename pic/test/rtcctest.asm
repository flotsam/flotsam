	processor 16f84
	include	  <p16f84.inc>
	__config  _WDT_OFF

temp	EQU	H'10'
count	EQU	H'11'

; clock at 4MHz = 1MHz cycles
; prescaler at 256 = 256us per rtcc 

;--------------------------
main:
	movlw	B'10000111' ; timer prescaler /256
	option	
	movlw	B'00000000' ; output
	tris	PORTA

;--------------------------
begin:
	movlw	D'62' ; about a sec?
	call	wait

	comf	PORTA,f ; flip output bits

	goto 	begin

;---------------------------
; waits by value set in w * 16ms
wait:
	clrf	TMR0
	movwf	count
waitloop:
	btfss	TMR0,6	; rtcc bits 64*256us = 16ms
	goto	$-1
	clrf	TMR0
	decfsz	count,f
	goto	waitloop
	retlw	0

end

