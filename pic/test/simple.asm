	Processor 16F84
	include "/usr/local/share/gputils/header/p16f84.inc"

start
	bsf STATUS, RP0
	movlw	D'0'
	movwf	TRISA	;set port a bits as outputs
	movlw	D'0'
	movwf	TRISB	;set port b bits as outputs
	bcf STATUS, RP0
	movlw	00
	movwf	0d		; 0d is location of count var

	movlw   01
	movwf   TRISA ; test the a outputs a bit...

loop
	clrwdt			; clear watchdog timer
	movlw	00
	movwf	TRISB	; set all ports b low 
	movlw	0f
	movwf   TRISB  ; set all ports b high
	goto	loop
end
