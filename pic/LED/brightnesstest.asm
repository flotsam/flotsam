; CPU configuration
	processor 16f84
	include	  <p16f84.inc>
	__config  _HS_OSC & _WDT_OFF & _PWRTE_ON

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; variables

tick	EQU	H'10'   ; counter for pulsewidth (high freq)
cycle	EQU	H'11'	; counter for the colour cycle (low freq)
pw	EQU	H'12'	; pulsewidth for the LED

; button stuff

incd	EQU	H'13'	; button down
decd	EQU	H'14'
resd	EQU	H'15'
dbinc	EQU	H'16'	; debounce
dbdec	EQU	H'17'
dbres	EQU	H'18'

temp	EQU	H'19'
triin	EQU	H'1a'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; i/o pins

; port A
outpin	EQU	H'01'	; led pin 

; port B
incbpin EQU	H'01'   ; increment the brightness
decbpin EQU	H'02'	; decrement brightness
resbpin	EQU	H'03'	; reset brightness

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

main:
	movlw   B'00000000'
        tris    PORTA           ; init port A as output
	movlw   B'11111111'
        tris    PORTB           ; init port B as input

	; init stuff
	clrf	tick
	clrf	cycle
	clrf	pw
	clrf	incd
	clrf	decd
	clrf	resd
	clrf	dbinc
	clrf	dbdec
	clrf	dbres

begin:

	; do the red pulsewidth
	movf	tick,w		; load tick to w
	subwf	pw,w		; subtract tick from rpw
	btfss	STATUS,C	; if carry is set, skip
	goto	redelse		; goto else
	bsf	PORTA,outpin	; set rpin on
	goto	redendif	; get out
redelse:
	bcf	PORTA,outpin	; else, rpin is off
redendif:

	incf	tick,f		; ++tick
	btfsc	STATUS,Z
	call	updatecycle	; update cycle once per pulse
	goto 	begin

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

updatecycle:
	call	checkinc	; check the increment button
	btfsc	incd,0		; if button has been pressed, increment
	incf	cycle,f

	call	checkdec	; check the decrement button
	btfsc	decd,0		; if button has been pressed, decrement
	decf	cycle,f

	call	checkres	; check the reset button
	btfsc	resd,0		; if button has been pressed, reset
	clrf	cycle

	; call maketriwave for red
	movf	cycle,w		; load the cycle
	movwf	triin		; into tryiin
	call	maketriwave	
	movwf	pw		; move result into red pulse width

	return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; turns pin on/off into momentry event when from off to on
checkinc:
	btfsc	PORTB,incbpin	; if button is not pressed
	goto	incbpressed	
	clrf	dbinc		; set debounce false
	clrf	incd		; set button off
	goto	endcheckinc
incbpressed:			; else
	btfsc	dbinc,0		; if debounce is false
	goto	incdbtrue
	movlw	D'1'
	movwf	dbinc		; set debounce true
	movwf	incd		; set button on
	goto	endcheckinc
incdbtrue:			; else
	clrf	incd		; set button off
endcheckinc:
	return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; turns pin on/off into momentry event when from off to on
checkdec:
	btfsc	PORTB,decbpin	; if button is not pressed
	goto	decbpressed	
	clrf	dbdec		; set debounce false
	clrf	decd		; set button off
	goto	endcheckdec
decbpressed:			; else
	btfsc	dbdec,0		; if debounce is false
	goto	decdbtrue
	movlw	D'1'
	movwf	dbdec		; set debounce true
	movwf	decd		; set button on
	goto	endcheckdec
decdbtrue:			; else
	clrf	decd		; set button off
endcheckdec:
	return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; turns pin on/off into momentry event when from off to on
checkres:
	btfsc	PORTB,resbpin	; if button is not pressed
	goto	resbpressed	
	clrf	dbres		; set debounce false
	clrf	resd		; set button off
	goto	endcheckres
resbpressed:			; else
	btfsc	dbres,0		; if debounce is false
	goto	resdbtrue
	movlw	D'1'
	movwf	dbres		; set debounce true
	movwf	resd		; set button on
	goto	endcheckres
resdbtrue:			; else
	clrf	resd		; set button off
endcheckres:
	return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; converts contents of triin to wave on w
maketriwave:
	movlw	D'128'
	movwf	temp
	movf	triin,w
	subwf	temp,w
	btfsc	STATUS,C	; if triin is less than 128
	goto	triwaveup	; goin up!
	movwf	temp
	movlw	D'128'
	subwf	temp,w
	return			; returns 128-triin
triwaveup:
	;movlw	D'128'
	;subwf	triin,w		; shift down 128
	movf	triin,w		; returns triin
	return
		

end