; CPU configuration
	processor 16f84
	include	  <p16f84.inc>
	__config  _HS_OSC & _WDT_OFF & _PWRTE_ON

; variables

tick	EQU	H'10'   ; counter for pulsewidth (high freq)
cycle	EQU	H'11'	; counter for the colour cycle (low freq)
rpw	EQU	H'12'	; pulsewidths for rgb
gpw	EQU	H'13'
bpw	EQU	H'14'
triin	EQU	H'15'	; value to use with maketriwave
temp	EQU	H'16'
scycle	EQU	H'17'   ; slow version of the cycle

roff	EQU	H'17'	; phase offsets for the colour change 
goff	EQU	H'18'
boff	EQU	H'19'

rpin	EQU	H'01'	; i/o pins for rgb
gpin	EQU     H'02'   
bpin	EQU	H'03'
invrpin	EQU	H'04'	; inverse of r for two colour blending

main:
	movlw   B'00000000'
        tris    PORTA           ; init port A as output
	movlw   B'11111111'
        tris    PORTB           ; init port B as input

	; init stuff
	clrf	tick
	clrf	cycle
	clrf	rpw
	clrf	gpw
	clrf	bpw
	clrf	roff

	; setup the offset values
	movlw	D'64'
	movwf	goff
	movlw	D'128'
	movwf	boff

begin:

	; do the red pulsewidth
	movf	tick,w		; load tick to w
	subwf	rpw,w		; subtract tick from rpw
	btfss	STATUS,C	; if carry is set, skip
	goto	redelse		; goto else
	bsf	PORTA,rpin	; set rpin on
	bcf	PORTA,invrpin	; set reverse r off
	goto	redendif	; get out
redelse:
	bcf	PORTA,rpin	; else, rpin is off
	bsf	PORTA,invrpin	; reverse r pin on
redendif:


	; do the green pulsewidth
	movf	tick,w		; load tick to w
	subwf	gpw,w		; subtract tick from rpw
	btfss	STATUS,C	; if carry is set, skip
	goto	greenelse	; goto else
	bsf	PORTA,gpin	; set rpin on
	goto	greenendif	; get out
greenelse:
	bcf	PORTA,gpin	; else, gpin is off
greenendif:


	; do the blue pulsewidth
	movf	tick,w		; load tick to w
	subwf	bpw,w		; subtract tick from rpw
	btfss	STATUS,C	; if carry is set, skip
	goto	blueelse	; goto else
	bsf	PORTA,bpin	; set bpin on
	goto	blueendif	; get out
blueelse:
	bcf	PORTA,bpin	; else, bpin is off
blueendif:


	incf	tick,f		; ++tick
	btfsc	STATUS,Z
	call	updatecycle	; update cycle once per pulse
	goto 	begin

	; 23 instr
	; 24 with updatecycle

updatecycle:
	incf	cycle,f
	btfss	cycle,0		; every other cycle
	incf	scycle,f	; update slow cycle

	; call maketriwave for red
	movf	scycle,w	; load the cycle
	movwf	triin		; into tryiin
	call	maketriwave	
	movwf	rpw		; move result into red pulse width

	; call maketriwave for green
	movf	goff,w		; load the offset for green
	addwf	scycle,w	; add it to cycle
	movwf	triin		; put it in tryiin
	call	maketriwave	
	movwf	gpw		; move result into green pulse width

	; call maketriwave for blue
	movf	boff,w		; load the offset for blue
	addwf	scycle,w	; add it to cycle
	movwf	triin		; put it in tryiin
	call	maketriwave	
	movwf	bpw		; move result into blue pulse width

	return

; converts contents of triin to wave on w
maketriwave:
	movlw	D'128'
	movwf	temp
	movf	triin,w
	subwf	temp,w
	btfsc	STATUS,C	; if triin is less than 128
	goto	triwaveup	; goin up!
	movwf	temp
	movlw	D'128'
	subwf	temp,w
	return			; returns 128-triin
triwaveup:
	;movlw	D'128'
	;subwf	triin,w		; shift down 128
	movf	triin,w		; returns triin
	return
		

end