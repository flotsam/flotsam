
(define (patch-ref patch n)
    (list-ref patch (modulo n (length patch))))

(define sub

        '(("freqa" 0.25 "freqb" 0.51 "typea" 3 "typeb" 3 "cutoff" 0.4 "resonance" 0.4 
       "lfodepth" 0.1 "lfofreq" 1 "decaya" 0.05 "sustaina" 0.05 "decayb" 0.05 
       "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)
        
         ("freqa" 1 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2 "typea" 2 
      "attacka" 0 "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "volumea" 0.4 "typeb" 2 
      "attackb" 0 "decayb" 0.05 "sustainb" 0.1 "releaseb" 0.5 "volumeb" 0.4 
      "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 "releasef" 0.5 "volumef" 0.2 
      "lfodepth" 0.5 "lfofreq" 0.1 "crushfreq" 0 "crushbits" 0 "slidea" 0.02 
      "slideb" 0.05 "distort" 0.7 "poly" 3)

           ("freqa" 0.5 "freqb" 1.501 "typea" 3 "cutoff" 0.1 "resonance" 0.4 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.04 "decayb" 0.04 "ftype" 1
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)
      
      ("freqa" 0.25 "freqb" 0.25001 "typea" 5 "typeb" 5 "cutoff" 0.5 "resonance" 0.4
        "lfodepth" 0.1 "lfofreq" 100 "attacka" 0 "attackb" 0 "decaya" 0.05 
            "decayb" 0.05 "volumea" 1 "volumeb" 1 "attackf" 0.1 "volumef" 1
           "decayb" 0.0 "slidea" 0 "slideb" 0 "poly" 1 "mainvolume" 1 "distort" 0)
        
     ("freqa" 2.0 "freqb" 0.251 "cutoff" 0.9 "resonance" 0.01 "ftype" 2 "typea" 8 
      "attacka" 0 "decaya" 0.5 "sustaina" 0 "releasea" 0 "typeb" 8 
      "attackb" 0 "decayb" 0.2 "sustainb" 0 "releaseb" 0 "attackf" 0 
      "decayf" 0.6 "sustainf" 0 "releasef" 0 "volumef" 0.2 "lfodepth" 0 "lfofreq" 5 
      "crushfreq" 0 "crushbits" 0 "mainvolume" 0.3
      "slidea" 0.5 "slideb" 0.3 "distort" 0.7 "poly" 3)      

    ("freqa" 0.5 "freqb" 0.501 "typea" 3 "cutoff" 0.1 "resonance" 0.4 "lfodepth" 0
       "lfofreq" 1 "decaya" 0.2 "decayb" 0.1 "ftype" 0
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)

    ("cutoff" 0.3 "typea" 7 "freqb" 0.250001 "typeb" 0 "attacka" 1 "attackb" 1
     "ftype" 0 "decaya" 20.2 "decayb" 20.2 "resonance" 0.1 "attackf" 0.6 
     "decayf" 0.1 "sustainf" 0.0 "volumef" 0.2 "volumea" 2 "volumeb" 2 
     "lfodepth" 0.2 "lfofreq" 0.1 "poly" 4 "distort" 0)
     
     ("freqa" 0.25 "freqb" 0.251 "typea" 3 "typeb" 3 "cutoff" 0.1 "resonance" 0.2 
      "lfodepth" 0.05 
       "lfofreq" 1 "decaya" 0.2 "decayb" 0.1 "ftype" 2 "distort" 0 "attackf" 0.4
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 2)
        
     ("freqa" 2 "freqb" 0.5 "typea" 7 "typeb" 8 "cutoff" 0.1 "resonance" 0.2 
        "decayf" 0.1 "lfodepth" 0 "lfofreq" 1 "decaya" 0.2 "decayb" 0.2 "distort" 0
        "volumef" 1 "decayf" 1 "poly" 3 "ftype" 3 "volumea" 3 "volumeb" 3
        "slidea" 0.5 "slideb" 0.5 "crushfreq" 0 "crushbits" 0 "mainvolume" 5)

     ("freqa" 0.125 "freqb" 0.2501 "typea" 3 "typeb" 0 "cutoff" 0.4 "resonance" 0.4 
        "decayf" 0.1 "attackf" 0
        "lfodepth" 0 "lfofreq" 1 "decaya" 0.3 "decayb" 0.4 "volumef" 0.1 "distort" 1
        "slidea" 0 "slideb" 0 "crushfreq" 0.9 "crushbits" 7 "mainvolume" 0.1)        
    
    ("freqa" 4 "freqb" 2.5 "distort" 0.4 
    "crushfreq" 0.2 "crushbits" 8 "typea" 7 "typeb" 7 "decaya" 0.6 "decayb" 0.6
    "mainvolume" 0.1 "slidea" 0.1 "slideb" 0.1 "cutoff" 0.5 "lfodepth" 0.5 
    "lfofreq" 0.1 "resonance" 0.4  "distort" 0
    "poly" 1)
    
     ("freqa" 0.125 "freqb" 0.2501 "typea" 3 "typeb" 0 "cutoff" 0.4 "resonance" 0.4 
        "decayf" 0.1 "attackf" 0
        "lfodepth" 0 "lfofreq" 1 "decaya" 0.1  "decayb" 0.1 "volumef" 0.1 "distort" 1
        "slidea" 0 "slideb" 0 "crushfreq" 0.9 "crushbits" 7 "mainvolume" 0.1)
        
        ("freqa" 0.5 "freqb" 0.501 "cutoff" 0.2 "resonance" 0.45 
    "ftype" 1 "typea" 1 "decaya" 0.2 "sustaina" 0.1 "releasea" 2 
    "volumea" 1 "typeb" 1 "attackb" 0 "decayb" 0.2 "sustainb" 0.1 "releaseb" 2 
    "volumeb" 1 "attackf" 0.4 "decayf" 0.6 "sustainf" 0 "releasef" 2 "volumef" 0.1
    "lfodepth" 0.1 "lfofreq" 0.1 "distort" 0.5 "poly" 6 "mainvolume" 0.1)
    
        ("freqa" 0.25 "freqb" 8.3 "typea" 3 "typeb" 3 "cutoff" 0.9
        "resonance" 0.2 "attackf" 0 "decayf" 0.5 "ring" 1.5
        "lfodepth" 0.8 "lfofreq" 0.1 "decaya" 0.1  "decayb" 0.1 "volumef" 0.1 
         "distort" 1 "sustaina" 0.05
        "slidea" 0 "slideb" 0.1 "crushfreq" 0.1 "crushbits" 5 "mainvolume" 0.5)
        
        ("freqa" 0.25 "freqb" 0.25 "typea" 3 "typeb" 3 "cutoff" 0.3
        "resonance" 0.45 "attackf" 0 "decayf" 0.1 
        "lfodepth" 0.2 "lfofreq" 0.1 "decaya" 0.05  "decayb" 0.05 "volumef" 0.5 
         "distort" 1
        "slidea" 0 "slideb" 0 "crushfreq" 0.1 "crushbits" 5 "mainvolume" 0.5)
        
        ("freqa" 0.5 "freqb" 2.57 "typea" 0 "typeb" 0 "cutoff" 0.9 "ring" 1.5 
        "resonance" 0.1 "attackf" 1 "decayf" 0.3 "poly" 8 "attackb" 2 "attacka" 0
        "lfodepth" 0 "lfofreq" 1 "decaya" 2  "decayb" 2 "volumef" 0.1 "distort" 0.4
        "slidea" 0.5 "slideb" 2 "crushfreq" 0.1 "crushbits" 8 "mainvolume" 5)))
        
        
(define fm 
        '(("freq" 0.5 "modfreq" 1.3 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.1 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1)

        ("freq" 0.5 "modfreq" 0.5 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.2 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1)

        ("freq" 0.25 "modfreq" 0.00125 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 2.6 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 1)

    ("freq" 0.125 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.3 
       "release" 0 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.6 "fbattack" 0.6 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 3 "fbvolume" 0.2 "slide" 0 
        "modslide" 0  "poly" 1 "mainvolume" 0.1 "crushfreq" 0.3 "crushbits" 2)
        
    ("freq" 1 "modfreq" 0.1 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.1 
      "release" 5.5 "modtype" 0 "modattack" 0.1 "moddecay" 0.2 "modsustain" 0.1 
      "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
      "volume" 1 "modvolume" 1 "fbvolume" 1 "crushfreq" 0 "crushbits" 0 "slide" 0.5 
       "modslide" 0.5  "poly" 5 "mainvolume" 0.3)
        
      ("freq" 0.5 "modfreq" 0.125 "type" 2 "attack" 0 "decay" 0.2 "sustain" 0.3 
       "release" 0.3 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 3 "fbvolume" 0.2 "crushfreq" 0 "crushbits" 0 "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 0.1 "distort" 0.4)
      
      ("freq" 0.5 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.2 "sustain" 0.3 
       "release" 0.3 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 0.0 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 5 "fbvolume" 0.2 "crushfreq" 0.05 "crushbits" 3 
        "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 0.04)

    ("freq" 2 "modfreq" 0.5 "type" 3 "attack" 1.5 "decay" 0.1 "sustain" 0.1 
       "release" 2.5 "modtype" 0 "modattack" 0.6 "moddecay" 0.6 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 2.6 "fbvolume" 0.3 "slide" 0 "modslide" 0 "poly" 4 
       "mainvolume" 0.1)

    ("freq" 1 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.1 "release" 0.5 
    "modtype" 0 "modattack" 0 "moddecay" 0.1 "modsustain" 0.0 "modrelease" 0.5 
    "fbattack" 2.5 "fbdecay" 0.1 "fbsustain" 0.1 "fbrelease" 0.5 "volume" 1 
     "modvolume" 3 "fbvolume" 2 "slide" 0 "modslide" 0 "poly" 1 "mainvolume" 0.5)

    ("freq" 0.5 "modfreq" 0.01 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.2 
     "release" 0.6 "modtype" 0 "modattack" 0.2 "moddecay" 0 "modsustain" 0.1 
     "modrelease" 0.5 "fbattack" 0.5 "fbdecay" 0.1 "fbsustain" 0.1 "fbrelease" 0.5 
     "volume" 1 "modvolume" 2 "fbvolume" 6 "crushfreq" 0.5 "crushbits" 8 
     "slide" 0 
     "modslide" 0  "poly" 0 "mainvolume" 0.3)))


        
(define drum 
        
        '(("kickfreqdecay" 0.2 "kickdecay" 2 "kickfreqvolume" 1
        "kickfreq" 0.5 "kickvolume" 1
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0.45
        "hat2decay" 0.5 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0.45
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.1
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.6 "snareresonance" 0.45
        "crushfreq" 0.1 "crushbits" 6 "poly" 1 "mainvolume" 0.1)
        
        ("kickfreqdecay" 0.08 "kickdecay" 0.2 "kickfreqvolume" 1
        "kickfreq" 0.4 "kickvolume" 2
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0
        "hat2decay" 0.1 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.8
    "snareftype" 2 "snarefilterattack" 0.2 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.1 "snareresonance" 0.4
        "crushfreq" 0.1 "crushbits" 2 "poly" 1 "mainvolume" 0.1)
        
        ("kickfreqdecay" 0.2 "kickdecay" 2 "kickfreqvolume" 1
        "kickfreq" 0.5 "kickvolume" 1
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0.45
        "hat2decay" 0.5 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0.45
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0 
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.6 "snareresonance" 0.45
        "crushfreq" 0.1 "crushbits" 6 "poly" 1 "mainvolume" 0.2)

        ("kickfreqdecay" 0.04 "kickdecay" 0.2 "kickfreqvolume" 5
        "kickfreq" 0.5 "kickvolume" 1
        "hat1decay" 0.05 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0
        "hat2decay" 0.05 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0
        "snaredecay" 0.05 "snarevolume" 1 "distort" 0.8
    "snareftype" 2 "snarefilterattack" 0.2 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.1 "snareresonance" 0.4
        "crushfreq" 0.1 "crushbits" 8 "poly" 1 "mainvolume" 0.3)
        
        ("kickfreqdecay" 0.01 "kickdecay" 0.4 "kickfreqvolume" 10
        "kickfreq" 0.5 "kickvolume" 2
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0.3
        "hat2decay" 0.05 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0.4
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.4 
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.6 "snareresonance" 0.3
        "crushfreq" 0.1 "crushbits" 6 "poly" 1 "mainvolume" 0.2)

    ("kickfreqdecay" 0.03 "kickdecay" 0.1 "kickfreqvolume" 1
        "kickfreq" 1 "kickvolume" 2
        "hat1decay" 0.01 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0.3
        "hat2decay" 0.05 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0.4
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.1 
    "snareftype" 0 "snarefilterattack" 0.2 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.8 "snareresonance" 0.3
        "crushfreq" 0.9 "crushbits" 3 "poly" 1 "mainvolume" 0.1)))
        
        
