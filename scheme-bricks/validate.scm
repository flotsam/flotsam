#lang scheme

(define dict '((sine . 1) (mooglp . 3)))

(require mzlib/string)

(define str "(sine (mooglp (sine 4) 4 3 2)))")

(define tree '(sine (mooglp (sine 4) 0 4 2)))

(define (arg-error tok tree)
  (printf "~a has ~a arguments, should be ~a~n" 
          (car tok) (length (cdr tree)) (cdr tok))
  #f)

(define (lookup-error tree)
  (printf "~a is an unknown function ~n" (car tree))
  #f)

(define (parse-error tree)
  (printf "don't understand what ~a is~n" (car tree))
  #f)

(define (validate-string str dict)
  (validate (eval-string (string-append "'(" str ")")) dict))

(define (validate tree dict)
  (cond ((or (number? tree) (string? tree)) #t)
        (else
         (if (list? tree)
             (let ((tok (assoc (car tree) dict)))
               (cond
                 ((and tok (eq? (length (cdr tree)) (cdr tok)))
                  (foldl
                   (lambda (arg r)
                     (if r (validate dict arg) r))
                   #t
                   (cdr tree)))
                 (else 
                  (if tok 
                      (arg-error tok tree) 
                      (lookup-error tree)))))
             (parse-error tree)))))

(display (validate-string str dict))(newline)
