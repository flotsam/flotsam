(define (rndvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) 
        (vector 0.5 0.5 0.5)))

(clear)
(clear-colour (vector 0 0 0))
(fluxus-init)
(push)
(shader "badprint.vert.glsl" 
        "badprint.frag.glsl")
;(scale (vector 5 5 5))
;(translate (vector -0.5 -0.5 -0.5))
;(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(define b (obj-make (obj-load "/home/dave/plimp.obj")))
(pop)

(grab b)
;(load "test-object.scm")

;(shader-set! (list "LightPos" (vector 10 10 0)
;                   "Size"     (vmul (rndvec) 1000)
;                   "Scale"    (vadd (rndvec) (vector 0.1 0.1 0.1))
;                   "Offset"   (vadd (vmul (rndvec) 1) (vector 1 1 1))
;                   "Register" (rndvec)
;                    ))

(shader-set! (list "LightPos" (vector 10 10 0)
                   "Size"     (vector 200 200 200)
                   "Scale"    (vector 0.3 0.3 0.3)
                   "Offset"   (vector 0.5 0.5 0.5)
                   "Register" (vector 0 0 0)
                    ))


(ungrab)

;(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
