uniform vec3 Tint;
uniform float Roughness;
varying vec3 N;
varying vec3 L;
varying vec3 V;

void main()
{ 
    vec3 n = normalize(N);
    vec3 v = normalize(V);
    vec3 l = normalize(L);
    float vdotn = dot(v,n);
    float ldotn = dot(l,n);
    float cos_theta_i = ldotn;
    float theta_r = acos(vdotn);
    float theta_i = acos(cos_theta_i);
    float cos_phi_diff = dot(normalize(v-n*vdotn), 
                             normalize(l-n*ldotn));
    float alpha = max(theta_i,theta_r);
    float beta = min(theta_i,theta_r);

    float r = Roughness*Roughness;
    float a = 1.0-0.5*r/(r+0.33);
    float b = 0.45*r/(r+0.09);

    if (cos_phi_diff>=0)
    {    
        b*=sin(alpha)*tan(beta);
    }
    else
    {
        b=0.0;
    }

    float o = cos_theta_i * (a+b);
    gl_FragColor = vec4(o,o,o,1);
//    gl_FragColor = vec4(Tint*orennayar,1.0);
}
