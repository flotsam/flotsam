// a daft little fragment shader to test fluxus
// abount as simple as it gets

varying vec3 LightDir;
varying vec3 Normal;

void main()
{ 
	float lambert = dot(LightDir,Normal);
	clamp(lambert,0.0,1.0);
	gl_FragColor = vec4(lambert,lambert,lambert,1);
}
