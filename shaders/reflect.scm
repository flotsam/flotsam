

(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(texture (load-texture "green.png"))
(shader "reflect.vert.glsl" 
        "reflect.frag.glsl")
(translate (vector -0.5 -0.5 -0.5))
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)



(grab b)
(load "test-object.scm")

(shader-set! (list "ReflectionMap" 0))
(ungrab)


;(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
