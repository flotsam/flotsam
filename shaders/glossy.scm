(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(shader "glossy.vert.glsl" 
        "glossy.frag.glsl")

;(scale (vector 5 5 5))
;(translate (vector -0.5 -0.5 -0.5))
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "LightPos" (vector -30 40 50)
                   "AmbientColour" (vector 0.1 0.1 0.1)
                   "DiffuseColour" (vector 1 0 0)
                   "SpecularColour" (vector 1 1 1)
                   "Roughness" 0.05
                   "AmbientIntensity" 1.0    
                   "DiffuseIntensity" 1.0    
                   "SpecularIntensity" 1.0    
                   "Sharpness" 0.6
                    ))
(ungrab)

(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
