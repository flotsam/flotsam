(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(shader "wood.vert.glsl" 
        "wood.frag.glsl")
;(scale (vector 5 5 5))
;(translate (vector -0.5 -0.5 -0.5))
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "LightPos" (vector 30 40 50)
                   "AmbientColour" (vector 0.1 0.1 0.1)
                   "DiffuseColour1" (vector 0.8 0.4 0.1)
                   "DiffuseColour2" (vector 0.5 0.1 0.1)
                   "SpecularColour" (vector 1 1 1)
                   "Shinyness" 50.0
                   "AmbientIntensity" 1.0    
                   "DiffuseIntensity" 1.0    
                   "SpecularIntensity1" 0.5  
                   "SpecularIntensity2" 1.0  
                   "GrainMult" 500.0
                   "GrainCentre" (vector 0 2 3)  
                    ))
(ungrab)

(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
