uniform vec3 LightPosition;
varying vec3 LightDir;
varying vec3 Normal;

void main()
{    
    Normal = gl_NormalMatrix*gl_Normal; 
    
    LightDir = LightPosition-vec3(gl_Position);
    normalize(LightDir);
    gl_Position = ftransform();
}
