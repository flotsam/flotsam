
(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(shader "toon.vert.glsl" 
        "toon.frag.glsl")
;(translate (vector -0.5 -0.5 -0.5))
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "LightPos" (vector 30 40 50)
                   "HighlightColour" (vector 0.6 0.9 0.6 1)
                   "MidColour" (vector 0 0 1 1)
                   "ShadowColour" (vector 0 0 0.4 1)
                   "HighlightSize" 0.1
                   "ShadowSize" 0.2
                   "OutlineWidth" 0.4))
(ungrab)


(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
