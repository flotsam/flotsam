(clear)
(clear-colour (vector 0.6 0.2 0.1))
(fluxus-init)
(push)
(shader "aniso.vert.glsl" 
        "aniso.frag.glsl")
;(scale (vector 5 5 5))
;(translate (vector -0.5 -0.5 -0.5))
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
;(define b (obj-make (obj-load "/home/dave/plimp.obj")))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "LightPos" (vector 10 40 200)
                   "AmbientColour" (vector 0.1 0.1 0.1)
                   "DiffuseColour" (vector 1.0 0.5 0.1)
                   "SpecularColour" (vector 1 1 1)
                   "Shinyness" 20.0
                   "AnisoPow" 50.0
                   "AmbientIntensity" 0.0    
                   "DiffuseIntensity" 1.0    
                   "SpecularIntensity" 1.0    
                   "SpecDirection" (vector 1 1 -0.4)
                    ))
(ungrab)

(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
