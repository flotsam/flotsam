

(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(shader "facingratio.vert.glsl" 
        "facingratio.frag.glsl")
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "InnerColour" (vector 0 0.2 0 1)
                   "OuterColour" (vector 0 0.5 1 1)))
(ungrab)

(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
