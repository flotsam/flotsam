varying vec3 N;
varying vec3 V;

void main()
{    
    N = normalize(gl_NormalMatrix*gl_Normal); 
	V = -vec3(gl_ModelViewMatrix*gl_Vertex);
    gl_Position = ftransform();
}
