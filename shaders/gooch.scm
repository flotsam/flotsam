(clear)
(clear-colour (vector 0 0.2 0.5))
(fluxus-init)
(push)
(shader "gooch.vert.glsl" 
        "gooch.frag.glsl")
(define b (build-blobby 5 (vector 30 30 30) (vector 1 1 1)))
(pop)

(grab b)
(load "test-object.scm")

(shader-set! (list "LightPos" (vector 30 40 50)
                   "DiffuseColour" (vector 1 1 1)
                   "WarmColour" (vector 0.6 0.6 0)
                   "CoolColour" (vector 0 0 0.6)
                   "OutlineWidth" 0.4
                    ))
(ungrab)

(set-camera-transform (mtranslate (vector -0.5 -0.5 -1.7)))
