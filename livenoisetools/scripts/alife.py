# Copyright (C) 2004 David Griffiths <dave@pawfal.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import string
import random
import operator

def writestring(string,ofile):
	ofile.write(str(len(string))+"\n"+string+"\n")

def readstring(ifile):
	line=ifile.readline()
	length=int(line)
	text=ifile.read(length)
	ifile.read(1) # read past newline
	return text

score_language = ['o','O','+','-','+','-','+','-','.','+','-','.','\\','|','/','<','%','[',']','[',']','[',']','1','2','3','4','5','6','7','8','9']
MAX_LSYS_STRING_SIZE = 4096

def MutateString(code,strength):
	ret=""
	for c in range(len(code)):
		if (random.random()<strength):
			if (random.random()<0.1):
				ret+='!'
			else:
				ret+=random.choice(score_language)
		else:
			ret+=code[c]
	return ret

def MakeString(size):
	string=""
	for i in range(0,size):
		string+=random.choice(score_language)
	return string

######################################################################################

class lsystem:
	"lindenmeyer system for strings, thankyou Aristid!"
	def __init__(self):
		self.axiom="!"
		self.string=""
		self.rules={}

	def setup(self):
		self.string=self.axiom
		
	def debug(self):
		print(self.axiom)
		print(self.string)
		for key,value in self.rules.items():
			print(key+" "+value)
		
	def process(self,generations):
		for i in range(0,generations):
			for key,value in self.rules.items():
				if len(self.string) < MAX_LSYS_STRING_SIZE:
					self.string=self.string.replace(key, value)
					
	def get(self):
		code=self.axiom+"\n"
		for i in range(1,len(self.rules)+1):
			code=code+self.rules[str(i)]+"\n"
		return code
		
	def set(self,code):
		self.string=""
		self.rules={}
		tokenised = code.split()
		if len(tokenised)>0:
			self.axiom=tokenised[0]
			for i in range(1,len(tokenised)):
				self.rules[str(i)]=tokenised[i]
				
	def save(self,ofile):
		writestring(self.axiom,ofile)
		ofile.write(str(len(self.rules))+"\n")
		for key,value in self.rules.items():
			writestring(key,ofile)
			writestring(value,ofile)

	def load(self,ifile):
		self.axiom=readstring(ifile)
		length = int(ifile.readline())
		key=""
		value=""
		for i in range(0,length):
			key=readstring(ifile)
			value=readstring(ifile)
			self.rules[key]=value

######################################################################################
	
class lsystembreeder:
	"a genetic breeder for lsystems"
	def __init__(self,popsize,mutrate):
		self.popsize=popsize
		self.mutrate=mutrate
		self.population=[]
		self.stringmin=1
		self.stringmax=20
		self.generations=1
		for i in range(0,self.popsize):
			self.population.append(lsystem())
		
	def reset(self):
		for lsys in self.population:
			lsys.axiom="!"+MakeString(random.randrange(self.stringmin,self.stringmax))
			for i in range(1,random.randrange(1,9)):
				lsys.rules[str(i)]=MakeString(random.randrange(self.stringmin,self.stringmax))
			lsys.setup()
			lsys.process(self.generations)

	def regenerate(self,generations):
		self.generations=generations
		for lsys in self.population:
			lsys.setup()
			lsys.process(self.generations)	
				
	def pick(self,chosen):
		for i in range(0,self.popsize):
			if i is not chosen:
				self.population[i].axiom=MutateString(self.population[chosen].axiom,self.mutrate)
				self.population[i].rules={}
				for key,value in self.population[chosen].rules.items():
					self.population[i].rules[key]=MutateString(self.population[chosen].rules[key],self.mutrate)
				self.population[i].setup()
				self.population[i].process(self.generations)
	
	def insert(self,score):
		self.population[0].set(score)
		self.population[0].setup()
		self.population[0].process(self.generations)
		self.pick(0)
