; pattern cascade kit 1.0.0
; o.+o.--o.+o.o.+o.--o.+o..
; (c) 2006   dave griffiths 
; GPL licence (see COPYING)
; ~~~~~~~~~~~~~~~~~~~~~~~~~

; this script define a simple patch system 
; for easy switching of voices when livecoding

(define (patch-ref patch n)
	; modulo so we don't have to remember how many voices there are :)
    (list-ref patch (modulo n (length patch)))) 

; some patches to get you started
	
(define sub

        '(("freqa" 0.5 "freqb" 0.51 "typea" 3 "typeb" 3 "cutoff" 0.4 "resonance" 0.4 
       "lfodepth" 0.1 "lfofreq" 1 "decaya" 0.05 "sustaina" 0.05 "decayb" 0.05 
       "slidea" 0.1 "slideb" 0.1 "mainvolume" 5)
        
         ("freqa" 1 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2 "typea" 2 
      "attacka" 0 "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "volumea" 0.4 "typeb" 2 
      "attackb" 0 "decayb" 0.05 "sustainb" 0.1 "releaseb" 0.5 "volumeb" 0.4 
      "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 "releasef" 0.5 "volumef" 0.2 
      "lfodepth" 0.5 "lfofreq" 0.1 "slidea" 0.02 "slideb" 0.05 "distort" 0.7 "poly" 3 "mainvolume" 5)

           ("freqa" 0.5 "freqb" 1.501 "typea" 3 "cutoff" 0.1 "resonance" 0.4 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.04 "decayb" 0.04 "ftype" 1
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 5)))
        
        
(define fm 
        '(("freq" 1 "modfreq" 0.5 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.5 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 0.5)

        ("freq" 1 "modfreq" 1 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 2 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 0.5)

        ("freq" 1 "modfreq" 0.00125 "type" 3 "attack" 0.1 "decay" 0.1 
        "sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 2.6 "fbvolume" 0 "slide" 0 
        "modslide" 0 "poly" 4 "mainvolume" 0.5)))
        
(define drum 
        
        '(("kickfreqdecay" 0.2 "kickdecay" 2 "kickfreqvolume" 1
        "kickfreq" 0.5 "kickvolume" 1
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0.45
        "hat2decay" 0.5 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0.45
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.1
    "snareftype" 0 "snarefilterattack" 0 "snarefilterdecay" 0.1 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.6 "snareresonance" 0.45
        "crushfreq" 0.1 "crushbits" 6 "poly" 1 "mainvolume" 0.1)
        
        ("kickfreqdecay" 0.08 "kickdecay" 0.2 "kickfreqvolume" 1
        "kickfreq" 0.4 "kickvolume" 2
        "hat1decay" 0.1 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0
        "hat2decay" 0.1 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0.8
    "snareftype" 2 "snarefilterattack" 0.2 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.1 "snareresonance" 0.4
        "crushfreq" 0.1 "crushbits" 2 "poly" 1 "mainvolume" 0.1)
        
        ("kickfreqdecay" 0.08 "kickdecay" 0.2 "kickfreqvolume" 1
        "kickfreq" 0.4 "kickvolume" 2
        "hat1decay" 0.08 "hat1volume" 1 "hat1cutoff" 0.5 "hat1resonance" 0
        "hat2decay" 0.03 "hat2volume" 1 "hat2cutoff" 0.9 "hat2resonance" 0
        "snaredecay" 0.1 "snarevolume" 1 "distort" 0
    "snareftype" 2 "snarefilterattack" 0.2 "snarefilterdecay" 0.5 
    "snarefiltersustain" 0 "snarefilterrelease" 0 "snarefiltervolume" 1
        "snarecutoff" 0.1 "snareresonance" 0.4
        "crushfreq" 0 "crushbits" 2 "poly" 1 "mainvolume" 0.1)))
        
        
