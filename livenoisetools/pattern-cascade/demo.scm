; pattern cascade kit 1.0.0
; o.+o.--o.+o.o.+o.--o.+o..
; (c) 2006   dave griffiths 
; GPL licence (see COPYING)
; ~~~~~~~~~~~~~~~~~~~~~~~~~

; a demo 
(clear)

(load "scheme/pattern.scm")
(load "scheme/cubes.scm")
(load "scheme/patches.scm")
(load "scheme/lsys.scm")

(pattern-top)

(pattern-voice 1 "sub" (patch-ref sub 1))
(pattern-voice 2 "fm" (patch-ref fm 0))
(pattern-voice 3 "drum" (patch-ref drum 1))

(pattern 1 4 (ls-gen 5 "!<1" 
    (list 
        (cons "1" "[O.2++++///+O.----.-o.]")
        (cons "2" "--1++12"))))

(pattern 2 2 (ls-gen 2 "!<++++++++++++1--1" 
    (list 
        (cons "1" "[O..2+++++O.----.-o.]")
        (cons "2" "--1++12"))))

(pattern 3 4 (ls-gen 1 "!111+++1" 
    (list 
        (cons "1" "[O.++o.++O.+o.]"))))

(pattern-cascade '(1 2 3))
