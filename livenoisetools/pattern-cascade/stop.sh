# pattern cascade kit 1.0.0
# o.+o.--o.+o.o.+o.--o.+o..
# (c) 2006   dave griffiths 
# GPL licence (see COPYING)
# ~~~~~~~~~~~~~~~~~~~~~~~~~

killall -9 jackd >& /dev/null
killall -9 noiseweapon >& /dev/null
killall -9 noisepattern >& /dev/null
killall -9 noiselab >& /dev/null
killall -9 kitchensync >& /dev/null
killall -9 fluxus >& /dev/null
