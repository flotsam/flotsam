; pattern cascade kit 1.0.0
; o.+o.--o.+o.o.+o.--o.+o..
; (c) 2006   dave griffiths 
; GPL licence (see COPYING)
; ~~~~~~~~~~~~~~~~~~~~~~~~~

; a very simple documented pattern cascade script - 
; use this as a starting point for your livecoding

; I think you need to clear fluxus, as the visuals 
; scripts may not tidy up after themselves
(clear)

; comment this out after the first time, or put 
; it in your .fluxus.scm init script in your home
; directory - it'll make the audio skip if you 
; load it every time you hit F5
(load "scheme/pattern.scm")

; load scripts we need (these are ok every F5)
(load "scheme/cubes.scm")
(load "scheme/patches.scm")
(load "scheme/lsys.scm")

; starts the pattern description off
(pattern-top)

; load a voice from the patch system (see patches.scm)
(pattern-voice 1 "sub" (patch-ref sub 0))

; patterns description
; the patterns describe the melodic and visual structure, 
; and are the core of the pattern cascade system

; pattern defines a new pattern:
; (pattern id speed scorecode-string)

; and ls-gen creates a string for you from the 
; lsystem rules you specify:
; (ls-gen generations axiom ruleslist)
; where a rules list looks like:
; (("fromstr" . "tostring") ("fromstr2" . "tostring2") ...)

(pattern 1 4 
    (ls-gen 5 "!<1" 
        (list 
            (cons "1" "O.o.o.o."))))

; ls-gen will evaluate the axiom and rules, and will 
; replace the "1" in the axiom with "O.o.o.o.", returning
; the string "!<O.o.o.o."

; more explanation: the ruleslist is a list containing 
; strings to search for and their replacement strings, 
; so for example, the first example lsystem described in 
; the wikipedia article on l systems (algae growth) would 
; look like this:
;
; (("A" . "AB")("B" . "A"))
;
; so:
;
; (ls-gen 4 "A" '(("A" . "AB")("B" . "A")))
;
; will return: "ABAABABA". most of the time I use cons 
; and list to build the rules explicitly - this just 
; means we can put variables into the list and they get 
; evaluated, as opposed to quoting lists like above, where 
; they wont.
;
; (ls-gen 4 "A" (list (cons "A" "AB")(cons "B" "A")))
;
; means you can do:
;
; (define rule1 "AB")
; (ls-gen 4 "A" (list (cons "A" rule1)(cons "B" "A")))
; 
; which is very useful control multiple patterns at once, 
; and making immediate sweeping changes to a song.

