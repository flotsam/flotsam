# pattern cascade kit 1.0.0
# o.+o.--o.+o.o.+o.--o.+o..
# (c) 2006   dave griffiths 
# GPL licence (see COPYING)
# ~~~~~~~~~~~~~~~~~~~~~~~~~

./stop.sh
# this is what I use with my setup, you'll need to edit...
jackd -R -dalsa -dhw:0 -r44100 -p2048 -n2 -s >& /dev/null &
sleep 1
noisepattern 4000 &
noiseweapon 4001 &
noiselab 4002 &
kitchensync 4003 osc.udp://localhost:4000 osc.udp://localhost:4004 osc.udp://localhost:4003 &
fluxus $1 &
