	; a test program

	psh		"--------------------------------"
	snd
	psh	 	"self test starting..."
	snd
	psh		"--------------------------------"
	snd
	psh 	"simple test (string equ)..."
	snd
	psh 	"test"
	psh 	"test2"
	equ
	jmz 	simplepass
	psh 	"it's the same?! bzzzt! fail :("
	snd
	jmp 	end
	
simplepass:
	psh 	"it's different! passed :)"
	snd



; maths tests

	psh		"--------------------------------"
	snd
	psh 	"maths tests..."
	snd
	psh 	10
	psh 	10
	add
	inc
	inc
	psh		100
	mul
	psh		2195
	sub
	psh		2
	div
	psh		2.5
	equ
	jmz		mathfail
	jmp		mathpass
	
mathfail:
	psh		"bzzzt! fail :("
	jmp		end

mathpass:
	psh		"passed :)"
	snd




; mem tests
	psh		"--------------------------------"
	snd
	psh		"mem tests..."
	snd
	psh		324.3
	wrf		0x10
	psh		"gone to mem"
	snd
	rdf		0x10
	psh		324.3
	equ
	jmz		memfail
	jmp		mempass

memfail:
	psh		"mem fail :("
	snd
	jmp		end

mempass:
	psh 	"mem pass! :)"
	snd	

end:
	
	
; register tests
	psh		"--------------------------------"
	snd
	psh		"register tests..."
	snd
	psh		"pc is"
	snd
	rdf		PC
	snd
	psh		"debug is"
	snd
	rdf		DEBUG
	snd
	psh		1
	wrf		DEBUG
	psh		"debugging should now be on..."
	snd
	psh		0
	wrf		DEBUG
	psh		"debugging should now be off..."
	snd

; loop, call and osc send tests
	psh		"--------------------------------"
	snd
	psh		"loop, call and osc msg tests..."
	snd
	psh		5
	wrf 	0xff
loop:
	cal		sendmsg
	rdf		0xff    ; load counter
	dec             ; decrement counter
	wrf		0xff    ; write counter back
	rdf		0xff    ; load counter again
	psh		0       
	equ             ; is it equal to 0?
	jmz		loop    ; loop if it isn't
	jmp 	endoscloop

sendmsg:
	psh		"sending..."
	snd
	rdf		0xff
	snd
	psh	 	1       ; data item 2 (push msg data in reverse)
	psh	 	1       ; data item 1
	psh	 	"ii"    ; format
	psh		"/blah" ; message path
	osc             ; send osc msg
	ret
	
endoscloop: 

; indirect addressing test
	psh		"--------------------------------"
	snd
	psh 	"indirect addressing test"
	snd
	
	psh		0
	wrf 	0xff
	psh		0x50    ; location to start writing to
	wrf		FSR
	
writeloop:
	
	psh		"writing"
	snd
	
	psh		4534.3
	wrf		*		; indirect addr register

	rdf		FSR 	; load fsr
	inc             ; increment fsr
	wrf		FSR    	; write fsr back
		
	rdf		0xff    ; load counter
	inc             ; increment counter
	wrf		0xff    ; write counter back
	rdf		0xff    ; load counter again
	psh		10       
	equ             ; is it equal to 10?
	jmz		writeloop    ; loop if it isn't

; read back

	psh		0
	wrf 	0xff
	psh		0x50    ; location to start reading from
	wrf		FSR		; FSR register	

readloop:
	
	rdf		*		; indirect addr register
	snd

	rdf		*		; indirect addr register
	psh		4534.3
	equ
	jmz		idraddrerror

	rdf		FSR    	; load fsr
	inc             ; increment fsr
	wrf		FSR    	; write fsr back
		
	rdf		0xff    ; load counter
	inc             ; increment counter
	wrf		0xff    ; write counter back
	rdf		0xff    ; load counter again
	psh		10       
	equ             ; is it equal to 10?
	jmz		readloop    ; loop if it isn't
	jmp		idraddrend
	
idraddrerror:
	psh		"indirect addressing error! :("
	snd
	jmp 	endloop

idraddrend:

	psh		"--------------------------------"
	snd
	psh		"define test"
	snd
	
def MY_tEsT 454

	psh		MY_tEsT
	psh		454
	equ
	jmz		deftestfail
	jmp		deftestend
	
deftestfail:
	psh		"define test failed :("
	snd
	jmp 	endloop
	
deftestend:
	psh		"define test passed :)"
	snd
	psh		"--------------------------------"
	snd
	psh		"string tests"
	snd
	psh 	5
	psh		"hello"
	mul
	psh		"hellohellohellohellohello"
	equ
	jmz		stringfail
	psh		"hello"
	psh 	5
	mul
	psh		"hellohellohellohellohello"
	equ
	jmz		stringfail
	psh		"onetwothree"
	psh		6
	div
	snd
	snd
	jmp		stringpassed
	
stringfail:
	psh		"string fail :("
	snd
	jmp		end
	
stringpassed:
	psh		"string passed :)"
	snd
	
end:
	psh		"--------------------------------"
	snd
	psh		"all passed! :D"
	snd
endloop:
	jmp		endloop
