; a program that plays itself

def counter 0xff          ; address of the loop counter

  nop                     ; bug - seems to skip the first instruction...
  psh "osc.udp://localhost:88001" ; push the destination osc port
  mov OSC_OUT_PORT        ; sets the osc message destination register                                     
  psh CODE_START          ; push the start address of code in memory                                      
  mov FSR                 ; load the code location to the indirect addressing register                    
  psh 80                  ; we'll play 80 memory addresses (the machine code is a bit smaller than that)  
  mov counter             ; load to the loop counter address                                              
	
loop:
  cal oscnotesend         ; send a note                                                                   
  lod FSR                 ; push the indirect address                                                     
  inc                     ; increment 1                                                                   
  mov FSR                 ; copy back                                                                     
  lod counter             ; push the counter                                                              
  dec                     ; decrement by 1                                                                
  mov counter             ; copy back                                                                     
	
  lod counter             ; push the counter again                                                        
  psh 0                   ; push zero                                                                     
  equ                     ; leaves 1 if the top two stack items are the same                              
  jmz loop                ; if 0, jump back to loop                                                       
  jmp end                 ; else jump to end                                                              
		
oscnotesend:              ; send osc note (stack based, so arguments go in reverse)                       
  psh 79                  ; sets accented note                                                            
  psh 0                   ; pan                                                                           
  psh 1                   ; volume                                                                        
  psh 0                   ; slidefreq (not used)                                                          
  lod *                   ; loads the value from the address in the FSR (pointer style)                   
  psh 100                 ; push 100                                                                      
  add                     ; add to the value to up the pitch a little (becomes the frequency)             
  psh 1                   ; instrument id                                                                 
  psh 0                   ; timestamp frac  (not used)                                                    
  psh 0                   ; timestamp seconds  (not used)                                                 
  psh "iiiffffi"          ; osc message format                                                            
  psh "/play"             ; the message path name                                                         
  osc                     ; finally - send the contents of the stack                                      
  ret                     ; return                                                                        
	
end:
  jmp end                 ; loop forever                                                                  
