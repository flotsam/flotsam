	psh		"osc.udp://localhost:5555"
	mov		OSC_OUT_PORT
	def		counter 0xff
	psh 	0
	mov		counter

loop
	lod		counter
	inc
	mov		counter
	psh		"sending..."
	snd
	lod		0xff
	lod	 	counter ; data item 2 (push msg data in reverse)
	psh	 	1       ; data item 1
	psh	 	"ii"    ; format
	psh		"/blah" ; message path
	osc             ; send osc msg
	jmp 	loop
