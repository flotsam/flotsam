#include <iostream>
#include <lo/lo.h>
#include <unistd.h>

using namespace std;

int DefaultHandler(const char *path, const char *types, lo_arg **argv,
		    int argc, void *data, void *user_data)
{
	cerr<<"received ["<<path<<"] ["<<types<<"] ";
	for (unsigned int i=0; i<strlen(types); i++)
	{
		switch (types[i])
		{
			case 'i': cerr<<argv[i]->i<<" "; break;
			case 'f': cerr<<argv[i]->f<<" "; break;
			case 's': cerr<<&argv[i]->s<<" "; break;
			default: break;
		}
	}
	cerr<<endl;
    return 1;
}

void ErrorHandler(int num, const char *msg, const char *path)
{
    cerr<<"liblo server error "<<num<<" in path "<<path<<": "<<msg<<endl;
}
	
int main(int argc, char **argv)
{
	lo_server_thread server = lo_server_thread_new(argv[1], ErrorHandler);
    lo_server_thread_add_method(server, NULL, NULL, DefaultHandler, NULL);
	lo_server_thread_start(server);
	while (1) { sleep(1); }
}
