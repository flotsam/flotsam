// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <iostream>

#include "OSCServer.h"

using namespace std;

static const unsigned int MAX_MSGS_STORED=2048;

bool Server::m_Exit=false;
pthread_mutex_t* Server::m_Mutex;
MarmaladeMachine Server::m_MVM(0x40);
string Server::m_ClientUrl;
bool Server::m_ClientRegistered=false;

Server::Server(const string &Port) 
{
	cerr<<"osc port ["<<Port<<"]"<<endl;
    m_Server = lo_server_thread_new(Port.c_str(), ErrorHandler);
    lo_server_thread_add_method(m_Server, NULL, NULL, DefaultHandler, NULL);
    lo_server_thread_add_method(m_Server, "/reset", NULL, ResetHandler, NULL);
    lo_server_thread_add_method(m_Server, "/client", "s", ClientHandler, NULL);
    lo_server_thread_add_method(m_Server, "/asm", "s", AsmHandler, NULL);
    lo_server_thread_add_method(m_Server, "/peek", "i", PeekHandler, NULL);
    lo_server_thread_add_method(m_Server, "/poke", "ii", PokeHandleri, NULL);
    lo_server_thread_add_method(m_Server, "/poke", "if", PokeHandlerf, NULL);
    lo_server_thread_add_method(m_Server, "/poke", "is", PokeHandlers, NULL);
	m_Mutex = new pthread_mutex_t;
	pthread_mutex_init(m_Mutex,NULL);
}

Server::~Server()
{
	m_Exit=true;
}

void Server::Run()
{
	cerr<<"Starting osc server"<<endl;
	lo_server_thread_start(m_Server);
	MarmaladeMachine::MsgDesc msg;
	while(1)
	{
		pthread_mutex_lock(m_Mutex);
		m_MVM.Execute();
		
		// check for messages
		if (m_MVM.PollMessage(msg))
		{
			string port = m_MVM.GetOutPort();
			pthread_mutex_unlock(m_Mutex);
			
			// send message
			lo_address oscaddr=lo_address_new_from_url(port.c_str());
			lo_message oscmsg=lo_message_new();

			bool abort=false;

			// convert arguments from mvm data
			int count=0;
			for (vector<Data*>::iterator i=msg.Args.begin(); i!=msg.Args.end(); i++)
			{		
				switch((*i)->Type())
				{
					case 's': lo_message_add_string(oscmsg,static_cast<Str*>(*i)->Value.c_str()); break;
					case 'n': lo_message_add_float(oscmsg,(float)(static_cast<Num*>(*i)->Value)); break;
					default: break;
				}
				count++;

				if (!abort) 
				{
					lo_send_message(oscaddr, msg.Path.c_str(), oscmsg);
					cerr<<"sending ["<<msg.Path<<"] to ["<<port<<"]"<<endl;
				}
				
				lo_address_free(oscaddr);
				lo_message_free(oscmsg);
			}						
		}
		else
		{
			pthread_mutex_unlock(m_Mutex);
		}
		
		if (m_ClientRegistered)
		{
			DebugInfo di = m_MVM.GetDebugInfo();
			if (di.StackPtr!=NULL) // debugging is turned on?
			{		
				// send debug message to client
				lo_address oscaddr=lo_address_new_from_url(m_ClientUrl.c_str());
				lo_message oscmsg=lo_message_new();
				DebugInfo di = m_MVM.GetDebugInfo();

				lo_message_add_int32(oscmsg,di.LineNumber); 
				lo_message_add_int32(oscmsg,di.PC); 
				lo_message_add_string(oscmsg,di.Instruction.c_str()); 
				lo_message_add_int32(oscmsg,di.StackPtr->Size()); 

				for (deque<Data*>::const_iterator i=di.StackPtr->GetRawStack()->begin(); i!=di.StackPtr->GetRawStack()->end(); i++)
				{					
					switch((*i)->Type())
					{
						case 's': lo_message_add_string(oscmsg,static_cast<Str*>(*i)->Value.c_str()); break;
						case 'n': lo_message_add_float(oscmsg,static_cast<Num*>(*i)->Value); break;
						default: break;
					}
				} 

				lo_send_message(oscaddr, "/debug", oscmsg);
				lo_address_free(oscaddr);
				lo_message_free(oscmsg);
			}
		}
		
		usleep(100);
	}
}

void Server::ErrorHandler(int num, const char *msg, const char *path)
{
    cerr<<"liblo server error "<<num<<" in path "<<path<<": "<<msg<<endl;
}
	
int Server::AsmHandler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	if (!strcmp(types,"s"))
	{	
		pthread_mutex_lock(m_Mutex);
		m_MVM.Asm(&argv[0]->s);
		pthread_mutex_unlock(m_Mutex);
	}
    return 1;
}

int Server::ClientHandler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
    if (!strcmp(types,"s"))
	{	
		m_ClientUrl=&argv[0]->s;
		m_ClientRegistered=true;
	}
	return 1;
}

int Server::PeekHandler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	if (!strcmp(types,"i"))
	{	
		pthread_mutex_lock(m_Mutex);
		int addr=argv[0]->i;

		if (m_ClientRegistered)
		{
			// send peek message to client
			lo_address oscaddr=lo_address_new_from_url(m_ClientUrl.c_str());
			lo_message oscmsg=lo_message_new();
			Data* d = m_MVM.Peek(addr);
			
			switch(d->Type())
			{
				case 's': lo_message_add_string(oscmsg,static_cast<Str*>(d)->Value.c_str()); break;
				case 'n': lo_message_add_float(oscmsg,static_cast<Num*>(d)->Value); break;
				default: break;
			}
			
			lo_send_message(oscaddr, "/peek", oscmsg);
			lo_address_free(oscaddr);
			lo_message_free(oscmsg);
		}		
		pthread_mutex_unlock(m_Mutex);
	}
		
    return 1;
}

int Server::PokeHandleri(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	pthread_mutex_lock(m_Mutex);
	cerr<<types<<endl;
	if (!strcmp(types,"ii")) m_MVM.Poke(argv[0]->i,new Num(argv[1]->i));
	pthread_mutex_unlock(m_Mutex);
    return 1;
}

int Server::PokeHandlerf(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	pthread_mutex_lock(m_Mutex);
	cerr<<types<<endl;
	if (!strcmp(types,"if")) m_MVM.Poke(argv[0]->i,new Num(argv[1]->f));
	pthread_mutex_unlock(m_Mutex);
    return 1;
}

int Server::PokeHandlers(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	pthread_mutex_lock(m_Mutex);
	cerr<<types<<endl;
	if (!strcmp(types,"is")) m_MVM.Poke(argv[0]->i,new Str(&argv[1]->s));
	pthread_mutex_unlock(m_Mutex);
    return 1;
}

int Server::ResetHandler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	pthread_mutex_lock(m_Mutex);
	m_MVM.Reset();
	pthread_mutex_unlock(m_Mutex);
    return 1;
}

int Server::DefaultHandler(const char *path, const char *types, lo_arg **argv,
		    int argc, void *data, void *user_data)
{
	cerr<<"received ["<<path<<"] ["<<types<<"]"<<endl;
    return 1;
}

