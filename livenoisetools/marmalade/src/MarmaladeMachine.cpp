// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "MarmaladeMachine.h"
#include "Tokenise.h"

MarmaladeMachine::MarmaladeMachine(int CodeStart):
m_Stack(STACK_SIZE),
m_Heap(HEAP_SIZE),
m_HeapTop(CodeStart),
m_CodeStart(CodeStart),
m_MsgReady(false)
{
	Reset();
}

MarmaladeMachine::~MarmaladeMachine()
{
}

void MarmaladeMachine::LoadAsm(const string &filename)
{
	FILE *asmfile=fopen(filename.c_str(),"r");
	if (!asmfile) 
	{
		cerr<<"file: "<<filename<<" not found"<<endl;
		return;
	}
	
	fseek(asmfile,0,SEEK_END);
	unsigned int size=ftell(asmfile);
	fseek(asmfile,0,SEEK_SET);
	
	char *code=new char[size];
	
	if (size!=fread(code,1,size,asmfile))
	{
		cerr<<"error reading file: "<<filename<<endl;
		return;
	}
	
	Asm(code);
}

void MarmaladeMachine::Asm(const string &code)
{
	m_HeapTop=m_CodeStart;
	map<int,int> TokenLineMap;
	vector<pair<type,string> > tokens=Tokenise(code,TokenLineMap);
	map<string,float> LabelMap;
	
	// first pass
	// write all data into it's position, string constantants, labels and all
	for (unsigned int pos=0; pos<tokens.size(); pos++)
	{
		string code=tokens[pos].second;

		if (tokens[pos].first==NUM)
		{		
			// an instruction or a number literal
			float instr=0;
			bool label=false;
			if (code=="nop") instr=NOP;
			else if(code=="psh") instr=PSH;
			else if(code=="pop") instr=POP;
			else if(code=="snd") instr=SND;
			else if(code=="osc") instr=OSC;
			else if(code=="jmp") instr=JMP;
			else if(code=="jmz") instr=JMZ;
			else if(code=="equ") instr=EQU;
			else if(code=="cal") instr=CAL;
			else if(code=="ret") instr=RET;
			else if(code=="rdf") instr=RDF;
			else if(code=="wrf") instr=WRF;
			else if(code=="rdi") instr=RDI;
			else if(code=="wri") instr=WRI;
			else if(code=="inc") instr=INC;
			else if(code=="dec") instr=DEC;
			else if(code=="add") instr=ADD;
			else if(code=="sub") instr=SUB;
			else if(code=="mul") instr=MUL;
			else if(code=="div") instr=DIV;
			else if(code=="def") 
			{
				// #define is not a true instruction, it's a preprocessor thingy
				
				pos++; // skip the equ
				string token=tokens[pos++].second;
				float value=strtof(tokens[pos++].second.c_str(),NULL);
				LabelMap[token]=value;
				label=true;
			}
			else // must be a number literal
			{	
				char *endpos=NULL;
				instr=strtof(code.c_str(),&endpos);
				
				if (code.c_str()==endpos) // error converting the number
				{
					// must be a label
					//cerr<<"label found after jmp or jmf, hopefully: "<<code<<endl;
					m_PCLineMap[m_HeapTop]=TokenLineMap[pos];
					m_Heap.Write(m_HeapTop++,code);
					label=true;					
				}
			}
			
			if (!label) 
			{
				m_PCLineMap[m_HeapTop]=TokenLineMap[pos];
				m_Heap.Write(m_HeapTop++,instr);
			}
		}
		else if (tokens[pos].first==STR)
		{
			// string literal
			m_PCLineMap[m_HeapTop]=TokenLineMap[pos];
			m_Heap.Write(m_HeapTop++,code);
		}				
		else if (tokens[pos].first==LAB)
		{
			// a label, record current position + 1
			// chop off the ":"
			LabelMap[code.substr(0,code.size()-1)]=m_HeapTop;	
			//cerr<<"label: "<<code.substr(0,code.size()-1)<<"="<<m_HeapTop<<endl;
		}				
	}
	
	// second pass
	// this time, go through the code replacing string constants/equ values and labels...
	for (int i=m_CodeStart; i<m_HeapTop; i++)
	{
		string token=m_Heap.ReadStr(i);
		if (token=="PC") m_Heap.Write(i,PC);
		else if (token=="CLOCK_SPEED") m_Heap.Write(i,CLOCK_SPEED);
		else if (token=="CLOCK_TICK") m_Heap.Write(i,CLOCK_TICK);
		else if (token=="TIME") m_Heap.Write(i,TIME);
		else if (token=="DEBUG") m_Heap.Write(i,DEBUG);
		else if (token=="OSC_OUT_PORT") m_Heap.Write(i,OSC_OUT_PORT);
		else if (token=="CODE_START") m_Heap.Write(i,m_CodeStart);
		else
		{
			map<string,float>::iterator l=LabelMap.find(token);
			if (l!=LabelMap.end())
			{
				m_Heap.Write(i,l->second); 
			}
		}
	}
}

void MarmaladeMachine::Reset()
{
	m_Stack.Clear();
	m_Heap.Clear();
	m_PCLineMap.clear();
	m_PCStack.clear();
	
	// init registers
	m_Heap.Write(PC,m_CodeStart);
	m_Heap.Write(CLOCK_SPEED,0);
	m_Heap.Write(TIME,0);
	m_Heap.Write(DEBUG,0);
	m_Heap.Write(OSC_OUT_PORT,"osc.udp://localhost:4444");
}

DebugInfo MarmaladeMachine::GetDebugInfo()
{
	DebugInfo di;
	
	if (m_Heap.ReadNum(DEBUG))
	{
		di.LineNumber=CurrentLine();
		di.PC = (int)m_Heap.ReadNum(PC);
		di.StackPtr = &m_Stack;
		int code = (int)m_Heap.ReadNum(di.PC);
		switch (code)
		{
			case NOP: di.Instruction="nop"; break;
			case PSH: di.Instruction="psh"; break;
			case POP: di.Instruction="pop"; break;
			case SND: di.Instruction="snd"; break;
			case OSC: di.Instruction="osc"; break;
			case JMP: di.Instruction="jmp"; break;
			case JMZ: di.Instruction="jmz"; break;
			case EQU: di.Instruction="equ"; break;
			case CAL: di.Instruction="cal"; break;
			case RET: di.Instruction="ret"; break;
			case RDF: di.Instruction="rdf"; break;
			case WRF: di.Instruction="wrf"; break;
			case RDI: di.Instruction="rdi"; break;
			case WRI: di.Instruction="wri"; break;
			case INC: di.Instruction="inc"; break;
			case DEC: di.Instruction="dec"; break;
			case ADD: di.Instruction="add"; break;
			case SUB: di.Instruction="sub"; break;
			case MUL: di.Instruction="mul"; break;
			case DIV: di.Instruction="div"; break;
			default : break;
		}			
	}
		
	return di;
}

int MarmaladeMachine::CurrentLine()
{
	return m_PCLineMap[(int)m_Heap.ReadNum(PC)];
}

void MarmaladeMachine::IncPC()
{
	Data* d=m_Heap.ReadRaw(PC);
	if (d->Type()=='n') 
	{
		Num *pc=static_cast<Num*>(d);
		pc->Value++; 
		if (pc->Value>HEAP_SIZE) pc->Value=0;
	}
}

void MarmaladeMachine::Execute()
{
	if (m_Heap.ReadNum(PC)>=m_HeapTop) m_Heap.Write(PC,m_CodeStart);
	GetDebugInfo().Dump();
	int code = (int)m_Heap.ReadNum((int)m_Heap.ReadNum(PC));
	
	switch (code)
	{
		case PSH: psh(); break;
		case POP: pop(); break;
		case SND: snd(); break;
		case OSC: osc(); break;
		case JMP: jmp(); break;
		case JMZ: jmz(); break;
		case EQU: equ(); break;
		case CAL: cal(); break;
		case RET: ret(); break;
		case RDF: rdf(); break;
		case WRF: wrf(); break;
		case RDI: rdi(); break;
		case WRI: wri(); break;
		case INC: inc(); break;
		case DEC: dec(); break;
		case ADD: add(); break;
		case SUB: sub(); break;
		case MUL: mul(); break;
		case DIV: div(); break;
		default : break;
	}	
	
	IncPC();
}



Data *MarmaladeMachine::Peek(unsigned int addr)
{
	return m_Heap.ReadRaw(addr);
}

void MarmaladeMachine::Poke(unsigned int addr, Data* s)
{
	m_Heap.WriteRaw(addr,s);
}


MarmaladeMachine::MsgDesc::MsgDesc()
{
}

MarmaladeMachine::MsgDesc::~MsgDesc()
{
	Clear();
}

void MarmaladeMachine::MsgDesc::Clear()
{
	for (vector<Data*>::iterator i=Args.begin(); i!=Args.end(); i++)
	{
		delete *i;
	}
	Args.clear();
}

bool MarmaladeMachine::PollMessage(MsgDesc &Message)
{
	if (!m_MsgReady) return false;	
	Message=m_MsgDesc;
	m_MsgReady=false;
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////

void MarmaladeMachine::psh()
{
	IncPC();
	Data* d=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
	if (d->Type()=='n') m_Stack.PushNum(static_cast<Num*>(d)->Value);
	if (d->Type()=='s') m_Stack.PushStr(static_cast<Str*>(d)->Value);
}

void MarmaladeMachine::pop()
{
	if (m_Stack.Size()>=1)
	{
		delete m_Stack.PopRaw(); 
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (pop)"<<endl;
	}
}

void MarmaladeMachine::snd()
{
	if (m_Stack.Size()>=1)
	{
		Data* data=m_Stack.PopRaw();
		if (data->Type()=='n') cerr<<static_cast<Num*>(data)->Value<<endl;
		else if (data->Type()=='s') cerr<<static_cast<Str*>(data)->Value<<endl;
		delete data;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (snd)"<<endl;
	}
}

void MarmaladeMachine::osc()
{
	if (m_Stack.Size()>=3)
	{
		m_MsgDesc.Clear();
		Data* a=m_Stack.PopRaw();
		if (a->Type()=='s') 
		{
			m_MsgDesc.Path=static_cast<Str*>(a)->Value;
			while (m_Stack.Size()>1)
			{			
				// transfer to the msg args from the stack, no need to copy
				m_MsgDesc.Args.push_back(m_Stack.PopRaw());
			}			
			m_MsgReady=true;
		}
		delete a;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (osc)"<<endl;
	}
}

void MarmaladeMachine::jmp()
{
	IncPC();
	Data* dest=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
	if (dest->Type()=='n') m_Heap.Write(PC,static_cast<Num*>(dest)->Value-1); // take into account the pc++ at the bottom
}

void MarmaladeMachine::jmz()
{
	IncPC();
	if (m_Stack.Size()>=1)
	{
		Data* dest=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
		Data* data=m_Stack.PopRaw();
		if (data->Type()=='n') 
		{
			if (static_cast<Num*>(data)->Value==0) 
			{
				if (dest->Type()=='n')
				{
					m_Heap.Write(PC,(int)static_cast<Num*>(dest)->Value-1); // take into account the pc++ at the bottom
				}
			}
		}
		delete data;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (jmz)"<<endl;
	}
}

void MarmaladeMachine::cal()
{
	IncPC();	
	Data* dest=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC)); 
	m_PCStack.push_front((int)m_Heap.ReadNum(PC)); // push onto the pc stack 
	if (dest->Type()=='n') m_Heap.Write(PC,static_cast<Num*>(dest)->Value-1); // take into account the pc++ at the bottom
}

void MarmaladeMachine::ret()
{
	if (m_PCStack.size()>=1)
	{
		int pc=*m_PCStack.begin();
		m_PCStack.pop_front();
		m_Heap.Write(PC,pc); 
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of pc stack at line: "<<CurrentLine()<<" (ret)"<<endl;
	}
}

void MarmaladeMachine::equ()
{
	if (m_Stack.Size()>=2)
	{
		Data* a=m_Stack.PopRaw();
		Data* b=m_Stack.PopRaw();
		if (a->Type()=='n' && b->Type()=='n') 
		{
			if (static_cast<Num*>(a)->Value==static_cast<Num*>(b)->Value)
			{
				m_Stack.PushNum(1);
			}
			else
			{
				m_Stack.PushNum(0);
			}
		}
		else if (a->Type()=='s' && b->Type()=='s') 
		{
			if (static_cast<Str*>(a)->Value==static_cast<Str*>(b)->Value) 
			{
				m_Stack.PushNum(1);
			}
			else
			{
				m_Stack.PushNum(0);
			}
		}
		else
		{
			m_Stack.PushNum(0);
		}
		
		delete a;
		delete b;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (equ)"<<endl;
	}
}


void MarmaladeMachine::wrf()
{
	IncPC();
	if (m_Stack.Size()>=1)
	{
		Data* a=m_Stack.PopRaw();
		Data* dest=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
		if (dest->Type()=='n')
		{
			unsigned int addr=(unsigned int)static_cast<Num*>(dest)->Value;
			if (addr<HEAP_SIZE)
			{
				if (a->Type()=='s') m_Heap.Write(addr,static_cast<Str*>(a)->Value);
				else if (a->Type()=='n') m_Heap.Write(addr,static_cast<Num*>(a)->Value);
			}
		}
		delete a;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (mov)"<<endl;
	}
}

void MarmaladeMachine::rdf()
{
	IncPC();
	Data* src=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
	if (src->Type()=='n')
	{ 
		unsigned int addr=(unsigned int)static_cast<Num*>(src)->Value;
		if (addr<HEAP_SIZE)
		{
			Data* a=m_Heap.ReadRaw(addr);
			if (a->Type()=='s') m_Stack.PushStr(static_cast<Str*>(a)->Value);
			else if (a->Type()=='n') m_Stack.PushNum(static_cast<Num*>(a)->Value);
		}
	}
}

void MarmaladeMachine::wri()
{
	IncPC();
	if (m_Stack.Size()>=1)
	{
		Data* a=m_Stack.PopRaw();
		Data* ind=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
		if (ind->Type()=='n')
		{
			Data* dest=m_Heap.ReadRaw((unsigned int)static_cast<Num*>(ind)->Value);
			if (dest->Type()=='n')
			{
				unsigned int addr=(unsigned int)static_cast<Num*>(dest)->Value;
				if (addr<HEAP_SIZE)
				{
					if (a->Type()=='s') m_Heap.Write(addr,static_cast<Str*>(a)->Value);
					else if (a->Type()=='n') m_Heap.Write(addr,static_cast<Num*>(a)->Value);
				}
			}
		}	
		delete a;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (mov)"<<endl;
	}
}

void MarmaladeMachine::rdi()
{
	IncPC();
	Data* ind=m_Heap.ReadRaw((int)m_Heap.ReadNum(PC));
	if (ind->Type()=='n')
	{ 
		Data* src=m_Heap.ReadRaw((unsigned int)static_cast<Num*>(ind)->Value);
		if (src->Type()=='n')
		{ 
			unsigned int addr=(unsigned int)static_cast<Num*>(src)->Value;
			if (addr<HEAP_SIZE)
			{
				Data* a=m_Heap.ReadRaw(addr);
				if (a->Type()=='s') m_Stack.PushStr(static_cast<Str*>(a)->Value);
				else if (a->Type()=='n') m_Stack.PushNum(static_cast<Num*>(a)->Value);
			}
		}
	}
}

void MarmaladeMachine::inc()
{
	if (m_Stack.Size()>=1)
	{
		Data* a=m_Stack.PopRaw();
		if (a->Type()=='n')
		{
			m_Stack.PushNum(static_cast<Num*>(a)->Value+1);
		}
		delete a;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (inc)"<<endl;
	}
}

void MarmaladeMachine::dec()
{
	if (m_Stack.Size()>=1)
	{
		Data* a=m_Stack.PopRaw();
		if (a->Type()=='n')
		{
			m_Stack.PushNum(static_cast<Num*>(a)->Value-1);
		}
		delete a;		
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (dec)"<<endl;
	}
}

void MarmaladeMachine::add()
{
	if (m_Stack.Size()>=2)
	{
		Data* a=m_Stack.PopRaw();
		Data* b=m_Stack.PopRaw();
		if (a->Type()=='n' && b->Type()=='n')
		{
			m_Stack.PushNum(static_cast<Num*>(a)->Value+static_cast<Num*>(b)->Value);
		}
		else if (a->Type()=='s' && b->Type()=='s')
		{
			m_Stack.PushStr(static_cast<Str*>(a)->Value+static_cast<Str*>(b)->Value);
		}
		else if (a->Type()=='s' && b->Type()=='n')
		{
			char num[256];
			snprintf(num,256,"%f",static_cast<Num*>(b)->Value);
			m_Stack.PushStr(static_cast<Str*>(a)->Value+string(num));
		}
		else if (a->Type()=='n' && b->Type()=='s')
		{
			char num[256];
			snprintf(num,256,"%f",static_cast<Num*>(a)->Value);
			m_Stack.PushStr(string(num)+static_cast<Str*>(b)->Value);
		}
		delete a;
		delete b;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (add)"<<endl;
	}
}

void MarmaladeMachine::sub()
{
	if (m_Stack.Size()>=2)
	{
		Data* a=m_Stack.PopRaw();
		Data* b=m_Stack.PopRaw();
		if (a->Type()=='n' && b->Type()=='n')
		{
			m_Stack.PushNum(static_cast<Num*>(b)->Value-static_cast<Num*>(a)->Value);
		}
		delete a;
		delete b;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (sub)"<<endl;
	}
}

void MarmaladeMachine::mul()
{
	if (m_Stack.Size()>=2)
	{
		Data* a=m_Stack.PopRaw();
		Data* b=m_Stack.PopRaw();
		if (a->Type()=='n' && b->Type()=='n')
		{
			m_Stack.PushNum(static_cast<Num*>(a)->Value*static_cast<Num*>(b)->Value);
		}
		if (a->Type()=='s' && b->Type()=='n')
		{
			string tmp;
			for (int n=0; n<static_cast<Num*>(b)->Value; n++)
			{
				tmp+=static_cast<Str*>(a)->Value;
			}			
			m_Stack.PushStr(tmp);
		}
		else if (a->Type()=='n' && b->Type()=='s')
		{
			string tmp;
			for (int n=0; n<static_cast<Num*>(a)->Value; n++)
			{
				tmp+=static_cast<Str*>(b)->Value;
			}			
			m_Stack.PushStr(tmp);
		}
		delete a;
		delete b;
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (mul)"<<endl;
	}
}

void MarmaladeMachine::div()
{
	if (m_Stack.Size()>=2)
	{
		Data* a=m_Stack.PopRaw();
		Data* b=m_Stack.PopRaw();
		if (a->Type()=='n' && b->Type()=='n')
		{
			float bottom = static_cast<Num*>(a)->Value;
			if (bottom!=0)
			{
				m_Stack.PushNum(static_cast<Num*>(b)->Value/bottom);
			}
			else
			{
				m_Stack.PushNum(0);
			}
		}
		if (a->Type()=='s' && b->Type()=='n')
		{
			string str = static_cast<Str*>(a)->Value;
			unsigned int spl = (int)static_cast<Num*>(b)->Value;
			if (spl<str.size())
			{
				m_Stack.PushStr(str.substr(0,spl));
				m_Stack.PushStr(str.substr(spl,str.size()));
			}
		}
		else if (a->Type()=='n' && b->Type()=='s')
		{
			string str = static_cast<Str*>(b)->Value;
			unsigned int spl = (int)static_cast<Num*>(a)->Value;
			if (spl<str.size())
			{
				m_Stack.PushStr(str.substr(0,spl));
				m_Stack.PushStr(str.substr(spl,str.size()));
			}
		}
		delete a;
		delete b;		
	}
	else 
	{
		if (m_Heap.ReadNum(DEBUG)) cerr<<"ran out of stack at line: "<<CurrentLine()<<" (div)"<<endl;
	}
}
