// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <string>

#ifndef ORANGE_DATA
#define ORANGE_DATA

using namespace std;

class Data
{
public:
	Data() {}
	virtual ~Data() {}
	virtual char Type()=0;
};

class Num : public Data
{
public:
	Num(float s) { Value=s; }
	virtual ~Num() {}
	virtual char Type() { return 'n'; }
	float Value;
};


class Str : public Data
{
public:
	Str(string s) { Value=s; }
	virtual ~Str() {}
	virtual char Type() { return 's'; }
	string Value;
};

#endif
