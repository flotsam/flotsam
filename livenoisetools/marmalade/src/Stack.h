// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Data.h"
#include <deque>
#include <string>

#ifndef ORANGE_STACK
#define ORANGE_STACK

using namespace std;

class Stack
{
public:
	Stack(unsigned int maxsize);
	~Stack();

	void Clear();
	char PeekType();
	void PushRaw(Data *);
	Data *PopRaw();
	void PushNum(float s);
	float PopNum();
	void PushStr(string s);
	string PopStr();
	
	unsigned int Size() { return m_Stack.size(); }
	void Print() const;
	bool Full() { if (m_Stack.size()<m_MaxSize) return false; else return true; }
	
	const deque<Data*> *GetRawStack() const { return &m_Stack; }
	
private:
	deque<Data*> m_Stack;
	unsigned int m_MaxSize;
};

#endif
