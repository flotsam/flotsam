// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <iostream>
#include "Stack.h"

Stack::Stack(unsigned int maxsize) :
m_MaxSize(maxsize)
{
}

Stack::~Stack()
{
	Clear();
}

void Stack::Clear()
{
	for (deque<Data*>::iterator i=m_Stack.begin(); i!=m_Stack.end(); i++)
	{
		delete *i;
	}
	
	m_Stack.clear();
}

char Stack::PeekType()
{
	if (Size()>=1) return (*m_Stack.begin())->Type();		
	return '0';
}

void Stack::PushRaw(Data *s)
{
	if (Size()<m_MaxSize) m_Stack.push_front(s);
}

Data *Stack::PopRaw()
{
	if (Size()>=1) 
	{
		Data *t=*m_Stack.begin();
		m_Stack.pop_front();
		return t;
	} 
	return NULL;
}

void Stack::PushNum(float s)
{
	if (Size()<m_MaxSize) m_Stack.push_front(new Num(s));
}

float Stack::PopNum()
{
	if (Size()>=1) 
	{
		Data *t=*m_Stack.begin();
		m_Stack.pop_front();
		if (t->Type()=='n') return static_cast<Num*>(t)->Value;
	} 
	return 0;
}

void Stack::PushStr(string s)
{
	if (Size()<m_MaxSize) m_Stack.push_front(new Str(s));
}

string Stack::PopStr()
{
	if (Size()>=1) 
	{
		Data *t=*m_Stack.begin();
		m_Stack.pop_front();
		if (t->Type()=='s') return static_cast<Str*>(t)->Value;
	} 
	return "";
}
	
void Stack::Print() const
{
	for (deque<Data*>::const_iterator i=m_Stack.begin(); i!=m_Stack.end(); i++)
	{
		if ((*i)->Type()=='n') cerr<<static_cast<Num*>(*i)->Value<<" ";
		else if ((*i)->Type()=='s') cerr<<"\""<<static_cast<Str*>(*i)->Value<<"\" ";
	}
}
