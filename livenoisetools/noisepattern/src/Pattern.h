// Copyright (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <vector>
#include <string>
#include <deque>
#include <spiralcore/Types.h>
#include <spiralcore/Event.h>
#include <spiralcore/Tuna.h>

#ifndef NE_PATTERN
#define NE_PATTERN

using namespace std;
using namespace spiralcore;

class Pattern
{
public:
	Pattern(Tuna *t);
	virtual ~Pattern();
	
	virtual void DoSlice(Time Now, float TimeSlice, vector<Event> &EventVec);
	virtual void Tick(const Time &TimeStamp, vector<Event> &EventVec);
	virtual void Modify(const string &Name, float32 Value);
	virtual void Modify(const string &Name, const string &Value);
	
	void SetMidi(int s) { m_Midi=s; }
	int GetMidi() { return m_Midi; }
	void AddReset(const Time &s) { m_ResetTimes.push_back(s); }
	
private:
	void Reset();
	int SkipBranch(unsigned int Pos, string Str);
	
	Tuna *m_Tuna;
	
	string m_Score;
	string m_NewScore;
	Time   m_NextTickTime;
	float m_Beat;
	float m_BeatMult;
	int m_Midi;
	int m_Position;
	int m_Note;
	deque<int> m_NoteStack;
	int m_RootNote;
	float m_RootFreq;
	float m_Pan;
	float m_PanDistance;
	float m_PanAngle;
	int m_NoteNum;
	bool  m_Playing;
	vector<Time> m_ResetTimes;
};

#endif
