(define lsys-draw-angle 10)
(define lsys-draw-depth 0)
(define lsys-draw-obs '())
(define lsys-draw-current-ob 0)
(define lsys-draw-current-pattern 0)
(define lsys-draw-collist '(
	#(0 0.43 0.93) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	))
(define lsys-draw-highlightcol (vector 0.93 0.43 0))
	
(line-width 2)

(define lsys-draw-build 
	(lambda (id ch)
    	(cond 
        	((char=? ch #\+)
            	(rotate (vector 0 0 lsys-draw-angle)))
        	((char=? ch #\-)
            	(rotate (vector 0 0 (- lsys-draw-angle))))
        	((char=? ch #\/)
            	(rotate (vector 0 lsys-draw-angle 0)))
        	((char=? ch #\\)
            	(rotate (vector 0 (- lsys-draw-angle) 0)))
        	((char=? ch #\.)
             	(translate (vector 0 0.1 0)))
       	((or (char=? ch #\o) (char=? ch #\O))
            	(scale (vector 0.95 0.95 0.95))
	           	(translate (vector 0 0.25 0))
  				(push)
          		(scale (vector 0.1 0.5 0.1))
				
				(if (char=? ch #\O) (scale (vector 2 1 2)))
				
            	(if (<= (length lsys-draw-obs) id)
					(set! lsys-draw-obs (append lsys-draw-obs (list '()))))
					
				(let ((ob (build-cube)))	
					(list-set! lsys-draw-obs id (append (list-ref lsys-draw-obs id) (list ob)))
					(pop)
					;(apply ob)
					;(parent ob)
					)
            	
            	(translate (vector 0 0.25 0))
				)
        	((char=? ch #\[)
            	(set! lsys-draw-depth (+ lsys-draw-depth 1))
            	(push))
        	((char=? ch #\])
            	(cond 
                	((> lsys-draw-depth 0)
                    	(set! lsys-draw-depth (- lsys-draw-depth 1))
                    	(pop)))))))

(define lsys-draw-list-build 
	(lambda (id strlist)
    (lsys-draw-build id (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (lsys-draw-list-build id (cdr strlist)))))

(define lsys-draw-destroy-all 
	(lambda (l)
    	(destroy (car l))
    	(if (eq? (cdr l) '())
        	0
        	(lsys-draw-destroy-all (cdr l)))))


(define lsys-draw-fix-stack
	(lambda ()
	    (if (< lsys-draw-depth 1)
	        0 
	        (begin 
	            (pop)
	            (set! lsys-draw-depth (- lsys-draw-depth 1))
	            (lsys-draw-fix-stack)))))

(define lsys-draw-highlight
	(lambda (id note)
		(let ((tree (list-ref lsys-draw-obs id)))
			(if (not (null? tree))
				(begin
					(lsys-draw-unhighlight id tree)
    				(cond
        				((< note (length tree))
            				(grab (list-ref tree note))
            				(hint-none)
							;(hint-wire)
							(hint-solid)
            				(colour lsys-draw-highlightcol)
            				(ungrab))))))))

(define lsys-draw-unhighlight
	(lambda (id l)
		(grab (car l))					
		(hint-none)
		(hint-wire)
		(wire-colour (vmul (list-ref lsys-draw-collist id) 1))
		(colour (vmul (list-ref lsys-draw-collist id) 1))
		(ungrab)
		(if (eq? (cdr l) '())
			0
			(lsys-draw-unhighlight id (cdr l)))))

(define lsys-draw-render
	(lambda ()
   		(if (osc-msg "/play") 
                (begin
                    (lsys-draw-highlight (- (inexact->exact (osc 2)) 1) (inexact->exact (osc 8)))
					(lsys-draw-render))))) ; call until we run out of /plays

(define lsys-draw
	(lambda (id str type)
		(set! id (- id 1))
		(if (< id (length lsys-draw-obs)) 
			(if (not (eq? (list-ref lsys-draw-obs id) '()))
				(begin 
					(lsys-draw-destroy-all (list-ref lsys-draw-obs id))
					(list-set! lsys-draw-obs id '()))))
		(push)
		(lsys-draw-list-build id (string->list str))
		(lsys-draw-fix-stack)
		(pop)
		(every-frame "(lsys-draw-render)")
		))
	
