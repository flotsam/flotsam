// Copyright (C) 2008 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"
#include "SampleStore.h"
#include "Sampler.h"

#ifndef SAMPLE_VOICE
#define SAMPLE_VOICE

using namespace spiralcore;

class SampleVoice : public Voice
{
public:
	SampleVoice(int SampleRate);
	virtual ~SampleVoice() {}
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);
	virtual void Reset();
	
private:

	Sampler   m_Sampler;
	Envelope  m_Envelope;
	SampleID  m_SampleID;
	bool m_PercussionMode;
	float m_PercussionFreq;
	Sample m_Buffer;
};

#endif
