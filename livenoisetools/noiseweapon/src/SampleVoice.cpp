// Copyright (C) 2008 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "SampleVoice.h"

using namespace std;

SampleVoice::SampleVoice(int SampleRate) :
Voice(SampleRate),
m_Sampler(SampleRate),
m_Envelope(SampleRate),
m_SampleID(0),
m_PercussionMode(false),
m_PercussionFreq(440.0f)
{
	Reset();
}

void SampleVoice::Reset()
{
	m_Envelope.Reset();
	m_Envelope.SetAttack(0);
	m_Envelope.SetDecay(0);
	m_Envelope.SetRelease(0);
}

void SampleVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_Buffer.GetLength())
	{
		m_Buffer.Allocate(BufSize);
	}	
	
	left.Zero();
	right.Zero();
	m_Sampler.Process(BufSize, left, right); // todo keep sample stereo
	m_Envelope.Process(BufSize, left, m_Buffer);
	StereoMix(left,left,right);
}

void SampleVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
	Event event;
	if (m_PercussionMode) 
	{
		event.ID=(int)t.Pitch;
		event.Frequency=m_PercussionFreq;
	}
	else 
	{
		event.ID=m_SampleID;
		event.Frequency=t.Pitch;
	}
	event.SlideFrequency=t.SlidePitch;
	event.Volume=t.Volume;
	event.Pan=t.Pan;
	m_Sampler.Play(t.Time, event);
	m_Envelope.Trigger(t.Time,t.Pitch,t.Volume);
}

void SampleVoice::Modify(const string &name, float v)
{ 
	if (name=="attack") m_Envelope.SetAttack(v);
	else if (name=="decay") m_Envelope.SetDecay(v);
	else if (name=="sustain") m_Envelope.SetSustain(v);
	else if (name=="release") m_Envelope.SetRelease(v);
	else if (name=="volume") m_Envelope.SetVolume(v);
	else if (name=="sampleid") m_SampleID=(int)v;
	else if (name=="percmode") m_PercussionMode=(bool)v;
	else if (name=="percfreq") m_PercussionFreq=v;
	else if (name=="reverse") m_Sampler.SetReverse((bool)v);
	else if (name=="samplepoly") m_Sampler.SetPoly((bool)v);
}




