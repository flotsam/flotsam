// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "AdditiveVoice.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

AdditiveVoice::AdditiveVoice(int SampleRate) :
Voice(SampleRate),
m_Envelope(SampleRate)
{
	for (int n=0; n<NUM_HARMS; n++)
	{	
		m_Wave[n]=new WaveTable(SampleRate);
		m_Level[n]=0;
	}
}

void AdditiveVoice::Reset()
{
	m_Envelope.Reset();
}

AdditiveVoice::~AdditiveVoice() 
{
	for (int n=0; n<NUM_HARMS; n++)
	{	
		delete m_Wave[n];
	}
}

void AdditiveVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_Buffer.GetLength())
	{
		m_Buffer.Allocate(BufSize);
	}
	left.Zero();
	for (int n=0; n<NUM_HARMS; n++)
	{		
		m_Wave[n]->SimpleProcess(BufSize,left);	
	}	
	m_Envelope.Process(BufSize, left, m_Buffer);
	StereoMix(left,left,right);
}

void AdditiveVoice::Trigger(const TriggerInfo &t)
{ 
	Voice::Trigger(t);
	m_Envelope.Trigger(t.Time,t.Pitch,t.Volume);
	
	float gain=1/(float)NUM_HARMS;
	float pitch=t.Pitch;
	float slidepitch=t.SlidePitch;
	
	m_Wave[0]->Trigger(t.Time,pitch*0.5,slidepitch*0.5,gain*m_Level[0]);
	m_Wave[1]->Trigger(t.Time,pitch*1.5,slidepitch*1.666666,gain*m_Level[1]);
	m_Wave[2]->Trigger(t.Time,pitch,slidepitch,gain*m_Level[2]);
	m_Wave[3]->Trigger(t.Time,pitch*2,slidepitch*2,gain*m_Level[3]);
	m_Wave[4]->Trigger(t.Time,pitch*2*1.5,slidepitch*2*1.5,gain*m_Level[4]);
	m_Wave[5]->Trigger(t.Time,pitch*4,slidepitch*4,gain*m_Level[5]);
	m_Wave[6]->Trigger(t.Time,pitch*2*1.25,slidepitch*2*1.25,gain*m_Level[6]);
	m_Wave[7]->Trigger(t.Time,pitch*2*1.5,slidepitch*2*1.5,gain*m_Level[7]);
	m_Wave[8]->Trigger(t.Time,pitch*8,slidepitch*8,gain*m_Level[8]);
}

void AdditiveVoice::Modify(const string &name, float v)
{ 
	if (name=="attack") m_Envelope.SetAttack(v);
	else if (name=="decay") m_Envelope.SetDecay(v);
	else if (name=="sustain") m_Envelope.SetSustain(v);
	else if (name=="release") m_Envelope.SetRelease(v);
	else if (name=="volume") m_Envelope.SetVolume(v);
	else if (name=="type")
	{
		for (int n=0; n<NUM_HARMS; n++)
		{		
			m_Wave[n]->SetType((int)v);	
		}
	}
	else 
	{
		int h=(int)atof(name.c_str()+4);
		m_Level[h]=v;
	}
}




