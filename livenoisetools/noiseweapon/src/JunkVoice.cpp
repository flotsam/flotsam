// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "JunkVoice.h"
#include <stdlib.h>

using namespace std;
map<string,int> JunkVoice::m_Names;

JunkVoice::JunkVoice(int SampleRate) :
Voice(SampleRate)
{
	m_Junk=(float*)malloc(sizeof(float)*44400);
	//free(m_Junk);
}

void JunkVoice::Reset()
{

}

void JunkVoice::RegisterNames()
{
	//RegisterName("typea", m_Names);
}

void JunkVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	for (unsigned int i=0; i<BufSize; i++)
	{
		left[i]=m_Junk[i];
		right[i]=m_Junk[i+BufSize];
	}	
}

void JunkVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
}

void JunkVoice::Modify(const string &name, float v)
{ 
	map<string,int>::iterator i=m_Names.find(name);
	if (i==m_Names.end()) return;
	
	switch (i->second)
	{
		
		default : cerr<<"unhandled modify code "<<i->second<<endl;
	}
}




