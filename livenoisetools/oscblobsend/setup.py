from distutils.core import setup, Extension

module1 = Extension('oscblobsend',
					include_dirs = ['src'],
					libraries = ['lo'],
					sources = ['OSCBlobSend.cpp'])

setup (name = 'oscblobsend',
       version = '1.0',
       description = 'sends binary data to an osc server',
       ext_modules = [module1])

