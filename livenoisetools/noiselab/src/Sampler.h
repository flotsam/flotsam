// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <map>
#include <string>
#include <vector>
#include <spiralcore/Types.h>
#include <spiralcore/Event.h>
#include <spiralcore/Sample.h>
#include <spiralcore/Trace.h>

#ifndef NE_SAMPLER
#define NE_SAMPLER

using namespace spiralcore;
using namespace std;

struct OutBuffer
{
	Sample Left;
	Sample Right;
};

class Sampler
{
public:
	Sampler(unsigned int samplerate);
	virtual ~Sampler();
	
	class SampleInfo
	{
	public:
		SampleInfo() : Poly(true), StartTime(0) {}
		~SampleInfo() {}
		bool Poly;
		float StartTime;
		EventID PlayingOn;
		Event Globals;
		string FileName;
	};

	void AddToQueue(SampleID ID, const string &Filename);
	void LoadQueue();
	void Unload(SampleID ID);
	void Globals(SampleID, const Event &Globals);
	void Info(SampleID, const SampleInfo &Info);
	EventID Play(float timeoffset, const Event &Channel);
	void Modify(EventID, const Event &Channel);
	void SetPitchSnap(bool s) { m_PitchSnap=s; }
	void SetScale(int s) { m_Scale=s; }
	void SetLatencyCompensate(float s) { m_LatencyCompensate=s; }
	void UnloadAll();
	SampleID Upload(Sample *s);
	void SetGlobalVolume(float s) { m_GlobalVolume=s; }
	void SetGlobalPitch(float s) { m_GlobalPitch=s; }
	
	virtual void Process(vector<OutBuffer*> &BufferVec, uint32 BufSize);
	
	void LoadPitchMap(const float *Pitch, int Count);
	float PitchSnap(float in);
	
	
private:
    bool m_PitchSnap;
    uint32 m_Scale;
	float m_LatencyCompensate;
	unsigned int m_SampleRate;
	float m_GlobalVolume;
	float m_GlobalPitch;
	
 	map<SampleID,Sample*> m_SampleMap;
 	map<SampleID,Event>  m_GlobalsMap;
 	map<SampleID,SampleInfo> m_InfoMap;
	
 	int m_NextSampleID;
 	map<EventID,Event> m_ChannelMap;
 	int m_NextEventID;
 	vector<float> m_PitchVec;
};

#endif

