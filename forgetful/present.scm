(define (load-time filename)
    (let ((file (open-input-file filename)))
    (let ((ret (read file)))
    (close file)
    ret)))

(define (make-timeline timeline)
    (define (loop t)
        (push)
        (translate (vector 0 (/ (car (car t)) 100000000) 0))
        (hint-ignore-depth)
        (build-text (car (cdr (car t))))
        (display (car (cdr (car t))))(newline)
        (pop)
        (if (null? (cdr t))
            0
            (loop (cdr t))))

    (push)
    (scale (vector 1 1 1))
    (loop (reverse timeline))
    (pop))


(clear)
(clear-colour (vector 0.5 0.5 0.5))
(texture (load-texture "font.png"))
(make-timeline (load-time "time.scm"))
(define camera (build-locator))
(lock-camera camera)
(define speed 0)

(define (update)
    (if (key-pressed "q") (set! speed (+ speed 0.001)))
    (if (key-pressed "a") (set! speed (- speed 0.001)))
    (grab camera)
    (translate (vector 0 speed 0))
    (ungrab))
                

(every-frame (update))