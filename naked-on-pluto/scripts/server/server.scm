;#Lang scheme
(require web-server/servlet
         web-server/servlet-env
         xml
         "request-interface.ss"
         "naked.ss"
         "logger.ss")

(open-log "game.log")

(define g (load-game-state "game.graph"))

(define (make-node name)
  (set! g (naked-add-node g name)))

(define (make-edge from to dir)
  (set! g (naked-add-edge g from to dir)))

(define (describe-node name info)
  (set! g (naked-node-describe g name info)))

(define (add-object name obj)
  (set! g (naked-node-add-object g name obj)))

(define (remove-object name obj)
  (set! g (naked-node-remove-object g name obj)))

(define (save-state)
  (save-game-state g "game.graph"))

(define registered-requests
  (list
   (register (req 'make-node '((name "no name"))) make-node)
   (register (req 'make-edge '((from "no from") (to "no to") 
                               (dir "no direction"))) make-edge)
   (register (req 'describe-node '((name "no name") (info "no info"))) describe-node)
   (register (req 'add-object '((name "no name") (obj "no obj"))) add-object)
   (register (req 'remove-object '((name "no name") (obj "no obj"))) remove-object)   
   (register (req 'save-state '()) save-state)
   (register (req 'ping '()) (lambda () "ack"))))

(define (start request)
  (let ((values (url-query (request-uri request))))
    (if (not (null? values)) ; do we have some parameters?      
        (let ((name (assq 'function_name values)))
          (when name ; is this a well formed request?
            (request-dispatch
             registered-requests
             (req (string->symbol (cdr name))
                  (filter
                   (lambda (v)
                     (not (eq? (car v) 'function_name)))
                   values)))))
        "hello")))
    
(printf "server is running...~n")

(serve/servlet start	
               #:port 8080
               #:command-line? #t
               #:servlet-path "/main"
               #:server-root-path
                (build-path "/home/dave/flotsam/naked-on-pluto/scripts"))

