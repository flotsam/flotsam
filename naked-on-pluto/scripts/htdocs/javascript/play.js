function one()
{
    this.a=3;
    this.func = function()
    {
        return this.a;
    }
}


function two()
{
    this.parent=one;
    this.parent();

    this.parent_func = this.func;
    this.func = function()
    {
        alert("hello");
        return this.parent_func();
    }

}

x = new two();
y = new two();

y.a = 300;

alert(x.func());

