//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// (C) 2010 Dave Griffiths GPLv3
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// util functions

function message(txt)
{
    area.message.value+=txt+"\n";
}

function clear()
{
    area.message.value="";
}

function choose(arr)
{
    return arr[Math.floor(Math.random()*arr.length)];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// a node

function node(name, desc, objects)
{
    this.name=name;
    this.desc=desc;
    this.objects=objects;
    this.time=0;

    this.info = function()
    {
        var objects = "";
        for (var i in this.objects)
        {
           objects+=this.objects[i]+" ";
        }
        var ret=this.get_desc();
        if (objects.length>0) ret+="\nYou can see: "+objects;
        return ret;
    }

    this.get_desc = function()
    {
        return this.desc;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// an edge between nodes

function edge(from, to, direction, desc, key, locked_desc, fail_text)
{
    this.from=from;
    this.to=to;
    this.direction=direction;
    this.desc=desc;
    this.key=key;
    this.locked_desc=locked_desc;
    this.fail_text=fail_text;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// a character

function character(name, start_location, phrases)
{
    this.name = name;
    this.location = start_location;
    this.phrases = phrases
    this.update = function(world)
    {        
        exits = world.find_outward_edges(this.location);
        if (exits.length>0)
        {
            this.location = choose(exits).to;
        }
    }
    this.get_phrase = function(world)
    {
        return choose(this.phrases);
    }
};


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// the graph of nodes and edges
// also does the parsing and game stuff, should be pulled apart

function graph(start, nodes, edges, chars)
{
    this.nodes=nodes;
    this.edges=edges;
    this.location=start;
    this.inventory=[];
    this.characters=chars;

    // dispatch text into function calls
    this.parse = function(str)
    {
        toks = str.toLowerCase().split(" ");
        switch(toks[0])
        {
            case "n": this.move(toks[0]); break;
            case "s": this.move(toks[0]); break;
            case "w": this.move(toks[0]); break;
            case "e": this.move(toks[0]); break;
            case "u": this.move(toks[0]); break;
            case "d": this.move(toks[0]); break;
            case "ne": this.move(toks[0]); break;
            case "nw": this.move(toks[0]); break;
            case "se": this.move(toks[0]); break;
            case "sw": this.move(toks[0]); break;
            case "exits": this.print_exits(); break;
            case "look": this.look(); break;
            case "take": this.take(toks[1]); break;
            case "drop": this.drop(toks[1]); break;
            case "inventory": this.print_inventory(); break;
            case "say": this.say(toks); break;
            case "build": this.add_node(toks); break;
            case "describe": this.describe_node(toks); this.look(); break;
            case "make": this.make(toks[1]); this.look(); break;
            case "dot": this.dot(); break;
            default: message("I don't understand "+toks[0]); break;
        }
    }

    // output a dot description of the world
    this.dot = function()
    {
        message("digraph {");
        for (var i in this.nodes)
        {
            message(this.nodes[i].name);
        }
        for (var i in this.edges)
        {
            message(this.edges[i].from + "->" + this.edges[i].to + " [label=\"" + this.edges[i].direction + "\"]");
        }
        message("}");
    }

    // get the reverse of a direction
    this.get_reverse_direction = function(dir)
    {
        switch(dir)
        {
            case "n": return "s"; break;
            case "s": return "n"; break;
            case "e": return "w"; break;
            case "w": return "e"; break;
            case "u": return "d"; break;
            case "d": return "u"; break;
            case "ne": return "sw"; break;
            case "nw": return "se"; break;
            case "se": return "nw"; break;
            case "sw": return "ne"; break;
        }
    }

    // checks a direction is valid
    this.valid_direction = function(dir)
    {
        return (dir=="n" || dir=="s" || dir=="w" || dir=="e"
            || dir=="u" || dir=="d" || dir=="ne" || dir=="nw"
            || dir=="sw" || dir=="se");
    }

    // adds a node from the current location 
    this.add_node = function(args)
    {
        if (args.length<3)
        {
            message("Build needs a place to make and a direction that it will be made in, eg \"build my house n\"");
        }
        else
        {
            var name = "";
            for (var i=0; i<args.length-1; i++)
            {
                if (i>0) 
                {
                    name+=args[i];
                    if (i<args.length-2) name+=" ";
                }
            }

            var direction = args[args.length-1];
            if (!this.valid_direction(direction))
            {
                message("I can't understand " + direction + " as a direction. I need n,e,s,w,u,d,ne,nw,se or sw");
                return;
            }

            var rev = this.get_reverse_direction(direction);
            nodes.push(new node(name, [name + " is sadly lacking a description..."], []));
            edges.push(new edge(this.location, name, direction, 
                     "Path " + direction + " to " + name, "", "", ""));

            // build two way edges            
            edges.push(new edge(name, this.location, rev, 
                     "Path " + rev + " to " + this.location, "", "", ""));
            message("Built "+name+" to the "+direction);
        }
    }

    // add a description for the current location
    this.describe_node = function(args)
    {
        p = "";
        for (var i in args)
        {
            if (i>0) p+=args[i]+" ";
        }
        this.find_node(this.location).desc=p;
    }

    // make a new object
    this.make = function(obj)
    {
        if (obj == undefined)
        {
            message("In order to make something I need a name, eg. \"make cheese\"");
            return;
        }
        this.find_node(this.location).objects.push(obj);
    }

    // returns the node from the name
    this.find_node = function(name)
    {
        for (var i in this.nodes)
        {
           if (this.nodes[i].name == name)
           {
               return this.nodes[i];
           }
        }
        return 0;
    }

    // finds all exits
    this.find_outward_edges = function(name)
    {
        ret = [];
        for (var e in this.edges)
        {
            if (this.edges[e].from == name)
            {
                ret.push(this.edges[e]);
            }
        }
        return ret;
    }

    // talk to anyone present
    this.say = function(toks)
    {
        p = "";
        for (var i in toks)
        {
            if (i>0) p+=toks[i]+" ";
        }
        message("You say \""+p+"\"");

        said_something = false;

        for (var i in this.characters)
        {
            if (this.characters[i].location==this.location)
            {
                message(this.characters[i].name+" says \""+this.characters[i].get_phrase(world)+"\"");
                said_something=true;
            }
        }

        if (!said_something)
        {
            message("There is no one here to hear you.");
        }
    }

    this.print_exits = function()
    {  
        exits = this.find_outward_edges(this.location);
        if (exits.length==0)
        {
            message("There are no exits from here, perhaps you need to build some.");
            return;
        }
        str = "Exits are:\n";
        for (var i in exits)
        {
            if (exits[i].key==0 || this.check_inventory(exits[i].key))
            {
                str+=exits[i].desc+"\n";
            }
            else
            {
                str+=exits[i].locked_desc+"\n";
            }
        }
        message(str);
    }

    this.print_inventory = function()
    {
        if (this.inventory.length==0)
        {
            message("You are not carrying anything");
        }
        else
        {
            str = "";
            for(var i in this.inventory)
            {
                str+=this.inventory[i]+" ";
            }
            message("You are carrying "+str);
        }
    }
    
    this.check_inventory = function(name)
    {
        for (var i in this.inventory)
        {
            if (this.inventory[i]==name) return true;         
        }
        return false;
    }

    this.update_characters = function(world)
    {
        for (var i in this.characters)
        {
            this.characters[i].update(world);
        }
    } 

    this.move = function(dir)
    {
        node = this.find_node(this.location);
        // search for an outward edge in this direction
        edges = this.find_outward_edges(node.name);
        for (var i in edges)
        {
            if (dir == edges[i].direction)
            {
                if (edges[i].key==0 || this.check_inventory(edges[i].key))
                {
                    this.location = edges[i].to;
                    this.update_characters(this);
                    this.look();                  
                    return;
                }
                else
                {
                    message(edges[i].fail_text);
                }
            }
        }
        message("You can't go in that direction!");
        this.print_exits();
    }

    this.take = function(name)
    {
        node = this.find_node(this.location);
        objects=[];        
        var found=false;
        for (var i in node.objects)
        {
            if (node.objects[i]==name)
            {
                this.inventory.push(name);
                message("You have taken the "+name);
                found=true;
            }
            else
            {
                objects.push(node.objects[i]);
            }
        }
        node.objects=objects;
        if (!found)
        {
            message("There is no "+name+" here!");
        }
    }

    this.drop = function(name)
    {
        node = this.find_node(this.location);
        inv=[];
        for (var i in this.inventory)
        {
            if (this.inventory[i]==name)
            {
                node.objects.push(name);
                message("You have dropped the "+name);
            }
            else
            {
                inv.push(this.inventory[i]);
            }
        }
        this.inventory=inv;        
    }

    this.look = function()
    {
        clear();
        message(this.find_node(this.location).name);
        message(this.find_node(this.location).info());
        for (var i in this.characters)
        {
           if (this.characters[i].location == this.location)
           {
               message(this.characters[i].name+" is here.");
           }
        }
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// stop the return key exiting the text field
function stoprkey(evt) 
{
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type=="text"))  { go(); return false;}
}

document.onkeypress = stoprkey; 

// start with an empty world
var world=new graph("Start",
  [new node("Start", ["An empty place"], [])],
  [],
  []
  );

function go()
{
    world.parse(area.input.value);
    area.input.value="";
}

world.look();

message("pinging server...");
server_call("ping",{}, function (data) {  message("server says: "+data); });

