// (c) 2010 Dave Griffiths GPLv3
//
// facebook test app - getting to know the api.
//
// most of the api calls are asychronous, so lots of 
// anonymous functions and deferred stuff is the way 
// they like it...

// init with our app id
FB.init({appId: '18990665644fc94b8a4ab7af636bbe00', status: true, 
         cookie: true, xfbml: true});

// utilities for messing with the page DOM 
// probably cleaner ways to do this, I'm learning...

// add some html to the page
function print(html)
{ 
    var div=document.createElement("div");   
    div.id = "foo";
    div.innerHTML = html;
    document.getElementById('werp').appendChild(div);
}

// clear the page
function clear()
{
    var element=document.getElementById('werp');
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

// converts a js object into text for displaying
function object_to_string(obj,depth)
{
    text="";
    for (i in obj)
    {
        if (typeof(obj[i])=="object")
        {            
            text+="<b>"+i+"</b>"+":"+object_to_string(obj[i],depth+1);
        }      
        else
        {
            text+=i+":"+obj[i]+"<br>";
        }
    }
    return text;
}

/////////////////////////////////////////////////////////////////
// the login stuff

// do the login onto facebook
function login()
{
    FB.login(function(response) {
        if (response.session) {
            clear(); // remove the login button
            start();
        } else {
            print(":( the login didn't work!");
        }
    });
}

// check to see if the user has logged in, and ask for login if they haven't
function check_login()
{
    FB.getLoginStatus(function(response) {
        if (response.session) {       
            // we are already logged in
            start();
        } else {
            // the login is a popup so need the user to click on a button
            // for the browser to allow this to happen
            print('<input type="button" value="Login to facebook" onClick="login();">');
        }
    });
}

////////////////////////////////////////////////////////////////
// actually do the stuff we want to do

function start()
{
    // print out the users details
    FB.api('/me', function(response) {
        print(object_to_string(response));
    });

    // print out all the information we have on their friends
    FB.api('/me/friends', function(response) {
        var text="";
        for (i in response.data)
        {
            // we just call the api with their UID
            FB.api('/'+response.data[i].id, function(response) {
                print(object_to_string(response));            
            });
            // could also go deeper - calling the api with all
            // the id elements in their data
        }
    });
}

///////////////////////////////////////////////////////////

// start the process going!
check_login();