(set! back-colour (vector 0 0 0))
(set! text-colour (vector 1 0.38 0))
(set! bg-colour (vector 0.03 0.09 0))
(set! poke-colour (vector 0.5 0.2 0))
(set! peek-colour (vector 0.2 0.5 0))
(set! menu-colour (vector 1 0.25 0.63))
(set! menu-hi-colour (vector 1 1 0))
(set! data-colour (vector 0.10 0.24 0))
(set! op-colour (vector 0.16 0.23 0.14))

(block-patch-init)
(let ((p
'(("typea" 631/1000 "freqa" 487/250 "slidea" 0 "decaya" 18/125 "decayb" 18/125 "freqb" 99/50 "mainvolume" 15/4 "typeb" 631/1000 "pan" 29/100 "attackb" 11/50 "sustainb" 0 "releaseb" 0 "sustaina" 109/1000 "releasea" 873/250 "attacka" 11/50 "cutoff" 39/1000 "ftype" 0 "volumef" 199/250 "decayf" 7/50 "resonance" 133/1000 "delay" 0 "delayfb" 0 "sustainf" 3/40 "releasef" 523/200 "attackf" 57/200 "poly" 3589/1000) ("mainvolume" 2393/1000 "decay" 2/25 "type" 803/250 "pan" 169/200 "freq" 2029/1000 "attack" 0 "sustain" 1/20 "release" 3/40 "distort" 0 "modattack" 0 "modtype" 0 "modvolume" 0 "modfreq" 3/2 "slide" 0 "crushbits" 0 "crushfreq" 0) ("mainvolume" 1477/500 "decaya" 289/1000 "typea" 421/125 "typeb" 3157/1000 "freqa" 41/40 "decayb" 217/1000 "pan" 709/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "attacka" 0 "sustaina" 0 "releasea" 0 "resonance" 52/125 "cutoff" 93/1000 "freqb" 923/1000 "ftype" 2371/1000 "volumef" 0 "lfodepth" 31/500 "lfofreq" 409/100 "crushfreq" 0 "crushbits" 0 "distort" 0) ("mainvolume" 319/250 "freq" 4 "modfreq" 7/500 "modvolume" 2161/500 "moddecay" 29/200 "decay" 3/25 "sustain" 1/40 "release" 773/1000 "poly" 166/25 "modtype" 0 "modslide" 0 "slide" 0 "modattack" 147/500 "modsustain" 1 "pan" 507/1000 "fbattack" 0 "fbvolume" 0 "attack" 129/200 "crushbits" 0 "crushfreq" 0) ("mainvolume" 5/4 "typea" 3157/1000 "freqb" 64/125 "freqa" 501/1000 "typeb" 947/500 "decaya" 107/1000 "decayb" 51/500 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 79/1000 "resonance" 333/1000 "ftype" 673/250 "slideb" 0 "ring" 2267/1000 "slidea" 0 "decayf" 0 "lfodepth" 0 "lfofreq" 0 "volumef" 421/1000 "delay" 0 "delayfb" 0 "attackf" 257/200 "crushfreq" 0 "crushbits" 0 "distort" 0))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "tabla")

(define voice-count 6)
