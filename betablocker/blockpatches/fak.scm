(set! back-colour (vector 0 0 0))
(set! text-colour (vector 0 1 0))
(set! bg-colour (vector 0 0.2 0))
(set! poke-colour (vector 1 0 0))
(set! peek-colour (vector 1 1 0))
(set! menu-colour (vector 0.5 1 0.5))
(set! menu-hi-colour (vector 1 1 1))
(set! data-colour (vector 0 0.4 0))
(set! op-colour (vector 0 0.7 0))

(block-patch-init)

(itchy-voice 1 "sub" '("freqa" 0.501 "freqb" 1.5 "typea" 3 "cutoff" 0.1
"volumef" 2 "decayf" 0.1 "distort" 0.98
"resonance" 0.44 "lfodepth" 0 "lfofreq" 3 "decaya" 0.05 "decayb" 0.05 
"sustaina" 0.2 "sustainb" 0.2 "slidea" 0.1 "slideb" 0.1 "mainvolume" 5))

(itchy-voice 2 "fm"  '("freq" 2.5 "modfreq" 0.001 "type" 3 "attack" 0.1 "decay" 0.1 
"sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0.5 "moddecay" 0.6 
"modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
"fbrelease" 0.5 "volume" 1 "modvolume" 5 "fbvolume" 0 "slide" 0 
"modslide" 0 "poly" 4 "mainvolume" 1))

(itchy-voice 3 "sub" '("freqa" 0.52 "freqb" 1.5 "typea" 7 "cutoff" 0.8 
"resonance" 0.2 "lfodepth" 0.2 "lfofreq" 0.1 "attacka" 0 "attackb" 0 "ftype" 3
"decaya" 0.1 "decayb" 0.1 "sustaina" 0.2 "sustainb" 0.2 "slidea" 0.1 
"slideb" 0.1 "mainvolume" 20))

(itchy-voice 4 "fm" '("freq" 0.5 "modfreq" 2.0 "type" 3 "attack" 0.1 "decay" 0.05 
        "sustain" 0 "release" 0 "modtype" 0 "modattack" 0 "moddecay" 0.6 
        "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
        "fbrelease" 0.5 "volume" 1 "modvolume" 0.2 "fbvolume" 0 "slide" 0 
        "modslide" 0 "mainvolume" 0.2))

(itchy-voice 5 "sub" '("freqa" 1 "freqb" 1.001 "typea" 8 "typeb" 8 "cutoff" 0.1
"resonance" 0.4 "lfodepth" 0.5 "lfofreq" 1 "decaya" 0.05 "decayb" 0.05 "distort" 0.98
"volumef" 0.9 "decayf" 0.2 "attackf" 0 "sustaina" 0 "sustainb" 0 
"slidea" 0.1 "slideb" 0.1 "mainvolume" 4))


(scratchy-samples 6 "808")

(define voice-count 6)
