(set! back-colour (vector 0 0 0.5))
(set! text-colour (vector 0.46 0.71 1))
(set! bg-colour (vector 1 0.25 0.63))
(set! poke-colour (vector 0.55 1 0.83))
(set! peek-colour (vector 1 0.55 0.83))
(set! menu-colour (vector 0.5 1 0.5))
(set! menu-hi-colour (vector 1 1 1))
(set! data-colour (vector 1 0.25 0.63))
(set! op-colour (vector 1 0.69 0.67))

(block-patch-init)
(let ((p
'(("crushfreq" 0 "crushbits" 10 "decaya" 18/125 "decayb" 18/125 "typea" 3789/1000 "typeb" 3789/1000 "distort" 0 "cutoff" 213/500 "volumef" 109/1000 "resonance" 449/1000 "ftype" 689/250 "decayf" 7/50 "delay" 0 "delayfb" 0 "mainvolume" 1193/500 "freqa" 41/40 "poly" 0 "freqb" 41/40 "releasef" 0 "sustainf" 0 "lfodepth" 1/4 "lfofreq" 151/1000 "ring" 0 "pan" 401/500) ("mainvolume" 531/1000 "crushfreq" 0 "crushbits" 0 "type" 2727/1000 "modtype" 509/1000 "fbattack" 131/1000 "fbvolume" 1 "fbdecay" 239/500 "fbsustain" 9/1000 "modfreq" 1 "freq" 2029/1000 "decay" 6/25 "sustain" 27/250 "release" 3/40 "poly" 3437/1000 "volume" 161/250 "modvolume" 5 "moddecay" 449/250 "attack" 0 "pan" 191/1000) ("mainvolume" 5/4 "ftype" 0 "cutoff" 93/1000 "resonance" 449/1000 "ring" 0 "typea" 3157/1000 "typeb" 3473/1000 "volumef" 593/1000 "distort" 397/500 "sustainf" 17/125 "decaya" 217/1000 "sustaina" 281/1000 "releasea" 79/500 "sustainb" 203/1000 "decayb" 289/1000 "attackb" 0 "releaseb" 119/500 "attackf" 0 "crushfreq" 43/500 "crushbits" 1909/250 "decayf" 88/125 "freqb" 2 "freqa" 1001/1000 "lfodepth" 39/125 "lfofreq" 7/250 "releasef" 0 "poly" 487/200 "pan" 9/25) ("mainvolume" 319/1000 "modvolume" 5 "fbvolume" 0 "fbdecay" 0 "fbattack" 0 "fbsustain" 69/200 "fbrelease" 1697/1000 "crushbits" 0 "crushfreq" 0 "attack" 0 "decay" 4/25 "poly" 0 "moddecay" 29/200 "modattack" 49/200 "freq" 101/100 "modfreq" 2 "distort" 0 "type" 1727/500 "modtype" 0 "sustain" 1 "release" 0 "delayfb" 0 "delay" 0 "volume" 54/125 "pan" 617/1000) ("mainvolume" 5 "resonance" 283/1000 "cutoff" 253/1000 "ftype" 2051/1000 "lfodepth" 39/250 "lfofreq" 151/1000 "delay" 0 "delayfb" 0 "decaya" 289/1000 "decayf" 211/1000 "sustaina" 7/50 "releasea" 3253/1000 "sustainf" 121/500 "releasef" 1 "volumef" 109/500 "decayb" 289/1000 "ring" 68/25 "typea" 3263/1000 "typeb" 921/250 "freqa" 4 "freqb" 2051/1000 "pan" 67/250 "releaseb" 3333/1000 "sustainb" 281/1000 "distort" 363/250 "slidea" 1267/1000))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "rip")

(define voice-count 6)
