(set! back-colour (vector 0 0 0))
(set! text-colour (vector 1 0.38 0))
(set! bg-colour (vector 0.03 0.09 0))
(set! poke-colour (vector 0.5 0.2 0))
(set! peek-colour (vector 0.2 0.5 0))
(set! menu-colour (vector 1 0.25 0.63))
(set! menu-hi-colour (vector 1 1 0))
(set! data-colour (vector 0.10 0.24 0))
(set! op-colour (vector 0.16 0.23 0.14))

(block-patch-init)

    (betablocker-osc-destination osc-itchy)
    (osc-send "/setclock" "" '())
    (osc-send "/clear" "" '())


(let ((p
'(("typea" 0 "freqa" 1963/1000 "slidea" 0 "decaya" 9/125 "decayb" 9/125 "freqb" 8 "mainvolume" 1761/500 "typeb" 0 "pan" 273/500 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 653/1000 "ftype" 0 "volumef" 0 "decayf" 7/50 "resonance" 1/8 "delay" 0 "delayfb" 0 "sustainf" 0 "releasef" 0 "attackf" 0 "poly" 0 "lfodepth" 281/1000 "lfofreq" 909/1000) ("mainvolume" 319/1000 "decay" 1/25 "type" 0 "pan" 169/200 "freq" 8 "attack" 0 "sustain" 0 "release" 0 "distort" 0 "modattack" 0 "modtype" 509/125 "modvolume" 5 "modfreq" 957/200 "slide" 0 "crushbits" 0 "crushfreq" 0 "fbvolume" 1 "fbattack" 0 "fbdecay" 1347/1000 "fbsustain" 0 "fbrelease" 0 "modslide" 0 "delayfb" 453/500 "delay" 1/200) ("mainvolume" 3863/1000 "decaya" 9/125 "typea" 8 "typeb" 8 "freqa" 32/125 "decayb" 9/125 "pan" 709/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "attacka" 0 "sustaina" 0 "releasea" 0 "resonance" 77/250 "cutoff" 9/25 "freqb" 63/250 "ftype" 929/500 "volumef" 0 "lfodepth" 1/4 "lfofreq" 757/1000 "crushfreq" 0 "crushbits" 0 "distort" 0 "ring" 0 "slideb" 0 "slidea" 0 "volumea" 1 "poly" 0) ("mainvolume" 2819/1000 "freq" 8 "modfreq" 4 "modvolume" 479/200 "moddecay" 1553/1000 "decay" 2/25 "sustain" 2/125 "release" 949/1000 "poly" 166/25 "modtype" 7199/1000 "modslide" 351/1000 "slide" 0 "modattack" 1323/1000 "modsustain" 301/500 "pan" 507/1000 "fbattack" 7/40 "fbvolume" 1 "attack" 0 "crushbits" 0 "crushfreq" 0 "type" 0 "fbdecay" 13/50 "fbsustain" 129/200 "fbrelease" 2477/1000 "distort" 0) ("mainvolume" 511/500 "typea" 8 "freqb" 307/1000 "freqa" 41/200 "typeb" 8 "decaya" 9/125 "decayb" 9/125 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 1029/500 "cutoff" 1 "resonance" 3/8 "ftype" 673/500 "slideb" 0 "ring" 2337/1000 "slidea" 0 "decayf" 0 "lfodepth" 39/250 "lfofreq" 303/500 "volumef" 0 "delay" 0 "delayfb" 0 "attackf" 0 "crushfreq" 0 "crushbits" 0 "distort" 0))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "tabla")

(define voice-count 6)
