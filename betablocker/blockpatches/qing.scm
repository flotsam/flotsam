(set! back-colour (vector 0 0 0.5))
(set! text-colour (vector 0.46 0.71 1))
(set! bg-colour (vector 1 0.25 0.63))
(set! poke-colour (vector 0.55 1 0.83))
(set! peek-colour (vector 1 0.55 0.83))
(set! menu-colour (vector 0.5 1 0.5))
(set! menu-hi-colour (vector 1 1 1))
(set! data-colour (vector 1 0.25 0.63))
(set! op-colour (vector 1 0.69 0.67))

(block-patch-init)

    (betablocker-osc-destination osc-itchy)
    (osc-send "/setclock" "" '())
    (osc-send "/clear" "" '())


(let ((p
'(("typea" 947/500 "freqa" 1/2 "slidea" 0 "decaya" 18/125 "decayb" 18/125 "freqb" 99/50 "mainvolume" 15/4 "typeb" 1789/500 "pan" 29/100 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 159/1000 "ftype" 801/500 "volumef" 199/250 "decayf" 7/50 "resonance" 133/1000 "delay" 0 "delayfb" 0) ("mainvolume" 1063/1000 "decay" 2/25 "type" 803/250 "pan" 169/200 "freq" 8 "attack" 0 "sustain" 1/20 "release" 3/40 "distort" 829/500 "modattack" 0 "modtype" 0 "modvolume" 0 "modfreq" 1/1000 "slide" 0 "crushbits" 0 "crushfreq" 0) ("mainvolume" 1477/1000 "decaya" 289/1000 "typea" 421/125 "typeb" 3157/1000 "freqa" 41/40 "decayb" 217/1000 "pan" 709/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "attacka" 0 "sustaina" 0 "releasea" 0 "resonance" 52/125 "cutoff" 213/1000 "freqb" 923/1000 "ftype" 737/500 "volumef" 0 "lfodepth" 109/500 "lfofreq" 227/500) ("mainvolume" 957/1000 "freq" 4 "modfreq" 1/2 "modvolume" 2161/500 "moddecay" 29/200 "decay" 3/25 "sustain" 183/1000 "release" 773/1000 "poly" 166/25 "modtype" 0 "modslide" 0 "slide" 383/1000 "modattack" 3137/1000 "modsustain" 1 "pan" 507/1000 "fbattack" 0 "fbvolume" 0 "attack" 129/200 "crushbits" 0 "crushfreq" 0) ("mainvolume" 1477/500 "typea" 3157/1000 "freqb" 2037/1000 "freqa" 2 "typeb" 763/250 "decaya" 181/500 "decayb" 289/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 759/1000 "resonance" 333/1000 "ftype" 673/250 "slideb" 0 "ring" 2267/1000 "slidea" 0 "decayf" 0 "lfodepth" 687/1000 "lfofreq" 227/500 "volumef" 0 "delay" 0 "delayfb" 0))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "kip")

(define voice-count 6)
