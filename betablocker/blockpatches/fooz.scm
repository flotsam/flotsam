(set! back-colour (vector 1 0 0.5))
(set! text-colour (vector 0 1 0))
(set! bg-colour (vector 0 0 0))
(set! poke-colour (vector 1 0 0))
(set! peek-colour (vector 1 1 1))
(set! menu-colour (vector 1 0 0))
(set! menu-hi-colour (vector 1 1 1))
(set! data-colour (vector 1 0.5 0.2))
(set! op-colour (vector 1 0 0.5))

(block-patch-init)

(itchy-voice 1 "sub" '("freqa" 3 "freqb" 3.01 "typea" 3 "cutoff" 0.1 
"resonance" 0.44 "lfodepth" 0.1 "lfofreq" 0.1 "decaya" 0.05 "decayb" 0.05 
"sustaina" 0 "sustainb" 0 "slidea" 0.1 "slideb" 0.1 "crushfreq" 0.6 "crushbits" 8 "mainvolume" 5))

(itchy-voice 2 "fm"  '("freq" 2.5 "modfreq" 1.0 "type" 3 "attack" 0.1 "decay" 0.1 
"sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
"modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
"fbrelease" 0.5 "volume" 1 "modvolume" 6 "fbvolume" 0 "slide" 0 
"modslide" 0 "poly" 4 "mainvolume" 0.3))
       
(itchy-voice 3 "sub" '("freqa" 1 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2 "typea" 2 
      "attacka" 0 "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "typeb" 2 
      "attackb" 0 "decayb" 0.05 "sustainb" 0.1 "releaseb" 0.5 
      "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 "releasef" 0.5 "volumef" 0.2 
      "lfodepth" 0.5 "lfofreq" 0.1  "slidea" 0.02 
      "slideb" 0.05 "distort" 0.7 "poly" 3 "mainvolume" 2))

(itchy-voice 4 "fm"  '("freq" 2.5 "modfreq" 1.0 "type" 3 "attack" 0.1 "decay" 0.1 
"sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
"modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 
"fbrelease" 0.5 "volume" 1 "modvolume" 6 "fbvolume" 0 "slide" 0 
"modslide" 0 "poly" 4 "mainvolume" 0.3))
       
(itchy-voice 5 "sub" '("freqa" 1 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2 "typea" 2 
      "attacka" 0 "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "typeb" 2 
      "attackb" 0 "decayb" 0.05 "sustainb" 0.1 "releaseb" 0.5 
      "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 "releasef" 0.5 "volumef" 0.2 
      "lfodepth" 0.5 "lfofreq" 0.1  "slidea" 0.02 
      "slideb" 0.05 "distort" 0.7 "poly" 3 "mainvolume" 2))

(scratchy-samples 6 "electro_d")

(define voice-count 6)
