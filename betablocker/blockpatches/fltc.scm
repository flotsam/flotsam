(set! back-colour (vector 0 0.4 0.7))
(set! text-colour (vector 1 1 0))
(set! bg-colour (vector 0 0.2 0.7))
(set! poke-colour (vector 1 0 0))
(set! peek-colour (vector 1 1 0))
(set! menu-colour (vector 0 0 1))
(set! menu-hi-colour (vector 0 0.5 1))
(set! data-colour (vector 0 0.2 0.7))
(set! op-colour (vector 1 0.5 0))

(block-patch-init)

;(itchy-voice 1 "junk" '())

(itchy-voice 1 "sub" '("freqa" 1 "freqb" 1.001 "typea" 8 "typeb" 8 "cutoff" 1
"resonance" 0.1 "lfodepth" 0 "lfofreq" 60 "decaya" 0.05 "decayb" 0.05 
"volumef" 0.9 "decayf" 0.2 "attackf" 0 "sustaina" 0.2 "sustainb" 0.2 
"slidea" 0.1 "slideb" 0.1 "mainvolume" 2))

(itchy-voice 2 "fm"  '("freq" 2.5 "modfreq" 2.5001 "type" 3 "attack" 0.1 "decay" 0.1 
"sustain" 0.1 "release" 2.5 "modtype" 0 "modattack" 0 "moddecay" 0.6 
"modsustain" 0.1 "modrelease" 0.5 "fbattack" 0.2 "fbdecay" 0.1 "fbsustain" 0.1 
"fbrelease" 0.5 "volume" 1 "modvolume" 5 "fbvolume" 0.6 "slide" 0 
"modslide" 0 "poly" 4 "crushfreq" 0.1 "crushbits" 5 "mainvolume" 0.2))

(itchy-voice 3 "sub" '("freqa" 1 "freqb" 1.001 "typea" 1 "typeb" 1 "cutoff" 0.1
"resonance" 0.4 "lfodepth" 0.2 "lfofreq" 0.6 "decaya" 0.05 "decayb" 0.05 "ftype" 2
"volumef" 0.9 "decayf" 0.2 "attackf" 0 "sustaina" 0.2 "sustainb" 0.2 
"slidea" 0.1 "slideb" 0.1 "mainvolume" 2))
 
(itchy-voice 4 "fm"  '("freq" 0.5 "modfreq" 0.01 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.2 
     "release" 0.6 "modtype" 0 "modattack" 0.2 "moddecay" 0 "modsustain" 0.1 
     "modrelease" 0.5 "fbattack" 0.5 "fbdecay" 0.1 "fbsustain" 0.1 "fbrelease" 0.5 
     "volume" 1 "modvolume" 2 "fbvolume" 6 "crushfreq" 0.5 "crushbits" 6 
     "poly" 1 "mainvolume" 0.2))

(itchy-voice 5 "sub" '("freqa" 1 "freqb" 1.001 "typea" 1 "typeb" 1 "cutoff" 0.1
"resonance" 0.4 "lfodepth" 0.2 "lfofreq" 0.6 "decaya" 0.05 "decayb" 0.05 "ftype" 2
"volumef" 0.9 "decayf" 0.2 "attackf" 0 "sustaina" 0.2 "sustainb" 0.2 
"slidea" 0.1 "slideb" 0.1 "mainvolume" 2))

(scratchy-samples 6 "kip")
	 
(define voice-count 6)
