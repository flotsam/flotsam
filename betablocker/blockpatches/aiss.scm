(set! back-colour (vector 0 0 0))
(set! text-colour (vector 1 0.38 0))
(set! bg-colour (vector 0.03 0.09 0))
(set! poke-colour (vector 0.5 0.2 0))
(set! peek-colour (vector 0.2 0.5 0))
(set! menu-colour (vector 1 0.25 0.63))
(set! menu-hi-colour (vector 1 1 0))
(set! data-colour (vector 0.10 0.24 0))
(set! op-colour (vector 0.16 0.23 0.14))

(block-patch-init)

    (betablocker-osc-destination osc-itchy)
    (osc-send "/setclock" "" '())
    (osc-send "/clear" "" '())


(let ((p
'(("typea" 8 "freqa" 1/4 "slidea" 0 "decaya" 289/1000 "decayb" 181/500 "freqb" 51/200 "mainvolume" 142/125 "typeb" 8 "pan" 273/500 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 493/1000 "ftype" 0 "volumef" 0 "decayf" 7/50 "resonance" 0 "delay" 0 "delayfb" 0 "sustainf" 3/40 "releasef" 523/200 "attackf" 0 "poly" 0) ("mainvolume" 239/500 "decay" 24/25 "type" 2 "pan" 169/200 "freq" 1/2 "attack" 403/500 "sustain" 1/20 "release" 3/40 "distort" 0 "modattack" 0 "modtype" 309/250 "modvolume" 1927/500 "modfreq" 957/200 "slide" 0 "crushbits" 4571/500 "crushfreq" 27/1000 "fbvolume" 1 "fbattack" 0 "fbdecay" 739/1000 "fbsustain" 263/1000 "fbrelease" 883/250 "modslide" 0) ("mainvolume" 1931/1000 "decaya" 217/500 "typea" 8 "typeb" 8 "freqa" 32/125 "decayb" 181/500 "pan" 709/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "attacka" 0 "sustaina" 0 "releasea" 0 "resonance" 83/1000 "cutoff" 2/5 "freqb" 63/250 "ftype" 929/500 "volumef" 0 "lfodepth" 1/4 "lfofreq" 757/1000 "crushfreq" 0 "crushbits" 0 "distort" 0 "ring" 0 "slideb" 0 "slidea" 0 "volumea" 1 "poly" 0) ("mainvolume" 691/1000 "freq" 8 "modfreq" 957/200 "modvolume" 479/200 "moddecay" 1553/1000 "decay" 3/25 "sustain" 1/40 "release" 773/1000 "poly" 166/25 "modtype" 7199/1000 "modslide" 351/1000 "slide" 0 "modattack" 1323/1000 "modsustain" 301/500 "pan" 507/1000 "fbattack" 263/500 "fbvolume" 1 "attack" 129/200 "crushbits" 0 "crushfreq" 0 "type" 8 "fbdecay" 2521/1000 "fbsustain" 2/5 "fbrelease" 2477/1000 "distort" 0) ("mainvolume" 5 "typea" 8 "freqb" 307/1000 "freqa" 501/1000 "typeb" 8 "decaya" 289/1000 "decayb" 217/1000 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0 "cutoff" 23/25 "resonance" 3/8 "ftype" 689/250 "slideb" 0 "ring" 0 "slidea" 0 "decayf" 0 "lfodepth" 281/500 "lfofreq" 227/500 "volumef" 0 "delay" 0 "delayfb" 0 "attackf" 0 "crushfreq" 0 "crushbits" 0 "distort" 0))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "808")

(define voice-count 6)
