(set! back-colour (vector 0 0 0.5))
(set! text-colour (vector 0.46 0.71 1))
(set! bg-colour (vector 1 0.25 0.63))
(set! poke-colour (vector 1 0.88 0.2))
(set! peek-colour (vector 0.78 1 0.3))
(set! menu-colour (vector 0.5 1 0.5))
(set! menu-hi-colour (vector 1 1 1))
(set! data-colour (vector 1 0.38 0))
(set! op-colour (vector 0.78 1 0))

(block-patch-init)

    (betablocker-osc-destination osc-itchy)
    (osc-send "/setclock" "" '())
    (osc-send "/clear" "" '())


(let ((p
'(("typea" 947/500 "freqa" 8 "slidea" 0 "decaya" 19/1000 "decayb" 21/1000 "freqb" 8 "mainvolume" 3863/1000 "typeb" 1789/500 "pan" 29/100 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0) ("mainvolume" 691/1000 "decay" 2/25 "type" 803/250 "pan" 97/100 "freq" 2 "attack" 0 "sustain" 0 "release" 0) ("mainvolume" 142/125 "decaya" 9/125 "typea" 421/125 "typeb" 3157/1000 "freqa" 4 "decayb" 6/125 "pan" 106/125 "attackb" 0 "sustainb" 0 "releaseb" 0 "attacka" 0 "sustaina" 0 "releasea" 0) ("mainvolume" 319/200 "freq" 4 "modfreq" 1/500 "modvolume" 2161/500 "moddecay" 29/200 "decay" 3/25 "sustain" 29/500 "release" 773/1000 "poly" 166/25 "modtype" 0 "modslide" 0 "slide" 0 "modattack" 0 "modsustain" 1 "pan" 507/1000 "fbattack" 0 "fbvolume" 0 "attack" 0) ("mainvolume" 213/125 "typea" 0 "freqb" 2051/1000 "freqa" 4 "typeb" 821/200 "decaya" 18/125 "decayb" 9/125 "attackb" 0 "sustainb" 0 "releaseb" 0 "sustaina" 0 "releasea" 0 "attacka" 0))
))

(itchy-voice 1 "sub" (list-ref p 0))
(itchy-voice 2 "fm"  (list-ref p 1))
(itchy-voice 3 "sub" (list-ref p 2))
(itchy-voice 4 "fm" (list-ref p 3))
(itchy-voice 5 "sub" (list-ref p 4)))

(scratchy-samples 6 "electro_d")

(define voice-count 6)
