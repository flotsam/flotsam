(define (draw-tree depth)
    (when (not (zero? depth))
        (translate (vector 1.2 0 0))
        (colour (vector (sin (+ depth (* 3 (time)))) 0.5 0))
        (draw-cube)
        (with-state
            (rotate (vector 0 0 27))
            (draw-tree (- depth 1)))
        (with-state
            (rotate (vector 0 0 -27))
            (draw-tree (- depth 1)))))

(clear)
(every-frame (draw-tree 1))

