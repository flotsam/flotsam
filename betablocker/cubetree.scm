(define (draw-cubes n)
    (cond 
        ((not (zero? n))
            (translate (vector 1.2 0 0))
            (colour (vector (sin (+ n (* 3 (time)))) 0.5 0))
            (draw-cube)
            (with-state
                (rotate (vector 0 0 27))
                (draw-cubes (- n 1)))
            (with-state
                (rotate (vector 0 0 -27))
                (draw-cubes (- n 1))))))

(clear)
(every-frame (draw-cubes 1))

