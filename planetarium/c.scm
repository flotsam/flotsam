(require fluxus-016/planetarium)
(require "camera-settings.ss")

(start-audio "alsa_pcm:capture_1" 1024 44100)


(clear)
(ortho)
(define p 0)
(define dome (dome-build 2 50 2048 
    (lambda () 
;        (blur 0.01)         
        (set! p (with-state 
                (translate (vector 0 0 -10))
                (hint-anti-alias)
                (hint-none)
                (hint-points)
                (point-width 10)
                (build-particles 1)))
        (with-primitive p
            (pdata-add "dir" "v")
            (pdata-map!
                (lambda (p)
                    (srndvec))
                "p")
            (pdata-map!
                (lambda (dir)
                    (vmul (srndvec) 0.1))
                "dir")
            (pdata-map!
                (lambda (c)
                    (vector 1 1 1))
                "c")))))
(setup-camera)


(define (animate n)
    (with-primitive p
        (pdata-index-map!
            (lambda (i dir p)
                (vadd dir (pdata-index-fold 
                    (lambda (fi other-p r)
                        (if (eq? fi i) r
                            (vadd r (vmul (vsub p other-p) (* -0.0001 (/ 1 (vdist other-p p)))))))
                    (vector 0 0 0) "p")))
            "dir" "p")
        (pdata-op "+" "p" "dir")))        

(dome-every-frame 
    (lambda () 
        (with-state 
            (translate (vector 0 0 -10))
            (rotate (vector 20 20 0))
            (animate 1))))