;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;; a grid is a space subdivision algorithm to speed up neigbourhood checking for 
;; the vertices on the graph. it's not big or clever, and it is crippled for 2D
(define grid%
  (class object%
    (public
      clear ;; clear out everything
      add ;; add an element to the grid
      get-neighbors ;; returns a list of close neigbours
      print ;; print stuff out
      )
    
    (init-field (size 1))
    (init-field (topology (vector 10 10 1)))
    (define domains '())
    (define id-domain-map (make-object map%))
    (define grid-centre (vector 0 0 0))
    
    (define (print)
      (for-each
       (lambda (domain)
         (if (not (send domain empty?))
             (printf "~a~n" (send domain size))))
       domains))
    
    (define (grid-new)
      (define (make-domains n d)
        (cond 
          ((zero? n) d)
          (else
           (make-domains (- n 1) (cons (make-object set%) d)))))
      
      (set! domains (make-domains (* (vector-ref topology 0) 
                                     (vector-ref topology 1) 
                                     (vector-ref topology 2)) '()))
      (super-new))
    
    (define (clear)
      (for-each 
       (lambda (domain)
         (send domain clear))
       domains)
      (send id-domain-map clear))
    
    (define (add position id)
      ;; get the position into grid space
      (let* ((gp (vector (+ (/ (vector-ref position 0) size) (/ (vector-ref topology 0) 2.0))
                         (+ (/ (vector-ref position 1) size) (/ (vector-ref topology 1) 2.0))
                         (+ (/ (vector-ref position 2) size) (/ (vector-ref topology 2) 2.0))))
             
             (x (inexact->exact (floor (vector-ref gp 0))))
             (y (inexact->exact (floor (vector-ref gp 1))))
             (z (inexact->exact (floor (vector-ref gp 2))))
             (index (+ (* z (vector-ref topology 1) (vector-ref topology 0)) 
                       (* y (vector-ref topology 0)) 
                       x)))
        (cond 
          ((or (< index 0) (>= index (length domains)))
           (printf "grid add: id:~a pos:~a is outside grid!~n" id position))
          (else
           (send (list-ref domains index) add id)
           (send id-domain-map add (cons id index))))))
    
    (define (get-neighbors id)
      (let ((index (send id-domain-map get id)))
        (append 
         (send (list-ref domains index) get-raw-list)
         (send (list-ref domains (- index 1)) get-raw-list)
         (send (list-ref domains (+ index 1)) get-raw-list)
         (send (list-ref domains (- index (vector-ref topology 1))) get-raw-list)
         (send (list-ref domains (+ index (vector-ref topology 1))) get-raw-list)         
         (send (list-ref domains (+ 1 (- index (vector-ref topology 1)))) get-raw-list)
         (send (list-ref domains (+ 1 (+ index (vector-ref topology 1)))) get-raw-list)
         (send (list-ref domains (- (- index (vector-ref topology 1)) 1)) get-raw-list)
         (send (list-ref domains (- (+ index (vector-ref topology 1)) 1)) get-raw-list))))
    
    (grid-new)))

(define (test-grid)
  (let ((g (make-object grid% 10)))
    (send g add (vector 0 0 0) 1)
    ;(display (send g get-neighbors 1))(newline)
    (if (not (equal? (send g get-neighbors 1) '(1))) (error "grid test failed"))
    (send g add (vector 1 0 0) 2)
    (if (not (equal? (send g get-neighbors 1) '(2 1))) (error "grid test failed"))
    (send g add (vector 1 14 0) 3)
    (send g add (vector 18 0 0) 4)
    ;(display (send g get-neighbors 1))(newline)
    (if (not (equal? (send g get-neighbors 1) '(2 1 4 3))) (error "grid test failed"))
    (send g add (vector 1 20 0) 5)
    (if (not (equal? (send g get-neighbors 1) '(2 1 4 3))) (error "grid test failed"))
    
    
    
    
    ))

(test-grid)
