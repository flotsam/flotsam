#!/usr/bin/env python
from Tkinter import *
from time import *

class clock:
	def __init__(self,master):
		self.starttime=0
		self.widget=Button(master,text="0:0",font=("Courier",25,"bold"),relief=FLAT,
			bg="black",borderwidth=0, activebackground="black", foreground="yellow",
            highlightthickness=0,command=self.reset)
		self.widget.pack()

	def idle(self):
		thistime = int(time()-self.starttime)
		self.widget.config(text=str(thistime/60)+":"+str(thistime%60))
		self.widget.after(1000, self.idle)

	def reset(self):
		if not self.starttime:
			self.widget.after_idle(self.idle)
		self.starttime=time()

win = Tk()
win.title("ticktock")
slider = clock(win)
win.mainloop()
