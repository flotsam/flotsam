(require "jam.ss")
(require scheme/class)
(require fluxus-016/planetarium)
(require fluxus-016/fluxa)

(set-dome-mode! #t)

(clear)
(ambient (vector 0.5 0.5 0.5))
(clear-colour 0)
(ortho)
(define j #f)

(define dome (dome-build 3 180 2048))

#;(with-primitive dome
    (wire-colour 0.4)
    (hint-wire))

(with-pixels-renderer (dome-pixels)
  (let ((l (make-light 'point 'free)))
    (light-diffuse 0 (vector 0.3 0.3 0.3))
    (light-diffuse l (vector 0.6 0.6 0.6))
    (light-position l (vector 50 90 80))
    (light-ambient l (vector 0.5 0.5 0.5))
    (shadow-light 0))
  (ambient 0.5)
  (set! j (make-object jam% 20 20 0))
  (dome-camera-lag 0.1)
  (send j build (with-state
                   ; (translate (vector 2 -1 -4))
                   ;     (rotate (vector 0 0 45)) 
                        (build-locator))))

(send (send j get-vcity) set-sync-offset! -0.04)

(dome-setup-main-camera)

(volume 10)

(every-frame 
  (with-pixels-renderer (dome-pixels)
    (send j update (time-now) (delta))))
