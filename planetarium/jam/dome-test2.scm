(require "jam.ss")
(require scheme/class)
(require "planetarium.ss")

(clear)

(define t 0)
(define s (build-cube))

(dome-build (lambda () 
    (set! t (with-state
      (translate (vector 0 0 -10))
      (build-cube)))))

(every-frame
 (with-primitive s
    (rotate (vector 1 0 0))))

(dome-every-frame 
  (lambda () 
   (with-primitive t
    (rotate (vector 1 0 0)))))
