(clear)

(texture-params 0 '(min linear mag linear))
(define p (with-state (translate (vector -200 0 0))
        (build-pixels 1024 1024 #t)))

(define (setup-camera n)
    (set-fov 90 1 1000)
    (current-camera n)
    (viewport (* n 0.25) 0 0.25 1)
    (set-camera (mmul (mtranslate (vector 0 0 0)) 
            (mrotate (vector 0 (* n 90) 0)))))

(define q (with-pixels-renderer p
;        (setup-camera (build-camera))
;        (setup-camera (build-camera))
        (setup-camera (build-camera))
        (setup-camera 0)
        
        (with-state
            (scale 100)
            (hint-unlit)
            (backfacecull 0)
            (colour 1)
            (texture (load-texture "textures/grid.png"))
            (build-sphere 20 20))
        
        (with-state
            (translate (vector 0 -10 0))
            (scale 500)
            (rotate (vector 90 0 0))
            (hint-unlit)
            (colour (vector 0.5 1 0.5))
            (build-plane))

        (let ((p (build-locator)))
            (with-state
                (parent p)
                (for ((x (in-range -10 10)))
                    (for ((y (in-range -10 10)))
                            (with-state
                                (with-state
                                    (translate (vmul (vector x -1 y) 10))
                                    (colour (rndvec))
                                   ; (rotate (vector 0 (* 90 (rndf)) 0))
                                    (scale (vector (+ 4 (rndf)) (* 10 (rndf)) (+ 4 (rndf))))
                                    (build-cube))))))
            
            p)))

(define o (with-state
        (texture (pixels->texture p))
        (hint-unlit)
        (scale -10)
        (build-sphere 20 20)))

#;(with-state
    (translate (vector 2 0 0))
    (hint-unlit)
    (texture (load-texture "textures/grid.png"))
    (build-sphere 20 20))


#;(every-frame
    (with-pixels-renderer p
        (with-primitive q
            (rotate (vector 1 0.2 0)))))
