(require "jam.ss")
(require scheme/class)
(require fluxus-017/fluxa)

(clear-texture-cache)
(clear-geometry-cache)

(clear)
(camera-lag 0.1)
(clear-colour 0)
(define l (make-light 'point 'free))
(light-diffuse 0 (vector 0.3 0.3 0.3))
(light-diffuse l (vector 0.6 0.6 0.6))
(light-position l (vector 50 90 80))
(light-ambient l (vector 0.5 0.5 0.5))
(shadow-light 0)

(define j (make-object jam% 20 20 0))

#;(send mycity set-tiles! (list
        (make-tile #f #f #f #f)
        (make-tile #f #t #t #f)
        (make-tile #f #f #t #t)
        
        (make-tile #f #f #f #f)
        (make-tile #t #f #t #f)
        (make-tile #t #f #t #f)
         
        (make-tile #f #f #f #f)
        (make-tile #t #t #f #f)
        (make-tile #t #f #f #t)))

(send j build (build-locator))
(send (send j get-vcity) set-sync-offset! -0.04)
(volume 10)

(define (animate)     
;    (printf "~a~n" (time-now))
    (send j update (time-now) (delta)))

(every-frame (animate))
