

(require fluxus-016/tricks)
(clear)
(ortho)
(clear-colour (vector 1 0 0))

(light-diffuse 0 (vector 0 0 0))
(let ((l (make-light 'point 'free)))
    (light-position l (vector 20 100 0))
    (light-diffuse l (vector 1 1 1)))

(define ob (with-state
        (colour (vector 0.1 0.8 1))
;        (rotate (vector -90 0 0))
        (load-primitive "meshes/bot.obj")))

(with-primitive ob
;    (apply-transform)
;    (bot-transform renderer)
    (recalc-normals 1)
    (cheap-toon ob 0.03 (vector 0 0 0))
    (recalc-normals 0))
