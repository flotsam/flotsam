// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class AnimatedEntity extends Entity 
{
	public var SrcPos:Vec3;
	public var DestPos:Vec3;
	public var Time:Float;
	public var Speed:Float;
	
	public function new(pos:Vec3, bitmapData:BitmapData) 
	{
		super(pos, bitmapData);
		Time=10;
		Speed=0;
	}
	
	public function MoveTo(pos:Vec3,speed:Float)
	{
		SrcPos=Pos;
		DestPos=pos;
		Time=0;
		Speed=speed;
	}
	
	override function Update(frame:Int, world:World)
	{
		super.Update(frame,world);
		
		if (Time<1) 
		{
			Time+=Speed;
			if (Time>1) Time=1;
			Pos=SrcPos.Lerp(DestPos,Time);
			UpdatePos();
		}
	}
}
