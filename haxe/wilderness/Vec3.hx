// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class Vec3 
{
	public var x:Float;
	public var y:Float;
	public var z:Float;
	
	public function new(px:Float, py:Float, pz:Float)
	{
		x=px; y=py; z=pz;
	}
	
	public function Add(other:Vec3) : Vec3
	{
		return new Vec3(x+other.x,y+other.y,z+other.z);
	}

	public function Sub(other:Vec3) : Vec3
	{
		return new Vec3(x-other.x,y-other.y,z-other.z);
	}
	
	public function Mag() : Float
	{
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	public function Lerp(other:Vec3,t:Float) : Vec3
	{
		return new Vec3(x*(1-t) + other.x*t,
						y*(1-t) + other.y*t,
						z*(1-t) + other.z*t);
	}
}
