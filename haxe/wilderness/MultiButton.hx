// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class MultiButton extends Button
{
	public var State:Int;
	public var Bitmaps:Array<BitmapData>;
	
	public function new(pos:Vec3, size:Vec3, bitmaps:Array<BitmapData>) 
	{
		super(pos,size,bitmaps[0],Click);
		State=0;
		Bitmaps=bitmaps;
	}

	public function SetState(s:Int)
	{
		State=s;
		ChangeBitmap(Bitmaps[State]);
	}

	function Click(_)
	{
		State++;
		if (State>=Bitmaps.length)
		{
			State=0;
		}
		ChangeBitmap(Bitmaps[State]);
	}
}
