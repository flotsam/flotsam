// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

import flash.display.MovieClip;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.text.TextFieldType;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

class TextEntry extends MovieClip
{
	public var TextField:TextField;
	public var Callback:Dynamic -> Void;
	
	public function new(x,y,w,h,f:Dynamic -> Void)
	{
		super();
		Callback = f;
		TextField = new TextField();
        TextField.background = true;
        TextField.border = true;
		TextField.text = "Enter a name";
		TextField.type = TextFieldType.INPUT;
		TextField.x = x;
		TextField.y = y;		
		TextField.width = w;
		TextField.height = h;		
		var tf = new flash.text.TextFormat();
        tf.font = "Verdana"; 
        tf.size = 20;                
        tf.color= 0x000000;           
        TextField.setTextFormat(tf);
		addChild(TextField);	           
		
		addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
		addEventListener(flash.events.KeyboardEvent.KEY_DOWN,OnKeyDown,false); 
	}
	
	function OnKeyDown(e:KeyboardEvent) 
	{
		// ENTER pressed ?
		if( e.keyCode == 13 ) 
		{
			var text = TextField.text;
			TextField.text = "";
			Callback(text);
		}
	}
	
	function OnMouseDown(_) 
	{
		if (TextField.text=="Enter a name")
		{
			TextField.text = "";
		}
	}
	
}
