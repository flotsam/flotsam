// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

// a plant as represented on the server

class ServerPlant
{
	public function new(own,ix,iy,t) { x=ix; y=iy; owner=own; type=t; }
	public var x:Int;
	public var y:Int;
	public var owner:String;
	public var type:Int;
}
