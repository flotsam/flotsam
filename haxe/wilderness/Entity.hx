// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

import flash.display.Sprite;
import flash.display.BitmapData;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;

class Entity extends Sprite
{
	public var Pos:Vec3;
		
	public function new(pos:Vec3, bitmapData:BitmapData) 
	{
		super();
		ChangeBitmap(bitmapData);
		Pos=pos;
		UpdatePos();		
	}

	public function MouseDown(f:Dynamic -> Void=null)
	{
		addEventListener(MouseEvent.MOUSE_DOWN, f);
	}

	public function ChangeBitmap(bitmapData:BitmapData)
	{
		graphics.clear();
		graphics.beginBitmapFill(bitmapData);
        graphics.drawRect(0,0,64,112);
		graphics.endFill();
	}

	public function Scale(size:Float)
	{
		var m:Matrix = transform.matrix;
		var x=32;
		var y=112;
		var p:Point = m.transformPoint(new Point(x, y));
		m.translate(-p.x, -p.y);
		m.scale(size,size);
		m.translate(p.x, p.y);
		transform.matrix = m;
	}
	
	public function Rotate(angle:Float)
	{
		var m:Matrix = transform.matrix;
		var x=32;
		var y=112;
		var p:Point = m.transformPoint(new Point(x, y));
		m.translate(-p.x, -p.y);
		m.rotate(angle*(Math.PI/180));
		m.translate(p.x, p.y);
		transform.matrix = m;
	}
	
	public function Pos2PixelPos(pos:Vec3) : Vec3
	{
		// do the nasty iso conversion
		// this is actually an orthogonal projection matrix! (I think)
		return new Vec3(250+(pos.x*36-pos.y*26),50+(pos.y*18+pos.x*9)-(pos.z*37),0);
	}
	
	public function UpdatePos() 
	{ 
		var pos = Pos2PixelPos(Pos);
		x=pos.x;
		y=pos.y;
	}
	
	public function Update(frame:Int, world:World)
	{
	}

}
