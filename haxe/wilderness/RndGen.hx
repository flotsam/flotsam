// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class RndGen
{
	var State:Int;
	
	public function new()
	{
		State=0;
	}
	
	public function Seed(s:Int)
	{
		State=s;
	}
	
	public function RndInt() : Int
	{
		State=cast(10331*State+1203432033,Int);
		return State;
	}
	
	public function RndFlt() : Float
	{
		return RndInt()/Math.pow(2,32)+0.5;
	}
	
}
