// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

import flash.media.Sound;
import flash.net.URLRequest;
import flash.media.SoundLoaderContext;

class SoundPlayer
{
	var Sounds:Array<Sound>;
	
	public function new(sounds:Array<String>)
	{
		Sounds = [];
		
		for (i in 0...sounds.length)
		{
			LoadSound(sounds[i]);
		}

	}
	
	function LoadSound(filename) 
    {
		var sound: Sound = new Sound();
        var req:URLRequest = new URLRequest(filename);
        var context:SoundLoaderContext = new SoundLoaderContext(8000,true);
        //sound.addEventListener(Event.COMPLETE, SoundLoaded);
        sound.load(req,context);
		Sounds.push(sound);
    }
	
	public function Play(id:Int)
	{
		if (id<Sounds.length)
		{
			Sounds[id].play(0);
		}
	}
	
}
