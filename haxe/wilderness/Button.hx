// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class Button extends Sprite 
{
	public var Pos:Vec3;
	
	public function new(pos:Vec3, size:Vec3, bitmapData:BitmapData, f: Dynamic -> Void ) 
	{
		super();
		graphics.beginBitmapFill(bitmapData);
        graphics.drawRect(0,0,size.x,size.y);
		graphics.endFill();
		addEventListener(MouseEvent.MOUSE_DOWN, f);
		x=pos.x;
		y=pos.y;
	}
	
	public function ChangeBitmap(bitmapData:BitmapData)
	{
		graphics.clear();
		graphics.beginBitmapFill(bitmapData);
        graphics.drawRect(0,0,64,64);
		graphics.endFill();
	}
}
