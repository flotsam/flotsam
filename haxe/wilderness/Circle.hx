// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

class Circle
{
	public var Centre:Vec3;
	public var Radius:Float;
	
	public function new(centre:Vec3, radius:Float)
	{
		Centre = centre;
		Radius = radius;
	}	
	
	public function Inside(pos:Vec3) : Bool
	{
		return pos.Sub(Centre).Mag()<Radius;
	}
}
