(define (make-strand)
    (let ((p (with-state
            (hint-unlit)
            ;(translate (vector 0 7 0))
            ;(rotate (vector 0 0 180))
            (build-ribbon 40))))
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                   (let ((a (* i 0.2)))
                   (vmul (vector (sin a) (cos a) 0) (* i 0.2))))
                "p")
            (pdata-copy "p" "pref")
            (pdata-map!
                (lambda (c)
                    (vector 1 1 1))
                "c")
            (pdata-map! 
                (lambda (w) 0.05) "w"))
    p))
                    
(define (animate-strand s i)
    (with-primitive s
        (rotate (vector 0.1 0 0))
        (pdata-set! "p" 0 (vector 0 (gh i) 0))
        (pdata-index-map!
            (lambda (i p pref)
                (if (not (zero? i))
                    (let* ((s (pdata-ref "p" (- i 1)))
                           (b (vmix p s 0.1)))
                        (vector (vx p) (vy b) (vz p)))
                    p))
        "p" "pref")))

(define (make-curtain)
    (map
        (lambda (i)    
            (with-state
                (rotate (vector 0 0 (* i 40)))
                (make-strand)))
        (build-list 15 (lambda (i) i))))

(define (animate-curtain c)
    (for-each 
        (lambda (s i)
            (animate-strand s i))
        c (build-list (length c) (lambda (i) i))))

(clear)
;(translate (vector -9 0 0))
(define p (make-curtain))
(every-frame (animate-curtain p)) 