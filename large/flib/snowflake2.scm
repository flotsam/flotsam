(define (ngon n)
    (let ((p (with-state
                    (hint-none)
                    (hint-unlit)
                    (hint-wire)
                    (build-line (+ n 1)))))
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                    (let ((a (+ (* (/ i n) 2 3.141) 0 #;(/ 3.141 4)))) 
                        (vmul (vector (sin a) (cos a) 0) 0.5)))
                "p")) p))

(define (snowflake l)
    (define (_ l i m)
        (cond    
            ((null? l) '())
            ((list? (car l))
                (let ((p (with-state
                                (rotate (vector 0 0 (* (/ i m) 360)))
                                (translate (vector 0 1 0))
                                (ngon (length (car l))))))
                    (with-state
                        (parent p)
                        (_ (car l) 0 (length (car l))))
                    (_ (cdr l) (+ i 1) m)))
            (else (_ (cdr l) (+ i 1) m))))
    (_ l 0 1))

(define (rndflake d l)
    (cond
        ((zero? l) '(0))
        ((zero? d) '(0))
        (else
            (cons 
                (if (zero? (random 4))
                    (rndflake (- d 1) (+ 2 (random 5)))
                    0)
                (rndflake d (- l 1))))))

(clear)
(show-axis 1)

(snowflake (rndflake 50 4))