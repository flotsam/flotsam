(clear)
(hint-none)
(hint-points)
(hint-anti-alias)
(point-width 8)
(define p (build-particles 500))
(define ss (inexact->exact (round (sqrt 500))))

(with-primitive p
    (pdata-index-map! 
        (lambda (i p)
            (vector (modulo i ss) (quotient i ss) 0))
        "p")
    (pdata-index-map! 
        (lambda (i c)
            (vector 1 1 1))
        "c"))

(blur 0)

(every-frame 
    (with-primitive p
        (pdata-index-map!
            (lambda (i p)
                (let ((t (+ (* 3.5 (time)) i)))
                (vadd p (vmul (vector (sin t) (cos t) 0) 0.01))))
            "p")))