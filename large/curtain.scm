(define (make-strand)
    (let ((p (with-state
            (hint-unlit)
            (translate (vector 0 7 0))
            (rotate (vector 0 0 180))
            (build-ribbon 50))))
        (with-primitive p
            (pdata-index-map!
                (lambda (i p)
                   (vector 0 (* i 0.28) 0))
                "p")
            (pdata-map!
                (lambda (c)
                    (vector 1 1 1))
                "c")
            (pdata-map! 
                (lambda (w) 0.1) "w"))
    p))
                    
(define (animate-strand s c)
    (with-primitive s
        (pdata-set! "p" 0 (vector (gh c) 0 0))
        (pdata-index-map!
            (lambda (i p)
                (if (not (zero? i))
                    (let* ((s (pdata-ref "p" (- i 1)))
                           (b (vmix p s 0.4)))
                        (vector (vx b) (vy p) (vz b)))
                    p))
        "p")))

(define (make-curtain)
    (map
        (lambda (i)    
            (with-state
                (translate (vector i 0 0))
                (make-strand)))
        (build-list 19 (lambda (i) i))))

(define (animate-curtain c)
    (for-each 
        (lambda (s c)
            (animate-strand s c))
        c (build-list (length c) (lambda (c) c))))

(clear)
(set-num-frequency-bins 19)
(translate (vector -9 0 0))
(define p (make-curtain))
(every-frame (animate-curtain p)) 