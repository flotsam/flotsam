(define (make-strand n)
    (let ((p (with-state
            (hint-unlit)    
            (hint-depth-sort)
            (build-ribbon 50))))
        (with-primitive p
            (translate (vector 0 0 (* n 0.1)))
            #;(colour (vector (+ 1 (* 0.5 (sin (* n 5)))) 
                            (+ 1 (* 0.5 (cos (* n 3)))) 1))
            (pdata-index-map!
                (lambda (i p)
                   (vector 0 (* i 0.28) 0))
                "p")
            (pdata-map!
                (lambda (c)
                    (vector (rndf) 1 1))
                "c")
            (pdata-map! 
                (lambda (w) 0.04) "w"))
    p))
                    
(define (animate-strand s c)
    (with-primitive s
        (pdata-set! "p" 0 (vector (gh c) 0 0))
        (pdata-index-map!
            (lambda (i p)
                (if (not (zero? i))
                    (let* ((s (pdata-ref "p" (- i 1)))
                           (b (vmix p s 0.5)))
                        (vector (vx b) (vy p) (vz p)))
                    p))
        "p")))

(define (make-curtain)
    (map
        (lambda (i)    
            (with-state
                (translate (vector (* i 0.4) 0 0))
                (make-strand i)))
        (build-list 20 (lambda (i) i))))

(define (animate-curtain c)
    (for-each 
        (lambda (s c)
            (animate-strand s c))
        c (build-list (length c) (lambda (c) c))))

(clear)


(rotate (vector 0 0 90))
(define p (with-state
    ;(rotate (vector 0 0 -90))
    ;(translate (vector -5 0 0))
    (make-curtain)))


(define p2 (with-state
    ;(rotate (vector 0 0 -90))
    (backfacecull 0)
    (scale (vector 1 -1 1))
    (make-curtain)))

(set-num-frequency-bins 20)
(gain 10)
(every-frame (begin
    (animate-curtain p)
    (animate-curtain p2)))

 