;(debug-enable 'debug)
;(debug-enable 'backtrace)
;(read-enable 'positions)

(blur 0)
(define brightness 1)
(gain 100)
(define np 200)

(define (rndvec)
    (vsub (vector (flxrnd)(flxrnd)(flxrnd)) (vector 0.5 0.5 0.5)))

(define (init-particles speed size col)
    (define (loop n)
        (pdata-set "s" n (vector size size size))
        (pdata-set "p" n (vector 0 0 0))
        (pdata-set "c" n (vmul (vector (* 0.1 (flxrnd)) (flxrnd) (flxrnd)) col))
        (pdata-set "vel" n (vmul (rndvec) speed))
        (if (zero? n)
            0
            (loop (- n 1))))

    (pdata-add "vel" "v")
    (loop (pdata-size)))

(define (animate-particles)
    (define (loop n)
        (if (zero? n)
            (pdata-set "vel" n (vmul (vadd (pdata-get "vel" n) 
                (vmul (vadd (vsub (pdata-get "p" (- (pdata-size) 1)) (pdata-get "p" n)) 
                    (vector (gh n) (gh (+ n 1)) (gh (+ n 2))))
                    0.001)) 0.95))
        
            (pdata-set "vel" n (vmul (vadd (pdata-get "vel" n) 
                (vmul (vadd (vsub (pdata-get "p" (- n 1)) (pdata-get "p" n)) 
                    (vector (gh n) (gh (+ n 10)) (gh (+ n 20))))
                    0.001)) 0.95)))

        (if (zero? n)
            0
            (loop (- n 1))))

    (loop (pdata-size))
    (pdata-op "+" "p" "vel"))


(define (make-subparticles p)
    (define (loop n l)
        (if (zero? n)
            l
            (loop (- n 1) (cons (build-particles 80) l))))

    (define (init l)
        (grab (car l))
        (init-particles 0.01 0.2 brightness)
        (ungrab)
        (if (null? (cdr l))
            0
            (init (cdr l))))

    (push)
    (texture (load-texture "cloud.png"))
    (grab p)
    (let ((subparts (loop (pdata-size) '())))
    (ungrab)
    (pop)
    (init subparts)
    subparts))
    

(define (animate-subparticles subparticles p)
    (define (anim l n)
        (grab p)
        (let ((pos (pdata-get "p" n)))
        (ungrab)

        (grab (car l))
        (cond 
            ((< 0.01 (flxrnd))
                (let ((chosen (random (pdata-size)))) 
                (pdata-set "p" chosen pos)
                (pdata-set "c" chosen (vmul (vector (flxrnd) (flxrnd) (flxrnd)) brightness))))))

        
        (pdata-op "+" "p" "vel")
        (pdata-op "*" "c" 0.95)
        (ungrab)
        (if (null? (cdr l))
            0
            (anim (cdr l) (+ n 1))))

    (anim subparticles 0))


(clear)
(set-num-frequency-bins np)
(hint-ignore-depth)
(blend-mode 'src-alpha 'one)
(texture (load-texture "star.png"))
(define p (build-particles np))

(grab p)
(init-particles 0.04 1 brightness)
(ungrab)

(define sp (make-subparticles p))

(push)
;(hint-origin)
(define camera (build-locator))
(pop)
(lock-camera camera)



(define (render)
    (grab p)
    (animate-particles)
    (let ((pos (pdata-get "p" 10)))
    (ungrab)

    (grab camera)
    (identity)
    (translate pos)
    (ungrab))

    (animate-subparticles sp p))

(every-frame (render))
