(define (get-line-from-mouse)
    (let* ((ndcpos (vector (* (- (/ (mouse-x) (vx (get-screen-size))) 0.5) 1.25) ; 2
                    (* (- (- (/ (mouse-y) (vy (get-screen-size))) 0.5)) 1) -1)) ;1.5
            (scrpos2 (vtransform (vmul ndcpos 500) (minverse (get-camera-transform))))
            (scrpos (vtransform ndcpos (minverse (get-camera-transform)))))
        (list scrpos scrpos2)))

(define (pos-from-mouse line)
    (let ((i (geo/line-intersect (car line) (cadr line))))
        (cond ((not (null? i))
                (cdr (assoc "p" (car i))))
            (else #f))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(define t 0)

(define (vg-npush-particles)
    (set! t (+ t 0.04))
    (pdata-map! 
        (lambda (p)
            (let* ((pp (vmul p 1))
                    (v (vector (- (noise (vx pp) (vy pp) (time)) 0.5)
                            (- (noise (vx pp) (+ (vy pp) 112.3) t) 0.5) 0)))
                (vadd (vadd p (vmul v 0.1))
                    (vmul (vector (crndf) (crndf) 0) 0.01))))
        "p"))

(define (cirndvec)
    (let ((o (srndvec)))
        (vector (vx o) (vy o) 0)))

(define (puff pos col size np)
    (for ((i (in-range 0 np)))
        (let ((c (random (pdata-size))))
            (pdata-set! "p" c (vadd (vmul (cirndvec) size) pos))
            (pdata-set! "c" c (vadd col (vmul (grndvec) 0.2))))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(clear)
(set-camera-transform (mtranslate (vector 0 -5 -7)))
(set-fov 50 1 1000)
(blend-mode 'src-alpha 'one)
(hint-ignore-depth)

(define p (with-state
        (texture (load-texture "flake.png"))
        (build-particles 2000)))

(with-primitive p
    (opacity 0.5)
    (hint-ignore-depth)
    (pdata-map!
        (lambda (p)
            (vmul (vector (crndf) (crndf) 0) 100))
        "p")
    (pdata-map!
        (lambda (c)
            (vector 1 1 1 0.5))
        "c")
    (pdata-map!
        (lambda (c)
            (let ((s (* 0.1 (grndf))))
                (vector s s 1)))
        "s"))


(define (update)
    (let ((line (get-line-from-mouse))
          (hit #f))
        (when (mouse-button 1) #;(key-pressed " ")
              (let ((pos (vadd (vmul (car line) 0.99) (vmul (cadr line) 0.01))))
                (set! hit #t)
                (with-primitive p (puff pos (vector 1 1 1) 0.1 10))))

        (with-state
         (translate (vadd (vmul (car line) 0.99) (vmul (cadr line) 0.01)))
         (if hit (scale 0.04) (scale 0.02))
         (hint-unlit)
         (colour (vector 0.7 0.7 0))
         (draw-sphere))
        (with-primitive p (vg-npush-particles))))

(every-frame (update))


