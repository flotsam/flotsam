
(define (hermite s p1 p2 t1 t2)
    ; the bernstein polynomials
    (define (h1 s)
        (+ (- (* 2 (expt s 3))
                (* 3 (expt s 2))) 1))
    
    (define (h2 s)
        (+ (* -2 (expt s 3))
            (* 3 (expt s 2))))
    
    (define (h3 s)
        (+ (- (expt s 3) (* 2 (expt s 2))) s))
    
    (define (h4 s)
        (- (expt s 3) (expt s 2)))
    
    (vadd
        (vadd 
            (vmul p1 (h1 s))
            (vmul p2 (h2 s)))
        (vadd
            (vmul t1 (h3 s))
            (vmul t2 (h4 s)))))

(define (hermite-tangent t p1 p2 t1 t2)
    (let ((p (hermite t p1 p2 t1 t2)))
        (list p (vsub (hermite (- t 0.01) p1 p2 t1 t2) p))))

(show-axis 1)

(define (animate)
    (let ((h (hermite-tangent (fmod (* 0.1 (time)) 1) (vector 0 0 0) (vector 1 0 0) 
                    (vector 0 10 0) (vector 0 10 0))))
        (with-state
            (translate (car h))
            (concat (maim (vector 0 0 1) (vnormalise (cadr h))))
            (scale 0.1)
            (draw-cube))))

(every-frame (animate))
