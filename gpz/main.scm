(require "pg.ss")
(require "gp.ss")
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; convert notes on a major scale to numbers
(define (char->num ch)
   (cond
       ((eq? ch 'G) 0)  
       ((eq? ch 'A) 1)  
       ((eq? ch 'B) 2)  
       ((eq? ch 'C) 3)  
       ((eq? ch 'D) 4)  
       ((eq? ch 'E) 5)  
       ((eq? ch 'F) 6)))

(define target (map char->num
                    (let* ((f (open-input-file "patterns/mary.scm"))
                           (r (trunc (read f) 13)))
                      (close-input-port f) r)))

(define target '(50 50 0 0 50 50 0 0))

(define p (create-random-population 1000 dict 8))
(display "created first population")(newline)

(display "-------------------")(newline)

(for ((n (in-range 0 1000)))
     (set! p (iterate-population p dict 8 target 
                                 0.2 pattern-fitness))
     (printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~n")
     (pretty-print-program (car p))(newline)     

     (let* ((pattern (build-pattern (length target) (car p)))
            (score (diff pattern target)))
       (printf "generation ~a~n" n)
       (printf "target: ~a~n" target)
       (printf "pattern: ~a~nscore: ~a~n" pattern score)
       (let ((f (open-output-file "log/run.txt" #:exists 'append)))
         (display (list score pattern target (car p)) f)(newline f)
         (close-output-port f)))
     #;(for-each
      (lambda (i)
        (display (build-pattern (length target) i))(newline))
      p))


