(require fluxus-017/fluxa)

(define (num->char n)
   (list-ref (list 'G 'A 'B 'C 'D 'E 'F) n))

(define (char->note ch)
    (cond
       ((eq? ch 'G) 392.00)  ; 0
       ((eq? ch 'A) 440)     ; 1
       ((eq? ch 'B) 493.88)  ; 2
       ((eq? ch 'C) 523.25)  ; 3
       ((eq? ch 'D) 587.33)  ; 4
       ((eq? ch 'E) 659.26)  ; 5
       ((eq? ch 'F) 698.46)  ; 6
       (else 0)))

#;(define (get-song-note filename n)
    (let* ((f (open-input-file filename))
            (notes (read f))
            (r (list-ref notes n)))
        (close-input-port f)
      (display r)(newline)
        (char->note r)))

(define (get-song-note filename n)
    (char->note (num->char
     (list-ref (list 2 1 0 1 2 2 2 1 1 1 2 4 4) n))))


(seq
  (lambda (time clock)
     (play time (mul (adsr 0 0.1 0.1 2) 
                   (sine (get-song-note "mary.txt" 
                               (modulo clock 13)))))
       0.3))
