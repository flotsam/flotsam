// Copyright (C) 2011 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <vector>
using namespace std;

#include "machine.h"
#include "thread.h"

void split(const string& str,
           vector<string>& tokens,
           const string& delimiters = " ")
{
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos     = str.find_first_of(delimiters, lastPos);
    
    while (string::npos != pos || string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
}

int main(int argc, char *argv[]) 
{
    machine m(NULL);
    m.add_thread(0);
    for (u32 i=0; i<1024; i++) m.m_output[i]=0;
    m.m_outpos=0;
    
    string inp;
    cin>>inp;
    vector<string> splitted;
    split(inp,splitted,",");
    
    u32 count=0;
    for(vector<string>::iterator i=splitted.begin();
        i!=splitted.end(); ++i)
    {
        m.poke(count++,atof(i->c_str()));
        //cerr<<"poking "<<count<<" with "<<atof(i->c_str())<<endl;
    }
    
    u32 cycles=atof(argv[1]);
    for(u32 i=0; i<cycles; i++)
    {
        m.run();
    }
    
    for(u32 i=0; i<m.m_outpos; i++)
    {
        cout<<(int)m.m_output[i]<<" ";
    }
	return 0;
}

