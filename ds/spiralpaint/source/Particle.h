#include <nds.h>
#include <vector>
#include "dada.h"

using namespace std;
using namespace Fluxus;


class Particle
{
public:
	Particle(const dVector &pos, int id);
	
	void Update();
	void Render();
	void SetVel(const dVector &s) { m_Vel=s; }
	void SetPos(const dVector &s) { m_Pos=s; }
	void SetTurn(float s) { m_Turn=s; }
	void SetAge(int s) { m_Age=s; }
	void SetColour(const dColour &s) { m_Colour=s; }
	void SetWidth(float s) { m_Width=s; }

private:
	int m_ID;
	dVector m_Pos;
	dVector m_Vel;
	int m_Age;
	float m_Turn;
	float m_Rand;
	dColour m_Colour;
	float m_Width;
};

class ParticleCloud
{
public:
	ParticleCloud(int count);
	
	void Update();
	void Render();
	void ResetTo(const dVector &pos, const dVector &dir);
	
private:
	vector<Particle> m_Particles;

};

