#include <nds.h>
#include "dada.h"
#include "Drawing.h"

using namespace std;
using namespace Fluxus;

void ClearScreen()
{
	for(int i=0; i<256*192; i++) VRAM_A[i] = RGB15(31,31,31);
}

void DrawRect(int x, int y, int width, int height, dColour col)
{	
	for (int xp=x; xp<x+width; xp++)
	{
		for (int yp=y; yp<y+height; yp++)
		{
			if (xp>0 && xp<256 && yp>0 && yp<192)
			{
				VRAM_A[xp + yp * 256] = 
					RGB15((int)(col.r*31),(int)(col.g*31),(int)(col.b*31));
			}
		}
	}
}

