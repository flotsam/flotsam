#include <nds.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include "dada.h"
#include "Particle.h"
#include "Drawing.h"

using namespace std;
using namespace Fluxus;

/////////////////////////////////////////////////////////

int main(void) 
{
	touchPosition touch;

	videoSetMode(MODE_FB0);
	vramSetBankA(VRAM_A_LCD);
        
	lcdMainOnBottom();
	ParticleCloud p(20);
	dVector pos;
	bool down=false;
	ClearScreen();	

	while(1)
	{
		scanKeys();

		if(keysHeld() & KEY_A) ClearScreen();	
 
		if(keysHeld() & KEY_TOUCH)
		{				
			touch=touchReadXY();
			if (!down)
			{
				down=true;
				pos.x=touch.px;			
				pos.y=touch.py;
			}
		}
		else
		{
			if (down)
			{
				p.ResetTo(pos,dVector(touch.px,touch.py,0)-pos);
				down=false;
			}
		}
		
		p.Render();
		p.Update();
	}
		
	return 0;
}
