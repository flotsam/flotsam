#include <nds.h>
#include <vector>
#include "dada.h"
#include "Particle.h"
#include "Drawing.h"

using namespace std;
using namespace Fluxus;

Particle::Particle(const dVector &pos, int id) : 
m_ID(id),
m_Pos(pos),
m_Age(0),
m_Rand(RandFloat()),
m_Colour(1,1,1),
m_Width(10)
{
}

void Particle::Update()
{
	m_Vel=dVector(sin(m_Age*m_Age*m_Turn*0.0001*m_Rand+m_ID),
	              cos(m_Age*m_Age*m_Turn*0.0001*m_Rand+m_ID),0);
	m_Pos+=m_Vel;
	m_Age++;
	m_Width*=0.99;
	//m_Colour*=0.999;
}

void Particle::Render()
{
	if (m_Width>1) DrawRect((int)m_Pos.x,(int)m_Pos.y,m_Width,m_Width,m_Colour*(m_Rand+0.7));
}

/////////////////////////////////////////////////////////

ParticleCloud::ParticleCloud(int count)
{
	for (int i=0; i<count; i++)
	{
		Particle newp(dVector(0,0,0),i);
		newp.SetVel(dVector(RandFloat()-0.5,RandFloat()-0.5,0)*0.03);
		m_Particles.push_back(newp);
	}
}

void ParticleCloud::ResetTo(const dVector &pos, const dVector &dir)
{
	float turn = dir.mag();
	dColour col(RandFloat(),RandFloat(),RandFloat());
	for (vector<Particle>::iterator i=m_Particles.begin();
		i!=m_Particles.end(); ++i)
	{
		i->SetPos(pos);
		i->SetTurn(turn);
		i->SetAge(0);
		i->SetColour(col);
		i->SetWidth(10);
	}
}
	
void ParticleCloud::Update()
{
	for (vector<Particle>::iterator i=m_Particles.begin();
		i!=m_Particles.end(); ++i)
	{
		i->Update();
	}
}

void ParticleCloud::Render()
{
	for (vector<Particle>::iterator i=m_Particles.begin();
		i!=m_Particles.end(); ++i)
	{
		i->Render();
	}
}
