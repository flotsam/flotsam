(include "vec.sc")

(define (super-sample samples x y width height thunk)
  (let sloop ((s samples) (v (vector 0 0 0)))
    (cond 
      ((zero? s) v)
      (else (sloop (- s 1) (vadd v (vdiv 
                                    (thunk (+ x (rndf)) (+ y (rndf)) width height)
                                    samples)))))))

