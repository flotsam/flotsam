(include "random.sc")

(define (sq x) (* x x))

(define (vx v) (vector-ref v 0))
(define (vy v) (vector-ref v 1))
(define (vz v) (vector-ref v 2))
(define (vw v) (vector-ref v 3))

(define (fabs a)
  (if (< a 0) (- a) a))

(define (feq? a b)
  (< (fabs (- a b)) 0.000001))

(define (clamp v min max)
  (if (< v min) min
      (if (> v max) max
          v)))

(define (vclamp v min max)
  (vector 
   (clamp (vx v) min max)
   (clamp (vy v) min max)
   (clamp (vz v) min max)))
