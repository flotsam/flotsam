(define (image w h)
	(define (_ n l)
		(cond 
			((zero? n) l)
			(else		
				(_ (- n 1) (cons (make-vector w 0) l)))))
	(_ h '()))

(define (rgb-image w h)
	(image (* w 3) h))
	
(define (image-w image) (/ (vector-length (list-ref image 0)) 3))
(define (image-h image) (length image))
	
(define (rgb-image-ref image x y)
	(let ((x (inexact->exact (floor x)))
		  (y (inexact->exact (floor y)))) ; todo: lerp
	(cond ; clip
		((and (> x 0) (< x (image-w image))
			  (> y 0) (< y (image-h image)))
				(vector
						(vector-ref (list-ref image y) (* x 3))
						(vector-ref (list-ref image y) (+ (* x 3) 1))
						(vector-ref (list-ref image y) (+ (* x 3) 2))))
		(else (vector 0 0 0)))))

(define (unsafe-rgb-image-ref image x y)
	(let ((x (inexact->exact (floor x)))
		  (y (inexact->exact (floor y)))) ; todo: lerp
		(vector
			(vector-ref (list-ref image y) (* x 3))
			(vector-ref (list-ref image y) (+ (* x 3) 1))
			(vector-ref (list-ref image y) (+ (* x 3) 2)))))

(define (rgb-image-set! image x y v)
	(let ((x (inexact->exact (floor x)))
		  (y (inexact->exact (floor y))))
	(cond ; clip
		((and (> x 0) (< x (image-w image))
			  (> y 0) (< y (image-h image)))
				(vector-set! (list-ref image y) (* x 3) (vx v))
				(vector-set! (list-ref image y) (+ (* x 3) 1) (vy v))
				(vector-set! (list-ref image y) (+ (* x 3) 2) (vz v))))))

(define (unsafe-rgb-image-set! image x y v)
	(let ((x (inexact->exact (floor x)))
		  (y (inexact->exact (floor y))))
		(vector-set! (list-ref image y) (* x 3) (vx v))
		(vector-set! (list-ref image y) (+ (* x 3) 1) (vy v))
		(vector-set! (list-ref image y) (+ (* x 3) 2) (vz v))))
		
(define (save-ppm image filename samples)
	(write-ppm filename (image-w image) (image-h image) samples 
		(lambda (x y w h)
			(rgb-image-ref image x y))))


