; Copyright (C) 2010 Dave Griffiths : See LICENCE file

("out.ppm" (640 400)
	((point-light #(3 5 -10) #(1 1 1))
	 (point-light #(10 10 0) #(0.5 0.5 0.5))
	 (point-light #(-10 10 0) #(0 0 0.5))
	)
	(
	 (shader reflect #(0.3 0.6 0.8) 0.5 0.01)
	 (sphere #(0 1 8) 3)
	 (shader reflect #(1 1 0) 0.5 0.01)
	 (sphere #(-2 -1 4) 1)
	 (shader reflect #(1 0 0) 0.5 0.01)
	 (sphere #(5 0 6) 2)
	 (shader reflect #(1 0 1) 0.5 0.01)
	 (sphere #(-3 -1.5 3) 0.5)
	 
	 (shader blinn #(0 0 1) 0.02)
	 (sphere #(-0.5 -1.75 2.7) 0.25)
	 (shader blinn #(1 0 0) 0.02)
	 (sphere #(-1.5 -1.75 2.9) 0.25)
	 
	 (shader lambert #(0.3 0.7 0.5))
	 (plane #(0 1 0) 2)
	 	 
	 ;(shader blinn #(1 0.5 0.2) 0.02)
	 ;(load-obj "mushroom.obj")
	 ;(translate #(-1.5 0 0))
	 ;(scale #(0.5 0.5 0.5))
	 
	 (translate #(1 -1.5 3))
	 (scale #(0.5 0.5 0.5))
	 (rotate #(0 45 0))
	 (shader blinn #(1 0.5 0.2) 0.02)
	 (load-obj "meshes/cube.obj")
	 (translate #(1 0 3))
	 (shader blinn #(1 1 0.2) 0.02)
	 (load-obj "meshes/cube.obj")
	 
	 (translate #(-1 1.8 -2))
	 (rotate #(0 20 0))
	 (scale #(0.75 0.75 0.75))
	 (shader blinn #(1 0.2 0.2) 0.02)
	 (load-obj "meshes/cube.obj")
	 

	 ;(shader constant #(1 1 0))
	 ;(translate #(0 0 1))
	 ;(vox #(10 10 10)
	 ;	((sphere #(0 0 0) 1)))
	 
	 ))

