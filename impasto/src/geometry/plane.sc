; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../maths/vec.sc")
(include "line.sc")
(include "../maths/intersection.sc")

(define (plane normal dist)
	(list 'plane normal dist))

(define (plane? p) (eq? (list-ref p 0) 'plane))  
(define (plane-normal p) (list-ref p 1))  
(define (plane-dist p) (list-ref p 2))  
  
(define (plane/line-intersect? p l)
  (plane/line-intersect p l))

(define (plane/line-intersect p l)
  (let ((n.l (vdot (plane-normal p) (line-vec l))))
    (cond ((zero? n.l) #f)
          (else
           (let ((t (/ (- (+ (vdot (line-start l) (plane-normal p)) (plane-dist p))) n.l)))
             (if (or (< t 0) (> t 1)) #f
                 (intersection t
                   (vadd (line-start l) (vmul (line-vec l) t)) (plane-normal p))))))))
