; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../maths/vec.sc")

(define (line a b) (list a b))
(define (line-start l) (car l))
(define (line-end l) (cadr l))

(define (line-vec l)
  (vsub (line-end l) (line-start l)))

(define (line-closest-point l point)
  (let* ((v (line-vec l))
         (l2 (vsub point (line-start l)))
         (d (vmag v))
         (t (vdot (vdiv v d) (vdiv l2 d))))
    (cond 
      ((<= t 0) (line-start l))
      ((> t 1) (line-end l))
      (else (vadd (line-start l) (vmul v t))))))

(define (line-closest-point l point)
  (let* ((v (line-vec l))
         (l2 (vsub point (line-start l)))
         (d (vmag v))
         (t (vdot (vdiv v d) (vdiv l2 d))))
    (cond 
      ((<= t 0) (line-start l))
      ((> t 1) (line-end l))
      (else (vadd (line-start l) (vmul v t))))))

