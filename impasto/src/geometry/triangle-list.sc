; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../maths/vec.sc")
(include "line.sc")
(include "triangle.sc")
(include "../maths/intersection.sc")

(define (triangle-list l) (list 'triangle-list l))
(define (triangle-list? t) (eq? (car t) 'triangle-list))
(define (triangle-list-triangles tl) (list-ref tl 1))

(define (triangle-list-random n pos scale) 
  (triangle-list 
    (build-list n (lambda (_) (triangle (vadd pos (vmul (srndvec) scale))
									    (vadd pos (vmul (srndvec) scale))
										(vadd pos (vmul (srndvec) scale)))))))

(define (triangle-list/line-intersect? ts l)
  (if (triangle-list/line-intersect ts l) #t #f))
	    
(define (triangle-list/line-intersect ts l)
  (foldl
  	(lambda (tri r)
      (let ((i (triangle/line-intersect tri l)))
	    (if (not r) 
		  i
	  	 (if (and i (< (intersection-t i) (intersection-t r))) i r))))
	#f
	(triangle-list-triangles ts)))
	
