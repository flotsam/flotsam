; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "random.sc")

(define (sq x) (* x x))

(define (vx v) (vector-ref v 0))
(define (vy v) (vector-ref v 1))
(define (vz v) (vector-ref v 2))
(define (vw v) (vector-ref v 3))

(define (vr v) (vector-ref v 0))
(define (vg v) (vector-ref v 1))
(define (vb v) (vector-ref v 2))
(define (va v) (vector-ref v 3))

(define (fabs a)
  (if (< a 0) (- a) a))

(define (feq? a b)
  (< (fabs (- a b)) 0.000001))

(define (clamp v min max)
  (if (< v min) min
      (if (> v max) max
          v)))

(define (vclamp v min max)
  (vector 
   (clamp (vx v) min max)
   (clamp (vy v) min max)
   (clamp (vz v) min max)))
