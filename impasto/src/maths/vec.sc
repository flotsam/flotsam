; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (v3->v4 v w)
  (vector (vx v) (vy v) (vz v) w))

(define (v4->v3 v)
  (vector (vx v) (vy v) (vz v)))

(define (veq? a b)
  (and (feq? (vx a) (vx b))
       (feq? (vy a) (vy b))
       (feq? (vz a) (vz b))))

(define (vprint a)
  (display a) (newline))

(define (vadd a b)
  (vector (+ (vx a) (vx b))
           (+ (vy a) (vy b))
           (+ (vz a) (vz b))))

(define (vsub a b)
  (vector (- (vx a) (vx b))
           (- (vy a) (vy b))
           (- (vz a) (vz b))))

(define (vmul a b)
  (vector (* (vx a) b)
           (* (vy a) b)
           (* (vz a) b)))

(define (vdiv a b)
  (vector (/ (vx a) b)
           (/ (vy a) b)
           (/ (vz a) b)))

(define (vneg a)
  (vector (- (vx a))
           (- (vy a))
           (- (vz a))))

(define (vdot a b)
  (+ (* (vx a) (vx b))
     (* (vy a) (vy b))
     (* (vz a) (vz b))))

(define (vcross a b)
  (vector (- (* (vy a) (vz b)) (* (vz a) (vy b)))
           (- (* (vz a) (vx b)) (* (vx a) (vz b)))
           (- (* (vx a) (vy b)) (* (vy a) (vx b)))))

(define (vdist a b)
  (sqrt (+ (sq (- (vx b) (vx a)))
           (sq (- (vy b) (vy a)))
           (sq (- (vz b) (vz a))))))
		   
(define (vdist-sq a b)
  (+ (sq (- (vx b) (vx a)))
     (sq (- (vy b) (vy a)))
     (sq (- (vz b) (vz a)))))

(define (vsq a)
  (+ (sq (vx a)) (sq (vy a)) (sq (vz a))))

(define (vmag a)
  (sqrt (+ (sq (vx a)) (sq (vy a)) (sq (vz a)))))

(define (vnorm a)
  (let ((mag (vmag a)))
    (vector (/ (vx a) mag)
             (/ (vy a) mag)
             (/ (vz a) mag))))

(define (vreflect a n)
    (vsub a (vmul n (* (vdot a n) 2))))
