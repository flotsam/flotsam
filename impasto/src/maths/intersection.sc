; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (intersection t point normal)
	(list t point normal))

(define (intersection-t intersection)
	(list-ref intersection 0))

(define (intersection-point intersection)
	(list-ref intersection 1))

(define (intersection-normal intersection)
	(list-ref intersection 2))

