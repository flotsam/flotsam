; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../scene/object.sc")

(define (point-light pos col)
	(list 'point-light pos col))

(define (point-light? l) (eq? (list-ref l 0) 'point-light))
(define (point-light-position l) (list-ref l 1))
(define (point-light-colour l) (list-ref l 2))
   
(define (do-shadow? object objects l)
  (foldl
   (lambda (ob int)
     (if (and (not (eq? object ob)) (not int))
         (object/line-intersect? ob l) int))
   #f
   objects))
  
(define (point-light-shadow? l object objects pos)
  (do-shadow? object objects (line pos (point-light-position l))))

(define (point-light-calc light object objects pos norm)
  (let ((l (vnorm (vsub (point-light-position light) pos))))
    (vmul (point-light-colour light) (vdot l norm))))
