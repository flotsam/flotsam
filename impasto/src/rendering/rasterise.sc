; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../maths/vec.sc")

(define (super-sample samples x y width height thunk)
  (let sloop ((s samples) (v (vector 0 0 0)))
    (cond 
      ((zero? s) v)
      (else (sloop (- s 1) (vadd v (vdiv 
                                    (thunk (+ x (rndf)) (+ y (rndf)) width height)
                                    samples)))))))

; need to rewrite...
(define (spat-voxels scene)
  (display "rendering...")(newline)
  
  (let ((vm (mtranslate (vector (/ (scene-w scene) 2) (/ (scene-h scene) 2) 0)))
        (img (rgb-image (scene-w scene) (scene-h scene)))
        (sprite (make-sprite 0 0 "tex/splat-small.ppm")))

    (for-each 
     (lambda (vox)
       (let ((g (vox-gradient vox))
	   		 (m (mmul vm (vox-mat vox))))
         (vox-for-each! 6 vox
                        (lambda (pos s)
                          (let ((vpos (vtransform (v3->v4 (vmul pos (* 20 0.15)) 1) m))
                                (n (list-ref g (tindex (vox-w vox) (vox-h vox) (vox-d vox) (vx pos) (vy pos) (vz pos)))))
                            
                            ;(let ((col (vector (abs (vx n)) (abs (vy n)) (abs (vz n)))))
                            ;	(sprite-blit-add-col! sprite col (vx vpos) (vy vpos) img))
                            
                            (let* ((l.n1 (vdot (vector -1 -1 0) n))
                                   (l.n2 (vdot (vector 0.5 -0.4 0) n))
                                   (col (vadd (vmul (vector 0.2 0.3 0.7) (max 0 l.n1))
                                              (vmul (vector 0.2 0.9 0.4) (max 0 l.n2)))))
                              (sprite-blit-add-col! sprite (vmul col 0.5) (vx vpos) (vy vpos) img))
                            
                            
                            )))))
       scene)
     (display "finished rendering...")(newline)
    
     (save-ppm img (scene-filename scene) 1)))
