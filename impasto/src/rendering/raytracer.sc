; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../geometry/sphere.sc")
(include "../scene/object.sc")
(include "shading.sc")

(define max-depth 16)
(define back-plane 20000.0)

(define (render-fragment ray out i scene object depth)
  (if (and i (< (intersection-t i) (vw out)) ; depth test against position on ray
             (> (intersection-t i) 0))
     (begin
      (v3->v4 
       (if (< depth max-depth)
	     (vclamp ((object-shader object) ray scene object (intersection-point i) (intersection-normal i) (+ depth 1)) 0 1)
		 (vector 0 0 0))
       (intersection-t i)))
      out))

(define (trace-scene ray scene depth)
	(foldl
      (lambda (object out)
		(render-fragment ray out (object/line-intersect object ray) scene object depth))
      (vector 0.7 0.8 1 back-plane)
      (scene-objects scene)))

(define (render x y w h scene)
  (let* ((x (* (* 2 (- 0.5 (/ x w))) (/ w h)))
         (y (* 2 (- 0.5 (/ y h))))
         (image-plane 1.0)
         (ray (line (vector 0 0 0) (vmul (vector x y image-plane) back-plane))))
    (v4->v3 (trace-scene ray scene 0))))
	  
(define (render-scene scene)
	(let ((img (rgb-image (scene-w scene) (scene-h scene))))
		; render stuff
		(let loopx ((y 0))
      		(let loopy ((x 0))			
				 (rgb-image-set! img x y (render x y (scene-w scene) (scene-h scene) scene))
				(cond ((< x (- (scene-w scene) 1)) (loopy (+ x 1)))))
			(cond ((< y (- (scene-h scene) 1)) (loopx (+ y 1)))))
		(save-ppm img (scene-filename scene) 1)))
