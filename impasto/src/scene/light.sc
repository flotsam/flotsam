; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../lights/point-light.sc")

(define (light l object objects pos norm)
	(cond 
		((point-light? l) (point-light-calc l object objects pos norm))
		(else (display "light not recognised by light ")(
			display l)(newline)
			#f)))

(define (shadow? l object objects pos)
   (cond 
     ((point-light? l) (point-light-shadow? l object objects pos))
		(else (display "shadow? not recognised by light ")(
			display l)(newline)
			#f)))
