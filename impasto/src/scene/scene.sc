; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../base/list.sc")
(include "../voxel/vox.sc")
(include "../rendering/image.sc")
(include "../rendering/sprite.sc")
(include "../lights/point-light.sc")
(include "../io/obj.sc")

(define (make-scene filename dim lights objects)
	(list filename dim lights objects))
	
(define (scene-filename scene) (list-ref scene 0))
(define (scene-w scene)	(car (list-ref scene 1)))
(define (scene-h scene) (cadr (list-ref scene 1)))
(define (scene-lights scene) (list-ref scene 2))
(define (scene-objects scene) (list-ref scene 3))
	
(define (build-scene desc)
  (make-scene
  	(list-ref desc 0)
  	(list-ref desc 1)
    (foldl
		(lambda (i r)
			(display i)(newline)
			 (cond
       			((eq? (car i) 'point-light) 
					(cons (point-light (list-ref i 1) (list-ref i 2)) r))
				))
		'()
		(list-ref desc 2))
  (let ((m (mat))
        (shader (make-lambert-shader (vector 1 1 1))))
  (foldl
   (lambda (i r)
     (display i)(newline)
     (cond       
       ((eq? (car i) 'rotate) (set! m (mmul m (mrotate (list-ref i 1)))) r)
       ((eq? (car i) 'scale) (set! m (mmul m (mscale (list-ref i 1)))) r)
       ((eq? (car i) 'translate) (set! m (mmul m (mtranslate (list-ref i 1)))) r)
       ((eq? (car i) 'shader) 
	   	 (cond 
	   		((eq? (list-ref i 1) 'lambert) (set! shader (make-lambert-shader (list-ref i 2))) r)
	   		((eq? (list-ref i 1) 'constant) (set! shader (make-constant-shader (list-ref i 2))) r)
	   		((eq? (list-ref i 1) 'blinn) (set! shader (make-blinn-shader (list-ref i 2) (list-ref i 3))) r)
	   		((eq? (list-ref i 1) 'perfect-mirror) (set! shader (make-perfect-mirror-shader (list-ref i 2))) r)
	   		((eq? (list-ref i 1) 'mirror) (set! shader (make-mirror-shader (list-ref i 2))) r)
	   		((eq? (list-ref i 1) 'reflect) (set! shader (make-reflect-shader (list-ref i 2) (list-ref i 3) (list-ref i 4))) r)
			(else (display "unknown shader:")(display (list-ref i 1)))) r)
       ((eq? (car i) 'sphere) (cons (object (sphere (list-ref i 1) (list-ref i 2)) shader) r))
       ((eq? (car i) 'plane) (cons (object (plane (list-ref i 1) (list-ref i 2)) shader) r))
       ((eq? (car i) 'triangle) (cons (object (triangle (list-ref i 1) (list-ref i 2) (list-ref i 3)) shader) r))
       ((eq? (car i) 'random-triangles) (cons (object (triangle-list-random (list-ref i 1) (list-ref i 2) (list-ref i 3)) shader) r))
       ((eq? (car i) 'load-obj) (cons (object (load-obj (list-ref i 1) (mat-copy m)) shader) r))
       ;((eq? (car i) 'raw) (cons (vox-read-raw (list-ref i 1) (list-ref i 2) (mat-copy m)) r))
       ((eq? (car i) 'vox)
        (let ((v (vox (cadr i) m)))
          (for-each
           (lambda (op)
             (cond
               ; (sphere centre radius)
               ((eq? (car op) 'sphere) (vox-sphere! v (list-ref op 1) (list-ref op 2)))
               ; (influence centre strength)
               ((eq? (car op) 'influence) (vox-influence! v (list-ref op 1) (list-ref op 2)))
               ; (threshold thresh)
               ((eq? (car op) 'threshold) (vox-threshold! v (list-ref op 1)))))
           (list-ref i 2))
          (cons (object v shader) r)))
	   )) '() (list-ref desc 3)))))

(define (load-scene filename)
  (let ((f (open-input-file filename)))
    (let ((r (build-scene (read f))))
      (close-input-port f)
      r)))

(define (tindex w h d x y z)	
  (+ (+ (* y w) x) (* z (* w h))))


  
  
