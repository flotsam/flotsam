; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../geometry/sphere.sc")
(include "../geometry/plane.sc")
(include "../geometry/triangle.sc")
(include "../geometry/triangle-list.sc")

(define (object geom shader)
  (list geom shader))
  
(define (object-geom o) (list-ref o 0))
(define (object-shader o) (list-ref o 1))

; dispatch based on object type
(define (object/line-intersect object line)
	  (cond 
		((sphere? (object-geom object)) (sphere/line-intersect (object-geom object) line))
		((plane? (object-geom object)) (plane/line-intersect (object-geom object) line))
		((voxels? (object-geom object)) (voxel/line-intersect (object-geom object) line))
		((triangle? (object-geom object)) (triangle/line-intersect (object-geom object) line))
		((triangle-list? (object-geom object)) (triangle-list/line-intersect (object-geom object) line))
		(else (display "object not recognised by intersect ")(display object)(newline) #f)))

(define (object/line-intersect? object line)
	  (cond 
		((sphere? (object-geom object)) (sphere/line-intersect? (object-geom object) line))
		((plane? (object-geom object)) (plane/line-intersect? (object-geom object) line))
		((voxels? (object-geom object)) (voxel/line-intersect? (object-geom object) line))
		((triangle? (object-geom object)) (triangle/line-intersect? (object-geom object) line))
		((triangle-list? (object-geom object)) (triangle-list/line-intersect? (object-geom object) line))
		(else (display "object not recognised by intersect ")(display object)(newline) #f)))
