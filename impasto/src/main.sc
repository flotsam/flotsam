; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "maths/maths.sc")
(include "unit-tests.sc")
(include "scene/scene.sc")
(include "rendering/raytracer.sc")
(include "io/obj.sc")

(unit-tests)

;(render-scene (load-scene "test.sc"))
(render-scene (load-scene "scene.sc"))

