; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (foldl func accum lst)
  (if (null? lst)
      accum
      (foldl func (func (car lst) accum) (cdr lst))))

;(define (for-each func lst)
;  (cond 
;  	((null? lst) 0)
;	(else
;	  (func (car lst))
;      (for-each func (cdr lst)))))

(define (build-list n proc)
  (define (_ n l)
    (cond 
	  ((zero? n) l)
	  (else
	    (build-list (- n 1) (cons (proc n) l)))))
  (_ n '()))
