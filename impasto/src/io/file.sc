; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (read-word f)
	(define (_ f word)
		(let ((c (read-char f)))
			(cond 
				((or (char=? c #\ ) (char=? c #\newline)) word)
				(else (_ f (string-append word (string c)))))))
	(_ f ""))

(define (read-line f)
	(define (_ f line)
		(let ((c (read-char f)))
			(cond 
				((char=? c #\newline) line)
				(else (_ f (string-append line (string c)))))))
	(_ f ""))
