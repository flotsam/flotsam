; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (string-split s c)
  (define (_ sl tl cl)
    (cond 
      ((null? sl) (if (null? cl) tl (append tl (list (list->string cl)))))
      ((eq? (car sl) c)
       (_ (cdr sl) (append tl (list (list->string cl))) '()))
      (else
       (_ (cdr sl) tl (append cl (list (car sl)))))))
  (_ (string->list s) '() '()))

(define (file->lines fn)
  (define (_ f cl l)
    (let ((o (read-char f)))
	  (cond 
	  	((eof-object? o) (append l (list (string-split cl #\ ))))
		((char=? o #\newline) (_ f "" (append l (list (string-split cl #\ )))))
		(else (_ f (string-append cl (string o)) l)))))
		
  (let* ((f (open-input-file fn))
  	     (r (_ f "" '())))
	(close-input-port f)
	r))
  
(define (string-list->vector l)
  (vector 
	(string->number (list-ref l 0))
	(string->number (list-ref l 1))
	(string->number (list-ref l 2))))
 
(define (get-data name lns l)
	(cond
	  ((null? lns) l)
	  ((null? (car lns)) (get-data name (cdr lns) l))
	  ((string=? (car (car lns)) name) 
	  	(cons (cdr (car lns)) (get-data name (cdr lns) l)))
	  (else (get-data name (cdr lns) l))))

(define (get-index i str)
  (let ((i (string->number (list-ref (string-split str #\/) i))))
    (if i (- i 1) #f)))

; face looks like ((vertex normal) (vertex normal) ... )
(define (make-face index verts normals)
  (define (_ i r)
	(cond
	  ((null? i) r)
	  (else
	  	(let ((vi (get-index 0 (car i)))
			  (ni (get-index 2 (car i)))) ; todo add texcoords
	      (cons (list
			  	  (list-ref verts vi)
				  (cond (ni (list-ref normals ni))))
			  (_ (cdr i) r))))))
  (_ index '()))

(define (face->triangle face m)
   (triangle-with-normals 
     (vtransform (list-ref (list-ref face 0) 0) m)
     (vtransform (list-ref (list-ref face 1) 0) m)
	 (vtransform (list-ref (list-ref face 2) 0) m)
     (vnorm (vtransformr (list-ref (list-ref face 0) 1) m))
     (vnorm (vtransformr (list-ref (list-ref face 1) 1) m))
	 (vnorm (vtransformr (list-ref (list-ref face 2) 1) m))))

(define (load-obj fn m)
  (let* ((t (file->lines fn))
         (verts (map string-list->vector (get-data "v" t '())))
		 (normals (map string-list->vector (get-data "vn" t '()))))
    (triangle-list
	  (map
        (lambda (face)
          (face->triangle (make-face face verts normals) m))
      (get-data "f" t '())))))
