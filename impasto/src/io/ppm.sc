; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(include "../maths/vec.sc")
(include "../rendering/rasterise.sc")
(include "../io/file.sc")

(define (write-ppm filename width height samples thunk)
  (display (string-append "creating " filename))(newline)
  (let ((f (open-output-file filename)))
    (display "P3" f)(newline f)
    (display "# made with scheme" f)(newline f)
    (display (string-append (number->string (inexact->exact (round width))) " "
							(number->string (inexact->exact (round height)))) f) (newline f)
    (display "256" f)(newline f)
    (let loopx ((y 0))
      (let loopy ((x 0))
        (let ((out (super-sample samples x y width height thunk)))
          (display (string-append 
                  (number->string (inexact->exact (round (* 256 (vx out))))) " "
                  (number->string (inexact->exact (round (* 256 (vy out))))) " " 
                  (number->string (inexact->exact (round (* 256 (vz out))))) " ") f))
        (cond ((< x (- width 1)) (loopy (+ x 1)))))
      (newline f)
      (cond ((< y (- height 1)) (loopx (+ y 1)))))
    (close-output-port f)))

(define (read-ppm-dim filename)
 (let ((f (open-input-file filename)))
    (read-line f)
    (read-line f)
	(let ((r (list (string->number (read-word f)) (string->number (read-word f)))))
		(close-input-port f)
		r)))

(define (read-scanline f x n v)
	(cond 
		((eq? n x) v)
		(else
			(vector-set! v n (/ (string->number (read-word f)) 256.0))
			(read-scanline f x (+ n 1) v))))

(define (read-scanlines f x n l)
	(cond 
		((zero? n) l)
		(else 
			(read-scanlines f x (- n 1)
				(append l (list (read-scanline f x 0 
					(make-vector x 0))))))))

(define (read-ppm filename)
  (let ((f (open-input-file filename)))
    (read-line f) ; P3
    (read-line f) ; # blah blah
	(let ((w (string->number (read-word f)))
		  (h (string->number (read-word f))))
      (read-line f) ; 256
	(let ((r (read-scanlines f (* w 3) h '())))
    (close-input-port f) r))))
