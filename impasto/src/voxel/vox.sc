; Copyright (C) 2010 Dave Griffiths : See LICENCE file

(define (vox size m)
  (list 'voxels (vx size) (vy size) (vz size) m
	(make-vector (* (vx size) (vy size) (vz size)) 0)))

(define (voxels? s) (eq? (car s) 'voxels))
(define (vox-w vox) (list-ref vox 1))
(define (vox-h vox) (list-ref vox 2))
(define (vox-d vox) (list-ref vox 3))
(define (vox-mat vox) (list-ref vox 4))
(define (vox-data vox) (list-ref vox 5))
	
(define (index vox v)	
	(+ (+ (* (vy v) (vox-w vox)) (vx v)) (* (vz v) (* (vox-w vox) (vox-h vox)))))
	
(define (vox-ref vox v)
	(vector-ref (vox-data vox) (index vox v)))

(define (vox-set! vox v s)
	(vector-set! (vox-data vox) (index vox v) s))

(define (vox-safe-ref vox v)
	(if (and (> (vx v) 0) (< (vx v) (vox-w vox))
	         (> (vy v) 0) (< (vy v) (vox-h vox))
			 (> (vz v) 0) (< (vz v) (vox-d vox)))
		(vector-ref (vox-data vox) (index vox v))
		0))

(define (voxel/line-intersect? vox ray)
	#t)
	
(define (voxel/line-intersect vox ray)
	(vox-fold 1 vox
		(lambda (pos v r)
			(let ((p (line-closest-point ray pos)))
			  (if (< (vdist pos p) 0.001)
			  	(begin
			  (display (vdist pos p))(newline)
				(intersection 1 p (vector 0 1 0)))
				r)))
		(intersection 0 (vector 0 0 0) (vector 0 0 0))))

(define (vox-map! vox thunk)
	(let loopd ((d 0))
		(cond ((< d (vox-d vox))
			(let looph ((h 0))
				(cond ((< h (vox-h vox))
					(let loopw ((w 0))
						(cond ((< w (vox-w vox))
							(vox-set! vox (vector w h d) (thunk (vector w h d) 
								(vox-ref vox (vector w h d))))
							(loopw (+ w 1)))))
					(looph (+ h 1)))))
			(loopd (+ d 1))))))

(define (vox-for-each! inc vox thunk)
	(let loopd ((d 0))
		(cond ((< d (vox-d vox))
			(let looph ((h 0))
				(cond ((< h (vox-h vox))
					(let loopw ((w 0))
						(cond ((< w (vox-w vox))
							(thunk (vector w h d) (vox-ref vox (vector w h d)))
							(loopw (+ w inc)))))
					(looph (+ h inc)))))
			(loopd (+ d inc))))))

(define (vox-fold inc vox thunk val)
	(let loopd ((d 0))
		(cond ((< d (vox-d vox))
			(let looph ((h 0))
				(cond ((< h (vox-h vox))
					(let loopw ((w 0))
						(cond ((< w (vox-w vox))
							(set! val (thunk 
								(vtransform (vector (/ w (vox-w vox)) (/ h (vox-h vox)) (/ d (vox-d vox)) 1) 
									(vox-mat vox)) 
								(vox-ref vox (vector w h d)) val))
							(loopw (+ w inc)))))
					(looph (+ h inc)))))
			(loopd (+ d inc)))))
	val)
	
(define (vox-sphere! vox c r)
	(let ((c (vector (* (vx c) (vox-w vox))
					 (* (vy c) (vox-h vox))
					 (* (vz c) (vox-d vox))))
		  (r (* r (vox-w vox))))
		(vox-map! vox 
			(lambda (pos v)
				(if (< (vdist c pos) r) 1 0)))))

(define (vox-influence! vox c s)
	(let ((c (vector (* (vx c) (vox-w vox))
					 (* (vy c) (vox-h vox))
					 (* (vz c) (vox-d vox))))
		  (s (* s (vox-w vox))))
		(vox-map! vox 
			(lambda (pos v)
				(let ((d (vdist c pos)))
					(+ v (* s (if (> d 0.0001) (/ 1 d) 1))))))))

(define (vox-threshold! vox t)
	(vox-map! vox 
		(lambda (pos v)
			(if (> v t) 1 0))))

(define (vox-gradient vox)
	(let ((l '()))
	(vox-for-each! 1 vox 
		(lambda (pos v)
			(set! l (cons  			
			(vector
				(- (vox-safe-ref vox (vector (- (vx pos) 1) (vy pos) (vz pos)))
				   (vox-safe-ref vox (vector (+ (vx pos) 1) (vy pos) (vz pos))))
				(- (vox-safe-ref vox (vector (vx pos) (- (vy pos) 1) (vz pos)))
				   (vox-safe-ref vox (vector (vx pos) (+ (vy pos) 1) (vz pos))))
				(- (vox-safe-ref vox (vector (vx pos) (vy pos) (- (vz pos) 1)))
				   (vox-safe-ref vox (vector (vx pos) (vy pos) (+ (vz pos) 1))))) l))))
			(reverse l)))

(define (_vox-read-raw size filename)
  (define (_ vx f n)
  	(cond ((not (zero? n))
		(vector-set! (vox-data vx) n (char->integer (read-char f)))
		(_ vx f (- n 1)))))
  (let ((f (open-input-file filename))
  	    (v (vox size (mat))))
	(_ v f (- (* (vx size) (vy size) (vz size)) 1))
    (close-input-port f)
	v))
	
(define (read-to-list f l)
  (let ((c (read-char f)))
    (cond
      ((eof-object? c) l)
	  (else
	   ;(if (not (zero? (char->integer c))) 
	   ;	(display (char->integer c))(newline))
       (read-to-list f (cons (char->integer c) l))))))

;(let ((f (open-input-file "lobster.raw")))
;	  (display (read-to-list f '()))(newline)
;  (close-input-port f))

(define (vox-read-raw size filename m)
  (define (_ vx f n t)
  	(cond ((not (zero? n))
		(let ((c (read-char f)))
			(cond 
				((eof-object? c) (display n)(newline))
			    (else
					(vector-set! (vox-data vx) (- t n) (char->integer c))
					(_ vx f (- n 1) t)))))))
  (let ((f (open-input-file filename))
  	    (v (vox size m)))
	(_ v f (* (vx size) (vy size) (vz size)) (* (vx size) (vy size) (vz size)))
    (close-input-port f)
	v))
