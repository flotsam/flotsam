(define amt 10000000)

(define (add l a)
	(cond
		((null? l) a)
		(else
			(add (cdr l) (vector 
				(+ (vector-ref a 0) (vector-ref (car l) 0))
				(+ (vector-ref a 1) (vector-ref (car l) 1))				
				(+ (vector-ref a 2) (vector-ref (car l) 2)))))))

(define (build-list n p)
	(define (_ n l)
		(cond 
			((zero? n) l)
			(else 
				(_ (- n 1) (cons (p n) l)))))
	(_ n '()))

(define l (build-list amt (lambda (_) (vector 1 2 3))))
				
(display (add l (vector 0 0 0)))
								
