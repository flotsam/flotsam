#include <stdio.h>
#include <list>

using namespace std;

static const int amt = 10000000;

class vec3
{
public:
	float x,y,z;
};

int main()
{
	list<vec3> l(amt);
	vec3 t;
	t.x=0;
	t.y=0;
	t.z=0;
	
	for(list<vec3>::iterator i=l.begin(); i!=l.end(); ++i)
	{
		i->x=1;
		i->y=2;
		i->z=3;
	}
	
	for(list<vec3>::iterator i=l.begin(); i!=l.end(); ++i)
	{
		t.x+=i->x;
		t.y+=i->y;
		t.z+=i->z;
	}
	printf("%f %f %f\n",t.x,t.y,t.z);
	return 0;
}
