(require (lib "string.ss"))

(define t (instantiate text% ()))

(define live-canvas%
  (class editor-canvas%
    (define/override (on-char event)
      (if (eq? 'f5 (send event get-key-code))
          (eval-string (send t get-text)))
      (super on-char event))
    (super-new)))

(editor-set-x-selection-mode #t)
(define f (instantiate frame% ("live code scheme!" #f 640 480)))
(define c (instantiate live-canvas% (f)))
(define t (instantiate text% ()))
(send c set-editor t)
(send f show #t)
(send c set-canvas-background (make-object color% 200 200 200))

(define mb (instantiate menu-bar% (f)))
(define m-edit (instantiate menu% ("Edit" mb)))
(define m-font (instantiate menu% ("Font" mb)))
(append-editor-operation-menu-items m-edit #f)
(append-editor-font-menu-items m-font)
