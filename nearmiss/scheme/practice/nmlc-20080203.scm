(require (lib "nearmiss.ss" "fluxus-0.14"))
(searchpath "/home/dave/noiz/fragments/808/")

(volume 1)
(define (sq x) (* x x))
(max-synths 100)

(seq
 0.1
 (lambda (time clock)
   (define (clock-div clock n)
     (zero? (modulo clock n)))
   
   (clock-map
    (lambda (c t)
      
      (if (or (clock-div clock 4)
              (clock-div clock 23))
          (play time (mul 5 (sample "BT7A0D0.WAV" 840))))
      
      (if (clock-div (+ clock 2) (* t 2)) 
          (play time (mul 5 (sample "ST0TAS3.WAV" 840))))
      
      (play time (mooglp (sample "HHCD8.WAV" 240) (/ (random 100) 100) 0.4))
      
      (if (clock-div clock 32) (play time (mul 2 (sample "RIDED0.WAV" 840))))

      (if (clock-div clock (* t 2))
          (play time (mul
                      (mooghp (saw (add (* (note c) 0.5)
                                 (mul (mul 500 (sine 0.5))
                                      (adsr 0.2 0.1 0.2 2))))
                               (add 1 (sine 0.2)) 0.44)
                            
                      (adsr 0.005 0.05 0.2 1))))

      
      (if (clock-div clock 2)
          (play time (mul
                      (formant (saw (add (note c) 
                                 (mul (mul 500 (sine (/ (note c) 0.5)))
                                      (adsr 0.2 0.1 0.2 2))))
                               (add 1 (sine 0.2)) 0.4)
                            
                      (adsr 0.005 0.05 0.1 1))))
      
      )
      clock     
      (list (+ 10 (random 20)) 20 (random 40) 20)
      (list 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 4 4 4 4 4 4))))