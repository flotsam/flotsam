(require (lib "nearmiss.ss" "fluxus-0.14"))
(searchpath "/home/dave/noiz/fragments/organik/closer/")

(volume 2)
(define (sq x) (* x x))

(seq
 (lambda (time clock)
   (clock-map
    (lambda (a b c)
      (let ((bent-time (+ 2 time (* 2.5 (sin (* time 0.3))))))
        (cond ((zero? (modulo clock 4))
               (play 
                (+ 1 time (* 0.9 (sin (* time 0.4))))
                (mooghp (sample "closer13.wav" (* a 1)) 
                       (+ 1 (* 0.1 (sin time))) 0.3))
               
               (play 
                bent-time
                (moogbp (sample "closer16.wav" (* b 0.3)) 
                        (+ 0.5 (* 0.5 (cos time))) 0.3))))
        
        (cond ((zero? (modulo clock 8))
              (play 
               time
               (sample "closer06.wav" (sq (* 0.1 (modulo clock 500)))))))
        
        (cond ((or (zero? (modulo clock 14))
                   (zero? (modulo clock 23)))
               (play bent-time 
                     (mooglp (mul (add 
                                   (mul (saw (note c)) 0.1)
                                   (pink (* c 0.1)))
                                  (mul (adsr 0.01 0.01 0.3 0.5) 0.2))
                             (+ 1 (* 0.1 (sin time)))
                             0.4))))
      ))
      clock
      (list 800 202 300 340 803)
      (list 600 503 700 804)
      (list 16 2 33 42 22))))
