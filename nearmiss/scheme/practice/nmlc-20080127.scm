(require (lib "nearmiss.ss" "fluxus-0.14"))
(searchpath "/home/dave/noiz/pattern-cascade/electro_d/")

;(reset)
(max-synths 10)
(volume 0.05)
(seq
 (lambda (time clock)
   
   (clock-map
    (lambda (n c a)
    
      (if (zero? (modulo clock 16))
	    (play time (mul (sine (* (note n) 8)) (mul 0.4 (adsr 0 1 0 0)))))
      
      (play time (mul 15 (mooghp (sample "electro_d_clhat01.wav" 440) 
                                 (* (random 100) 0.01) 0.4)))
    
      (if (zero? (modulo (+ 4 clock) 8))
          (play time (mul 10 (sample "electro_d_snare02.wav" 240))))
      
      (if (or (zero? (modulo clock 6))
              (zero? (modulo clock 5)))
          (play time (mooglp (mul (saw (note n)) 
		                     (mul 10 (adsr a 0.5 0 0)))
                             c 0.44)))
       
      (if (or (zero? (modulo clock 8))
	  		  (zero? (modulo clock 13)))
          (play time (crush (mul 20 (sample "electro_d_kick01.wav" 540)) 2 3.1)))
     
      (play time (mooghp (add (mul (saw (note n)) (adsr a 0.1 0 0))
                              (mul (saw (* (note n) 0.7501)) (adsr a 0.1 0 0)))
                         (mul c (adsr 0.1 a 0 0)) 0.1))
      )
    clock
    (list 20 22 24 16 (+ (random 10) 20))
    (list 0.1 0.3 0.5 0.5 0.9 0.1 0.8)
    (list 5 0.01 0.1 0.3 0.4))
))
 
