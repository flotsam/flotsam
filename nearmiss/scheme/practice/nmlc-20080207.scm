(require (lib "nearmiss.ss" "fluxus-0.14"))

(searchpath "/home/dave/noiz/pattern-cascade/space_b/")
(searchpath "/home/dave/noiz/fragments/")

(define vl (list  "voc/1.wav"  "voc/5.wav" "voc/12.wav" "voc/4.wav" "voc/15.wav" "voc/10.wav"))

(seq
    (lambda (time clock)

        (clock-map
            (lambda (n n2 v)

                (if (zero? (modulo (+ clock 2) 6))
                (play time (mul (mooghp (add 
                        (saw (note n)) 
                        (saw (* 0.3 (note n))))
                       (+ 1 (sin (* 0.5 time))) 0.2)
                     (adsr 0 0.05 0.1 5))))

                (if (zero? (modulo (+ clock 0) 4))
                    (play time (mul (white 50) (adsr 0 0.02 0 0))))

                (cond ((< (modulo clock 256) 32)
                    (play time (mul 
                        (mul (sine (add 
                            (note (+ (random 50) 40))
                            (mul 110 (sine 3)))) 0.3) (adsr 0 0.05 0.05 5)))))
 
                (cond ((< (modulo clock 1024) 512)
                    
                    (if (or (zero? (modulo clock 6))
                            (zero? (modulo clock 8)))
                        (play time (crush (sample "space_b_kick01.wav" 1240) 5.5 0.1)))
    
                    (if (zero? (modulo (+ clock 4) 8))                       
                        (play time (mul 2 (sample "space_b_snare01.wav" 1240))))
    
                    (if (or (zmod (+ clock 4) 13)
                            (zmod (+ clock 4) 2))
                        (play time (mooghp (mul 20 (sample v 140)) (add 1 (sine 0.3)) 0.4)))
    
                    (if (zmod (+ clock 3) 4)
                        (play time (sample "space_b_clhat01.wav" 440))))

                    (else
                        (if (zmod clock 8)
                            (play time (sample "space_b_kick01.wav" 220)))
                    
                        (if (or (zmod (+ clock 2) 4) (zmod (+ clock 2) 6))                       
                            (play time (mul 2 (sample "space_b_snare01.wav" 1240))))

                        (if (zmod clock 8)
                            (play time (sample "aa.wav" 420)))

                        (if (zmod clock 16)
                            (play time (mul (mooghp (add 
                                (squ (note n)) 
                                (squ (* 0.3 (note n))))
                               (add 1 (sine (+ 10 (random 300)))) 0.43)
                             (adsr 0 0.05 0.1 5))))
                    ))
                    
                #;(if (or (zero? (modulo (+ clock 8) 16))
                        (zero? (modulo (+ clock 8) 7)))
                    (let ((n (* n 2)))
                    (play time (mul (mul 0.1 (sine (add (note n) 
                        (mul 500 (sine (* 0.333 (note n)))))))
                         (adsr 2 0.05 0.1 2)))))



               )
            clock
            (list 20 28 26 28 21 22 (+ 20 (random 20)))
            (list 22 26 24 26)
            vl)


        0.1))