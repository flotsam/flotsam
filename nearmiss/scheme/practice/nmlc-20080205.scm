(require (lib "nearmiss.ss" "fluxus-0.14"))
(searchpath "/home/dave/noiz/fragments/808/")

(volume 0.05)
(define (sq x) (* x x))
(max-synths 100)
(eq 1 1 1)

(seq
 (lambda (time clock)
   (define (clock-div clock n)
     (zero? (modulo clock n)))
   
   (clock-map
    (lambda (c t a)
      (let ((swing-time (+ time (if (zero? (modulo clock 2)) 0 0.05))))
        
      (if (clock-div clock 4)
          (play swing-time (sample "HHCD4.WAV" 540)))
      
      (if (clock-div (+ clock 3) 4)
          (play swing-time (sample "CLOP2.WAV" 540)))
     
      (if (or (clock-div (+ clock 2) a)
              (clock-div (+ clock 2) 16))
          (play swing-time (sample "ST3T0S7.WAV" 540)))
      
      (if (or (clock-div clock 16)
              (clock-div clock 42))
          (play time (mul 
                      (sine (add 50 (mul 500 (adsr 0 0.04 0 0))))
                      (adsr 0 0.2 0 0.3))))
             
      (if (clock-div clock t)
          (play swing-time (mul 
                       (sine (add (* (note c) 0.25)
                                (add
                                  (mul 
                                   (sine (* (note c) 0.5))
                                   (mul 100 (adsr 0.5 0.5 0 0)))
                                  (mul 
                                   (sine (* (note c) 0.333))
                                   (mul 100 (adsr 0 0.5 0 0))))))
                       (adsr 0.05 0.6 0 0.3))))

        (if (clock-div clock 8)
            (play swing-time (formant 
                            (mul
                             (saw (note 14)) 
                             (adsr 0.04 0.1 0.05 2))
                            0.5 ;(add 1 (sine 0.1))
                            0.4)))

        ))
      clock     
      (list (+ 10 (random 20)) 20 (random 40) 20)
      (list 8 8 3 4 3)
      (list 8 8 4 4 2 1))

    0.2))