(require (lib "nearmiss.ss" "fluxus-0.14"))

(define (synth wave n)
    (mul (moogbp (wave (note n)) 
        (add (mul 3 (adsr 0 0.05 0 0)) (mul 0.1 (sine 1))) 0.43)
        (adsr 0 0.5 0.1 3)))

(seq
    (lambda (time clock)
        (clock-map
            (lambda (n m)
                (clock-split clock 32
                    (lambda ()
                        (if (zmod clock 10) 
                            (play time (synth saw n))))
                    (lambda ()
                        (if (zmod clock 4) 
                            (play time (synth squ n)))))

                (clock-split clock 128
                    (lambda ()
                        (if (zmod clock 18)
                            (play time (mul (sine (add (note m) (mul 10 (sine 10))))
                                                      (adsr 0.5 2 0.2 3)))))
                    (lambda ()
                        (if (zmod clock 2)
                            (play time (mul (sine (note m)) 
                                (adsr 0 0.1 0.2 3))))))
                )
            clock
            (list 4 8 10 2 5 9)
            (list 30 31 44 32 20))
        0.1))