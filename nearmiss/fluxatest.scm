;------------------------------------------------------------------
; a simple genetic programming framework
(require scheme/base)
(require fluxus-015/fluxa)

(define max-depth 10)

(define-struct arg-desc (type lo hi))

(define-struct function-desc (name arg-desc-list))

(define (rand-range lo hi)
  (+ lo (* (random (* (- hi lo) 100000)) 0.00001)))

(define (rand-pick l)
  (list-ref l (random (length l))))

(define (make-arg arg-desc vocab depth)
  (cond
    ((and (< depth max-depth) (zero? (random 2))) ; function
     (make-function vocab (+ depth 1)))
    (else ; terminal
     (cond
       ((eq? 'float (arg-desc-type arg-desc))
        (rand-range (arg-desc-lo arg-desc) (arg-desc-hi arg-desc)))
       ((eq? 'string (arg-desc-type arg-desc))
        "HANDCLP1.WAV")
       (else (error))))))

(define (make-args arg-desc-list vocab out depth)
  (cond
    ((null? arg-desc-list) out)
    (else
     (cons (make-arg (car arg-desc-list) vocab (+ depth 1))
           (make-args (cdr arg-desc-list) vocab out (+ depth 1))))))
    
(define (make-function-inner function-desc vocab depth)
  (cons (function-desc-name function-desc)
        (make-args (function-desc-arg-desc-list function-desc) vocab '() (+ depth 1))))

(define (make-function vocab depth)
  (make-function-inner (rand-pick vocab) vocab depth))

;----------------------------------------------------------------------

(define (mutate-args rate vocab args-list depth)
  (cond
    ((null? args-list) '())
    ((> depth max-depth) ; just copy if too big
     (cons (car args-list)
           (mutate-args rate vocab (cdr args-list) (+ depth 1))))
    (else
     (cond
       ((list? (car args-list))
        (cons (mutate-function-inner rate vocab (car args-list) (+ depth 1))
              (mutate-args rate vocab (cdr args-list) (+ depth 1))))
       (else
        (if (zero? (random rate))
            (cons (make-function vocab depth)
                  (mutate-args rate vocab (cdr args-list) (+ depth 1)))
            (cons (car args-list)
                  (mutate-args rate vocab (cdr args-list) (+ depth 1)))))))))

(define (mutate-function-inner rate vocab function depth)
  (cond
    ((null? function) (error))
    (else    
     (if (zero? (random rate))
         (make-function vocab depth)
         (cons (car function) (mutate-args rate vocab (cdr function) (+ depth 1)))))))

(define (mutate-function rate vocab function)
  (mutate-function-inner rate vocab function 0))

;------------------------------------------------------------------------------

; make an initial population by mutating an individual
(define (make-population size function rate vocab)
    (build-list size (lambda (n) (mutate-function rate vocab function))))

; returns a mutated version of this population
(define (mutate-population population rate vocab)
    (map
        (lambda (function)
            (mutate-function rate vocab function))
        population))

; gets a list of fitnesses of a population, using the supplied fitness function
(define (fitness-list population fitness-proc fitness-user-data)
    (map
        (lambda (function)
            (fitness-proc function fitness-user-data))
        population))

; helpers to get the min and max of a list
(define (list-max l)
    (foldl
        (lambda (e t)
            (if (> e t) e t))
        0
        l))

(define (list-min l)
    (foldl
        (lambda (e t)
            (if (< e t) e t))
        999999999
        l))

; culls the bottom part of the population (death)
(define (cull population fitlist score)
    (let* ((max (list-max fitlist))
            (min (list-min fitlist))
            (cutoff (+ min (* score (- max min)))))  
        (foldl
            (lambda (i f r)
                (if (>= f cutoff)
                    (cons i r) r))
            '()
            population
            fitlist)))

; makes a new population by mutation of individuals
(define (new-population rate vocab source-pop size)
    (if (null? source-pop)
        '()
        (build-list size
            (lambda (n)
                (mutate-function rate vocab (rand-pick source-pop))))))

(define (fittest l)
    (cadr
        (foldl
            (lambda (i r)
                (if (> i (caddr r))
                    (list (+ (car r) 1) (car r) i)
                    (list (+ (car r) 1) (cadr r) (caddr r))))
            (list 0 0 0) ; current, fittest, fitness
            l)))

;-----------------------------------------------------------------

(define vocab
  (list
  (make-function-desc 'sine (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'saw (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'tri (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'squ (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'white (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'pink (list (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'adsr (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)
                                  (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'add (list (make-arg-desc 'float -1000 1000) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'sub (list (make-arg-desc 'float -1000 1000) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'mul (list (make-arg-desc 'float -1000 1000) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'div (list (make-arg-desc 'float -1000 1000) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'pow (list (make-arg-desc 'float -1000 1000) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'mooglp (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'moogbp (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'mooghp (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'formant (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'sample (list (make-arg-desc 'string -1 1) (make-arg-desc 'float -1000 1000)))
  (make-function-desc 'crush (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'distort (list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
  (make-function-desc 'klip(list (make-arg-desc 'float -1 1) (make-arg-desc 'float -1 1)))
))

(define (run)
    (let ((p (make-function vocab 0)))
        (display p)(newline)
        (play-now (eval p)))
    (sleep 0.1)
    (run))

(thread run)

;(play-now (sample "HANDCLP1.WAV" 440))